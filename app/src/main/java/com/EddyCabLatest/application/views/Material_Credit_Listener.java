package com.EddyCabLatest.application.views;

public interface Material_Credit_Listener {
    public void onTabSelected(MaterialTabCredit tab);

    public void onTabReselected(MaterialTabCredit tab);

    public void onTabUnselected(MaterialTabCredit tab);
}
