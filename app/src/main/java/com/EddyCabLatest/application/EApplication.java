package com.EddyCabLatest.application;

import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.provider.Settings;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.EddyCabLatest.application.moduleUser.modal.JoblistItems;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.utilities.DBHelper;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.geofire.GeoFire;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EApplication extends Application {

    public static final String TAG = EApplication.class.getSimpleName();
    private static EApplication mInstance;
    private RequestQueue mRequestQueue;
    private SQLiteDatabase db;

    public static boolean isDriverLocationUpdateExecuted = false;
    public static boolean callBookingStatus = false;
    public static boolean verifyingSatus = false;
    public static boolean historyCancelledStatus = false;
    public static boolean setBidTriggerMethodsCall = false;
    public static boolean setReservationStatus = false;
    public static boolean previousReservationStatus = false;

    public static int mapPadding = 40;
    public static boolean isFirstDialog = false;
    public static boolean isNewJobDialogShown = false;
    public static boolean userCreditUsed = false;
    public static boolean fromFragmentIntent = false;
    public static boolean fromCreateReservation = false;
    public static boolean hideCrossImageInLocateDriver = false;
    public static boolean driverEnteredPickUpLocation = false;
    public static boolean userIsNear500 = false;
    //Number of jobs bidded by driver
    public static int noOfJobsBidded = 0;
    private JSONObject dataForRoute;
    private String user_id = "0";
    private NotificationCompat.Builder mBuilder;
    private String tagName;
    public static Firebase ref;
    public static GeoFire geofire;
    private Firebase ref1;
    private String accesToken;
    private SessionManager session;
    private String acces_token;
    private String aplanguage;
    private boolean fromjob = false;
    private boolean fromselect = false;
    private int credit_value;
    private int bonus_value;
    private boolean fromlocate=false;

    private static ArrayList<JoblistItems> modalDataList;
    private ArrayList<JoblistItems> pickList;
    private boolean has_arrived=false;
    private boolean verifystatus;
    private NotificationManager mNotificationManager;

    public void setcreditandbonus(int credit, int bonus)

    {
        this.credit_value = credit;
        this.bonus_value = bonus;

    }


    public int getCredit() {
        return credit_value;
    }

    public int getBonus() {
        return bonus_value;
    }

    public static void setfirebaseandroidcontext(Context context) {
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String credit;

    public static boolean isNewJobDialogShown() {
        return isNewJobDialogShown;
    }

    public static void setIsNewJobDialogShown(boolean isNewJobDialogShown) {
        EApplication.isNewJobDialogShown = isNewJobDialogShown;
    }

    public static boolean isDriverEnteredPickUpLocation() {
        return driverEnteredPickUpLocation;
    }

    public static void setDriverEnteredPickUpLocation(boolean driverEnteredPickUpLocation) {
        EApplication.driverEnteredPickUpLocation = driverEnteredPickUpLocation;
    }

    public static boolean isUserPickedByDriver() {
        return userPickedByDriver;
    }

    public static void setUserPickedByDriver(boolean userPickedByDriver) {
        EApplication.userPickedByDriver = userPickedByDriver;
    }

    public static boolean userPickedByDriver = false;


    public static boolean userRateByDriver = false;
    public static HashMap<String, String> getUserRateData;

    public boolean openFragment1 = false;

    @Override
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());
        Firebase.setAndroidContext(this);
        session = new SessionManager(this);
        DBHelper helper = new DBHelper(this);
        db = helper.getWritableDatabase();
        SessionManager sessionManager = new SessionManager(this);
        sessionManager.createState();
        ref = new Firebase("https://eddycab.firebaseio.com/users/");
        ref1 = new Firebase("https://eddycab.firebaseio.com/users/current_location");
        geofire = new GeoFire(ref1);
        mInstance = this;
        EApplication.ref.authWithCustomToken("hxNmCZr6OGVl0QcqrcvUh4XVswD7USZgEPDSzJMZ", new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticationError(FirebaseError error) {
                System.err.println("Login Failed! " + error.getMessage());
            }

            @Override
            public void onAuthenticated(AuthData authData) {
                System.out.println("Login Succeeded!");
            }
        });
       /* Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {
                handleUncaughtException (thread, e);
            }
        });*/


    }

    /*public void handleUncaughtException (Thread thread, Throwable e)
    {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        Intent intent = new Intent ();
        intent.setAction ("com.EddyCab.application.moduleDriver.Driver_MainActivity"); // see step 5.
        intent.setFlags (Intent.FLAG_ACTIVITY_TASK_ON_HOME); // required when starting from Application
        startActivity (intent);

        System.exit(1); // kill off the crashed app
    }*/
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        db.close();
    }

    public static synchronized EApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        String tag_new = TextUtils.isEmpty(tag) ? TAG : tag;



        getRequestQueue().add(req);


    }

/*
    public <T> void addToRequestQueue(Request<T> req) {
        int socketTimeout = 9000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        req.setTag(TAG);
        cancelPendingRequests(TAG);
        getRequestQueue().add(req);
    }
*/

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    /* Add data to table */
    public void addToDB(String tableName, HashMap<String, String> dbValuePair) {
        ContentValues values = new ContentValues();
        for (Map.Entry<String, String> entry : dbValuePair.entrySet()) {
            values.put(entry.getKey(), entry.getValue());
        }
        db.insert(tableName, null, values);
    }


    /* Get Data from table */
    public ArrayList<HashMap<String, String>> getAllToList(String tableName) {
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
        String query = "SELECT * FROM " + tableName;
        Cursor tasksCursor = db.rawQuery(query, null);
        tasksCursor.moveToFirst();
        if (!tasksCursor.isAfterLast()) {
            do {
                HashMap<String, String> val = new HashMap<String, String>();
                for (int index = 0; index < tasksCursor.getColumnCount(); index++) {
                    val.put(tasksCursor.getColumnName(index), tasksCursor.getString(index));
                }
                arrayList.add(val);
            } while (tasksCursor.moveToNext());
        }
        tasksCursor.close();
        return arrayList;
    }

    public boolean CheckIsDataAlreadyInDBorNot(String tableName, String dbField, String fieldValue) {

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(tableName);
        if (!TextUtils.isEmpty(dbField) && !TextUtils.isEmpty(dbField)) {
            queryBuilder.append(DBTableFields.WHERE);
            queryBuilder.append(dbField);
            queryBuilder.append(" = '");
            fieldValue = fieldValue.replace("'", "''");
            queryBuilder.append(fieldValue);
            queryBuilder.append("'");
        }
        Cursor tasksCursor = db.rawQuery(queryBuilder.toString(), null);
        if (tasksCursor.getCount() <= 0) {
            tasksCursor.close();
            return false;
        }
        tasksCursor.close();
        return true;
    }

    public boolean CheckIsDataAlreadyInDBorNotWithNameAndAddress(String table, String tableName, String tableAddress, String name, String address) {

        name = name.replace("'", "''");
        address = address.replace("'", "''");
        String query = "SELECT * FROM " + table + " WHERE " + tableName + " = '" + name + "' AND " + tableAddress + " = '" + address + "'";
        Cursor tasksCursor = db.rawQuery(query, null);
        if (tasksCursor.getCount() <= 0) {
            tasksCursor.close();
            return false;
        }
        tasksCursor.close();
        return true;
    }

    public void removeFavourite(String table, String tableName, String tableAddress, String name, String address) {
        name = name.replace("'", "''");
        address = address.replace("'", "''");
        db.delete(table, tableName + " = '" + name + "' AND " + tableAddress + " = '" + address + "'", null);
    }

    public void addAllToDB(String tableName, ArrayList<HashMap<String, String>> list) {
        for (int index = 0; index < list.size(); index++) {
            ContentValues values = new ContentValues();
            for (Map.Entry<String, String> entry : list.get(index).entrySet()) {
                values.put(entry.getKey(), entry.getValue());
            }
            db.insert(tableName, null, values);
        }
    }

    public ArrayList<HashMap<String, String>> getAllToSortedList(String tableName, String[] sortFields, String order) {
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(tableName);

        if (sortFields.length > 0 && !TextUtils.isEmpty(order)) {
            queryBuilder.append(DBTableFields.ORDER_BY);
            String sortField = TextUtils.join(",", sortFields);
            queryBuilder.append(sortField);
            queryBuilder.append(order);
        }
        Cursor tasksCursor = db.rawQuery(queryBuilder.toString(), null);
        tasksCursor.moveToFirst();
        if (!tasksCursor.isAfterLast()) {
            do {
                HashMap<String, String> val = new HashMap<String, String>();
                for (int index = 0; index < tasksCursor.getColumnCount(); index++) {
                    val.put(tasksCursor.getColumnName(index), tasksCursor.getString(index));
                }
                arrayList.add(val);
            } while (tasksCursor.moveToNext());
        }
        tasksCursor.close();
        return arrayList;
    }

    /* Search Dynamic function */
    public ArrayList<HashMap<String, String>> getAllSearchDataList(String tableName, String whereField, String whereValue, String[] sortFields, String order) {
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT * FROM ");
        queryBuilder.append(tableName);
        if (!TextUtils.isEmpty(whereField) && !TextUtils.isEmpty(whereValue)) {
            StringBuilder searchQuery = new StringBuilder();
            searchQuery.append("'%");
            whereValue = whereValue.replace("'", "''");
            searchQuery.append(whereValue);
            searchQuery.append("%'");

            queryBuilder.append(DBTableFields.WHERE);
            queryBuilder.append(whereField);
            queryBuilder.append(DBTableFields.LIKE);
            queryBuilder.append(searchQuery.toString());
        }
        if (sortFields.length > 0 && !TextUtils.isEmpty(order)) {
            queryBuilder.append(DBTableFields.ORDER_BY);
            String sortField = TextUtils.join(", ", sortFields);
            queryBuilder.append(sortField);
            queryBuilder.append(order);
        }

        Cursor tasksCursor = db.rawQuery(queryBuilder.toString(), null);
        tasksCursor.moveToFirst();
        if (!tasksCursor.isAfterLast()) {
            do {
                HashMap<String, String> val = new HashMap<>();
                for (int index = 0; index < tasksCursor.getColumnCount(); index++) {
                    val.put(tasksCursor.getColumnName(index), tasksCursor.getString(index));
                }
                arrayList.add(val);
            } while (tasksCursor.moveToNext());
        }
        tasksCursor.close();
        return arrayList;

    }

    public void deleteRecent() {
        int count = db.rawQuery("SELECT * FROM recent_searchTable", null).getCount();
        int limit = 0;
        if (count > 10) {
            limit = count - 10;
            Log.e("COUNT", String.valueOf(limit));
            String deleteQuery = "DELETE FROM recent_searchTable WHERE recentUID IN (SELECT recentUID FROM recent_searchTable ORDER BY recentUID LIMIT " + limit + ");";
            db.execSQL(deleteQuery);
            Log.e("QUERY", "WORKING");
        }
    }

    /* Delete particular Data from table */
    public void deleteAllInDB(String tableName) {
        db.delete(tableName, null, null);
    }

    /* Methods to Change Multi - Language */
    public void setLang(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    /* Return Nearby Location API URL */

    public String getNearByURL(double Lat, double Lang, int Radius, String type) {

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + Lat + "," + Lang);
        googlePlacesUrl.append("&radius=" + Radius);
        googlePlacesUrl.append("&types=" + type);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + Config.GOOGLE_MAP_API_KEY);
        googlePlacesUrl.append("&language=" +"");

        return googlePlacesUrl.toString();
    }

    public String getNearByURLByQuery(double Lat, double Lang, int Radius, String type, String query) {

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + Lat + "," + Lang);
        googlePlacesUrl.append("&radius=" + Radius);
        googlePlacesUrl.append("&query=" + query);
        googlePlacesUrl.append("&types=" + type);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + Config.GOOGLE_MAP_API_KEY);

        return googlePlacesUrl.toString();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public boolean setBookingStatus(boolean status) {
        callBookingStatus = status;
        return callBookingStatus;

    }

    public void setverifyStatus(boolean status) {
        this.verifyingSatus = status;


    }

    public boolean setHistryCancelledStatus(boolean status) {
        historyCancelledStatus = status;
        return historyCancelledStatus;
    }

    public void showToastMessageFunction(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public boolean setBidTriggerMethodsCall(boolean status) {
        setBidTriggerMethodsCall = status;
        return setBidTriggerMethodsCall;
    }

    public void setUserRateByDriver(boolean status) {
        userRateByDriver = status;

    }

    public boolean getUserRateByDriver() {

        return userRateByDriver;
    }

    public static boolean setDriverLocationUpdate(boolean status) {
        isDriverLocationUpdateExecuted = status;
        return isDriverLocationUpdateExecuted;
    }

    public static boolean setNewReservationStatus(boolean status) {
        setReservationStatus = status;
        return setReservationStatus;
    }

    public static boolean setPreviousReservationStatus(boolean status) {
        previousReservationStatus = status;
        return previousReservationStatus;
    }


    public static HashMap<String, String> putUserRateData(HashMap<String, String> data) {
        getUserRateData = data;
        return getUserRateData;
    }

    public static int setMapPadding(int padding) {
        mapPadding = padding;
        return mapPadding;
    }

    public int getMapPadding() {
        return mapPadding;
    }

    public static boolean isUserIsNear500() {
        return userIsNear500;
    }

    public static void setUserIsNear500(boolean userIsNear) {
        EApplication.userIsNear500 = userIsNear;
    }

    public void setJsonData(JSONObject json) {
        this.dataForRoute = json;
        ArrayList<HashMap<String, String>> arrayList = new ArrayList();
        HashMap<String, String> data = new HashMap<>();
        data.put(DBTableFields.JSON_DATA_FOR_DRIVER, json.toString());
        arrayList.add(data);
   /*     EApplication.getInstance().deleteAllInDB(DBTableFields.JSON_DATA_FOR_DRIVER_TABLE);*/

        Log.e("::", "Data Cleared_json");

        // insert new data to table
        EApplication.getInstance().addAllToDB(DBTableFields.JSON_DATA_FOR_DRIVER_TABLE, arrayList);
        Log.e("::", "Data Inserted_json");

    }

    public String getJsonData() {
//        Log.e("::", "Data Inserted_json_data"+ getAllToList(DBTableFields.JSON_DATA_FOR_DRIVER_TABLE).get(0).toString());

        for (Map<String, String> map : getAllToList(DBTableFields.JSON_DATA_FOR_DRIVER_TABLE)) {
            tagName = map.get("json_data");
            System.out.println(tagName);
        }
        return tagName;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String userId) {
        this.user_id = userId;
    }

    public void showNotification(Context driver_onRouteActivity, Class class_name, int id_notification, String title, String content_title, String message) {


        Intent intent1 = new Intent(driver_onRouteActivity, class_name);
        PendingIntent contentIntent = PendingIntent.getActivity(driver_onRouteActivity, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification(R.drawable.push_icon_eddy, "Eddycab Notification", System.currentTimeMillis());
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_design);
        contentView.setImageViewResource(R.id.image, R.drawable.push_icon_eddy);
        contentView.setTextViewText(R.id.title1, title);
        if (session.getAppLanguage()=="th") {
            contentView.setTextViewTextSize(R.id.title1, TypedValue.COMPLEX_UNIT_SP, 18f);
        } else {
            contentView.setTextViewTextSize(R.id.title1, TypedValue.COMPLEX_UNIT_SP, 16f);
        }


        notification.contentView = contentView;

        //Intent notificationIntent = new Intent(this, MainActivity.class);
        //PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.contentIntent = contentIntent;

        notification.flags |= Notification.FLAG_AUTO_CANCEL; //clear the notification
        notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
        notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
        notification.defaults |= Notification.DEFAULT_SOUND; // Sound
        mNotificationManager.notify(1, notification);

    }

    public void setAccesToken(String accesToken) {
        this.accesToken = accesToken;
    }

    public void setfromdrivernewjob(boolean fromjob) {
        this.fromjob = fromjob;

    }

    public boolean getfromdrivernewjob() {

        return this.fromjob;
    }

    public void setfromuserselect(boolean fromselect) {
        this.fromselect = fromselect;

    }

    public boolean getfromuserselect() {
        return this.fromselect;
    }

    public void setfromuserlocate(boolean fromlocate) {
        this.fromlocate = fromlocate;


    }

    public boolean getfromuserlocate()

    {
        return this.fromlocate;
    }



    public void setDriverNewjobdata(ArrayList<JoblistItems> pickList,ArrayList<JoblistItems> dataList) {
        this.modalDataList = dataList;
        this.pickList = pickList;


    }

    public ArrayList<JoblistItems> getDriverNewjobdatadList() {
        return this.modalDataList;


    } public ArrayList<JoblistItems> getDriverNewjobdataPickList() {
        return this.pickList;
    }

    public void sethas_arrived(boolean b) {

        this.has_arrived=b;
    }public boolean gethas_arrived() {

       return has_arrived;
    }


    public boolean getverifystatus() {
        return verifystatus;
    }
}












