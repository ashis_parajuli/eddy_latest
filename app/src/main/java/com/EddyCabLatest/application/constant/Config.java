package com.EddyCabLatest.application.constant;


public class Config {


    public static final String PRIVACY_POLICY_DRIVER_NEPALI =
            "<a href=https://www.eddycab.com/np/legal-2/privacy/drivers/";
    public static final String PRIVACY_POLICY_DRIVER_ENGLISH =
            "<a href=https://www.eddycab.com/en/legal/privacy/drivers/";
    public static final String PRIVACY_POLICY_PASSENGER_ENGLISH = "<a href=https://www.eddycab.com/en/legal/privacy/users/";
    public static final String PRIVACY_POLICY_PASSENGER_NEPALI =
            "<a href=https://www.eddycab.com/np/legal-2/privacy/users/";

    public static final String TERMS_DRIVER_NEPALI =
            "<a href=https://www.eddycab.com/np/legal-2/user-terms/";
    public static final String TERMS_DRIVER_ENGLISH =
            "<a href=https://www.eddycab.com/en/legal/terms-conditions/";


    public static final String GOOGLE_MAP_API_KEY = "AIzaSyAi6qreeIcyoDDxtrcMnYqKwTMR064C4xc";//test

    public static final String PAYPAL_CLIENT_ID_ = "AQDTVxMVuMITWk_OCYlCT5c3ekg8etlHNi6bJYL3S_qC38HvSMiD0YIkwPsUEtp5Q_E-ipWOnXG1gH--";//for testing purpose
    public static final String PAYPAL_CLIENT_ID = "AU6As_0-vaWMoIt2x3g2VAQx9NvIxhoXTvBFTC_---B3YlPS32VWhFz71-rddxbFFlxul6otWVhcs9pJ";

    public static final String GCM_SENDER_KEY = "1024195933443";
    public static final String GCM_SERVER_KEY = "AIzaSyAixutqlnbtZWTyfD0OmtFmIAIz2Csmowc";
    /* Live Base URL*/
//    public static final String APP_BASE_URL = "http://ec2-54-169-185-24.ap-southeast-1.compute.amazonaws.com/";
    // public static final String APP_BASE_URL = "http://52.34.236.57/eddycab/public/";
    public static final String APP_BASE_URL = "https://www.eddycab.com/API/public/";
    public static final String USER_REGISTER_URL = "api/users/register";
    public static final String USER_LOGIN_URL = "api/users/login";
    public static final String DRIVER_CHARGE = "api/taxi-charge";
    public static final String FIREBASE_JOBLIST = "https://blistering-fire-4004.firebaseio.com/example2/";
    public static final String CREDIT_BONUS = "api/bananas/credit-and-bonus/";

    public static final String GET_NEW_RESERVATIONS = "api/reservations/new-reservations-by-driver/";
    public static final String USER_SET_BOOKING_URL = "api/reservations/set-booking";
    public static final String USER_GET_DRIVER_LOCATION_POST_URL = "api/reservations/get-driver-location";


    public static final String GET_BIDDING_RESULT_URL = "api/reservations/biddings/";
    public static final String GET_USER_PROFILE = "api/users/get-user-profile/";


    public static final String USER_RESERVATION_URL = "api/reservations/create";
    public static final String API_RESERVATION_CANCEL = "api/reservations/cancel-reservation/";
    public static final String USER_RESERVATION_RESPONSE_URL = "api/reservations/biddings/";
    public static final String USER_CHECK_RESERVATION_CONFIRM_URL_POST = "api/reservations/check-reservation-confirmation";
    public static final String UPDATE_FAV_HOME_URL = "api/users/set-favourite-home-place";
    public static final String UPDATE_FAV_WORK_URL = "api/users/set-favourite-work-place";
    public static final String UPDATE_USER_PROFILE_NAME = "api/users/set-profile-name";
    public static final String EDIT_PROFILE = "api/users/edit-profile";
    public static final String UPDATE_USER_PROFILE_MOBILE = "api/users/set-profile-mobile";
    public static final String UPDATE_USER_PROFILE_CHANGE_PASSWORD = "api/users/set-profile-password";
    public static final String UPDATE_USER_PROFILE_CHANGE_PROFILE_PIC = "api/users/set-profile-picture";


    public static final String UPDATE_DRIVER_PROFILE = "api/drivers/update-profile";


    public static final String GET_BUY_BANANA_PRICING_URL = "api/bananas/pricing";
    public static final String GET_USER_WALLET_URL = "api/transactions/get-wallet-listing/";

    public static final String PAYMENT_VOUCHER_URL = "api/vouchers/pay-by-voucher";
    public static final String PAYMENT_PAYPAL_PAYSBUY_URL = "api/transactions/transaction-ipn";

    public static final String DRIVER_BID_POST = "api/reservations/driverPostBid";
    public static final String DRIVER_POST_BID_RESPONSE = "api/reservations/driver-biddings-response";
    public static final String DRIVER_RESERVATION_ASSIGN_POST = "api/reservations/reservation-assign";
    public static final String DRIVER_RESERVATION_IGNORE_POST = "api/reservations/reservation-ignore";
    public static final String DRIVER_RESERVATION_CANCEL_POST = "api/reservations/cancel-reservation-by-driver";
    public static final String DRIVER_DRIVER_PICKUP_PASSENGER_POST = "api/reservations/pickup-passenger";

    public static final String API_BIDDER_CANCEL_BY_USER = "api/reservations/cancel-bidder";


    public static final String USER_TRIP_HISTORY_URL = "api/reservations/user-trip-history/";
    public static final String Driver_BOOKING_HISTORY_URL = "api/reservations/driver-booking-history/";

    public static final String NEAREST_DRIVER_BY_LOCATION = "api/reservations/get-nearest-drivers";

    public static final String DRIVER_GET_QUITE_TIME_URL = "api/quietsetting/get-quiet-setting/";
    public static final String DRIVER_SET_QUITE_TIME_URL = "api/quietsetting/set-quiet-setting";

    public static final String SET_USER_LOCATION_POST_URL = "api/locations/set-location";
    public static final String GET_USER_LOCATION_URL = "api/locations/get-user-location/";

    public static final String GET_PUSH_NOTIFICATION_DRIVER = "api/reservations/send-reservation-push-notifications/";

    public static final String USER_FEEDBACK_RATING_URL = "api/feedbacks/user-feedback";
    public static final String SUPPORT_FEEDBACK_POST_URL = "api/support/insert-feedback";


    public static final String GET_MESSAGES_FROM_GCM_URL = "api/notifications/get-all-notifications/";
    public static final String CHECK_RESERVATION_STATUS_URL = "api/reservations/check-reservation-status";

    public static final String DRIVER_JOB_COMPLETE_URL = "api/reservations/reservation-complete";
    public static final String DRIVER_PAY_VOUCHER_URL = "api/reservations/reservation-receipt";
    public static final String IS_DRIVER_PICKUP_URL = "api/reservations/check-driver-status/";

    public static final String DELETE_NOTIFICATION_MESSAGE = "api/notifications/remove-message/";
    public static final String DELETE_ALL_NOTIFICATION_MESSAGE = "api/notifications/remove-user-messages/";
    public static final String USER_LOGOUT = "api/users/logout/";
    public static final String CHANGE_LANGUAGE = "api/users/set-language";
    public static final String LANG_ENG = "English";
    public static final String LANG_THAI = "Thai";
    public static final String USER_TYPE_DRIVER = "driver";
    public static final String USER_TYPE_USER = "user";
    public static final String USER_TYPE_USER_ID = "1";
    public static final String Driver_TYPE_USER_ID = "2";
    public static final float CAMERA_FACTORY_UPDATE = 15;
    public static final String DEVICE_TYPE = "android";
    public static final String DRIVING_MODE = "DRIVING";
    public static final long COUNTDOWN_TIME = 30000;
    public static final long COUNTDOWN_TIME_FAST = 10000;
    public static final long COUNTDOWN_DRIVER = 5000;
    public static final String FORGET_PASSWORD_SEND_MESSAGE = "api/users/forget-password/";
    public static final String CONFIRM_VERIFICATION_CODE = "api/users/verify-code";
    public static final String API_SET_NEW_PASSWORD = "api/users/set-new-password";
    public static final String RATE_USER_BY_DRIVER = "api/feedbacks/driver-feedback";
    public static final String URL_RESEEND_CODE_GET = "api/users/resend-code/";


    //Seven eleven API Collection
    public static final String SEVEN_PRODUCT_CATEGORIES = "api/products/product-categories";

    public static final String DRIVER_RECHARGE_HISTORY = "api/bananas/recharge-card-history/";
    public static final String RECHARGE_CARD = "api/bananas/recharge-card/";
}
