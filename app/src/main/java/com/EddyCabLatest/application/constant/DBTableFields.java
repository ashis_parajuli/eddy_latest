package com.EddyCabLatest.application.constant;

/**
 * Created by newimac on 5/21/15.
 */
public class DBTableFields {

    /*Database Table Name*/
    public static final String TABLE_DB_LOG = "dbLog_Table";
    public static final String TABLE_FREQUENT_SEARCH = "frequent_searchTable";
    public static final String TABLE_RECENT_SEARCH = "recent_searchTable";
    public static final String TABLE_DRIVER_BID = "Driver_Bid";

    /*TABLE_DB_LOG Table Item Name*/
    public static final String DB_LOG_UID = "db_log_uid";
    public static final String DB_LOG_ID = "db_log_id";
    public static final String DB_LOG_TIMESTAMP = "db_log_timestamp";
    public static final String DB_LOG_STATUS = "db_log_status";


    /* TABLE_FREQUENT_SEARCH Table Item Name */
    public static final String TABLE_FREQUENT_SEARCH_UID = "searchFrequent_UID";
    public static final String TABLE_FREQUENT_SEARCH_ID = "searchFrequent_ID";
    public static final String TABLE_FREQUENT_SEARCH_LAT = "searchFrequent_Lat";
    public static final String TABLE_FREQUENT_SEARCH_LANG = "searchFrequent_Lang";
    public static final String TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE = "searchFrequent_Address_Distance";
    public static final String TABLE_FREQUENT_SEARCH_ADDRESS_NAME = "searchFrequent_Address_Name";
    public static final String TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS = "searchFrequent_Address_Address";
    public static final String TABLE_FREQUENT_SEARCH_IMAGES = "searchFrequent_Images";

    /* TABLE_RECENT_SEARCH Table Item Name */
    public static final String TABLE_RECENT_SEARCH_UID = "recentUID";
    public static final String TABLE_RECENT_SEARCH_ID = "recent_ID";
    public static final String TABLE_RECENT_SEARCH_LAT = "recentSearch_LAT";
    public static final String TABLE_RECENT_SEARCH_LANG = "recentSearch_LANG";
    public static final String TABLE_RECENT_SEARCH_ADDRESS_NAME = "recentSearch_Name";
    public static final String TABLE_RECENT_SEARCH_ADDRESS_ADDRESS = "recentSearch_Address";
    public static final String TABLE_RECENT_SEARCH_ADDRESS_DISTANCE = "recentSearch_Distance";
    public static final String TABLE_RECENT_SEARCH_IMAGES = "recentSearch_Images";


    /* TABLE_DRIVER_BID_RESPONSE Table Fields */
    public static final String TABLE_BID_UID = "BID_UID";
    public static final String TABLE_BID_RESERVATION_ID = "RESERVATION_ID";
    public static final String TABLE_BID_DRIVER_ID = "DRIVER_ID";
    public static final String TABLE_BID_BEST_STAR = "BEST_RATING";
    public static final String TABLE_BID_LOWEST_PRICE = "LOW_BID_AMOUNT";
    public static final String TABLE_BID_NEAREST_DRIVER = "NEAREST_DISTANCE";

    public static final String DRIVER_MOBILE = "driver_mobile";
    public static final String DRIVER_REDG = "driver_redg";
    public static final String DRIVER_PROFILE = "driver_profile";
    public static final String DRIVER_NAME = "driver_name";
    public static final String DRIVER_ID = "driver_id";


    public static final String LIKE = " LIKE ";


    public static final String WHERE = " WHERE ";
    public static final String ORDER_BY = " ORDER BY ";
    public static final String ASC = " ASC";
    public static final String DESC = " DESC";


    public static final String BANANA_BUY_CREDIT = "credit";
    public static final String BANANA_BUY_AMOUNT = "amount";

    public static final String USER_WALLET_DATE_TIME = "datetime";
    public static final String USER_WALLET_BUY_BANANA = "buy_banana";
    public static final String USER_WALLET_FREE_BANANA = "free_banana";
    public static final String USER_WALLET_USE_BANANA = "use_banana";


    public static final String USER_TRIP_DATE_TIME = "datetime";
    public static final String USER_TRIP_FROM_LOCATION = "pickup_location";
    public static final String USER_TRIP_TO_LOCATION = "dropoff_location";
    public static final String USER_TRIP_AMOUNT = "ride_fare";
    public static final String USER_TRIP_STATUS = "status";
    public static final String USER_TRIP_DRIVER_FULLNAME = "fullname";
    public static final String USER_TRIP_DRIVER_ID = "driver_id";
    public static final String USER_TRIP_USER_ID = "user_id";
    public static final String USER_TRIP_RESERVATION_ID = "reservation_id";
    public static final String USER_TRIP_DRIVER_PROFILE = "driver_profile";
    public static final String USER_TRIP_PICKUP_LAT = "pickup_lat";
    public static final String USER_TRIP_PICKUP_LONG = "pickup_long";
    public static final String USER_TRIP_DROPOFF_LAT = "drop_lat";
    public static final String USER_TRIP_DROPOFF_LONG = "drop_long";
    public static final String USER_TRIP_MOBILE = "driver_mobile";
    public static final String USER_TRIP_USER_PP = "user_profile";
    public static final String USER_TRIP_DROFF_NAME = "user_name";
    public static final String USER_TRIP_REFERAL_CODE = "user_referal_code";
    public static final String USER_TRIP_ADDRESS = "user_trip_address";
    public static final String USER_TRIP_MONTH = "reservation_month";
    public static final String USER_TRIP_MONTH_NUM = "reservation_month_num";

    public static final String USER_TRIP_LIST_HEADER = "list_header";

    public static final String DRIVER_SETTING_QUITE_TIME_FROM = "from_time";
    public static final String DRIVER_SETTING_QUITE_TIME_TO = "to_time";
    public static final String DRIVER_SETTING_QUITE_TIME_NAME = "period_name";
    public static final String DRIVER_SETTING_QUITE_TIME_STATUS = "status";

    // RESERVATION STATE ::  RESERVATION_NEW, RESERVATION_PENDING, RESERVATION_DRIVER_CONFIRMED, RESERVATION_ON_ROUTE, RESERVATION_COMPLETED, RESERVATION_CANCELLED
    public static final String RESERVATION_STATE_NEW = "RESERVATION_NEW";
    public static final String RESERVATION_STATE_PENDING = "RESERVATION_PENDING";
    public static final String RESERVATION_STATE_ON_ROUTE = "RESERVATION_ON_ROUTE";
    public static final String RESERVATION_STATE_CANCELLED = "RESERVATION_CANCELLED";
    public static final String RESERVATION_STATE_COMPLETED = "RESERVATION_COMPLETED";
    public static final String RESERVATION_STATE_DRIVER_CONFIRMED = "RESERVATION_DRIVER_CONFIRMED";
    public static final String RESERVATION_STATE_WAITING_DRIVER_RESPONSE = "RESERVATION_WAITING_DRIVER";

    public static final String JSON_DATA_FOR_DRIVER = "json_data";
    public static final String TABLE_JSON_ID = "json_data_id";
    public static final String JSON_DATA_FOR_DRIVER_TABLE = "json_data_table";

}
