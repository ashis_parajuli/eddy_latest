package com.EddyCabLatest.application.utilities.swipe;

public interface SwipeAdapterInterface {

    int getSwipeLayoutResourceId(int position);

    void notifyDatasetChanged();


}
