package com.EddyCabLatest.application.utilities.swipe;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.CustomRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListViewAdapter extends BaseAdapter implements SwipeItemMangerInterface, SwipeAdapterInterface {

    private Context mContext;
    private ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
    public LayoutInflater minflater;

    ViewHolder holder;

    public ListViewAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public ListViewAdapter(Context mContext, ArrayList<HashMap<String, String>> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    public void setInflater(LayoutInflater mInflater) {
        this.minflater = mInflater;
    }

    //    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    //    @Override
    public View generateView(int position, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listview_item, null);
        SwipeLayout swipeLayout = (SwipeLayout) v.findViewById(getSwipeLayoutResourceId(position));
        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
//                YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.trash));
            }
        });
        swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
                Toast.makeText(mContext, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });
        v.findViewById(R.id.delete).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Toast.makeText(mContext, "click delete", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        return v;
    }

//    @Override
//    public void fillValues(int position, View convertView) {
//        TextView t = (TextView)convertView.findViewById(R.id.position);
//        t.setText((position + 1) + ".");
//    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);

    /**
     * return the {@link com.daimajia.swipe.SwipeLayout} resource id, int the view item.
     * @param position
     * @return
     */
//    public abstract int getSwipeLayoutResourceId(int position);

    /**
     * generate a new view item.
     * Never bind SwipeListener or fill values here, every item has a chance to fill value or bind
     * listeners in fillValues.
     * to fill it in {@code fillValues} method.
     * @param position
     * @param parent
     * @return
     */
//    public abstract View generateView(int position, ViewGroup parent);

    /**
     * fill values or bind listeners to the view.
     *
     * @param position
     * @param convertView
     */
//    public abstract void fillValues(int position, View convertView);
    @Override
    public void notifyDatasetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public final View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = minflater.inflate(R.layout.listview_item, null);
            holder.swipeLayout = (SwipeLayout) convertView.findViewById(getSwipeLayoutResourceId(position));
            holder.message = (TextView) convertView.findViewById(R.id.txt_message);
            holder.image = (ImageView) convertView.findViewById(R.id.driverProfileImage);
            holder.time = (TextView) convertView.findViewById(R.id.txt_time);
            holder.tDelete = (LinearLayout) convertView.findViewById(R.id.delete);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
            }
        });
        mItemManger.bind(convertView, position);
        holder.message.setText(arrayList.get(position).get("message"));
        holder.time.setText(arrayList.get(position).get("time"));

        holder.tDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(mContext, , Toast.LENGTH_SHORT).show();

                executeDeleteFunction(position, arrayList.get(position).get("notification_id"));

//                notifyDataSetChanged();
                mItemManger.closeAllItems();
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        TextView message;
        TextView time;
        public LinearLayout tDelete;
        public SwipeLayout swipeLayout;
        public ImageView image;
    }
    private void executeDeleteFunction(final int position, String notification_id) {

        CustomRequest req = new CustomRequest(Request.Method.GET, Config.APP_BASE_URL + Config.DELETE_NOTIFICATION_MESSAGE + notification_id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean error = response.getBoolean("error");
                            if (!error) {
                                Log.e("response", response.toString());
                                arrayList.remove(position);
                                notifyDatasetChanged();

                                Toast.makeText(mContext, mContext.getResources().getString(R.string.deleted_succesfully), Toast.LENGTH_SHORT).show();
                            } else {
                                EApplication.getInstance().showToastMessageFunction("Error true: " + error);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                EApplication.getInstance().showToastMessageFunction("Error Listiner active");
            }
        });
        EApplication.getInstance().addToRequestQueue(req, "low");
    }

    @Override
    public void openItem(int position) {
        mItemManger.openItem(position);
    }

    @Override
    public void closeItem(int position) {
        mItemManger.closeItem(position);
    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {
        mItemManger.closeAllExcept(layout);
    }

    @Override
    public void closeAllItems() {
        mItemManger.closeAllItems();
    }

    @Override
    public List<Integer> getOpenItems() {
        return mItemManger.getOpenItems();
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return mItemManger.getOpenLayouts();
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {
        mItemManger.removeShownLayouts(layout);
    }

    @Override
    public boolean isOpen(int position) {
        return mItemManger.isOpen(position);
    }

    @Override
    public Attributes.Mode getMode() {
        return mItemManger.getMode();
    }

    @Override
    public void setMode(Attributes.Mode mode) {
        mItemManger.setMode(mode);
    }
}
