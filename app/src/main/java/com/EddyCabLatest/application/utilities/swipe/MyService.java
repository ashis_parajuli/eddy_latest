package com.EddyCabLatest.application.utilities.swipe;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.EddyCabLatest.application.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

/**
 * Created by User on 1/4/2016.
 */
public class MyService extends Service implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {


    private static final int ASHIS = 1234;
    Intent intentForPendingIntent;
    HandlerThread handlerThread;
    Looper looper;
    GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRrequest;
    private static final int UPDATE_INTERVAL = 2000;
    private static final int FASTEST_INTERVAL = 1000;
    private static final int DSIPLACEMENT_UPDATES = 5;
    private Handler handler1;
    private Runnable runable1;
    private Location mLastLocation;
    private Location locationOld;
    private double distance = 0;
    private double totalWaiting = 0;
    private float speed;
    private long timeGpsUpdate;
    private long timeOld = 0;
    private NotificationManager mNotificationManager;
    Notification notification;
    PendingIntent resultPendingIntent;
    NotificationCompat.Builder mBuilder;


    // Sets an ID for the notification
    int mNotificationId = 001;
    private static final String TAG = "BroadcastService";
    public static final String BROADCAST_ACTION = "speedExceeded";
    public static final String BROADCAST_ACTION_OLD = "old_data";
    private Handler handler = new Handler();
    Intent intentforBroadcast;
    int counter = 0;
    private Runnable sendUpdatesToUI;
    private long diffTime = 0;

    private double oldDistance1 = 0;
    private double diffDistance = 0;
    private Runnable runnable;
    private boolean startcalculating = false;
    private Intent intentforBroadcast_old;
    private int i;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("binder", "binded");

        return null;
    }


    @Override
    public void onCreate() {

        intentforBroadcast = new Intent(BROADCAST_ACTION);
        intentforBroadcast_old = new Intent(BROADCAST_ACTION_OLD);
        distance = 0;
        speed = 0;
        totalWaiting = 0;

        Log.e("created Service", "created ");
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        createLocationRequest();
        mGoogleApiClient.connect();

        showNotification();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)

    private void showNotification() {

        mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.eddy_auto_taxi)
                        .setContentTitle("Total Waiting Time")
                        .setContentText(totalWaiting + "");

/*
        Intent resultIntent = new Intent(this, Driver_OnRouteActivity.class);

// Because clicking the notification opens a new ("special") activity, there's
// no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);*/
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
// Builds the notification and issues it.

        mNotifyMgr.notify(mNotificationId, mBuilder.build());


        startForeground(001, mBuilder.getNotification());
    }


    @Override
    public void onLocationChanged(final Location location) {
        Log.e("distance from service", location + "");
        if (mLastLocation != null) {
            if (handler != null) {
                handler.removeCallbacks(runnable);
            }
        }

     /*   ArrayList<Float> locations = new ArrayList<>();
        locations.add(location.getAccuracy());*/

       /* locations.add(location.getLatitude());*/


//MAX NUMBER
      /*  Collections.sort(locations);
        Collections.reverse(locations);
        locations.get(0);
*/
        DisplayLoggingInfo(location);
        if ((location.getSpeed() > 0 && location.hasAccuracy()) && (location.getAccuracy() < 50)) {
            speed = location.getSpeed();
            distance += mLastLocation.distanceTo(location);
    /*	diffDistance=distance-oldDistance1;*/
       /*     Toast.makeText(MyService.this, "distance from service" + distance, Toast.LENGTH_SHORT).show();*/

            mLastLocation = location;

/*		timeOld=location.getTime();*/
                  /*      Log.e("location: sped", location.hasSpeed() + "");*/


            //speed = (long) (distance / delta);


/*	diaplayViews();*/
    /*if (map != null) {
        map.addMarker(new MarkerOptions()
				.position(new LatLng(location.getLatitude(), location.getLongitude()))
				.title("Hello world"));


	}*/

        }




       /* Log.e("distanceold", "onLocationChanged: " + timeOld + "and" + location.getTime());*/
        //handler.removeCallbacks(runable);

       /* Toast.makeText(MyService.this, "speed" + speed, Toast.LENGTH_SHORT).show();*/
/*	timeGpsUpdate = location.getTime();*/
       /* Toast.makeText(MyService.this, "Accuracy" + location.getAccuracy(), Toast.LENGTH_SHORT).show();*/


    }

    /*protected boolean isBetterLocation(Location location, Location currentBestLocation) {


        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > 1000;
        boolean isSignificantlyOlder = timeDelta < -1000;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 20;

        // Check if the old and new location are from the same provider


        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if ( !isLessAccurate) {
            return true;
        } else if ( !isSignificantlyLessAccurate) {
            return true;
        }
        return false;
    }*/

    private void createLocationRequest() {
        mLocationRrequest = new LocationRequest();
        mLocationRrequest.setInterval(UPDATE_INTERVAL);
        mLocationRrequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRrequest.setSmallestDisplacement(DSIPLACEMENT_UPDATES);
    }

    private void methodToCalculateWaitingTime() {
        oldDistance1 = distance;
        handler1 = new Handler();
        Log.e("Here", "here1");
/*	oldDistance1=distance;*/
/*diffDistance= distance-diffDistance;*/
        runable1 = new Runnable() {
            public void run() {
                diffDistance = distance - oldDistance1;
                if (diffDistance < 200) {
                    increaseTime();
                    oldDistance1 = distance;

                }
        /*	Log.e("Here", "here2:" + mLastLocation.getSpeed());*/

                //diffTime=(mLastLocation.getTime()-timeOld)/1000;
                /*diffDistance= distance-oldDistance1;*/

/*
increaseTime();*/
                handler1.postDelayed(runable1, 10000);


                if (ActivityCompat.checkSelfPermission(MyService.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MyService.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                /*locationOld = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                mLastLocation = locationOld;*/

            }

        };

        handler1.postDelayed(runable1, 10000); //start the timer

    }


    private void increaseTime() {
/*waiting2min=waiting2min+10;*/


	/*	if (waiting2min == 120)
        {
			totalWaiting=totalWaiting+1;
		}
else
		{
waitingTime=waitingTime+10;

		}

*/


        totalWaiting++;
        showNotification();

        Log.e("waiting Time", "increaseTime: " + totalWaiting);
    }

    @Override
    public void onDestroy() {
      /*  Toast.makeText(MyService.this, "distroyed", Toast.LENGTH_SHORT).show();*/
        Log.e("destroyed", "destroyed");
        if (handler1 != null) {
            handler1.removeCallbacks(runable1);
        /*handler1 = null;
        runable1 = null;*/
        }
        if (mGoogleApiClient.isConnected()) {

            mGoogleApiClient.disconnect();
        }


    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (handler1 != null) {

            handler1.removeCallbacks(runable1);
        }
        return super.onUnbind(intent);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.e("Connection_fusion", "connected");

        startLocationUpdates();


    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRrequest, this);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Location location = plotTheInitialMarkerAndGetInitialGps();
                if (location.getLongitude() == 0 && location.getLongitude() == 0) {
                    Log.e("delayed", "delayed");
                    handler.postDelayed(runnable, 1000);
                } else {
                    handler.removeCallbacks(runnable);
                    mLastLocation = location;
                    startcalculating = true;
                    intentforBroadcast_old.putExtra("latitudeold", mLastLocation.getLatitude());
                    intentforBroadcast_old.putExtra("longitudeold", mLastLocation.getLongitude());
                    LocalBroadcastManager.getInstance(MyService.this).sendBroadcast(intentforBroadcast_old);
                    methodToCalculateWaitingTime();
                }

            }

        };
        handler.postDelayed(runnable, 1000);

    }

    private Location plotTheInitialMarkerAndGetInitialGps() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }

        locationOld = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        mLastLocation = locationOld;

        timeOld = locationOld.getTime();


        return mLastLocation;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onStart(intent, startId);


        handler1 = new Handler();
    /*sendUpdatesToUI = new Runnable() {
        public void run() {
			DisplayLoggingInfo();
			handler.postDelayed(this, 10000); // 5 seconds
		}
	};
	handler.postDelayed(sendUpdatesToUI, 10000); // 1 second*/
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        return START_NOT_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {

        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        super.onStart(intent, startId);
    }

    private void DisplayLoggingInfo(Location location) {
        Log.d(TAG, "entered DisplayLoggingInfo");
        intentforBroadcast.putExtra("speed", speed);
        intentforBroadcast.putExtra("distance", distance);
        intentforBroadcast.putExtra("latitude", location.getLatitude());
        intentforBroadcast.putExtra("longitude", location.getLongitude());
        intentforBroadcast.putExtra("waitingtime", totalWaiting);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentforBroadcast);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
