package com.EddyCabLatest.application.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.EddyCabLatest.application.constant.DBTableFields;

import java.util.HashMap;


public class SessionManager {

    private final SharedPreferences credit_pref;
    private final SharedPreferences.Editor editor_credit;
    private final SharedPreferences bonus_pref;
    private final SharedPreferences.Editor editor_bonus;
    private final SharedPreferences smsverification_pref;
    private final SharedPreferences.Editor editor_sms;
    // Shared Preferences + Editor for Shared preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    public String getLastReservationId() {

        return pref.getString(KEY_USER_LASTRESERVATIONID, null);
    }

    public void setLastReservationId(String lastReservationId) {
        editor.putString(KEY_USER_LASTRESERVATIONID, lastReservationId);
        editor.commit();

        this.lastReservationId = lastReservationId;
    }

    String lastReservationId;


    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared pref file name
    private static final String PREF_NAME = "BananaBikeSharedPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_LANGUAGE_SET = "IsLanguage";
    private static final String IS_USER_TYPE = "IsUserType";

    // Login Credential (make variable public to access from outside)
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PICTURE = "picture";
    public static final String KEY_RATING = "rating";
    public static final String KEY_BANANA_CREDIT = "total_credit";
    public static final String KEY_BANANA_BONUS = "total_bonus";
    public static final String KEY_BANANA_CREDIT_ONLY = "credit_only";
    public static final String KEY_DETAILS = "details";
    public static final String KEY_USER_REFERRAL = "referral_code";
    public static final String KEY_USER_MOBILENO = "mobile_no";
    public static final String KEY_USER_LICENSENO = "license_no";
    public static final String KEY_USER_LASTRESERVATIONID = "last_reservationid";
    public static final String ACCES_TOKEN = "acces_token";


    // Language Credential (make variable public to access from outside)
    private static final String APP_LANGUAGE = "AppLanguage";
    private static final String APP_USER = "AppUser";
    private static final String IS_VERIFIED = "verified";


    // create reservation variable
    public static final String RESERVATION_STATE = "Reservation_State";
    public static final String RESERVATION_NEW_ID = "reservation_id";
    public static final String RESERVATION_PAY_TYPE = "payment_methods";

    // Shared pref file name
    private static final String REG_PREF_NAME = "Register_SharedPref";

    //first state file name
    public static final String KEY_FIRST_STATE = "isfirst_state";


//state Shared Preferences

    SharedPreferences statePref;
    SharedPreferences.Editor stateEditor;
    // Shared Preferences
    SharedPreferences RegPref;

    // Editor for Shared preferences
    SharedPreferences.Editor appTextEditor;

    // For Location Address Store
    private static final String LOCATION_PREF_NAME = "Location_Pref";
    SharedPreferences LocationPref;
    SharedPreferences.Editor editorLocation;


    // Reservation Preferences
    private static final String RESERVATION_PREF_NAME = "Reservation_Pref";
    SharedPreferences ReservationPref;
    SharedPreferences.Editor editorReservation;

    private static final String DRIVER_CREDIT = "Driver_Credit";
    private static final String DRIVER_BONUS = "Driver_Bonus";
    SharedPreferences Credit_value;
    SharedPreferences.Editor Credit_preferences_value;

    private static final String USER_RESERVATION_PREF_NAME = "User_ReservationPref";
    SharedPreferences UserReservationPref;
    SharedPreferences.Editor editorUserReservation;


    private static final String PICKUP_LAT_LNG_NAME = "User_ReservationPref";
    SharedPreferences PickUpLatLngPref;
    SharedPreferences.Editor editorPickupLatLng;

    //Last ReservationType User
    private static final String PREVIOUS_RESERVATIONTYPE_NAME = "User_ReservationPref";
    public static final String PREVIOUS_RESERVATIONTYPE_KEY = "User_ReservationKey";
    SharedPreferences ReservationTypePref;
    SharedPreferences.Editor editorReservationType;
    //Last Driver mobile Number
    private static final String DRIVER_lATEST_NUMBER = "driver_latestPref";
    public static final String DRIVER_lATEST_NUMBER_KEY = "driver_number_key";
    SharedPreferences driverNoPref;
    SharedPreferences.Editor editorDriverNo;
    public static final String IS_SOURCE_LOCATION = "Source_Location";
    public static final String SOURCE_LOCATION_NAME = "Source_Location_Name";
    public static final String SOURCE_LOCATION_ADDRESS = "Source_Location_Address";
    public static final String SOURCE_LOCATION_GEO_POINT_LAT = "Source_Location_geoPointLat";
    public static final String SOURCE_LOCATION_GEO_POINT_LANG = "Source_Location_geoPointLang";

    public static final String IS_DESTINATION_LOCATION = "Destination_Location";
    public static final String DESTINATION_LOCATION_NAME = "Destination_Location_Name";
    public static final String DESTINATION_LOCATION_ADDRESS = "Destination_Location_Address";
    public static final String DESTINATION_LOCATION_GEO_POINT_LAT = "Destination_Location_geoPointLat";
    public static final String DESTINATION_LOCATION_GEO_POINT_LANG = "Destination_Location_geoPointLang";

    public static final String RESERVATION_ID = "Reservation_ID";
    public static final String DRIVER_ID = "Driver_ID";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        RegPref = _context.getSharedPreferences(REG_PREF_NAME, PRIVATE_MODE);
        appTextEditor = RegPref.edit();
        LocationPref = _context.getSharedPreferences(LOCATION_PREF_NAME, PRIVATE_MODE);
        editorLocation = LocationPref.edit();

        ReservationPref = _context.getSharedPreferences(RESERVATION_PREF_NAME, PRIVATE_MODE);
        editorReservation = ReservationPref.edit();

        UserReservationPref = _context.getSharedPreferences(USER_RESERVATION_PREF_NAME, PRIVATE_MODE);
        editorUserReservation = UserReservationPref.edit();

        credit_pref = _context.getSharedPreferences(DRIVER_CREDIT, PRIVATE_MODE);
        bonus_pref = _context.getSharedPreferences(DRIVER_BONUS, PRIVATE_MODE);
        editor_credit = credit_pref.edit();
        editor_bonus = bonus_pref.edit();

        PickUpLatLngPref = _context.getSharedPreferences(PICKUP_LAT_LNG_NAME, PRIVATE_MODE);
        editorPickupLatLng = PickUpLatLngPref.edit();

//        statePref=_context.getSharedPreferences(KEY_FIRST_STATE, PRIVATE_MODE);
//        stateEditor=statePref.edit();

        ReservationTypePref = _context.getSharedPreferences(PREVIOUS_RESERVATIONTYPE_NAME, PRIVATE_MODE);
        editorReservationType = ReservationTypePref.edit();

        driverNoPref = _context.getSharedPreferences(DRIVER_lATEST_NUMBER, PRIVATE_MODE);
        editorDriverNo = driverNoPref.edit();


        smsverification_pref=_context.getSharedPreferences(IS_VERIFIED,PRIVATE_MODE);
        editor_sms=smsverification_pref.edit();
    }


    public void addcredit(String credit) {
        editor.putString(DRIVER_CREDIT, credit);
        editor.commit();

    }

    public void addbonus(String credit) {
        editor.putString(DRIVER_BONUS, credit);
        editor.commit();

    }

    public String getDriverCredit() {
        return pref.getString(DRIVER_CREDIT, "0");
    }


    public String getDriverBonus() {
        return pref.getString(DRIVER_BONUS, "0");
    }

    public void createState() {
        editorUserReservation.putString(RESERVATION_STATE, UserReservationPref.getString(RESERVATION_STATE, DBTableFields.RESERVATION_STATE_NEW));
        editorUserReservation.commit();
    }


    /**
     * Create login session
     */
    public void createLoginSession(String id, String name, String email, String picture, String rating, String details, String referral, String mobileNo, String licenseNo, String lastReservationId, String acces_token) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing login details in pref
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PICTURE, picture);
        editor.putString(KEY_RATING, rating);
        editor.putString(KEY_DETAILS, details);
        editor.putString(KEY_USER_REFERRAL, referral);
        editor.putString(KEY_USER_MOBILENO, mobileNo);
        editor.putString(KEY_USER_LICENSENO, licenseNo);
        editor.putString(ACCES_TOKEN, acces_token);


        // commit changes
        editor.commit();
    }

    public void createupdatedriverdata(String name, String email, String mobileNo, String profile) {


        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);


        editor.putString(KEY_USER_MOBILENO, mobileNo);
        editor.putString(KEY_PICTURE, profile);

        // commit changes
        editor.commit();
    }

    public void createupdateuserdata(String name, String email, String mobileNo, String licenseNo) {

        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USER_MOBILENO, mobileNo);
        editor.putString(KEY_PICTURE, licenseNo);

        // commit changes
        editor.commit();
    }


    public void createLanguageSession(String language) {

        // Storing login value as TRUE
        editor.putBoolean(IS_LANGUAGE_SET, true);

        // Storing language value
        editor.putString(APP_LANGUAGE, language);
        editor.commit();
    }

    public void createUserTypeSession(String user) {

        // Storing login value as TRUE
        editor.putBoolean(IS_USER_TYPE, true);

        // Storing language value
        editor.putString(APP_USER, user);
        editor.commit();
    }


    public void createSMSVerifySession() {
        editor_sms.putBoolean(IS_VERIFIED, true);
        editor_sms.commit();
    }


    public boolean getverifySession() {
return smsverification_pref.getBoolean(IS_VERIFIED,false);
    }

    public void deleteVerifySession()
    {


      editor_sms.clear();
        editor_sms.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<>();
        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_PICTURE, pref.getString(KEY_PICTURE, null));
        user.put(KEY_RATING, pref.getString(KEY_RATING, null));
        user.put(KEY_DETAILS, pref.getString(KEY_DETAILS, null));
        user.put(KEY_USER_REFERRAL, pref.getString(KEY_USER_REFERRAL, null));
        user.put(KEY_USER_MOBILENO, pref.getString(KEY_USER_MOBILENO, null));
        user.put(KEY_USER_LICENSENO, pref.getString(KEY_USER_LICENSENO, null));
        user.put(KEY_USER_LASTRESERVATIONID, pref.getString(KEY_USER_LASTRESERVATIONID, null));
        user.put(ACCES_TOKEN, pref.getString(ACCES_TOKEN, null));


        return user;
    }

    public void clearUserDetails() {
        editor.clear();
        editor.commit();
    }

    // Pref Banana Credit Set + Get
    public void setUserBananaCredit(String credit, String total_bonus, String credit_only) {
        editor.putString(KEY_BANANA_CREDIT, credit);
        editor.putString(KEY_BANANA_BONUS, total_bonus);
        editor.putString(KEY_BANANA_CREDIT_ONLY, credit_only);
        editor.commit();
    }

    public String getUserBananaCredit() {
        return pref.getString(KEY_BANANA_CREDIT, "0");
    }

    public String getUserBananaBonus() {
        return pref.getString(KEY_BANANA_BONUS, "0");
    }

    public String getUserBananaCreditOnly() {
        return pref.getString(KEY_BANANA_CREDIT_ONLY, "0");
    }

// user Reservation create pref Set + Get

    public void createReservationByUser(String reservationState) {

        editorUserReservation.putString(RESERVATION_STATE, reservationState);
        editorUserReservation.commit();
    }

    public String getReservationByUser() {
        return UserReservationPref.getString(RESERVATION_STATE, null);
    }

    public void createReservationNewUser(String reservationID, String reservationPayType) {

        editorUserReservation.putString(RESERVATION_NEW_ID, reservationID);
        editorUserReservation.putString(RESERVATION_PAY_TYPE, reservationPayType);
        editorUserReservation.commit();
    }

    public HashMap<String, String> getReservationNewData() {
        HashMap<String, String> newReservation = new HashMap<>();

        newReservation.put(RESERVATION_NEW_ID, UserReservationPref.getString(RESERVATION_NEW_ID, null));
        newReservation.put(RESERVATION_PAY_TYPE, UserReservationPref.getString(RESERVATION_PAY_TYPE, null));
        return newReservation;
    }


    // Get Login State quick check
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    // Get Language quick check
    public boolean isLanguageSet() {
        return pref.getBoolean(IS_LANGUAGE_SET, false);
    }

    // Get UserType quick check
    public boolean isUserSet() {
        return pref.getBoolean(IS_USER_TYPE, false);
    }

    public String getAppLanguage() {
        return pref.getString(APP_LANGUAGE, null);
    }

    public String getAppUserType() {
        return pref.getString(APP_USER, null);
    }

    /* ============================================================================================ */
// Create Reservation
    public void createLastReservation(String ReservationID, String DriverID) {
        editorReservation.putString(RESERVATION_ID, ReservationID);
        editorReservation.putString(DRIVER_ID, DriverID);
        editorReservation.commit();
    }

    // Get Reservation ID
    public HashMap<String, String> getReservationIdWithDriver() {
        HashMap<String, String> reservation = new HashMap<>();

        reservation.put(DRIVER_ID, ReservationPref.getString(DRIVER_ID, null));
        reservation.put(RESERVATION_ID, ReservationPref.getString(RESERVATION_ID, null));

        return reservation;
    }


    //clear reservation data
    public void clearReservationData() {
        editorReservation.clear();
        editorReservation.commit();
    }

    /* ============================================================================================ */


    public void createLastPickLatLng(String pickupLatLng) {
        editorPickupLatLng.putString(PICKUP_LAT_LNG_NAME, pickupLatLng);
        editorPickupLatLng.commit();

    }

    public String getPickupLatLng() {
        return PickUpLatLngPref.getString(PICKUP_LAT_LNG_NAME, null);
    }

    public void clearPickLatLng() {
        editorPickupLatLng.clear();
        editorPickupLatLng.commit();
    }

    /* ============================================================================================ */
// Set and Get Pref Stored Location
    public void createSourceLocation(String name, String address, String geoPointLat, String geoPointLang) {
        // Storing login value as TRUE
        editorLocation.putBoolean(IS_SOURCE_LOCATION, true);
        editorLocation.putString(SOURCE_LOCATION_NAME, name);
        editorLocation.putString(SOURCE_LOCATION_ADDRESS, address);
        editorLocation.putString(SOURCE_LOCATION_GEO_POINT_LAT, String.valueOf(geoPointLat));
        editorLocation.putString(SOURCE_LOCATION_GEO_POINT_LANG, String.valueOf(geoPointLang));
        // commit changes
        editorLocation.commit();
    }
    public void createDestinationLocation(String name, String address, String geoPointLat, String geoPointLang) {
        // Storing login value as TRUE
        editorLocation.putBoolean(IS_DESTINATION_LOCATION, true);

        editorLocation.putString(DESTINATION_LOCATION_NAME, name);
        editorLocation.putString(DESTINATION_LOCATION_ADDRESS, address);
        editorLocation.putString(DESTINATION_LOCATION_GEO_POINT_LAT, geoPointLat);
        editorLocation.putString(DESTINATION_LOCATION_GEO_POINT_LANG, geoPointLang);
        // commit changes
        editorLocation.commit();
    }

    /*....................................... following for getting destinations address...................................*/
    public HashMap<String, String> getSourceLocation() {
        HashMap<String, String> sourceLocation = new HashMap<>();
        ;

        if (!LocationPref.getString(SOURCE_LOCATION_NAME, "").equals("") &&
                !LocationPref.getString(SOURCE_LOCATION_GEO_POINT_LAT, "").equals("") && !LocationPref.getString(SOURCE_LOCATION_GEO_POINT_LANG, "").equals("")) {
            sourceLocation.put(SOURCE_LOCATION_NAME, LocationPref.getString(SOURCE_LOCATION_NAME, null));
            sourceLocation.put(SOURCE_LOCATION_ADDRESS, LocationPref.getString(SOURCE_LOCATION_ADDRESS, null));
            sourceLocation.put(SOURCE_LOCATION_GEO_POINT_LAT, LocationPref.getString(SOURCE_LOCATION_GEO_POINT_LAT, null));
            sourceLocation.put(SOURCE_LOCATION_GEO_POINT_LANG, LocationPref.getString(SOURCE_LOCATION_GEO_POINT_LANG, null));
            return sourceLocation;
        }
        // return user
        return null;
    }

    public HashMap<String, String> getDestinationLocation() {
        HashMap<String, String> destinationLocation = new HashMap<>();

        destinationLocation.put(DESTINATION_LOCATION_NAME, LocationPref.getString(DESTINATION_LOCATION_NAME, null));
        destinationLocation.put(DESTINATION_LOCATION_ADDRESS, LocationPref.getString(DESTINATION_LOCATION_ADDRESS, null));
        destinationLocation.put(DESTINATION_LOCATION_GEO_POINT_LAT, LocationPref.getString(DESTINATION_LOCATION_GEO_POINT_LAT, null));
        destinationLocation.put(DESTINATION_LOCATION_GEO_POINT_LANG, LocationPref.getString(DESTINATION_LOCATION_GEO_POINT_LANG, null));
        // return user
        return destinationLocation;
    }


    /*for clearing pickup address and dropoff address from session*/

    public void clearPickUPDropOffLocationAddress() {
        editorLocation.clear();
        editorLocation.commit();

    }

    /* ============================================================================================= */
// Create Reservation
    public void createReservationType(String ReservationID) {
        editorReservationType.putString(PREVIOUS_RESERVATIONTYPE_KEY, ReservationID);
        editorReservationType.commit();
    }

    // Get Reservation ID
    public String getReservationType() {
        String reservationtype = ReservationTypePref.getString(PREVIOUS_RESERVATIONTYPE_KEY, "bike");
        return reservationtype;
    }

    //clear reservation data
    public void clearReservationType() {
        editorReservationType.clear();
        editorReservationType.commit();
    }

    public boolean isSourceLocationSet() {
        return LocationPref.getBoolean(IS_SOURCE_LOCATION, false);
    }

    public boolean isDestinationLocationSet() {
        return LocationPref.getBoolean(IS_DESTINATION_LOCATION, false);
    }

    //Driver LastNumber
    public void createLastDriverNumber(String driverNumber) {
        editorDriverNo.putString(DRIVER_lATEST_NUMBER_KEY, driverNumber);
        editorDriverNo.commit();
    }

    // Get Reservation ID
    public String getDriverNumber() {
        String reservationtype = driverNoPref.getString(DRIVER_lATEST_NUMBER_KEY, "none");
        return reservationtype;
    }

    public void clearDriverNumber() {
        editorDriverNo.clear();
        editorDriverNo.commit();
    }


}
