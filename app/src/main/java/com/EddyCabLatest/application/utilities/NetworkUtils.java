package com.EddyCabLatest.application.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.Log;


public class NetworkUtils {

    private static ConnectivityManager connectivityManager;
    private static NetworkInfo activeNetworkInfo;

    public static boolean isNetworkAvailable(Context act) {
if(act!=null)
{
    connectivityManager = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);


   activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

}
        else
{
    Log.e("connected","net connected");

}
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

    public static void showNoConnectionDialog(Context ctx) {
        final Context context = ctx;
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setCancelable(true);
        builder.setMessage("Internet Connection Not Available");
        builder.setTitle("Connectivity");
        builder.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        context.startActivity(new Intent(
                                Settings.ACTION_WIRELESS_SETTINGS));
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });


        builder.show();
    }

}
