package com.EddyCabLatest.application.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.EddyCabLatest.application.constant.DBTableFields.*;
/**
 * Created by newimac on 5/21/15.
 */
public class DBHelper extends SQLiteOpenHelper{

    public static final int VERSION = 1;
    public static final String DB_NAME = "banana_bike_db.sqlite";

    private String TABLE_JSON_DRIVER;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        createTables(db);
        initiateLogin(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        dropAndCreate(db);

    }


    protected void dropAndCreate(SQLiteDatabase db) {
       db.execSQL("DROP TABLE IF EXISTS " + TABLE_DB_LOG + ";");
       db.execSQL("DROP TABLE IF EXISTS " + TABLE_FREQUENT_SEARCH + ";");
       db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_SEARCH + ";");
       db.execSQL("DROP TABLE IF EXISTS " + TABLE_DRIVER_BID + ";");
       db.execSQL("DROP TABLE IF EXISTS " + JSON_DATA_FOR_DRIVER_TABLE + ";");


        createTables(db);
        initiateLogin(db);
    }

    private void initiateLogin(SQLiteDatabase db) {

        ContentValues values = new ContentValues();
        values.put(DB_LOG_UID, 1);
        values.put(DB_LOG_ID, "");
        values.put(DB_LOG_TIMESTAMP, "");
        values.put(DB_LOG_STATUS, "");

        // Inserting Row
        db.insert(TABLE_DB_LOG, null, values);

    }

    private void createTables(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_DB_LOG + " (" +
                DB_LOG_UID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                DB_LOG_ID + " INTEGER," +
                DB_LOG_TIMESTAMP + " TEXT," +
                DB_LOG_STATUS + " TEXT" +");" );



        db.execSQL("CREATE TABLE " + TABLE_FREQUENT_SEARCH + " (" +
                TABLE_FREQUENT_SEARCH_UID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                TABLE_FREQUENT_SEARCH_ID + " INTEGER," +
                TABLE_FREQUENT_SEARCH_LAT + " TEXT," +
                TABLE_FREQUENT_SEARCH_LANG + " TEXT," +
                TABLE_FREQUENT_SEARCH_IMAGES + " TEXT," +
                TABLE_FREQUENT_SEARCH_ADDRESS_NAME + " TEXT," +
                TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS + " TEXT," +
                TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE + " TEXT" +");" );

        db.execSQL("CREATE TABLE " + TABLE_RECENT_SEARCH + " (" +
                TABLE_RECENT_SEARCH_UID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                TABLE_RECENT_SEARCH_ID + " INTEGER," +
                TABLE_RECENT_SEARCH_LAT + " TEXT," +
                TABLE_RECENT_SEARCH_LANG + " TEXT," +
                TABLE_RECENT_SEARCH_IMAGES + " TEXT," +
                TABLE_RECENT_SEARCH_ADDRESS_NAME + " TEXT," +
                TABLE_RECENT_SEARCH_ADDRESS_ADDRESS + " TEXT," +
                TABLE_RECENT_SEARCH_ADDRESS_DISTANCE + " TEXT" +");" );

        db.execSQL("CREATE TABLE " + TABLE_DRIVER_BID + " (" +
                TABLE_BID_UID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +

                TABLE_BID_RESERVATION_ID + " TEXT," +
                TABLE_BID_BEST_STAR + " TEXT," +
                TABLE_BID_LOWEST_PRICE + " TEXT," +
                DRIVER_MOBILE + " TEXT," +
                DRIVER_REDG + " TEXT," +
                DRIVER_PROFILE + " TEXT," +
                DRIVER_NAME + " TEXT," +
                DRIVER_ID + " TEXT," +
                TABLE_BID_NEAREST_DRIVER + " TEXT" +");" );

        db.execSQL("CREATE TABLE " + JSON_DATA_FOR_DRIVER_TABLE + " (" +
                TABLE_JSON_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                JSON_DATA_FOR_DRIVER + " TEXT" + ");" );


    }

}
