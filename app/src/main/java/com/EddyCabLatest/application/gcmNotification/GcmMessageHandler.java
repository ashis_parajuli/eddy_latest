package com.EddyCabLatest.application.gcmNotification;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_ActivityBidSuccess;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.moduleDriver.activity.TransparentActivity;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserRateDriver;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class GcmMessageHandler extends GcmListenerService {
    public String TAG = GcmMessageHandler.class.getSimpleName();
    public static final int MESSAGE_NOTIFICATION_ID = 435345;
    private  boolean newJobDialogShown;
 private  boolean isUserNear;

    String NOTIFICATION_SCREEN = "";


    @Override
    public void onMessageReceived(String from, final Bundle data) {

        String NOTIFICATION_DATA = data.toString();

        Log.e("===", "NOTIFICATION_DATA" + NOTIFICATION_DATA);

        final String NOTIFICATION_TITLE = data.getString("message");
        Log.e("===", "NOTIFICATION_TITLE" + NOTIFICATION_TITLE);

        final String NOTIFICATION_CONTENT = data.getString("custom");
        Log.e("===", "NOTIFICATION_CONTENT" + NOTIFICATION_CONTENT);


        if (!TextUtils.isEmpty(NOTIFICATION_CONTENT) && !TextUtils.equals(NOTIFICATION_CONTENT, "null")) {
            try {
                JSONObject objectData = new JSONObject(NOTIFICATION_CONTENT);
                NOTIFICATION_SCREEN = objectData.getJSONObject("data").getString("screen");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("NOTIFICATION_CONTENT", "No data found !!");
        }

        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        Log.d("current task :", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClass().getSimpleName());
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        Log.e("Package name:", componentInfo.getPackageName());
        if (componentInfo.getPackageName().contains("com.bananabike.app")) {
            //Activity in foreground, broadcast intent
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Context context = getApplicationContext();
                    switch (NOTIFICATION_SCREEN) {

                        case "newreservations":
                        isUserNear = EApplication.getInstance().isUserIsNear500();
                            if (!isUserNear) {
                                newJobDialogShown = EApplication.getInstance().isNewJobDialogShown();
                                if (!newJobDialogShown) {
                                    EApplication.getInstance().setIsNewJobDialogShown(true);
                                    Intent intent_new_reservation = new Intent(context, TransparentActivity.class);
                                    intent_new_reservation.putExtra("newReservation", "newReservation");
                                    intent_new_reservation.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent_new_reservation.putExtra("message", getResources().getString(R.string.new_job_avaiable));
                                    startActivity(intent_new_reservation);


                                }
                            }
//                            EApplication.getInstance().setBidTriggerMethodsCall(true);


                                break;

                                case "assign":
                                    Toast.makeText(GcmMessageHandler.this, "Assigned to user", Toast.LENGTH_SHORT).show();
                         Intent intent = new Intent(context, Driver_ActivityBidSuccess.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("fromGCM", true);
                        startActivity(intent);
                                    break;
                                case "ratings":
                                    Toast.makeText(GcmMessageHandler.this, "Rating to user", Toast.LENGTH_SHORT).show();
                                    SessionManager session = new SessionManager(context);
                                    String user_type = session.getAppUserType();
                                    String journeyFee = "";
                                    String date = "";
                                    try {
                                        JSONObject objectData = new JSONObject(NOTIFICATION_CONTENT);
                                        journeyFee = objectData.getJSONObject("data").getString("fee");
                                /*String dateTime = objectData.getJSONObject("data").getString("datetime");
                                if (dateTime.contains(" ")) {
                                    date = dateTime.substring(0, dateTime.indexOf(" "));
                                }*/
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (user_type.equalsIgnoreCase(Config.USER_TYPE_USER)) {
                                        Intent intent_rate_driver = new Intent(context, Activity_UserRateDriver.class);
                                        intent_rate_driver.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent_rate_driver.putExtra("fee", journeyFee);
                                        // intent_rate_driver.putExtra("journeyDate", date);
                                        intent_rate_driver.putExtra("message", NOTIFICATION_TITLE);

                                        startActivity(intent_rate_driver);
                                    } else {

                                /*Intent intent_rate_user = new Intent(context, Driver_MainActivity.class);
                                intent_rate_user.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent_rate_user);*/
                                    }


                                    break;

                                case "reservationcancelled":
                                    Toast.makeText(GcmMessageHandler.this, "Canceled", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(context, TransparentActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.putExtra("message", "reservationcancelled");
                                    startActivity(i);
                                    break;


                                case "biddingcancelled":
                                    Intent intent_cancel_bid = new Intent(context, Driver_MainActivity.class);
                                    intent_cancel_bid.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent_cancel_bid);
                                    break;


                            }
                    }
                }

                );
            }else {
            //Activity Not Running
            //Generate Notification
            Log.e("NOTIFICATION_CONTENT", "SCREEN :: " + NOTIFICATION_SCREEN);
            createNotification(from, NOTIFICATION_TITLE, NOTIFICATION_SCREEN);
        }

    }

    // Creates notification based on title and body received
    private void createNotification(String from, String title, String NOTIFICATION_SCREEN_STATE) {

        Context context = getBaseContext();


        if (!TextUtils.isEmpty(NOTIFICATION_SCREEN_STATE) && !TextUtils.equals(NOTIFICATION_SCREEN_STATE, "null")) {

            PendingIntent contentIntent = null;
            switch (NOTIFICATION_SCREEN_STATE) {

                case "newreservations":
                    boolean isUserNear = EApplication.getInstance().isUserIsNear500();
                    if (!isUserNear) {
                        Intent intent_new_reservation = new Intent(context, Driver_MainActivity.class);
                        contentIntent = PendingIntent.getActivity(context, 0, intent_new_reservation, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                    break;

                case "assign":
                    Intent intent = new Intent(context, Driver_ActivityBidSuccess.class);
                    intent.putExtra("fromGCM", true);
                    contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
                case "ratings":
                    SessionManager session = new SessionManager(context);
                    String user_type = session.getAppUserType();

                    if (user_type.equalsIgnoreCase(Config.USER_TYPE_USER)) {
                        Intent intent_rate_driver = new Intent(context, Activity_UserRateDriver.class);
                        contentIntent = PendingIntent.getActivity(context, 0, intent_rate_driver, PendingIntent.FLAG_UPDATE_CURRENT);
                    } else {
                        Intent intent_rate_user = new Intent(context, Driver_MainActivity.class);
                        contentIntent = PendingIntent.getActivity(context, 0, intent_rate_user, PendingIntent.FLAG_UPDATE_CURRENT);
                    }


                    break;

                case "reservationcancelled":

                case "biddingcancelled":
                    Intent intent_cancel_bid = new Intent(context, Driver_MainActivity.class);
                    contentIntent = PendingIntent.getActivity(context, 0, intent_cancel_bid, PendingIntent.FLAG_UPDATE_CURRENT);
                    break;
            }


            if (contentIntent != null) {
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.icon_notification)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                        .setContentIntent(contentIntent)
                        .setContentText(title);


                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notificationBuilder.build());
            } else {
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.icon_notification)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                                // .setContentIntent(contentIntent)
                        .setContentText(title);


                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notificationBuilder.build());
            }

        } else {


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                    this).setSmallIcon(R.drawable.icon_notification)
                    .setContentTitle(title)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                            // .setContentIntent(contentIntent)
                    .setContentText(title);


            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notificationBuilder.build());
        }


    }
}
