package com.EddyCabLatest.application;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.activity.DriverLoginScreenActivity;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserMain;
import com.EddyCabLatest.application.utilities.SessionManager;

public class SelectUserType extends AppCompatActivity implements View.OnClickListener {

    SessionManager session;
    EApplication app;
    private Toolbar app_toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        app = EApplication.getInstance();
        session = new SessionManager(getApplicationContext());
        if (session.getAppLanguage().equals("Thai")) app.setLang("np");
        else app.setLang("en");
        setContentView(R.layout.activity_select_user);
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        Button btn_User_Driver = (Button) findViewById(R.id.btn_User_Driver);
        Button btn_User_User = (Button) findViewById(R.id.btn_User_User);
        if ((session.getAppLanguage()).toString().equalsIgnoreCase("Thai")) {
            btn_User_Driver.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            btn_User_User.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
        }
        btn_User_Driver.setOnClickListener(this);
        btn_User_User.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent_user = new Intent(SelectUserType.this, SelectLanguage.class);
            startActivity(intent_user);
            this.finish();
            overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_User_Driver:
                session.createUserTypeSession(Config.USER_TYPE_DRIVER);
                Intent intent_driver = new Intent(getApplicationContext(), DriverLoginScreenActivity.class);
                startActivity(intent_driver);
                this.finish();
                return;

            case R.id.btn_User_User:
                session.createUserTypeSession(Config.USER_TYPE_USER);
                Intent intent = new Intent(getApplicationContext(), Activity_UserMain.class);
                startActivity(intent);
                this.finish();
                return;

        }
    }

}
