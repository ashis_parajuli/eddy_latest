package com.EddyCabLatest.application;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;


/**
 * Created by dell on 12/9/2015.
 */
public class PreSplashActivity extends Activity {
    ////
    private static final int SAMPLE_REQUEST_00 = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Start permission
            checkAndRequestPermissions(new String[]{
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
            }, SAMPLE_REQUEST_00);
        } else {
            // Do your task here...
            startSplashTask();
        }

    }

    private void checkAndRequestPermissions(String[] permissions, int requestCode) {
        boolean allGranted = true;
        for (String permission : permissions) {
            if (!hasSelfPermission(permission)) {
                allGranted = false;
            }
        }
        if (allGranted) {
            // All permission Granted
            startSplashTask();
        } else {
            ActivityCompat.requestPermissions(this, permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasSelfPermission(String permission) {
        return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case SAMPLE_REQUEST_00: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[3] == PackageManager.PERMISSION_GRANTED
                        ) {
                    // coarse location permission granted
                    startSplashTask();
                } /*else if (grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    showAlertDialog("Functionality limited",
                            "Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                } else if (grantResults[1] == PackageManager.PERMISSION_DENIED && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showAlertDialog("Functionality limited",
                            "Since storage access has not been granted, this app will not be able to change your profile picture.");
                } else {
                    showAlertDialog("Functionality limited",
                            "Since location and storage access has not been granted, this app will not be able to discover beacons the background and change your profile picture.");
                }*/ else {
                    finish();
                }
            }
        }
    }

    private void startSplashTask() {
        startActivity(new Intent(PreSplashActivity.this, SplashScreen.class));
        finish();
    }
}
