package com.EddyCabLatest.application;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserMain;
import com.EddyCabLatest.application.utilities.SessionManager;


public class SplashScreen extends Activity{

    // Splash screen timer
    private int SPLASH_TIME_OUT = 1000;

    // Shared Preference Manager
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        session = new SessionManager(getApplicationContext());

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {


                if(session.isLanguageSet()){
                    if (session.isUserSet()) {

                        if (session.getAppUserType().equals(Config.USER_TYPE_USER)){
                            if(session.isLoggedIn())
                            {
                                Intent intent = new Intent(getApplicationContext(), Activity_UserMain.class);
                                startActivity(intent);
                                finish();

                            }
                               else {
                                Intent intent = new Intent(getApplicationContext(), SelectLanguage.class);
                                startActivity(intent);
                                finish();
                            }


                        }else {

                            if (session.isLoggedIn()) {
                                Intent intent = new Intent(getApplicationContext(), Driver_MainActivity.class);
                                startActivity(intent);
                                finish();
                            }else {
                                Intent intent = new Intent(getApplicationContext(), SelectLanguage.class);
                                startActivity(intent);
                                finish();
                            }
                        }

                    }else {
                        Intent intent = new Intent(getApplicationContext(), SelectUserType.class);
                        startActivity(intent);
                        finish();
                    }
                }else {

                    Intent intent = new Intent(getApplicationContext(), SelectLanguage.class);
                    startActivity(intent);
                    finish();
                }


            }
        }, SPLASH_TIME_OUT);
    }

}
