package com.EddyCabLatest.application;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.SessionManager;

public class SelectLanguage extends AppCompatActivity implements View.OnClickListener{

    SessionManager session;
    EApplication app;
    private Toolbar app_toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);

        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        Log.e("SelectLanguage", "Executed ...");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        /* Activity Loading Animation */
        overridePendingTransition  (R.anim.right_slide_in, R.anim.no_anim);

        app = EApplication.getInstance();
        session = new SessionManager(getApplicationContext());

        Button eng_language = (Button) findViewById(R.id.btn_Eng_Language);
        Button thai_language = (Button) findViewById(R.id.btn_Thai_Language);

        eng_language.setOnClickListener(this);
        thai_language.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_Eng_Language:
                session.createLanguageSession(Config.LANG_ENG);
                app.setLang("en");
                Launch_Activity();
                return;
            case R.id.btn_Thai_Language:
                session.createLanguageSession(Config.LANG_THAI);
                app.setLang("np");
                Launch_Activity();
                return;
        }
    }

    private void Launch_Activity() {
        Intent intent = new Intent(getApplicationContext(), SelectUserType.class);
        startActivity(intent);
        this.finish();
    }
}
