package com.EddyCabLatest.application.moduleUser.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleUser.fragment.Fragment_GetLocation_Favourites;
import com.EddyCabLatest.application.moduleUser.fragment.Fragment_GetLocation_Foursquare;
import com.EddyCabLatest.application.moduleUser.fragment.Fragment_GetLocation_Google;
import com.EddyCabLatest.application.moduleUser.fragment.Fragment_GetLocation_Map;
import com.EddyCabLatest.application.moduleUser.fragment.Fragment_GetLocation_Places;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.EddyCabLatest.application.views.MaterialTab;
import com.EddyCabLatest.application.views.MaterialTabHost;
import com.EddyCabLatest.application.views.MaterialTabListener;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Activity_PickupLocation extends AppCompatActivity implements MaterialTabListener {

    SessionManager session;
    Toolbar app_toolBar;
    ViewPager pager;
    MaterialTabHost tabHost;
    ViewPagerAdapter adapter;
    String target;
    private String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search_location);

        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setVisibility(View.VISIBLE);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);


        //Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);

        session = new SessionManager(Activity_PickupLocation.this);

        target = getIntent().getStringExtra("ADDRESS_REQUEST");


        // TabHost Define and init
        tabHost = (MaterialTabHost) findViewById(R.id.materialTabHost);
        pager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);


        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                tabHost.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab().setIcon(adapter.getIcon(i), adapter.getPageTitle(i).toString()).setTabListener(this));
        }

        tabHost.setSelectedNavigationItem(1);
        pager.setCurrentItem(1);
    }

    @Override
    public void onTabSelected(MaterialTab tab) {
        pager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabReselected(MaterialTab materialTab) {

    }

    @Override
    public void onTabUnselected(MaterialTab materialTab) {

    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private String[] TITLE = new String[]{
                getResources().getString(R.string.favourites),
                getResources().getString(R.string.places),
                getResources().getString(R.string.foursquare),
                getResources().getString(R.string.google),
                getResources().getString(R.string.map)};

        private Integer[] tab_icons = {
                R.drawable.eddy_star_blank,
                R.drawable.eddy_auto_taxi,
                R.drawable.foursquare_eddy,
                R.drawable.eddy_google,
                R.drawable.eddy_map};

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Fragment_GetLocation_Favourites();
                case 1:
                    return new Fragment_GetLocation_Places();
                case 2:
                    return new Fragment_GetLocation_Foursquare();
                case 3:
                    return new Fragment_GetLocation_Google();
                case 4:
                    if (NetworkUtils.isNetworkAvailable(Activity_PickupLocation.this)) {
                        Bundle bundle = new Bundle();
                        bundle.putString("pickOrDestination", target);
                        Fragment_GetLocation_Map fragMap = new Fragment_GetLocation_Map();
                        fragMap.setArguments(bundle);
                        return fragMap;
                    } else {
                        NetworkUtils.showNoConnectionDialog(Activity_PickupLocation.this);
                    }
                    return new Fragment_GetLocation_Map();
            }

            return null;
        }

        @Override
        public int getCount() {
            return TITLE.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLE[position];
        }

        private Drawable getIcon(int position) {
            return getResources().getDrawable(tab_icons[position]);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                if (EApplication.getInstance().fromCreateReservation) {
                    Intent intent = new Intent(getApplicationContext(), Activity_UserMain.class);
                    startActivity(intent);
                    this.finish();
                    overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
                } else if (EApplication.getInstance().fromFragmentIntent) {
                    Intent intent = new Intent(getApplicationContext(), Activity_UserMain.class);
                    startActivity(intent);
                    this.finish();
                    overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void setRequestAddress(String Name, String Address, String Lat, String Lang) {

        if (target.equals("HOME_ADDRESS")) {

            HashMap<String, String> userDetails = session.getUserDetails();
            String userID = userDetails.get(SessionManager.KEY_ID);

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", userID);
            params.put("home_title", Name);
            params.put("home_address", Address);
            params.put("home_lat", Lat);
            params.put("home_long", Lang);

            Log.e("Parameters", params.toString());

            String FAV_HOME_URL = Config.APP_BASE_URL + Config.UPDATE_FAV_HOME_URL;


            UpdateProfile_favouriteLocation(params, FAV_HOME_URL);
        }
        if (target.equals("WORK_ADDRESS")) {
            HashMap<String, String> userDetails = session.getUserDetails();
            String userID = userDetails.get(SessionManager.KEY_ID);

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", userID);
            params.put("work_title", Name);
            params.put("work_address", Address);
            params.put("work_lat", Lat);
            params.put("work_long", Lang);
            Log.e("Parameters", params.toString());

            String FAV_WORK_URL = Config.APP_BASE_URL + Config.UPDATE_FAV_WORK_URL;

            UpdateProfile_favouriteLocation(params, FAV_WORK_URL);

        }
        if (target.equalsIgnoreCase("sourceLocation") && session.isLoggedIn()) {
            session.createSourceLocation(Name, Address, Lat, Lang);

            Intent intent = new Intent(Activity_PickupLocation.this, Activity_CreateReservation.class);
            startActivity(intent);

            finish();

        }


    }

    private void UpdateProfile_favouriteLocation(final HashMap<String, String> params, String REQ_URL) {

        if (NetworkUtils.isNetworkAvailable(Activity_PickupLocation.this)) {


            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_PickupLocation.this);
            CustomProgressBarDialog.progressDialog.show();
            CustomRequest req = new CustomRequest(Request.Method.POST, REQ_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responseObject) {
                            Log.e("Response 1st API", responseObject.toString());

                            try {
                                boolean error = responseObject.getBoolean("error");

                                if (!error) {

                                    Toast.makeText(Activity_PickupLocation.this, getResources().getString(R.string.success), Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(Activity_PickupLocation.this, Update_USerProfile.class);
                                    startActivity(intent);
                                    Activity_PickupLocation.this.finish();

                                } else
                                    Toast.makeText(Activity_PickupLocation.this, getResources().getString(R.string.failed_to_update), Toast.LENGTH_LONG).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            CustomProgressBarDialog.progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error", "Error: " + error.getMessage());

                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                @Override

                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };

            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(req, "low");

        } else {
            Toast.makeText(Activity_PickupLocation.this, getResources().getString(R.string.no_internet_availeble), Toast.LENGTH_LONG).show();
            NetworkUtils.showNoConnectionDialog(Activity_PickupLocation.this);
        }

    }
}
