package com.EddyCabLatest.application.moduleUser.modal;

/**
 * Created by User on 7/10/2015.
 */
public class WalletModelItem {

    String date;
    int buy_banana;
    int free_banana;
    int used_banana;


    public WalletModelItem(String date, int buy_banana, int free_banana, int used_banana) {
        this.date = date;
        this.buy_banana = buy_banana;
        this.free_banana = free_banana;
        this.used_banana = used_banana;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getBuy_banana() {
        return buy_banana;
    }

    public void setBuy_banana(int buy_banana) {
        this.buy_banana = buy_banana;
    }

    public int getFree_banana() {
        return free_banana;
    }

    public void setFree_banana(int free_banana) {
        this.free_banana = free_banana;
    }

    public int getUsed_banana() {
        return used_banana;
    }


    public void setUsed_banana(int used_banana) {
        this.used_banana = used_banana;
    }
}
