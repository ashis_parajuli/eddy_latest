package com.EddyCabLatest.application.moduleUser.activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleDriver.fragment.Fragment_UserHelp;
import com.EddyCabLatest.application.moduleUser.adapter.UserNavDrawerAdapter;
import com.EddyCabLatest.application.moduleUser.fragment.FragmentUserRateShareFragment;
import com.EddyCabLatest.application.moduleUser.fragment.FragmentUser_NewBooking;
import com.EddyCabLatest.application.moduleUser.fragment.Fragment_UserAbout;
import com.EddyCabLatest.application.moduleUser.fragment.UserDrawerLogoutHelp;
import com.EddyCabLatest.application.moduleUser.fragment.User_FragmentJobHistory;
import com.EddyCabLatest.application.moduleUser.modal.NavDrawerItem;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.utilities.gpsTrackerUser;
import com.EddyCabLatest.application.views.CircularImageView;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Activity_UserMain extends AppCompatActivity implements View.OnClickListener{

    private static final int REQUEST_CHECK_SETTINGS = 0x01;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    CharSequence mDrawerTitle;
    FrameLayout content_frame;
    Toolbar app_toolBar;
    public static TextView appTitle, userBananaCredit;
    RelativeLayout actionUserBananaCredit;

    RelativeLayout rl_clear;
    Button btn_tour_logout;
    Button btn_help_logout;

    LinearLayout drawer_layout_navigation;
    LinearLayout layout_Logout_State;
    ListView mDrawerList_Login_State;

    Button btn_about, btn_help, create_account;
    TextView login_user;
    EApplication app;
    SessionManager session;
    boolean loadFragStatus;
    boolean loadCancelHistry;
    Context context;


    ArrayList<NavDrawerItem> mNavItems = new ArrayList<>();
    private Bundle saved;
    private String language;
    private LocationRequest mLocationRrequest;
    private GoogleApiClient mGoogleApiClient;
    private Location locationOld;
    private Location mLastLocation;
    private Location location;
    private PowerManager.WakeLock mWakeLock;

    @Override
    protected void onDestroy() {

        this.mWakeLock.release();
        super.onDestroy();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = Activity_UserMain.this;
        app = EApplication.getInstance();



        saved = savedInstanceState;
        session = new SessionManager(getApplicationContext());
        if (session.getAppLanguage().equals("Thai")) app.setLang("np");
        else app.setLang("en");

        setContentView(R.layout.activity_user_main);
              /* ToolBar SetUp */
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        appTitle = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
 /*   userBananaCredit = (TextView) app_toolBar.findViewById(R.id.user_banana_credit);*/
   /* actionUserBananaCredit = (RelativeLayout) app_toolBar.findViewById(R.id.userBananaCreditLayout);*/
        rl_clear = (RelativeLayout) app_toolBar.findViewById(R.id.rl_clear);
Intent intent= new Intent(Activity_UserMain.this, gpsTrackerUser.class);
        startService(intent);

        /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);


        final String TITLES[] = new String[]{
                getResources().getString(R.string.new_booking),
                getResources().getString(R.string.history),
                getResources().getString(R.string.profile),
                getResources().getString(R.string.rate_n_share)};

        int ICONS[] = {R.mipmap.icon_new_booking,
                R.mipmap.icon_history,
                R.mipmap.icon_profile,
                R.mipmap.icon_rate_share};
        /* Adding Drawer Item to Modal */
        for (int i = 0; i < TITLES.length; i++)
            mNavItems.add(new NavDrawerItem(TITLES[i], ICONS[i]));
        /* References Init */
        mDrawerTitle = getTitle().toString();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        content_frame = (FrameLayout) findViewById(R.id.content_frame);

        drawer_layout_navigation = (LinearLayout) findViewById(R.id.drawer_layout_navigation);
        layout_Logout_State = (LinearLayout) findViewById(R.id.layout_Logout_State);
        mDrawerList_Login_State = (ListView) findViewById(R.id.Login_State);

        /* Update Nav State LoggedIn/ LoggedOut */
        if (session.isLoggedIn()) {
            layout_Logout_State.setVisibility(View.GONE);
            mDrawerList_Login_State.setVisibility(View.VISIBLE);
        } else {
            layout_Logout_State.setVisibility(View.VISIBLE);
            mDrawerList_Login_State.setVisibility(View.GONE);
        }

        btn_tour_logout = (Button) findViewById(R.id.btn_tour_logout);
        btn_tour_logout.setOnClickListener(this);


        btn_help_logout = (Button) findViewById(R.id.btn_help_logout);
        btn_help_logout.setOnClickListener(this);
        //code to add header and footer to list view
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.item_drawer_user_header, mDrawerList_Login_State, false);
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.item_drawer_user_footer, mDrawerList_Login_State, false);
        mDrawerList_Login_State.addHeaderView(header, null, false);
        mDrawerList_Login_State.addFooterView(footer, null, false);


        UserNavDrawerAdapter adapter = new UserNavDrawerAdapter(getApplicationContext(), mNavItems);
        mDrawerList_Login_State.setAdapter(adapter);

        // Drawer Item click listeners
        mDrawerList_Login_State.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 3) {
                    mDrawerLayout.closeDrawers();
                    Intent profileIntent = new Intent(getApplicationContext(), Activity_UserProfile.class);
                    startActivity(profileIntent);
                    finish();
                }

                if (position == 5) {
              /*  actionUserBananaCredit.setVisibility(View.GONE);*/
                    rl_clear.setVisibility(View.VISIBLE);
                } else {
               /* actionUserBananaCredit.setVisibility(View.VISIBLE);*/
                    rl_clear.setVisibility(View.GONE);

                }

                LoadFragmentView(position);
                // getSupportActionBar().setTitle(TITLES[position - 1]);

                appTitle.setText(TITLES[position - 1]);
                mDrawerLayout.closeDrawers();


                mDrawerLayout.closeDrawers();

            }
        });

        if ((session.getAppLanguage()) != "en") {
            appTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);


        }

        /* Click Event for login and create account */
        btn_about = (Button) findViewById(R.id.btn_about);
        btn_about.setOnClickListener(this);

        btn_help = (Button) findViewById(R.id.btn_help);
        btn_help.setOnClickListener(this);

        create_account = (Button) findViewById(R.id.create_account);
        create_account.setOnClickListener(this);

        login_user = (TextView) findViewById(R.id.login_user);
        if ((session.getAppLanguage()) != "en") {
            btn_about.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            btn_help.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            create_account.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            login_user.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);

        }

        login_user.setOnClickListener(this);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, app_toolBar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();

            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();


        if (session.isLoggedIn()) {
            HashMap<String, String> userDetails = session.getUserDetails();
            String userProfilePicture = userDetails.get(SessionManager.KEY_PICTURE);
     /*   actionUserBananaCredit.setVisibility(View.VISIBLE);*/
            layout_Logout_State.setVisibility(View.GONE);
            mDrawerList_Login_State.setVisibility(View.VISIBLE);
            String ImageURL = Config.APP_BASE_URL + userProfilePicture;


            CircularImageView userprofileImage = (CircularImageView) findViewById(R.id.nav_user_profilepic);

            Picasso.with(this)
                    .load(ImageURL)
                    .placeholder(R.drawable.contact_avatar)   // optional
                    .error(R.drawable.contact_avatar)         // optional
                    .rotate(0)                             // optional
                    .into(userprofileImage);


        } else {

        }
        rl_clear.setOnClickListener(this);

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();
    }


    private void LoadFragmentView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 1:

                String RESERVATION_STATE = session.getReservationByUser();
                if (RESERVATION_STATE == DBTableFields.RESERVATION_STATE_PENDING) {

                    Intent intentPending_waiting = new Intent(this, Activity_UserSelectDriver.class);
                    Log.e("state", "pending");

                    startActivity(intentPending_waiting);


                }

                else if (RESERVATION_STATE == DBTableFields.RESERVATION_STATE_DRIVER_CONFIRMED) {
                    Intent intent_locate = new Intent(this, Activity_UserLocateDriver.class);
                    startActivity(intent_locate);

                }
                else
                {

                    fragment= new FragmentUser_NewBooking();

                }


                break;
            case 2:
                fragment = new User_FragmentJobHistory();
                break;
            case 3:
                break;
            case 4:
                fragment = new FragmentUserRateShareFragment();
                    /*showShareRateDialog();*/
               /*     fragment = new User_FragmentWallet();*/

                break;
            case 5:
                fragment = new Fragment_UserAbout();
                   /* fragment = new User_FragmentMessages();*/
                break;
            case 6:
                fragment = new Fragment_UserHelp();
                    /*showShareRateDialog();*/
                break;
            case 7:


                break;

            case 10:
                fragment = new UserDrawerLogoutHelp();
                break;
            default:

                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            mDrawerList_Login_State.setItemChecked(position, true);
            mDrawerList_Login_State.setSelection(position);
        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    /* *
    *
    * CHECK THE USER RESERVATION STATE
    * AUTOMATIC REDIRECT TO THE RESERVATION STATE
    * SET AND GET VALUE USING SHARED PREFERENCES FOR QUICK CHECK
    *
    * */


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
            return;
        }
        loadFragStatus = EApplication.getInstance().callBookingStatus;
        if (loadFragStatus) {
            EApplication.getInstance().setBookingStatus(false);
        }
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_about:
                mDrawerLayout.closeDrawers();
                appTitle.setText(getResources().getText(R.string.aboutUs));
                LoadFragmentView(5);

                break;

            case R.id.btn_help:
                if (session.isLoggedIn()) {
                    mDrawerLayout.closeDrawers();
                    appTitle.setText(getResources().getText(R.string.help_n_Support));
                    LoadFragmentView(6);
                }

                break;

            case R.id.create_account:
                mDrawerLayout.closeDrawers();
                Intent user_register = new Intent(getApplicationContext(), Activity_UserRegister.class);
                startActivity(user_register);
                break;

            case R.id.login_user:
           /* actionUserBananaCredit.setVisibility(View.VISIBLE);*/
                mDrawerLayout.closeDrawers();
                Intent user_login = new Intent(getApplicationContext(), Activity_UserLogin.class);
                startActivity(user_login);
                break;

       /* case R.id.userBananaCreditLayout:*/
               /* Intent intentCredit = new Intent(getApplicationContext(), BuyBananaCredit.class);
                startActivity(intentCredit);
                break;*/
            case R.id.btn_help_logout:

                mDrawerLayout.closeDrawers();
                appTitle.setText(getResources().getText(R.string.help_n_Support));
                LoadFragmentView(10);
                break;
            case R.id.btn_tour_logout:
                Intent intentTourLogout = new Intent(getApplicationContext(), Activity_UserTourSlider.class);
                startActivity(intentTourLogout);
                break;

            case R.id.rl_clear:
                clearmessagesPostApi();
                break;

        }

    }


    private void clearmessagesPostApi() {
        if (NetworkUtils.isNetworkAvailable(getApplicationContext())) {
            HashMap<String, String> userDetails = session.getUserDetails();
            String user_id = userDetails.get(SessionManager.KEY_ID);
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserMain.this);
            CustomProgressBarDialog.progressDialog.show();
            CustomProgressBarDialog.progressDialog.setCancelable(false);


            CustomRequest req = new CustomRequest(Request.Method.GET, Config.APP_BASE_URL + Config.DELETE_ALL_NOTIFICATION_MESSAGE + user_id, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                boolean error = response.getBoolean("error");
                                if (!error) {
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                    LoadFragmentView(5);
                                    Log.e("response", response.toString());
                                } else {
                                    EApplication.getInstance().showToastMessageFunction("Error true: " + error);
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomProgressBarDialog.progressDialog.dismiss();
                    Log.e("===", "Error Listiner active");
                }
            }) {


                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            req.setRetryPolicy(new DefaultRetryPolicy(
                    60000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(req, "low");

        } else {
            NetworkUtils.showNoConnectionDialog(Activity_UserMain.this);
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        if (session.isLoggedIn()) {
      /*  userBananaCredit.setText(session.getUserBananaCredit());*/
            layout_Logout_State.setVisibility(View.GONE);
            mDrawerList_Login_State.setVisibility(View.VISIBLE);
        } else {
            layout_Logout_State.setVisibility(View.VISIBLE);
            mDrawerList_Login_State.setVisibility(View.GONE);
        }

        LoadFragmentView(1);
        appTitle.setText(getResources().getString(R.string.new_booking));



    }

    private void MethodNavigateToPlaystore() {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public void ShareApplicationUsing(String APP_NAME, String PACKAGE_NAME, String MESSAGE) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, MESSAGE);
        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(sharingIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith(PACKAGE_NAME)) {
                sharingIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved) {
            startActivity(sharingIntent);
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(Activity_UserMain.this);
            alert.setTitle("Warning");
            alert.setMessage(APP_NAME + " App not found");
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });
            alert.show();
        }
    }

 /*   private Location startLocationUpdates() {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRrequest, this);
        locationOld = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if ((locationOld != null)) {

            mLastLocation = locationOld;

        } else {
            mLastLocation = startLocationUpdates();
            location = mLastLocation;
        }
        return mLastLocation;

    }*/


    public void setApplicationTitle(String title) {
        appTitle.setText(title);
    }



}

