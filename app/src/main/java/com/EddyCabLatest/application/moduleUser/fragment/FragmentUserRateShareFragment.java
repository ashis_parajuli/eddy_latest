package com.EddyCabLatest.application.moduleUser.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.views.CircularImageView;


public class FragmentUserRateShareFragment extends Fragment {

    private CircularImageView imageView;
    private Button shareFacebook;
    private Button shareTwitter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sharefragmentuser, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        shareFacebook = (Button) view.findViewById(R.id.shareFacebook);
        shareTwitter = (Button) view.findViewById(R.id.shareTwitter);
        shareFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*[4/8/16, 2:27:37 PM] Brijesh Gupta: https://twitter.com/eddycabnepal
[4/8/16, 2:28:58 PM] Brijesh Gupta: https://www.facebook.com/Eddycab-551105325059598/*/
                String facebookUrl = "https://www.facebook.com/Eddycab-551105325059598/";
                linkUrl(facebookUrl);
            }
        });
        shareTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String twitterUrl = "https://twitter.com/eddycabnepal";
                linkUrl(twitterUrl);
            }
        });

    }

    private void linkUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        startActivity(intent);

    }
}
