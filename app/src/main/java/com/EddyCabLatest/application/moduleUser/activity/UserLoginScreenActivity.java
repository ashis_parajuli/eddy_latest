package com.EddyCabLatest.application.moduleUser.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.SelectUserType;
import com.EddyCabLatest.application.moduleDriver.activity.DriverHelpActivity;
import com.EddyCabLatest.application.utilities.SessionManager;


public class UserLoginScreenActivity extends AppCompatActivity {

    Toolbar app_toolBar;
    TextView login;
    Button tour, help, createAccount;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_drawer_login);

        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        login = (TextView) findViewById(R.id.login_user);
        tour = (Button) findViewById(R.id.btn_tour_logout);
        help = (Button) findViewById(R.id.btn_help_logout);
        createAccount = (Button) findViewById(R.id.create_account);
        session = new SessionManager(getApplicationContext());

        if ((session.getAppLanguage()) != "en") {
            login.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            tour.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            help.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            createAccount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentLogin = new Intent(UserLoginScreenActivity.this, Activity_UserLogin.class);
                startActivity(intentLogin);

            }
        });
        tour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentTourLogout = new Intent(UserLoginScreenActivity.this, Activity_UserTourSlider.class);
                startActivity(intentTourLogout);
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentHelpLogout = new Intent(UserLoginScreenActivity.this, DriverHelpActivity.class);
                startActivity(intentHelpLogout);
            }
        });

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserLoginScreenActivity.this, Activity_UserRegister.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Intent intent = new Intent(UserLoginScreenActivity.this, SelectUserType.class);
                startActivity(intent);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
