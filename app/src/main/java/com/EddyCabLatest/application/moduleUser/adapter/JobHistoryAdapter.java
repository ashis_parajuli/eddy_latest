package com.EddyCabLatest.application.moduleUser.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class JobHistoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<HashMap<String, String>> dListItems;
    private Calendar c;
    private Date dateObj;


    public JobHistoryAdapter(Context context, ArrayList<HashMap<String, String>> dListItems) {
        this.context = context;
        this.dListItems = dListItems;
    }


    @Override
    public int getCount() {
        return dListItems.size();
    }

    @Override
    public Object getItem(int i) {
        return dListItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            LayoutInflater dInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = dInflater.inflate(R.layout.previous_job_single_item, null);
        }

        TextView tripSourceAddress = (TextView) convertView.findViewById(R.id.tripFromLocation);
        TextView tripDestinationAddress = (TextView) convertView.findViewById(R.id.tripToLocation);
        TextView month = (TextView) convertView.findViewById(R.id.month);
        TextView time = (TextView) convertView.findViewById(R.id.time_joblist);
        TextView tripStatus = (TextView) convertView.findViewById(R.id.tripStatus);
      /*  TextView tripAmount = (TextView) convertView.findViewById(R.id.tripAmount);*/


        tripSourceAddress.setText(dListItems.get(position).get(DBTableFields.USER_TRIP_FROM_LOCATION));
        tripDestinationAddress.setText(dListItems.get(position).get(DBTableFields.USER_TRIP_TO_LOCATION));
        String dateTime = dListItems.get(position).get(DBTableFields.USER_TRIP_DATE_TIME);

        String time_value = formateDateFromstring("yyyy-MM-dd HH:mm", "H:mm a", dateTime);
        String date_value = formateDateFromstring("yyyy-MM-dd HH:mm", "dd MMM", dateTime);


        time.setText(time_value);


        month.setText(date_value);
        /* month.setText(dListItems.get(position).get(DBTableFields.USER_TRIP_MONTH)+""+dListItems.get(position).get(DBTableFields.USER_TRIP_MONTH_NUM));*/
        String amount = dListItems.get(position).get(DBTableFields.USER_TRIP_AMOUNT).trim();
     /*   tripAmount.setText(TextUtils.equals(amount, "null") ? "N/A" : dListItems.get(position).get(DBTableFields.USER_TRIP_AMOUNT));
*/
        String status = dListItems.get(position).get(DBTableFields.USER_TRIP_STATUS);
        if (status.equals("cancelled")) {
            tripStatus.setText(context.getResources().getString(R.string.cancelled));
            tripStatus.setBackgroundColor(Color.RED);
        }
        if (status.equals("active")) {
            tripStatus.setText(context.getResources().getString(R.string.active));
            tripStatus.setBackgroundColor(Color.YELLOW);

        }
        if (status.equals("completed")) {
            tripStatus.setText(context.getResources().getString(R.string.completed));
            tripStatus.setBackgroundColor(Color.parseColor("#528D49"));

        }
        if (status.equals("new")) {
            tripStatus.setText(context.getResources().getString(R.string.new_stat));
            tripStatus.setBackgroundColor(Color.parseColor("#FFFFE100"));

        }

        return convertView;
    }

    public String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
            Log.e("parse", " dateFormat" + outputDate);
        } catch (ParseException e) {

        }

        return outputDate;

    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }
}
