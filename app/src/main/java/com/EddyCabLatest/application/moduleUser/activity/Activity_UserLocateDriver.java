package com.EddyCabLatest.application.moduleUser.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.extended.AutofitTextView;
import com.EddyCabLatest.application.moduleUser.modal.LocateModel;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CircularImageView;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.directions.route.Routing;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_UserLocateDriver extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener {

    private static final int REQUEST_CHECK_SETTINGS = 0x01;
    private static String TAG = "===";
    EApplication app;
    SessionManager session;
    public boolean dialogishown = false;
    Toolbar app_toolBar;
    public String driver_id_value;

    public String driver_mobile;
    TextView txt_eta_time;
    public String distance;
    TextView txt_fee_bht;
    AutofitTextView txt_driver_name;
    TextView txt_driver_no;
    TextView txt_reg_no;
    ImageView img_cross;
    CircularImageView img_circ_driver;


    //variables added to draw google map and Route
    private MapView mapView;
    private GoogleMap map;
    private LatLng startPoint;
    private LatLng pickUp;
    LatLng driverNewLocation;


    private Polyline polyline;

    private double pickUpLatitude = 0.0;

    private double pickUpLongitude = 0.0;
    String language;


    String DRIVER_ID;
    String DriverNumber;

    GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRrequest;
    private static final int UPDATE_INTERVAL = 0;
    private static final int FASTEST_INTERVAL = 0;
    private static final int DSIPLACEMENT_UPDATES = 20;
    Marker marker;

    private boolean visible = false;

    String reservationType;
    private Location locationOld;
    private Location mLastLocation;
    private boolean isShown = false;
    private CoordinatorLayout coordinatorLayout;
    private boolean isPicked = false;
    public ImageView call_driver;
    public com.github.clans.fab.FloatingActionButton time_to_reach_user;

    private String User_ID;
    private String reservation_id;

    private ValueEventListener user_events;
    private Routing routing;
    private LatLng driverNewLocation_value;
    private boolean isUserNear500 = false;

    private Button call_costumer;
    private boolean has_arrived = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user_track_driver);


        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);


        initViews();


        session = new SessionManager(Activity_UserLocateDriver.this);

        HashMap<String, String> userDetails = session.getUserDetails();

        // Initializing Application Instances
        app = EApplication.getInstance();

        reservationType = session.getReservationType();
        reservationType = session.getReservationType();
        if (reservationType.equals("none")) {
            reservationType = "bike";
        }
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        createLocationRequest();
        mGoogleApiClient.connect();
        // Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);


        if (session.isSourceLocationSet()) {
            HashMap<String, String> destinationLocation = session.getSourceLocation();
            pickUpLatitude = Double.parseDouble(destinationLocation.get("Source_Location_geoPointLat"));
            pickUpLongitude = Double.parseDouble(destinationLocation.get("Source_Location_geoPointLang"));
            pickUp = new LatLng(pickUpLatitude, pickUpLongitude);
            Log.e(TAG, "pickUp LatLang is :" + String.valueOf(pickUp));
            Log.e("===", "pickUp LatLang is :" + String.valueOf(pickUpLatitude) + "   " + String.valueOf(pickUpLongitude));
        }


        HashMap<String, String> reservationWithDriver = session.getReservationIdWithDriver();
        DRIVER_ID = reservationWithDriver.get(SessionManager.DRIVER_ID);
        // set data to bottom of layout
        /*EApplication.getInstance().showToastMessageFunction("Inside onCreate 1");*/
        initializaMap(savedInstanceState);
        /*GetDriverDetailForViewsToShow();*/


    }

    private void createLocationRequest() {
        mLocationRrequest = new LocationRequest();
        mLocationRrequest.setInterval(UPDATE_INTERVAL);
        mLocationRrequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRrequest.setSmallestDisplacement(DSIPLACEMENT_UPDATES);
    }


    private void initViews() {
        //initializing Views from layout
        txt_eta_time = (TextView) findViewById(R.id.txt_eta_time);
      /*  txt_fee_bht = (TextView) findViewById(R.id.txt_fee_bht);*/
        txt_driver_name = (AutofitTextView) findViewById(R.id.txt_driver_name);
        txt_driver_no = (TextView) findViewById(R.id.txt_driver_no);
        txt_reg_no = (TextView) findViewById(R.id.txt_reg_no);
        img_cross = (ImageView) findViewById(R.id.img_cross_red);
        call_driver = (ImageView) findViewById(R.id.call_driver);
        call_costumer = (Button) findViewById(R.id.call_customer);
        time_to_reach_user = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.time_to_reach_user);

        time_to_reach_user.setOnClickListener(this);

        call_costumer.setOnClickListener(this);
        img_cross.setVisibility(View.INVISIBLE);
        time_to_reach_user.setVisibility(View.VISIBLE);

        img_circ_driver = (CircularImageView) findViewById(R.id.img_circ_driver);
        img_cross.setOnClickListener(this);
        img_cross.setEnabled(true);
        img_cross.setVisibility(View.INVISIBLE);
        call_driver.setOnClickListener(this);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);


        img_cross.setVisibility(View.VISIBLE);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            String driver_name = extras.getString("name");
            String driver_profile = extras.getString("profile");

            String driver_redg = extras.getString("redg");
            driver_mobile = extras.getString("mobile");
            String driver_id = extras.getString("id");

            txt_driver_no.setText(driver_id);
            txt_reg_no.setText(driver_redg);
            txt_driver_name.setText(driver_name);
            Picasso.with(Activity_UserLocateDriver.this)
                    .load(Config.APP_BASE_URL + driver_profile)
                    .placeholder(R.drawable.user_avtar)
                    .error(R.drawable.user_avtar)
                    .into(img_circ_driver);
        } else {
            LocateModel locateModel = new LocateModel();

            String driver_name = locateModel.getDriver_name();

            if (driver_name != null) {
                String driver_profile = locateModel.getDriver_profile();

                String driver_redg = locateModel.getDriver_redg();
                driver_mobile = locateModel.getDriver_mobile();
                String driver_id = locateModel.getDriver_id();
                txt_driver_no.setText(driver_id);
                txt_reg_no.setText(driver_redg);
                txt_driver_name.setText(driver_name);

                Picasso.with(Activity_UserLocateDriver.this)
                        .load(Config.APP_BASE_URL + driver_profile)
                        .placeholder(R.drawable.user_avtar)
                        .error(R.drawable.user_avtar)
                        .into(img_circ_driver);
            } else {

                GetDriverDetailForViewsToShow();
            }
        }
    }

    @Override
    public void onResume() {
/*        EApplication.getInstance().showNotification(this, Activity_UserLocateDriver.class, 001, "Goto Track your user", getResources().getString(R.string.Click_here), getResources().getString(R.string.Locate_driver));*/
        visible = true;
        mapView.onResume();
        super.onResume();
        updatedriverLocation_pick_complete();
//        EApplication.getInstance().showToastMessageFunction("onResume called");
    }


    @Override
    public void onDestroy() {
        if (user_events != null) {
            EApplication.ref.child(User_ID + "/reservations/" + reservation_id).removeEventListener(user_events);
        }
        if (mGoogleApiClient.isConnected()) {

            mGoogleApiClient.disconnect();
        }

        visible = false;


        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        visible = false;
        mapView.onPause();


        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;

        if (user_events != null) {
            EApplication.ref.child(User_ID + "/reservations/" + reservation_id).removeEventListener(user_events);
        }
        if (mGoogleApiClient.isConnected()) {

            mGoogleApiClient.disconnect();
        }

        visible = false;


        super.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    private void updatedriverLocation_pick_complete() {
        HashMap<String, String> userDetails = session.getUserDetails();
        User_ID = userDetails.get(SessionManager.KEY_ID);
        HashMap<String, String> reservationWithDriverID = session.getReservationIdWithDriver();
        final String driver_id = reservationWithDriverID.get(SessionManager.DRIVER_ID);
        reservation_id = reservationWithDriverID.get(SessionManager.RESERVATION_ID);
        session.setLastReservationId(reservation_id);
        user_events = EApplication.ref.child(User_ID + "/reservations/" + reservation_id).addValueEventListener(new ValueEventListener() {


            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //  Log.e("datasnap",dataSnapshot.toString());
                if (dataSnapshot.getValue() != null) {
                    if (dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("cancelled")) {
                        if (!dataSnapshot.child("cancel_by").exists()) {


                            generateDriverCancelledDialog();
                            EApplication.ref.child(User_ID + "/reservations/" + reservation_id).removeEventListener(this);
                        }


                    } else {
                        if (dataSnapshot.child("status").exists()) {
                            if (dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("completed")) {
                                if (dataSnapshot.child("ride_fare").exists()) {
                                    String ride_fare = dataSnapshot.child("ride_fare").getValue().toString();

                                    EApplication.ref.child(User_ID + "/reservations/" + reservation_id).removeEventListener(this);
                                    Intent intent_rate_driver = new Intent(Activity_UserLocateDriver.this, Activity_UserRateDriver.class);
                                    intent_rate_driver.putExtra("ride_fare", ride_fare);
                                    intent_rate_driver.putExtra("reservation_id", reservation_id);
                                    startActivity(intent_rate_driver);
                                    finish();
                                }

                            } else {

                                if (dataSnapshot.child("driver_latitude").exists() && (dataSnapshot.child("driver_longitude").exists())) {
                                    double driver_latitude = Double.parseDouble(dataSnapshot.child("driver_latitude").getValue().toString());
                                    double driver_longitude = Double.parseDouble(dataSnapshot.child("driver_longitude").getValue().toString());
                                    driverNewLocation = new LatLng(driver_latitude, driver_longitude);
                                    if (!isPicked) {
                                        setDriverLocationMarker(driverNewLocation);
                                    }

                                }

                                if (dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("onroute")) {
                                    time_to_reach_user.setVisibility(View.INVISIBLE);
                                    img_cross.setVisibility(View.INVISIBLE);
                                    isPicked = true;
                                    map.setMyLocationEnabled(false);
                                    //  EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.driver_pickedyou));
                                    dialogIsNearAndPicked();
                                } else if (dataSnapshot.child("has_arrived").exists()) {
                                    if ((dataSnapshot.child("has_arrived").getValue().equals(true))) {
                                        has_arrived = true;
                                        //EApplication.ref.child(User_ID + "/reservations/" + reservation_id + "/status").setValue("has_arrived");
                                        EApplication.ref.child(User_ID + "/reservations/" + reservation_id + "/has_arrived").setValue(false);
                                        img_cross.setVisibility(View.INVISIBLE);
                                        if (!has_arrived) {
                                            dialogDriverNearUserPickupLocation();

                                        }
                                        //isUserNear500 = true;
                                        EApplication.getInstance().sethas_arrived(true);
                                    }
                                }
                            }


                        }
                    }

                }

            }


        });
    }

    private void setDriverLocationMarker(LatLng driverNewLocation) {
        driverNewLocation_value = driverNewLocation;

        map.clear();
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(driverNewLocation);

        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_driverpin));
        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.position(pickUp);

        markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));


        marker = map.addMarker(markerOptions);

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();


                builder.include(pickUp);
                builder.include(driverNewLocation_value);
                if (mLastLocation != null) {
                    builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                }
                LatLngBounds bounds = builder.build();

                int padding_in_dp = 30;  // 6 dps
                final float scale = getResources().getDisplayMetrics().density;
                int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding_in_px));


            }
        });
    }

    private void GetDriverDetailForViewsToShow() {
        if (NetworkUtils.isNetworkAvailable(Activity_UserLocateDriver.this)) {
            session = new SessionManager(Activity_UserLocateDriver.this);

            HashMap<String, String> reservationWithDriverID = session.getReservationIdWithDriver();
            final String driver_id = reservationWithDriverID.get(SessionManager.DRIVER_ID);
            final String reservation_id = reservationWithDriverID.get(SessionManager.RESERVATION_ID);

            final HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", driver_id);
            params.put("reservation_id", reservation_id);
            Log.e("params ::", "" + params);
            StringRequest reqDiverLocate = new StringRequest(Request.Method.POST, Config.APP_BASE_URL + Config.USER_GET_DRIVER_LOCATION_POST_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String responseObject) {
                            String newResponse = responseObject;
                            if (responseObject.contains("ï»¿")) {
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }
                            try {
                                JSONObject response = new JSONObject(newResponse);
                                Log.d("Response Success", response.toString());
                                boolean error = response.getBoolean("error");
                                if (!error) {
                                    // String reservation_id = response.getJSONObject("Reservation").getString("id");
                                    // String driver_id = response.getJSONObject("Reservation").getString("driver_id");
                                    String driver_name = response.getString("driver_name");
                                    String driver_license_no = response.getString("driver_license_no");
                                    String driver_profile_picture = response.getString("driver_profile_picture");
                                    //String driver_image = response.getJSONObject("driver").getString("profile_picture");
                                    txt_driver_no.setText(driver_id);
                                    txt_reg_no.setText(driver_license_no);
                                    txt_driver_name.setText(driver_name);
                                    Picasso.with(Activity_UserLocateDriver.this)
                                            .load(Config.APP_BASE_URL + driver_profile_picture)
                                            .placeholder(R.drawable.user_avtar)
                                            .error(R.drawable.user_avtar)
                                            .into(img_circ_driver);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error", "Error: " + error.getMessage());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);

                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            reqDiverLocate.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(reqDiverLocate, "high");

        } else {
            NetworkUtils.showNoConnectionDialog(Activity_UserLocateDriver.this);
        }


    }

    private void locationSettingRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRrequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates locationSettingsStates = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog` by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    (Activity) Activity_UserLocateDriver.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.

                        break;
                }
            }
        });

    }


    private void setDriverLocationMarkerAfterPickUp(final LatLng driverNewLocation) {
        //screwed up logic to solve dunno what issue
        map.clear();
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(driverNewLocation);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_driverpin));
        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.position(pickUp);
        markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));

        marker = map.addMarker(markerOptions);
        driverNewLocation_value = driverNewLocation;
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                builder.include(pickUp);
                builder.include(driverNewLocation_value);
                if ((mLastLocation != null)) {
                    builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                }
                LatLngBounds bounds = builder.build();

                int padding_in_dp = 30;  // 6 dps
                final float scale = getResources().getDisplayMetrics().density;
                int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding_in_px));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(driverNewLocation_value.latitude, driverNewLocation_value.longitude), 15));


            }
        });
    }

    private void dialogDriverNearUserPickupLocation() {
        final Dialog dialog = new Dialog(Activity_UserLocateDriver.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.eddy_arrived_notification);
        dialog.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView txt_message = (TextView) dialog.findViewById(R.id.txt_message);
        txt_message.setText(getResources().getString(R.string.reachLocation));
        ImageView btnOK = (ImageView) dialog.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        if (visible) dialog.show();
    }

    private void dialogIsNearAndPicked() {
        final Dialog dialog = new Dialog(Activity_UserLocateDriver.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.eddy_arrived_notification);
        dialog.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView txt_message = (TextView) dialog.findViewById(R.id.txt_message);
        txt_message.setText(getResources().getString(R.string.driver_pickedyou));
        ImageView btnOK = (ImageView) dialog.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        if (visible) dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_cross_red:
                methodCancelReservation();
                break;
            case R.id.call_driver:
                methodCall();
                break;
            case R.id.time_to_reach_user:

                calculate_eta();


                break;

            case R.id.call_customer:

                methodCall_customer();
                break;
            default:
                break;
        }
    }


    private void calculate_eta() {
        if ((mLastLocation != null) && (driverNewLocation != null)) {
            String URL_DISTANCE = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" + mLastLocation.getLatitude() + ","
                    + mLastLocation.getLongitude() + "&destinations=" + driverNewLocation.latitude + "," + driverNewLocation.longitude + "&mode=driving&language=en-EN&sensor=false";
            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DISTANCE,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject responseObject = new JSONObject(response);
                                JSONArray objectArray = responseObject.getJSONArray("rows");
                                JSONArray arrayElements = objectArray.getJSONObject(0).getJSONArray("elements");

                                distance = arrayElements.getJSONObject(0).getJSONObject("duration").getString("text");
                                Log.e("DISTANCE::", String.valueOf(distance));
                                Toast toast = Toast.makeText(Activity_UserLocateDriver.this, getResources().getString(R.string.time_to_reach) + " : " + distance, Toast.LENGTH_SHORT);
                                TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                                v.setTextColor(getResources().getColor(R.color.primary));
                                toast.show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(stringRequest, "low");
        }


    }

    //Method to cancel Reservation
    private void methodCancelReservation() {

        final Dialog dialog_cancel = new Dialog(Activity_UserLocateDriver.this);
        dialog_cancel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_cancel.setContentView(R.layout.reservation_cance_dialog);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog_cancel.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog_cancel.getWindow().setAttributes(paramLayout);
        TextView txtMessage = (TextView) dialog_cancel.findViewById(R.id.txt_message);
        txtMessage.setText(getResources().getString(R.string.wanto_cancelReservation));
        Button btnOk = (Button) dialog_cancel.findViewById(R.id.btnOK);
        Button btn_no = (Button) dialog_cancel.findViewById(R.id.btn_no);

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_cancel.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_cancel.dismiss();

                RESERVATION_CANCEL_API();
            }
        });
        if (visible) dialog_cancel.show();
    }

    private void RESERVATION_CANCEL_API() {

        if (NetworkUtils.isNetworkAvailable(Activity_UserLocateDriver.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserLocateDriver.this);
            if (visible) CustomProgressBarDialog.progressDialog.show();

            HashMap<String, String> userDetails = session.getUserDetails();
            String userID = userDetails.get(SessionManager.KEY_ID);
            HashMap<String, String> getDriverWithReservation = session.getReservationIdWithDriver();
            reservation_id = getDriverWithReservation.get(SessionManager.RESERVATION_ID);
            if (reservation_id == null) {
                session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                Intent intentCancelReservation = new Intent(Activity_UserLocateDriver.this, Activity_UserMain.class);
                EApplication.getInstance().setHistryCancelledStatus(true);
                session.clearPickUPDropOffLocationAddress();
                startActivity(intentCancelReservation);
                finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
            }
            final HashMap<String, String> params = new HashMap<>();
            params.put("user_id", userID);
            params.put("reservation_id", reservation_id);
            Log.e("Parameters", params.toString());
            String REQUESTURL = Config.APP_BASE_URL + Config.API_RESERVATION_CANCEL;
            CustomRequest req = new CustomRequest(Request.Method.POST, REQUESTURL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responseObject) {
                            CustomProgressBarDialog.progressDialog.dismiss();
                            if (user_events != null) {

                                EApplication.ref.child(User_ID + "/reservations/" + reservation_id).removeEventListener(user_events);

                            }

                            visible = false;


                            String newResponse = responseObject.toString();
                            if (newResponse.contains("ï»¿")) {
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }
                            try {
                                JSONObject response = new JSONObject(newResponse);
                                boolean error = response.getBoolean("error");

                                if (!error) {


                                    session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                                    Intent intentCancelReservation = new Intent(Activity_UserLocateDriver.this, Activity_UserMain.class);
                                    EApplication.getInstance().setHistryCancelledStatus(true);
                                    session.clearPickUPDropOffLocationAddress();
                                    startActivity(intentCancelReservation);
                                    finish();
                                    overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                                } else
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                    CustomProgressBarDialog.progressDialog.dismiss();

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(req, "high");

        } else {
            if (!isShown) {
                NetworkUtils.showNoConnectionDialog(Activity_UserLocateDriver.this);
                isShown = true;
            } else {

                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);

                snackbar.show();
            }

        }
    }


    private void generateDriverCancelledDialog() {

        final Dialog dialog_calceled = new Dialog(Activity_UserLocateDriver.this);
        dialog_calceled.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_calceled.setContentView(R.layout.custom_dialog_driver_cancelled);
        dialog_calceled.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog_calceled.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog_calceled.getWindow().setAttributes(paramLayout);
        if (visible) dialog_calceled.show();

        ImageView btnOk = (ImageView) dialog_calceled.findViewById(R.id.action_cross);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_calceled.dismiss();

                // set user state to preferences
                session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                Intent intentCancelReservation = new Intent(Activity_UserLocateDriver.this, Activity_UserMain.class);
                EApplication.getInstance().verifyingSatus = false;
                startActivity(intentCancelReservation);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                finish();
            }
        });

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("LOG", connectionResult.toString());

    }

    @Override
    public void onConnected(Bundle bundle) {

        locationSettingRequest();
        locationOld = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mLastLocation = locationOld;
        Log.e("connected_userlocate", "connec");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRrequest, this);
        startPoint = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());


    }

    @Override
    public void onConnectionSuspended(int i) {
        EApplication.getInstance().showToastMessageFunction("Connection Suspended");

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver_wait_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                EApplication.getInstance().setfromuserlocate(true);
                Intent intentCancelReservation = new Intent(Activity_UserLocateDriver.this, Activity_UserMain.class);
                EApplication.getInstance().verifyingSatus = false;

                startActivity(intentCancelReservation);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);

                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void methodCall() {

        DriverNumber = session.getDriverNumber();
        if (driver_mobile != null) {
            String number = "tel:" + "+977" + driver_mobile.trim();
            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            startActivity(callIntent);
        } else {
            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.couldnot_fetch_mobile_number));
        }


    }

    private void methodCall_customer() {

        String number = "tel:" + "+97714410178";
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
        if (ActivityCompat.checkSelfPermission(Activity_UserLocateDriver.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return;
        }
        startActivity(callIntent);


    }


    @Override
    public void onBackPressed() {


    }

    private void initializaMap(Bundle savedInstanceState) {

        MapsInitializer.initialize(Activity_UserLocateDriver.this);
        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(Activity_UserLocateDriver.this)) {
            case ConnectionResult.SUCCESS:
                mapView = (MapView) findViewById(R.id.content_map_view);
                mapView.onCreate(savedInstanceState);
                if (mapView != null) {
                    map = mapView.getMap();
                    map.getUiSettings().setMyLocationButtonEnabled(true);
                    map.getUiSettings().setRotateGesturesEnabled(false);
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling

                        return;
                    }
                    map.setMyLocationEnabled(true);

                    map.setPadding(converintodp(10), converintodp(10), converintodp(10), converintodp(10));
                }


                break;
            case ConnectionResult.SERVICE_MISSING:
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;
            default:

        }
    }

    private int converintodp(int dp) {
        int padding_in_dp = dp;  // 6 dps
        final float scale = getResources().getDisplayMetrics().density;
        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
        return padding_in_px;
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location.hasAccuracy() && location.getAccuracy() < 50) {
            mLastLocation = location;
            Toast.makeText(Activity_UserLocateDriver.this, "location" + location.toString(), Toast.LENGTH_SHORT).show();

            if (isPicked) {
                setDriverLocationMarkerAfterPickUp(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));

            }


        }


    }
}

