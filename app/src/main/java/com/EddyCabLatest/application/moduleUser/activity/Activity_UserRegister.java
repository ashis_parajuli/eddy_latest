package com.EddyCabLatest.application.moduleUser.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.gcmNotification.GCMClientManager;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by newimac on 6/15/15.
 */
public class Activity_UserRegister extends AppCompatActivity implements View.OnClickListener {

    EApplication app;

    Toolbar app_toolBar;
    private static final String str_text_terms = "<a href=http://www.eddycab.com/terms-and-conditions/ >Terms and condition</a>";
    private static final String str_text_privacy = "<a href=http://www.eddycab.com/privacy-policy/ >Privacy policy</a>";

    CheckBox user_register_checkbox;
    public EditText user_register_name, user_register_email, user_register_pass_code, user_register_country_code, user_register_mobile_number, user_register_referral_code;

    private GCMClientManager pushClientManager;
    String PROJECT_NUMBER = Config.GCM_SENDER_KEY;
    public String GCM_REG_DEVICE_ID;
    String phoneNo;
    String TAG = "===";
    String userId = "";
    SessionManager session;
    Button user_register_sign_up;
    private TextView user_register_terms_condition;
    private TextView privacy_policy;
    private TextView apptitle_toolbar_driver_help;
    private EditText user_register_country_code_confirm;
    private EditText user_register_pass_code_confirm;
    private String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);

         /*ToolBar SetUp*/
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setVisibility(View.VISIBLE);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        apptitle_toolbar_driver_help = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
        apptitle_toolbar_driver_help.setText(getResources().getString(R.string.register_user));
        /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        /* Application Singleton Class Controller Init */
        app = EApplication.getInstance();
        /* References Init */
        user_register_name = (EditText) findViewById(R.id.user_register_name);

        user_register_email = (EditText) findViewById(R.id.user_register_email);

        user_register_pass_code = (EditText) findViewById(R.id.user_register_pass_code);

        user_register_country_code = (EditText) findViewById(R.id.user_register_country_code);
        user_register_pass_code_confirm = (EditText) findViewById(R.id.user_register_pass_code_confirm);

        session = new SessionManager(Activity_UserRegister.this);
        user_register_mobile_number = (EditText) findViewById(R.id.user_register_mobile_number);

        user_register_terms_condition = (TextView) findViewById(R.id.terms_condition);
        privacy_policy = (TextView) findViewById(R.id.privacy_policy);
        user_register_terms_condition.setOnClickListener(this);
      /*  privacy_policy.setOnClickListener(this);*/

        user_register_sign_up = (Button) findViewById(R.id.user_register_sign_up);
        user_register_sign_up.setOnClickListener(this);
        user_register_terms_condition.setMovementMethod(LinkMovementMethod.getInstance());
        privacy_policy.setMovementMethod(LinkMovementMethod.getInstance());

        user_register_terms_condition.setLinkTextColor(Color.BLUE);
      

        privacy_policy.setLinkTextColor(Color.BLUE);


        if ((session.getAppLanguage()).equalsIgnoreCase(Config.LANG_ENG)) {

            user_register_terms_condition.setText(Html.fromHtml(Config.TERMS_DRIVER_ENGLISH + ">Terms and condition</a>"));
            privacy_policy.setText(Html.fromHtml(Config.PRIVACY_POLICY_PASSENGER_ENGLISH + ">Privacy Policy</a>"));
        } else {
            user_register_terms_condition.setText(Html.fromHtml(Config.TERMS_DRIVER_NEPALI + ">सेवाशर्त र यसका प्रावधानहरु</a>"));


            privacy_policy.setText(Html.fromHtml(Config.PRIVACY_POLICY_PASSENGER_NEPALI + ">गोपनीयता नीति</a>"));

        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(Activity_UserRegister.this, Activity_UserMain.class);
                startActivity(intent);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                this.finish();
                //return true;
                //this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.user_register_sign_up:
                // user_register_sign_up.setEnabled(true);
                methodRegisterUser();
                break;
        }
    }

    /* Register User Methods */
    private void methodRegisterUser() {

        /* Get GCM Device Registration ID */
        pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {

                GCM_REG_DEVICE_ID = registrationId;
                Log.e("===", "device_id is : " + GCM_REG_DEVICE_ID);


                String email_value = user_register_email.getText().toString().trim();
                String name_value = user_register_name.getText().toString().trim();
                String passcode = user_register_pass_code.getText().toString().trim();
                String mobilenumber = user_register_mobile_number.getText().toString().trim();
                String confirm = user_register_pass_code_confirm.getText().toString().trim();
                String countrycode = user_register_country_code.getText().toString().trim();
                boolean isfilled = user_register_name.getText().toString().trim().equals("") && user_register_pass_code.getText().toString().trim().equals("") && user_register_mobile_number.getText().toString().trim().equals("") && user_register_pass_code_confirm.getText().toString().trim().equals("");


                if (!isfilled) {


                    if (!user_register_pass_code_confirm.getText().toString().trim().equals(user_register_pass_code.getText().toString().trim())) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.password_donot_match));
                        user_register_pass_code.setError(getResources().getString(R.string.password_donot_match));
                        user_register_pass_code_confirm.setError(getResources().getString(R.string.password_donot_match));
                    } else {


                        if (user_register_mobile_number.getText().toString().trim().length() < 10) {
                            user_register_mobile_number.setError(getResources().getString(R.string.enter_the_10_digit_mobile));


                        } else {
                            String promo_offer = "";
                            final HashMap<String, String> params = new HashMap<>();
                            params.put("fullname", name_value);
                            params.put("email", email_value);
                            params.put("password", passcode);
                            params.put("password_confirm", confirm);
                            phoneNo = user_register_mobile_number.getText().toString().trim();
                            params.put("mobile_number", mobilenumber);
                            params.put("country_code", countrycode);
                            params.put("type", Config.USER_TYPE_USER_ID);
                            params.put("email_promo_offer", promo_offer);
                            params.put("device_id", GCM_REG_DEVICE_ID);
                            params.put("device_type", Config.DEVICE_TYPE);
                            Log.e("Parameter", params.toString());
                            if (NetworkUtils.isNetworkAvailable(Activity_UserRegister.this)) {

                                if (!TextUtils.isEmpty(GCM_REG_DEVICE_ID)) {
                                    CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserRegister.this);

                                    CustomProgressBarDialog.progressDialog.setCancelable(false);
                                    CustomProgressBarDialog.progressDialog.show();

                                    JsonObjectRequest req = new JsonObjectRequest(Config.APP_BASE_URL + Config.USER_REGISTER_URL, new JSONObject(params),
                                            new Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    Log.e("Response:", "User Register Response>>" + response.toString());
                                                    try {
                                                        boolean error = response.getBoolean("error");
                                                        if (!error) {
                                                            String message = response.getString("message");

                                                            //EApplication.getInstance().showToastMessageFunction(message);
                                                            String userPhone = response.getJSONObject("user").getString("mobile_number");
                                                            userId = response.getJSONObject("user").getString("id");
                                                            CustomProgressBarDialog.progressDialog.dismiss();
                                                            //Dismiss the activity and resume Main Activity
                                                            Intent intent = new Intent(Activity_UserRegister.this, Activity_SmsVerification.class);
                                                            intent.putExtra("phoneNo", userPhone);
                                                            intent.putExtra("user_id", userId);
                                                            intent.putExtra("user_detail", response.toString());
                                                            startActivity(intent);
                                                        } else
                                                            EApplication.getInstance().showToastMessageFunction(response.getString("message"));

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    CustomProgressBarDialog.progressDialog.dismiss();
                                                }
                                            }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e("Error: ", error + "");
                                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.register_failed));
                                            CustomProgressBarDialog.progressDialog.dismiss();
                                        }
                                    })  {
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {
                                            return params;
                                        }

                                        @Override
                                        public Request.Priority getPriority() {
                                            return Request.Priority.HIGH;
                                        }

                                        @Override
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                            String current_language = session.getAppLanguage();
                                            Log.e("current_language", current_language);
                                            if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                                language = "en";
                                            }
                                            if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                                language = "np";
                                            }
                                            headers.put("lang", language);
                                            return headers;
                                        }
                                    };
                                    req.setRetryPolicy(new DefaultRetryPolicy(
                                            20000,
                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                    app.addToRequestQueue(req, "high");

                                } else {
                                    Toast.makeText(Activity_UserRegister.this, getResources().getString(R.string.devicenot_registered), Toast.LENGTH_SHORT);
                                }

                            } else {

                                Toast.makeText(Activity_UserRegister.this, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT);
                                NetworkUtils.showNoConnectionDialog(Activity_UserRegister.this);
                            }

                        }


                    }
                } else {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_enter_all_fields));

                    if (user_register_name.getText().toString().trim().equals("")) {
                        user_register_name.setError(getResources().getString(R.string.enterfullname));
                    }

                    if (user_register_pass_code.getText().toString().trim().equals("")) {
                        user_register_pass_code.setError(getResources().getString(R.string.passcode_required));
                    }
                    if (user_register_mobile_number.getText().toString().trim().equals("")) {
                        user_register_mobile_number.setError(getResources().getString(R.string.enter_mobile_no));
                    }
                    if (user_register_pass_code_confirm.getText().toString().trim().equals("")) {
                        user_register_pass_code_confirm.setError(getResources().getString(R.string.passcode_required));
                    }
                }
            }

            @Override
            public void onFailure(String ex) {

                // If there is an error registering, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off when retrying.
                CustomProgressBarDialog.progressDialog.dismiss();
                runOnUiThread(new Runnable() {
                    public void run() {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                    }
                });

                super.onFailure(ex);
                user_register_sign_up.setEnabled(true);
            }
        });


    }


}
