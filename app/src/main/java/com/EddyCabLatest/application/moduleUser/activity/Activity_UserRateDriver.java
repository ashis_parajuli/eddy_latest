package com.EddyCabLatest.application.moduleUser.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.utilities.CustomRatingBar_with_rating;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Activity_UserRateDriver extends AppCompatActivity implements View.OnClickListener {
    Button btn_submit_rating;

    ToggleButton txt_bikecondition, txt_communicate, txt_driving, txt_pick_up_time, txt_service, txt_drop_of_time;
    TextView edt_feedback_only;
    LinearLayout middle1;
    LinearLayout middle2;
    EditText edt_pls_feedback;
    TextView txtFee;
    TextView txtDate;
    SessionManager session;
    CustomRatingBar_with_rating ratingBar;

    String reservationId;
    String driverId;
    String userId;

    //values
    String rating_value = "0.0";
    String feedbackfor = " ";
    String TAG = "===";
    private String ride_fare;
    private String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_user_rate_driver);
        //init session
        session = new SessionManager(Activity_UserRateDriver.this);
        session.createReservationByUser(DBTableFields.RESERVATION_STATE_COMPLETED);
        //getIntent
        Intent intent = getIntent();
        ride_fare = intent.getStringExtra("ride_fare");
        reservationId = intent.getStringExtra("reservation_id");
        String journeyFee = getIntent().getStringExtra("fee");
        //String journeyDate = getIntent().getStringExtra("journeyDate");
        String message = getIntent().getStringExtra("message");
        EApplication.getInstance().showToastMessageFunction(message);
        //get id from session
        HashMap<String, String> userDetails = session.getUserDetails();
        userId = userDetails.get(SessionManager.KEY_ID);

        HashMap<String, String> reservationWithDriver = session.getReservationIdWithDriver();
        // reservationId = reservationWithDriver.get(SessionManager.RESERVATION_ID);
        driverId = reservationWithDriver.get(SessionManager.DRIVER_ID);
        // EApplication.getInstance().showToastMessageFunction("Sucess on getting session datas");

        //getting current date of system
        SimpleDateFormat dfDate_day = new SimpleDateFormat("MMM dd, yyyy");
        String dt = "";
        Calendar c = Calendar.getInstance();
        dt = dfDate_day.format(c.getTime());
        btn_submit_rating = (Button) findViewById(R.id.btn_submit_rating);
        txt_service = (ToggleButton) findViewById(R.id.txt_service);
        txt_pick_up_time = (ToggleButton) findViewById(R.id.txt_pick_up_time);
        txt_driving = (ToggleButton) findViewById(R.id.txt_driving);
        txt_communicate = (ToggleButton) findViewById(R.id.txt_communicate);
        txt_bikecondition = (ToggleButton) findViewById(R.id.txt_bikecondition);
        txt_drop_of_time = (ToggleButton) findViewById(R.id.txt_drop_of_time);

        edt_feedback_only = (EditText) findViewById(R.id.edt_feedback_only);
        edt_pls_feedback = (EditText) findViewById(R.id.edt_pls_feedback);
        middle1 = (LinearLayout) findViewById(R.id.ll_middle1);
        middle2 = (LinearLayout) findViewById(R.id.ll_middle2);
        txtFee = (TextView) findViewById(R.id.txt_bhat_value);
        txtDate = (TextView) findViewById(R.id.txt_date);
        txtFee.setText((ride_fare) + "");
        txtDate.setText(dt);
        ratingBar = (CustomRatingBar_with_rating) findViewById(R.id.ratingBar);
        ratingBar.setOnScoreChanged(new CustomRatingBar_with_rating.IRatingBarCallbacks() {
            @Override
            public void scoreChanged(float score) {
                rating_value = String.valueOf(score);
                int intRating = Math.round(score);
                if (intRating > 3 && intRating <= 5) {
                    middle1.setVisibility(View.GONE);
                    middle1.setEnabled(false);
                    edt_pls_feedback.setEnabled(false);
                    middle2.setVisibility(View.GONE);

                } else if (intRating <= 3 && intRating >= 0) {
                    middle1.setVisibility(View.VISIBLE);
                    edt_pls_feedback.setEnabled(true);
                    middle2.setVisibility(View.GONE);

                }
            }
        });

        btn_submit_rating.setOnClickListener(this);
        txt_service.setOnClickListener(this);
        txt_pick_up_time.setOnClickListener(this);
        txt_driving.setOnClickListener(this);
        txt_communicate.setOnClickListener(this);
        txt_bikecondition.setOnClickListener(this);
        txt_drop_of_time.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_user_rate_driver, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit_rating:
                methodPostUserFeedback();
                break;
            case R.id.txt_service:
                feedbackfor = "service";
                break;

            case R.id.txt_pick_up_time:
                feedbackfor = "Pick up Time";
                break;
            case R.id.txt_driving:
                feedbackfor = "Driving";
                break;
            case R.id.txt_communicate:
                feedbackfor = "Communicate";
                break;
            case R.id.txt_bikecondition:
                feedbackfor = "Bike condition";
                break;
            case R.id.txt_drop_of_time:
                feedbackfor = "Drop off time";
                break;
            default:
                break;
        }

    }

    double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#");
        return Double.valueOf(twoDForm.format(d));
    }

    private void methodPostUserFeedback() {
        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserRateDriver.this);
        CustomProgressBarDialog.progressDialog.show();
        if (rating_value != "0.0") {
            if (NetworkUtils.isNetworkAvailable(Activity_UserRateDriver.this)) {

                final HashMap<String, String> params = new HashMap<>();
                params.put("reservation_id", reservationId);
                params.put("from_id", userId);
                params.put("to_id", driverId);
                params.put("rating", rating_value);
                params.put("feedback_for", feedbackfor);
                if (params.get("feedback_for") == null) {

                    params.put("comments", edt_feedback_only.getText().toString().trim());
                } else if (params.get("feedback_for") != null) {
                    params.put("comments", edt_pls_feedback.getText().toString().trim());
                }
                Log.e("Parameters", params.toString());

                CustomRequest requestPostRating = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.USER_FEEDBACK_RATING_URL, params,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject jsonObject) {
                                try {
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                    boolean error = jsonObject.getBoolean("error");
                                    String message = jsonObject.getString("message");
                                    if (!error) {

                                        session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);
                                        EApplication.getInstance().setHistryCancelledStatus(true);
                                        Log.e(TAG, "Response:" + jsonObject.toString());
                                        EApplication.getInstance().showToastMessageFunction(message);
                                        EApplication.ref.child(userId).removeValue();
                                        Intent intentToMain = new Intent(getApplicationContext(), Activity_UserMain.class);
                                        startActivity(intentToMain);
                                        finish();
                                    } else {
                                        EApplication.getInstance().showToastMessageFunction(message + " " + getResources().getString(R.string.please_tryagain));
                                    }
                                } catch (JSONException e) {
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CustomProgressBarDialog.progressDialog.dismiss();
                        //EApplication.getInstance().showToastMessageFunction("Response Error" + volleyError);

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.e("Driver_New_JObs_Volley", "TimeoutError");
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        } else if (error instanceof NoConnectionError) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                            Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Driver_New_JObs_Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                            Log.e("Driver_New_JObs_Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Driver_New_JObs_Volley", "ParseError");
                        }
                    }
                })  {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return params;
                    }

                    @Override
                    public Request.Priority getPriority() {
                        return Request.Priority.HIGH;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                        String current_language = session.getAppLanguage();
                        Log.e("current_language", current_language);
                        if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                            language = "en";
                        }
                        if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                            language = "np";
                        }
                        headers.put("lang", language);
                        return headers;
                    }
                };
                requestPostRating.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                EApplication.getInstance().addToRequestQueue(requestPostRating, "high");


            }

        } else {
            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_rate_driver));
            if (CustomProgressBarDialog.progressDialog != null) {

                CustomProgressBarDialog.progressDialog.dismiss();
            }

        }
    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(Activity_UserRateDriver.this, Activity_UserMain.class);
        session.createReservationByUser(DBTableFields.RESERVATION_STATE_COMPLETED);
        startActivity(intent);*/
    }
}
