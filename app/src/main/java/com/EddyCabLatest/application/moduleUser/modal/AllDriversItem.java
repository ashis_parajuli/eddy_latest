package com.EddyCabLatest.application.moduleUser.modal;

/**
 * Created by User on 7/27/2015.
 */
public class AllDriversItem {

    String rating_count;
    String bid_rate;
    String withinMin;
    String driver_id;
    String reservation_id;


    public AllDriversItem(String rating_count, String bid_rate, String withinMin, String driver_id, String reservation_id) {
        this.rating_count = rating_count;
        this.bid_rate = bid_rate;
        this.withinMin = withinMin;
        this.driver_id = driver_id;
        this.reservation_id = reservation_id;
    }


    public String getRating_count() {
        return rating_count;
    }

    public void setRating_count(String rating_count) {
        this.rating_count = rating_count;
    }

    public String getBid_rate() {
        return bid_rate;
    }

    public void setBid_rate(String bid_rate) {
        this.bid_rate = bid_rate;
    }

    public String getWithinMin() {
        return withinMin;
    }

    public void setWithinMin(String withinMin) {
        this.withinMin = withinMin;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(String reservation_id) {
        this.reservation_id = reservation_id;
    }
}
