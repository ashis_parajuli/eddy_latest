package com.EddyCabLatest.application.moduleUser.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.utilities.CustomRatingBar;

import java.util.ArrayList;
import java.util.HashMap;


public class AdapterQuotes extends BaseAdapter {

    private ArrayList<HashMap<String, String>> dataList;
    private Context mContext;
    private LayoutInflater mInflater;

    public AdapterQuotes(Context context, ArrayList<HashMap<String, String>> adapterDataList) {
        this.mContext = context;
        this.dataList = adapterDataList;

    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_list_adapter_best_quote, parent, false);
            holder.item_quotes_bestRate = (CustomRatingBar) convertView.findViewById(R.id.item_quotes_bestRate);
           /* LayerDrawable stars = (LayerDrawable) holder.item_quotes_bestRate.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
            holder.item_quotes_bestRate.setFocusable(false);*/
          /*  holder.item_quotes_bidAmount = (TextView) convertView.findViewById(R.id.item_quotes_bidAmount);*/
            holder.item_quotes_withinTime = (TextView) convertView.findViewById(R.id.item_quotes_withinTime);
            holder.drivername_job = (TextView) convertView.findViewById(R.id.drivername_job);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.item_quotes_bestRate.setScore(Float.parseFloat(dataList.get(position).get(DBTableFields.TABLE_BID_BEST_STAR)));
        //TODO address
        holder.item_quotes_withinTime.setText(""+formatNumber(Double.parseDouble(dataList.get(position).get(DBTableFields.TABLE_BID_NEAREST_DRIVER))));
        holder.drivername_job.setText("" + dataList.get(position).get(DBTableFields.DRIVER_NAME));
        return convertView;
    }
    class ViewHolder {
        TextView item_quotes_withinTime; //for time to reach to user
        TextView drivername_job; //for time to reach to user
        /* TextView item_quotes_bidAmount; //for rating counts*/
        CustomRatingBar item_quotes_bestRate;     //for bid rate
    }

    private String formatNumber(double distance) {
        String unit = "m";
        if (distance < 1) {
            distance *= 100;
            unit = "mm";
        } else if (distance > 1000) {
            distance /= 1000;
            unit = "km";
        }

        return String.format("%4.3f%s", distance, unit);
    }
}
