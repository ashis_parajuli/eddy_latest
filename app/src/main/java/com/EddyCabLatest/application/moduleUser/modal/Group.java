package com.EddyCabLatest.application.moduleUser.modal;

import java.util.ArrayList;

/**
 * Created by User on 6/12/2015.
 */
public class Group {

    private String groupName;
    private ArrayList<Child> Items;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ArrayList<Child> getItems() {
        return Items;
    }

    public void setItems(ArrayList<Child> items) {
        Items = items;
    }

}
