package com.EddyCabLatest.application.moduleUser.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.activity.Activity_PickupLocation;
import com.EddyCabLatest.application.moduleUser.adapter.AdapterFrequentSearch;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by newimac on 7/6/15.
 */
public class Fragment_GetLocation_Google extends Fragment {

    EditText searchEditText;
    ListView searchListView;
    TextView emptyMessage;

    ArrayList<HashMap<String, String>> searchDataList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.getlocation_fragment_google, container, false);

        setupUI(rootView.findViewById(R.id.main_parent));

        searchEditText = (EditText) rootView.findViewById(R.id.searchEditText);
        searchListView = (ListView) rootView.findViewById(R.id.googleSearchList);
        emptyMessage = (TextView) rootView.findViewById(android.R.id.empty);

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch(searchEditText.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });


        return rootView;
    }

    private void performSearch(String query) {

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
        CustomProgressBarDialog.progressDialog.show();


        String requestURL="https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + query +"&region=np"+ "&key="+Config.GOOGLE_MAP_API_KEY;

        Log.e("===","Request Url is :"+requestURL);;



        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("===","Response is: "+ response);

                searchDataList = new ArrayList<>();

                try {
                    JSONObject objectData = new JSONObject(response);

                    JSONArray dataArray = objectData.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++){

                        String name = dataArray.getJSONObject(i).getString("name");
                        String formattedAddress = dataArray.getJSONObject(i).getString("formatted_address");

                        String geometry_lat = dataArray.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getString("lat");
                        String geometry_lng = dataArray.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getString("lng");


                        HashMap<String, String> mapData = new HashMap<>();

                        mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, name);
                        mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, formattedAddress);
                        mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, geometry_lat);
                        mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, geometry_lng);
                        mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, "1.0 KM");



                        searchDataList.add(mapData);

                    }

                    Log.e("Array Data", searchDataList.toString());
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }


                AdapterFrequentSearch adapter = new AdapterFrequentSearch(getActivity(), searchDataList);
                searchListView.setAdapter(adapter);
                searchListView.setEmptyView(emptyMessage);
                searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        ((Activity_PickupLocation) getActivity()).setRequestAddress(
                                searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME),
                                searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS),
                                searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT),
                                searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));

                    }
                });

                CustomProgressBarDialog.progressDialog.hide();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", "That didn't work!");

                CustomProgressBarDialog.progressDialog.hide();
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    EApplication.getInstance().hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }
}
