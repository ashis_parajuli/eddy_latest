package com.EddyCabLatest.application.moduleUser.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by indraimac on 8/3/15.
 */
public class AdapterBuyBanana extends BaseAdapter {

    private ArrayList<HashMap<String, String>> dataList;
    private Context mContext;
    private LayoutInflater mInflater;

    public AdapterBuyBanana(Context context, ArrayList<HashMap<String, String>> adapterDataList){
        this.mContext = context;
        this.dataList = adapterDataList;
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
           /* convertView = mInflater.inflate(R.layout.item_banana_credit, parent, false);

            holder.bananaPrice = (TextView) convertView.findViewById(R.id.bananaPrice);
            holder.bananaQuantity = (TextView) convertView.findViewById(R.id.bananaQuantity);*/

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.bananaPrice.setText(dataList.get(position).get(DBTableFields.BANANA_BUY_AMOUNT)+" "+ mContext.getResources().getString(R.string.bhatt));
        holder.bananaQuantity.setText("+ "+dataList.get(position).get(DBTableFields.BANANA_BUY_CREDIT));

        return convertView;
    }

    class ViewHolder {
        TextView bananaPrice;
        TextView bananaQuantity;
    }

}
