package com.EddyCabLatest.application.moduleUser.fragment;


import android.Manifest;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_ActivityBidSuccess;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.moduleDriver.fragment.FragmentJobList;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Fragment_DriverNewHelp extends Fragment implements View.OnClickListener {


    Button about_btn_call_customer;
    Button about_btn_Submit;
    EditText edt_about_feedback;
    SessionManager session;
    EApplication app;
    String UserId;
    private Context mcontext;
    private String language;
    private ChildEventListener eventlistener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.help_driver, container, false);
        // Initializing Application Instances
        session = new SessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        UserId = userDetails.get(SessionManager.KEY_ID);
        mcontext = container.getContext();
        about_btn_call_customer = (Button) rootView.findViewById(R.id.about_btn_call_customer);
        about_btn_call_customer.setOnClickListener(this);

        about_btn_Submit = (Button) rootView.findViewById(R.id.about_btn_Submit);
        about_btn_Submit.setOnClickListener(this);

        edt_about_feedback = (EditText) rootView.findViewById(R.id.edt_about_feedback);
        edt_about_feedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edt_about_feedback.getText().toString().trim().length() > 5) {
                    about_btn_Submit.setBackgroundColor(getResources().getColor(R.color.primary));
                    about_btn_Submit.setEnabled(true);
                } else {
                    about_btn_Submit.setBackgroundColor(getResources().getColor(R.color.light_gray));
                    about_btn_Submit.setEnabled(false);
                }


            }
        });


        return rootView;
    }


    @Override
    public void onDestroy() {

        if (eventlistener != null) {

            EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(eventlistener);
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_btn_call_customer:
                String number = "tel:" + "+97714410178";
                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
                if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
                break;

            case R.id.about_btn_Submit:

                postFeedBackApi();


                break;
        }
    }

    public void getjoblistfirebase() {
        eventlistener = EApplication.ref.child(UserId + "/new_reservations/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    if (dataSnapshot.child("accepted").exists()) {
                        if ((dataSnapshot.child("accepted").getValue().equals(false)) && (dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("new"))) {

                            if (isAdded()) {
                                EApplication.getInstance().showNotification(mcontext, Driver_MainActivity.class, 011, getResources().getString(R.string.new_job), getResources().getString(R.string.new_job), getResources().getString(R.string.new_job_avaiable));

                            }
                            Intent intent = new Intent(mcontext, Driver_MainActivity.class);
                            if (isAdded())
                                startActivity(intent);


                        }
                    }


                    Log.e("data_joblist", dataSnapshot + "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (isAdded()) {
                    if (dataSnapshot.getValue() != null) {

                        if (dataSnapshot.child("accepted").exists()) {
                            if (dataSnapshot.child("accepted").getValue().equals(true)) {
                                //EApplication.ref.child(userID + "/new_reservations/" + dataSnapshot.getKey() + "/accepted/").removeValue();
                                EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                if (eventlistener != null) {
                                    EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                    if (eventlistener != null) {

                                        EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                    }
                                }
                                try {
                                    if (dataSnapshot.getValue() != null) {

                                        Intent intent_bid_succes = new Intent(mcontext, Driver_ActivityBidSuccess.class);

                                        Map<String, String> usersMap_data = (Map<String, String>) dataSnapshot.getValue();
                                        JSONObject usersJSON = null;
                                        usersJSON = new JSONObject(usersMap_data);

                                        String selected_id = dataSnapshot.getKey();
                                        String user_fullname = dataSnapshot.child("user_fullname").getValue().toString();
                                        String distance_value = dataSnapshot.child("distance").getValue().toString();
                                        String user_pp = dataSnapshot.child("user_pp").getValue().toString();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("id", selected_id);

                                        bundle.putString("user_fullname", user_fullname);
                                        bundle.putString("user_pp", user_pp);
                                        bundle.putString("distance_value", distance_value);
                                        bundle.putString("user_data_for", usersJSON.toString());

                                        intent_bid_succes.putExtras(bundle);


                                        mcontext.startActivity(intent_bid_succes);
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            } else {
                                //jobListRequest1();

                            }

                        }

                    }
                }


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // jobListRequest1();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }

    private void postFeedBackApi() {

        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            CustomProgressBarDialog.progressDialog.show();
            final HashMap<String, String> params = new HashMap<>();
            params.put("feedback", edt_about_feedback.getText().toString().trim());
            params.put("user_id", UserId);

            Log.e("Parameters", params.toString());

            String feedback_url = Config.APP_BASE_URL + Config.SUPPORT_FEEDBACK_POST_URL;
            Log.e("feedback_url ::", feedback_url);
            CustomRequest req = new CustomRequest(Request.Method.POST, feedback_url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("response", response.toString());
                            try {
                                boolean error = response.getBoolean("error");
                                Log.e("ERROR", String.valueOf(error));
                                if (!error) {
                                    CustomProgressBarDialog.progressDialog.setCancelable(true);
                                    CustomProgressBarDialog.progressDialog.hide();
                                    showSucessDialouge();
                                } else {
                                    CustomProgressBarDialog.progressDialog.setCancelable(true);
                                    CustomProgressBarDialog.progressDialog.hide();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomProgressBarDialog.progressDialog.setCancelable(true);
                    CustomProgressBarDialog.progressDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            req.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(req, "high");

        } else {
            NetworkUtils.showNoConnectionDialog(getActivity());
        }
    }

    private void showSucessDialouge() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.thanks_feedback);
        dialog.setCancelable(true);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
               /* FragmentJobList jobFragment = new FragmentJobList();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame_driver, jobFragment);
                transaction.addToBackStack(null);
                transaction.commit();*/
                Intent intent = new Intent(mcontext, Driver_MainActivity.class);
                startActivity(intent);
            }
        });
        dialog.show();

    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    FragmentJobList jobFragment = new FragmentJobList();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame_driver, jobFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    Driver_MainActivity.appTitle.setText(R.string.new_booking);
                    return true;
                }

                return false;
            }
        });
        getjoblistfirebase();
    }
}
