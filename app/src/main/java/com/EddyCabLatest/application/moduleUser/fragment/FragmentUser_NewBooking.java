package com.EddyCabLatest.application.moduleUser.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.activity.Activity_PickupLocation;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserLocateDriver;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserLogin;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserMain;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserSelectDriver;
import com.EddyCabLatest.application.moduleUser.activity.UserLoginScreenActivity;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.utilities.gpsTrackerUser;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class FragmentUser_NewBooking extends Fragment implements View.OnClickListener, GoogleMap.OnCameraChangeListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final int REQUEST_CHECK_SETTINGS = 0x01;
    private String TAG = "FragmentUser_NewBooking ===";

    MapView mapView;
    LatLng locationPoint = new LatLng(0.0, 0.0);
    SessionManager session;

    Button btn_pick_me_up;
    ImageView setMyLocationIcon;

    String LocationName = "Kathmandu", LocationAddress = "Nepal", SearchFieldText;


    Geocoder geocoder;
    List<Address> addresses;
    double LAT;
    double LANG;

    private GoogleMap map;
    private gpsTrackerUser gps_tracker;
    HashMap<String, String> sourceLocation;
    boolean isFirstCall = true;

    ArrayList<LatLng> arr = new ArrayList<>();
    boolean isFirstStart = true;
    String reservationType;
    String driversNo;
    String drivers_nearby;
    private boolean dialog = false;
    private Context mcontext;
    private double lat_camera;
    private double long_camera;

    private LinearLayout pick_up_click;
    private TextView pickup_location_source;
    private String language;
    private String user_ID;
    private boolean visible = false;
    private gpsTrackerUser gpsTracker;
    private LatLng startPoint;
    private EditText im_here_notes;
    private CountDownTimer myCountdownTimer;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRrequest;
    protected PowerManager.WakeLock mWakeLock;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_fragment_booking, container, false);
        mcontext = container.getContext();

        initializaMap(rootView, savedInstanceState);


        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        session = new SessionManager(mcontext);
        session.createReservationType("bike");
        reservationType = session.getReservationType();
        if (reservationType.equals("none")) {
            reservationType = "bike";
        }

        setMyLocationIcon = (ImageView) rootView.findViewById(R.id.setMyLocationIcon);
        setMyLocationIcon.setOnClickListener(this);


        pick_up_click = (LinearLayout) rootView.findViewById(R.id.pick_up_click);
        btn_pick_me_up = (Button) rootView.findViewById(R.id.btn_pick_me_up);
        pickup_location_source = (TextView) rootView.findViewById(R.id.pickup_location_source);
        btn_pick_me_up.setOnClickListener(this);
        im_here_notes = (EditText) rootView.findViewById(R.id.im_here_notes);

        dialog = true;
        pick_up_click.setOnClickListener(this);
        gpsTracker = new gpsTrackerUser(mcontext);
        sourceLocation = session.getSourceLocation();
        if (session.isSourceLocationSet()) {
            if (sourceLocation == null) {
                if (gpsTracker.canGetLocation()) {
                    startPoint = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                    MarkerOptions options = new MarkerOptions();
                    options.position(startPoint);
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));
                    if (startPoint != null) {
                        map.addMarker(options);

                        gpsTrackerUser gps = new gpsTrackerUser(mcontext);
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(startPoint, 10);
                        map.animateCamera(cameraUpdate);

                    }
                } else {

                    startPoint = new LatLng(Double.parseDouble(sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LAT)),
                            Double.parseDouble(sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LANG)));
                }

            }


            pickup_location_source.setText(sourceLocation.get(SessionManager.SOURCE_LOCATION_NAME) + "\n " + sourceLocation.get(SessionManager.SOURCE_LOCATION_ADDRESS));
        }
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mcontext)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)

                    .addApi(LocationServices.API)
                    .build();
        }

        mGoogleApiClient.connect();


        return rootView;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //Intent intent = new Intent(mcontext,Activity_UserMain.this);
                        //
                        EApplication.getInstance().showToastMessageFunction("accepted");
                        // All required changes were successfully made

                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to

                        break;
                    default:
                        break;
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // coarse location permission granted
            dialog = false;
            methodSetNearbyLocations();
        } else {
            EApplication.getInstance().showToastMessageFunction("You need to enable GPS permission to see Nearby Drivers");
        }


    }


    private void checkCurrentLanguage() {
        String current_language = session.getAppLanguage();
        if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
            drivers_nearby = "Drivers Nearby";
        } else {
            drivers_nearby = "คนขับใกล้เคียง";
        }
    }

    private void methodSetNearbyLocations() {
        //Checking for Internet connection
        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            gps_tracker = new gpsTrackerUser(getActivity());
            if (gps_tracker.canGetLocation()) {
               /* SetNearbyDriverList(gps_tracker.getLatitude(), gps_tracker.getLongitude());*/
                map.getUiSettings().setZoomControlsEnabled(false);

                map.getUiSettings().setMyLocationButtonEnabled(false);
                map.setOnCameraChangeListener(this);

                if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                map.setMyLocationEnabled(true);
                locationPoint = new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude());
/*			map.setOnCameraChangeListener(this);*/
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(locationPoint, Config.CAMERA_FACTORY_UPDATE);
                map.animateCamera(cameraUpdate);

                /* load nearby driver after map load complete */
                SetNearbyDriverList(gps_tracker.getLatitude(), gps_tracker.getLongitude());


            } else {
                gps_tracker.showSettingsAlert();
            }

        } else {
            if (dialog) {
                dialog = false;
                NetworkUtils.showNoConnectionDialog(getActivity());


            }
        }

    }

    @Override
    public void onResume() {


        super.onResume();
        if (mapView != null) {
            mapView.onResume();
            methodSetNearbyLocations();
        }

        checkCurrentLanguage();


        if (!isFirstCall) {
            if (session.getSourceLocation() != null) {
                sourceLocation = session.getSourceLocation();
                String address = sourceLocation.get(SessionManager.SOURCE_LOCATION_NAME) + "\n " + sourceLocation.get(SessionManager.SOURCE_LOCATION_ADDRESS);
                String lat = sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LAT);
                String lang = sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LANG);
                Log.e("address", address);
                pickup_location_source.setText(address);
                Double latititude = Double.parseDouble(lat);
                Double longitude = Double.parseDouble(lang);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latititude, longitude)).zoom(15).build();

                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
        visible = true;

        checkStatus(false);


    }

    @Override
    public void onStop() {

        super.onStop();
    }

    @Override
    public void onDestroy() {

        if (mapView != null) {
            mapView.onDestroy();
        }
        super.onDestroy();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mcontext = context;
    }

    @Override
    public void onLowMemory() {
        if (mapView != null) {
            mapView.onLowMemory();
        }

        super.onLowMemory();
        super.onLowMemory();

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_pick_me_up:

                checkStatus(true);
                break;
            case R.id.pick_up_click:

                String RESERVATION_STATE = session.getReservationByUser();


                if (RESERVATION_STATE.equalsIgnoreCase(DBTableFields.RESERVATION_STATE_PENDING)) {

                    Intent intentPending_waiting = new Intent(mcontext, Activity_UserSelectDriver.class);
                    Log.e("state", "pending");

                    startActivity(intentPending_waiting);


                } else if ((RESERVATION_STATE.equalsIgnoreCase(DBTableFields.RESERVATION_STATE_DRIVER_CONFIRMED)) || (RESERVATION_STATE.equalsIgnoreCase(DBTableFields.RESERVATION_STATE_ON_ROUTE))) {
                    Intent intent_locate = new Intent(mcontext, Activity_UserLocateDriver.class);
                    intent_locate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_locate);

                } else

                {
                    EApplication.getInstance().fromFragmentIntent = true;
                    EApplication.getInstance().fromCreateReservation = false;
                    Intent sourceIntent = new Intent(getActivity(), Activity_PickupLocation.class);
                    sourceIntent.putExtra("ADDRESS_REQUEST", "sourceLocation");
                    startActivity(sourceIntent);

                }
                break;
            case R.id.setMyLocationIcon:
                setmyLocation();


                break;

            default:
                break;

        }
    }

    private void setmyLocation() {
        gps_tracker = new gpsTrackerUser(mcontext);
        if (gps_tracker.canGetLocation()) {

            locationPoint = new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(locationPoint, Config.CAMERA_FACTORY_UPDATE);
            map.animateCamera(cameraUpdate);

        } else

            gps_tracker.showSettingsAlert();

    }

    private void checkStatus(boolean move) {

        if (session.isLoggedIn()) {
            String RESERVATION_STATE = session.getReservationByUser();
            if (RESERVATION_STATE != null) {


                if (RESERVATION_STATE.equalsIgnoreCase(DBTableFields.RESERVATION_STATE_PENDING)) {

                    Intent intentPending_waiting = new Intent(mcontext, Activity_UserSelectDriver.class);
                    Log.e("state", "pending");

                    startActivity(intentPending_waiting);


                } else if ((RESERVATION_STATE.equalsIgnoreCase(DBTableFields.RESERVATION_STATE_DRIVER_CONFIRMED)) || (RESERVATION_STATE.equalsIgnoreCase(DBTableFields.RESERVATION_STATE_ON_ROUTE))) {
                    Intent intent_locate = new Intent(mcontext, Activity_UserLocateDriver.class);
                    intent_locate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_locate);

                } else {
                    setmyLocation();
                    if (move) {

                        sourceLocation = session.getSourceLocation();
                        if (sourceLocation != null) {

                            gps_tracker = new gpsTrackerUser(mcontext);
                            session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);

                            createReservation_ByUser_FirstTime("CASH");


                        } else {

                            Toast.makeText(mcontext, getResources().getString(R.string.please_chosse_pic_up_location), Toast.LENGTH_LONG).show();

                        }
                    }
                }


            }

        } else {
            Intent intent = new Intent(mcontext, UserLoginScreenActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        lat_camera = cameraPosition.target.latitude;
        long_camera = cameraPosition.target.longitude;
        new onCamerachange().execute();

    }

    private void createReservation_ByUser_FirstTime(final String Reservation_Type) {

        if (NetworkUtils.isNetworkAvailable(mcontext)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(mcontext);
            if (visible) CustomProgressBarDialog.progressDialog.show();
            CustomProgressBarDialog.progressDialog.setCancelable(true);
            HashMap<String, String> userDetails = session.getUserDetails();
            user_ID = userDetails.get(SessionManager.KEY_ID);
            sourceLocation = session.getSourceLocation();
            final HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_ID);
            params.put("use_credit", "0");
            params.put("pickup_lat", String.valueOf(sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LAT)) + "");
            params.put("pickup_long", String.valueOf(sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LANG)) + "");
            params.put("notes", im_here_notes.getText().toString().trim() + "");
            params.put("pickup_title", sourceLocation.get(SessionManager.SOURCE_LOCATION_NAME) + "");
            params.put("pickup_address", sourceLocation.get(SessionManager.SOURCE_LOCATION_ADDRESS) + "");
            params.put("reservation_type", reservationType);//tuktuk
            Log.e("Parameters", params.toString());

            String REQUEST_URL = Config.APP_BASE_URL + Config.USER_RESERVATION_URL;
            Log.e("REQUEST_URL ::", REQUEST_URL);
            CustomRequest req = new CustomRequest(Request.Method.POST, REQUEST_URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responseObject) {
                            CustomProgressBarDialog.progressDialog.dismiss();
                            String newResponse = responseObject.toString();
                            if (newResponse.contains("Ã¯Â»Â¿")) {
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }
                            try {
                                JSONObject response = new JSONObject(newResponse);
                                boolean error = response.getBoolean("error");
                                Log.e("ERROR", String.valueOf(error));
                                String message = response.getString("message");
                                if (!error) {
                                    EApplication.getInstance().setPreviousReservationStatus(false);
                                    String ReservationID = response.getString("Reservation");
                                    RequestPushNotificationToDriver(ReservationID);
                                    session.createReservationNewUser(ReservationID, Reservation_Type);
                                    reservationCreateSuccess(ReservationID);
                                } else if (error) {
                                    Log.e("errr", "onResponse: " + response.toString());
                                    wantToCancelDialog(getResources().getString(R.string.alert), response.getString("message"), response.getString("reservation_id"));

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            CustomProgressBarDialog.progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    VolleyLog.d("Error", "Error: " + error);
                    EApplication.getInstance().showToastMessageFunction("" + error);
                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            req.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            EApplication.getInstance().addToRequestQueue(req, "high");
        } else NetworkUtils.showNoConnectionDialog(mcontext);


    }


    private void reservationCreateSuccess(final String ReservationID) {
        final Dialog dialog_main_cancel = new Dialog(mcontext);
        dialog_main_cancel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_main_cancel.setContentView(R.layout.custom_dailog_reservation_waiting);
        dialog_main_cancel.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog_main_cancel.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog_main_cancel.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog_main_cancel.getWindow().setAttributes(paramLayout);
        final TextView txt_count_down = (TextView)
                dialog_main_cancel.findViewById(R.id.txt_count_down);
        myCountdownTimer = new CountDownTimer(Config.COUNTDOWN_TIME, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_count_down.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                if (myCountdownTimer != null) {
                    myCountdownTimer.cancel();
                    myCountdownTimer = null;
                    // dialog_main_cancel.dismiss();
                    //LoadDialogForCancelReservation(ReservationID);

                }

                LoadBiddingResponse(ReservationID);
                dialog_main_cancel.dismiss();
            }
        }.start();
        if ((dialog_main_cancel != null) && (!dialog_main_cancel.isShowing())) {
            if (visible) dialog_main_cancel.show();
            ImageView action_btn_quiet = (ImageView) dialog_main_cancel.findViewById(R.id.action_btn_quiet);
            action_btn_quiet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LoadDialogForCancelReservation(ReservationID);
                }
            });
        }


    }

    private void LoadDialogForCancelReservation(final String reservationID) {
        final AlertDialog.Builder builder = new AlertDialog.Builder((Activity_UserMain) getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getResources().getString(R.string.warningTitle));
        builder.setMessage(getResources().getString(R.string.reservation_cancel_message));
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(mcontext);
                if (visible) CustomProgressBarDialog.progressDialog.show();
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_ID);
                params.put("reservation_id", reservationID);

                Log.e("Parameters", params.toString());
                CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.API_RESERVATION_CANCEL, params,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject responseObject) {
                                // myCountdownTimer.cancel();
                                Log.e("Response Cancelled", responseObject.toString());
                                try {

                                    boolean error = responseObject.getBoolean("error");
                                    if (!error) {

                                        if (myCountdownTimer != null) {
                                            myCountdownTimer.cancel();
                                            myCountdownTimer = null;

                                            //LoadDialogForCancelReservation(ReservationID);

                                        }
                                        CustomProgressBarDialog.progressDialog.dismiss();
                                        EApplication.getInstance().showToastMessageFunction(responseObject.getString("message"));
                                        session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                                        session.clearPickUPDropOffLocationAddress();
                                        pickup_location_source.setText(getResources().getString(R.string.pick_up_location));
                                        Intent intent = new Intent(mcontext, Activity_UserMain.class);
                                        startActivity(intent);
                                       /* finish();*/
                                    } else
                                        EApplication.getInstance().showToastMessageFunction("" + responseObject.getString("message"));
                                    CustomProgressBarDialog.progressDialog.dismiss();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Error", "Error: " + error.getMessage());

                        CustomProgressBarDialog.progressDialog.dismiss();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                            Log.e("Driver_New_JObs_Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Driver_New_JObs_Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.e("Driver_New_JObs_Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Driver_New_JObs_Volley", "ParseError");
                        }
                    }
                }) {


                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                        String current_language = session.getAppLanguage();
                        Log.i("current_language", current_language);
                        if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                            language = "en";
                        }
                        if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                            language = "np";
                        }
                        headers.put("lang", language);
                        return headers;
                    }


                    @Override
                    public Request.Priority getPriority() {
                        return Request.Priority.HIGH;
                    }
                };
                req.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                EApplication.getInstance().addToRequestQueue(req, "high");
            }
        });


        if (visible) builder.show();
    }


    private void LoadBiddingResponse(final String reservation_id) {
        Log.e("Methods:", "Get Bidding Result");

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(mcontext
        );


        if (isAdded())
            if (visible) CustomProgressBarDialog.progressDialog.show();
        CustomProgressBarDialog.progressDialog.setCancelable(false);

//        EApplication.getInstance().showToastMessageFunction("reservation id inside loadbidding response" + reservation_id);
        String BIDDING_URL = Config.APP_BASE_URL + Config.GET_BIDDING_RESULT_URL + reservation_id;

        CustomRequest req = new CustomRequest(Request.Method.GET, BIDDING_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("===", "Response bidding API" + response.toString());

                        try {
                            boolean error = response.getBoolean("error");
                            if (!error) {
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_PENDING);
                                EApplication.getInstance().setverifyStatus(true);
                                if(isAdded())
                                CustomProgressBarDialog.progressDialog.dismiss();

                                Intent bidResponseIntent = new Intent(mcontext, Activity_UserSelectDriver.class);
                                if (isAdded())
                                    startActivity(bidResponseIntent);

                            } else {
                                CustomProgressBarDialog.progressDialog.dismiss();

                                reservationCancelAPIExecute(reservation_id);
                                if (isAdded())
                                    noBiddersFoundDialog(reservation_id);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (isAdded())
                            CustomProgressBarDialog.progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //LoadBiddingResponse(reservation_id);
                CustomProgressBarDialog.progressDialog.dismiss();
                loadTryAgainDialog(reservation_id);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.i("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);
                return headers;
            }


            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        EApplication.getInstance().addToRequestQueue(req, "high");
    }

    private void loadTryAgainDialog(final String reservation_id) {
        final Dialog try_again_dialog = new Dialog(mcontext);
        try_again_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        try_again_dialog.setContentView(R.layout.custom_two_button_dialog);
        try_again_dialog.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(try_again_dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        try_again_dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) try_again_dialog.findViewById(R.id.dialog_title);
        dialog_title.setText(getResources().getString(R.string.alert));
        TextView dialog_message = (TextView) try_again_dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(getResources().getString(R.string.no_internet_availeble_please_try_again));
        Button dialog_action = (Button) try_again_dialog.findViewById(R.id.dialog_action);
        Button dialog_action_no = (Button) try_again_dialog.findViewById(R.id.dialog_action_no);
        if (visible) try_again_dialog.show();
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try_again_dialog.dismiss();
                LoadBiddingResponse(reservation_id);
            }
        });


        dialog_action_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try_again_dialog.dismiss();
                // reservationCancelAPIExecute(reservation_id);
            }
        });
    }

    private void noBiddersFoundDialog(final String reservation_id) {
        final Dialog dialog_no_bidder = new Dialog(mcontext);
        dialog_no_bidder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_no_bidder.setContentView(R.layout.custom_success_failure_dialog);
        dialog_no_bidder.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog_no_bidder.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog_no_bidder.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog_no_bidder.findViewById(R.id.dialog_title);
        dialog_title.setText(getResources().getString(R.string.alert));
        TextView dialog_message = (TextView) dialog_no_bidder.findViewById(R.id.dialog_message);
        dialog_message.setText(getResources().getString(R.string.no_SELECTders_found));
        Button dialog_action = (Button) dialog_no_bidder.findViewById(R.id.dialog_action);
        if (visible) dialog_no_bidder.show();
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_no_bidder.dismiss();
                // reservationCancelAPIExecute(reservation_id);
            }
        });
    }

    private void RequestPushNotificationToDriver(final String reservationID) {

        CustomRequest req = new CustomRequest(Request.Method.GET, Config.APP_BASE_URL + Config.GET_PUSH_NOTIFICATION_DRIVER + reservationID, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseObject) {
                        Log.e("Reservation Response", responseObject.toString());
                        try {

                            boolean error = responseObject.getBoolean("error");
                            if (error) {
                                RequestPushNotificationAgainMethods(reservationID);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }
        };

        int socketTimeout = 9000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);

        EApplication.getInstance().addToRequestQueue(req, "high");
    }

    private void RequestPushNotificationAgainMethods(String reservationID) {
        RequestPushNotificationToDriver(reservationID);
    }

    private void reservationCancelAPIExecute(String reservation_id) {

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(mcontext);
        if (visible) CustomProgressBarDialog.progressDialog.show();

        final HashMap<String, String> params = new HashMap<>();
        params.put("user_id", user_ID);
        params.put("reservation_id", reservation_id);

        Log.e("Parameters", params.toString());

        String REquestUrl = Config.APP_BASE_URL + Config.API_RESERVATION_CANCEL;

        StringRequest req = new StringRequest(Request.Method.POST, REquestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String responseObject) {
                        String newResponse = responseObject;
                        CustomProgressBarDialog.progressDialog.dismiss();

                        if (responseObject.contains("Ã¯Â»Â¿")) {
                            newResponse = newResponse.substring(newResponse.indexOf("{"));
                        }

                        try {
                            JSONObject response = new JSONObject(newResponse);
                            boolean error = response.getBoolean("error");
                            if (!error) {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);
                                //EApplication.getInstance().showToastMessageFunction(response.getString("message"));
                               /* session.clearPickUPDropOffLocationAddress();*/
                                EApplication.getInstance().setPreviousReservationStatus(false);
                               /* reservationCancelSuccessDialog();*/
                                if (isAdded()) {
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.canceled_reservation));
                                }
                                //Toast.makeText(mcontext, getResources().getString(R.string.canceled), Toast.LENGTH_SHORT).show();


                            } else

                            {
                                EApplication.getInstance().showToastMessageFunction("" + response.getString("message"));
                                CustomProgressBarDialog.progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            CustomProgressBarDialog.progressDialog.dismiss();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());

                CustomProgressBarDialog.progressDialog.dismiss();

            }
        }) {
            @Override

            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.i("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);
                return headers;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        EApplication.getInstance().addToRequestQueue(req, "high");
    }


    private void wantToCancelDialog(final String alertTitle, String message, final String reservId) {

        final Dialog dialog = new Dialog(mcontext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_of_transparent_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        dialog_title.setText(alertTitle);
        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(message);
        if (visible) dialog.show();
        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        dialog_action.setText(getResources().getString(R.string.ok_cancel));
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservationCancelAPIExecute(reservId);
                dialog.dismiss();
            }
        });

        Button dialog_dismiss = (Button) dialog.findViewById(R.id.dialog_dismiss);
        dialog_dismiss.setText(getResources().getString(R.string.dismiss_cancel));
        dialog_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
     /*   if (visible) dialog.show();*/

    }

    private void SetNearbyDriverList(double Lat, double Lang) {
        HashMap<String, String> params = new HashMap<>();
        params.put("current_lat", String.valueOf(Lat));
        params.put("current_long", String.valueOf(Lang));
        params.put("driver_type", reservationType);


        CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.NEAREST_DRIVER_BY_LOCATION, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseObject) {

                        try {
                            boolean error = responseObject.getBoolean("error");
                            arr.clear();

                            if (!error) {
                                arr.clear();
                                JSONArray arrayObject = responseObject.getJSONArray("Drivers");
                                if (arrayObject.length() > 0) {
                                    driversNo = String.valueOf(arrayObject.length());
                             /*       numberOfNearbyDriver.setText(driversNo + " " + drivers_nearby);*/

                                    for (int i = 0; i < arrayObject.length(); i++) {
                                        JSONObject objectData = (JSONObject) arrayObject.get(i);

                                        String DriverName = objectData.getString("fullname");
                                        String DriverDistance = objectData.getString("distance");
                                        double DriverLat = objectData.getDouble("lat");
                                        double DriverLang = objectData.getDouble("long");

                                        if (isFirstStart) {
                                            arr.add(i, new LatLng(DriverLat, DriverLang));
                                            arr.add(i, new LatLng(DriverLat, DriverLang));
                                            if (arr.size() > 0) {
                                                setMarkerPosition(DriverName, DriverDistance, DriverLat, DriverLang);
                                            } else {

                                            }
                                        }

                                    }
                                }


                            }

                            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                                @Override
                                public void onMapLoaded() {
                                    map.getUiSettings().setRotateGesturesEnabled(true);

                                    if (isFirstStart) {
                                        if (arr.size() > 0) {
                                            // Log.e(TAG, "latlang in array" + arr.toString());
                                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                            for (LatLng marker : arr) {
                                                builder.include(marker);
                                            }
                                            builder.include(new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude()));

                                            LatLngBounds bounds = builder.build();
                                            //map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                                            map.moveCamera(CameraUpdateFactory.zoomTo(map.getCameraPosition().zoom - 0.5f));

                                            isFirstStart = false;

                                        } else {
                                            arr.clear();
                                            map.clear();
                                            driversNo = "0";
                                          /*  img_reserv_type_tuktuk.setEnabled(true);*/
                                        /*img_reserv_type_banana.setEnabled(true);*/
                                      /*      numberOfNearbyDriver.setText(driversNo + " " + drivers_nearby);*/
                                            updateCameraToCurrentPosition();
                                        }


                                    }


                                }


                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.i("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);
                return headers;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        EApplication.getInstance().addToRequestQueue(req, "high");

    }

    private void updateCameraToCurrentPosition() {

        gpsTrackerUser gps = new gpsTrackerUser(getActivity());
        locationPoint = new LatLng(gps.getLatitude(), gps.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(locationPoint, 10);
        map.animateCamera(cameraUpdate);
        isFirstStart = true;
    }

    private void setMarkerPosition(String name, String distance, double lat, double lang) {
        LatLng LatLangPosition = new LatLng(lat, lang);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(LatLangPosition);
        markerOptions.title(name);
        if (reservationType.equals("bike")) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_driverpin));
        }
        markerOptions.visible(true);
        markerOptions.snippet(distance);
        map.addMarker(markerOptions);
       /* img_reserv_type_tuktuk.setEnabled(true);
        img_reserv_type_banana.setEnabled(true);*/
    }

    private void initializaMap(View rootView, Bundle savedInstanceState) {

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(mcontext)) {
            case ConnectionResult.SUCCESS:
                mapView = (MapView) rootView.findViewById(R.id.content_map_view);
                mapView.onCreate(savedInstanceState);
                MapsInitializer.initialize(mcontext);
                if (mapView != null) {
                    map = mapView.getMap();
                }


                break;
            case ConnectionResult.SERVICE_MISSING:
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;
            default:

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    class onCamerachange extends AsyncTask<Void, Void, Void>

    {

        @Override
        protected Void doInBackground(Void... params) {


            if (NetworkUtils.isNetworkAvailable(mcontext)) {

                try {

                    addresses = geocoder.getFromLocation(lat_camera, long_camera, 1);

                    // addresses = geocoder.getFromLocation(lat_camera, long_camera, 1);
                } catch (IOException e1) {
                    getManualAddressUsingApi(lat_camera, long_camera);
                    System.out.println("exception");

                  /*  e1.printStackTrace();*/
                }
            }

            return null;


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (isAdded()) {
                if (addresses != null) {
                    if (addresses.size() > 0) {


                        String address = addresses.get(0).getAddressLine(0);
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String knownName = addresses.get(0).getFeatureName();

                        String correctAddress = "", correctCity = "", correctState = "", correctCountry = "", correctKnownName = "";

                        if (address != null) correctAddress = address;
                        if (city != null) correctCity = city;
                        if (state != null) correctState = state;
                        if (country != null) correctCountry = country;
                        if (knownName != null) correctKnownName = knownName;

                        LocationName = correctAddress + " " + correctCity;
                        LocationAddress = correctCountry + " " + correctState + " " + correctKnownName;
                        SearchFieldText = correctAddress + " " + correctCity + ", " + correctCountry;

                        pickup_location_source.setText(SearchFieldText);
                        // search driver again on camera updated position
                        session.createSourceLocation(LocationName, LocationAddress, String.valueOf(lat_camera), String.valueOf(long_camera));
                        // session.createSourceLocation(LocationName, LocationAddress, String.valueOf(lat_camera), String.valueOf(long_camera));
                        startPoint = new LatLng(lat_camera, long_camera);
                    } else if (addresses.size() <= 0) {
                        pickup_location_source.setText(getResources().getString(R.string.pick_up_location));
                    }
                }
            }


            super.onPostExecute(aVoid);
        }
    }

    private void getManualAddressUsingApi(final double lat_camera, final double long_camera) {
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat_camera + "," + long_camera + "&sensor=true&language=hi";
        StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsobObject = new JSONObject(response);

                    String jsonArray = (jsobObject.getJSONArray("results")).getJSONObject(0).getString("formatted_address").toString();
                    pickup_location_source.setText(jsonArray);
                    // search driver again on camera updated position
                    session.createSourceLocation(jsonArray, "", String.valueOf(lat_camera), String.valueOf(long_camera));
                    System.out.println("");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        EApplication.getInstance().addToRequestQueue(req, "high");

    }


}








