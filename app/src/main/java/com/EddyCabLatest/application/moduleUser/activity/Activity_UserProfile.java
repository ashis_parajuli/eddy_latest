package com.EddyCabLatest.application.moduleUser.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.SelectLanguage;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.FileUtils;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CircularImageView;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by newimac on 6/16/15.
 */
public class Activity_UserProfile extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, View.OnTouchListener {

    public static final String TAG = Activity_UserProfile.class.getSimpleName();

    private ImageView navBackPress;
    private ImageView ChangeProfilePictureView;
    private LinearLayout changeLanguage;
    private TextView txt_user_name;
    private TextView txt_user_email;
    private TextView txt_user_mobile;
    private TextView txt_user_passcode;
    private TextView txt_user_home;
    private TextView txt_user_work;

    private CircularImageView profile_Image;
    ImageView image_setting;
    SessionManager session;
    EApplication application;
    String UserID;
    Button txt_logout;
    private boolean isSpinnerTouched = false;


    String USER_NAME;
    String USER_Email;
    String USER_MOBILE;
    String USER_FAV_HOME;
    String USER_FAV_WORK;
    String USER_REFERRAL_CODE;
    String USER_COUNTRY_CODE;
    String profileImgPath = "";
    long totalSize = 0;
    private static final int SAMPLE_REQUEST_00 = 0;
    boolean imageisUploaded = false;
    private TextView edit_profile;
    private String USER_PASSWORD;
    private String profileImgPath_selected;
    private String id;
    private String name;
    private String email;
    private String mobile;
    private String license;

    private String image;
    private String referralCode;
    private TextView language_text;
    private String newLanguage;
    private Bitmap bm;
    private String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user_profile);

         /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);

        application = EApplication.getInstance();

        session = new SessionManager(Activity_UserProfile.this);

        Log.e("Package Name :: ", Activity_UserProfile.this.getPackageName());

        /* Reference Action Event */
        navBackPress = (ImageView) findViewById(R.id.img_back_arrow);

        navBackPress.setOnClickListener(this);


        HashMap<String, String> userDetails = session.getUserDetails();

        id = userDetails.get("id");
        name = userDetails.get("name");
        email = userDetails.get("email");
        mobile = userDetails.get("mobile_no");
        license = userDetails.get("license_no");
        image = userDetails.get("picture");
        referralCode = userDetails.get("referral_code");


        ChangeProfilePictureView = (ImageView) findViewById(R.id.changeProfilePic);
        /*ChangeProfilePictureView.setOnClickListener(this);*/

        ChangeProfilePictureView.setOnClickListener(this);


        txt_logout = (Button) findViewById(R.id.txt_logout);
        language_text = (TextView) findViewById(R.id.languageText);
        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DoYouWantToLogOutDiloge();

            }
        });

        changeLanguage = (LinearLayout) findViewById(R.id.changeLanguage);

        changeLanguage.setOnClickListener(this);
        String language;
        if (session.getAppLanguage().equals(Config.LANG_ENG)) {
            language_text.setText(getResources().getString(R.string.nepali_language));
        } else {
            language_text.setText(getResources().getString(R.string.english));
        }

        edit_profile = (TextView) findViewById(R.id.Edit);
        edit_profile.setOnClickListener(this);
        /* References for value set */
        profile_Image = (CircularImageView) findViewById(R.id.image_profile);
        txt_user_name = (TextView) findViewById(R.id.nameLayoutView);

        txt_user_email = (TextView) findViewById(R.id.emailLayoutView);

        txt_user_mobile = (TextView) findViewById(R.id.mobileLayoutView);
        txt_user_passcode = (TextView) findViewById(R.id.passCodeLayoutView);
        txt_user_home = (TextView) findViewById(R.id.userHomeAddress);
        txt_user_home.setOnClickListener(this);
        txt_user_work = (TextView) findViewById(R.id.userWorkAddress);
        txt_user_work.setOnClickListener(this);

        txt_user_name.setText(name);
        txt_user_email.setText(email);
        txt_user_mobile.setText(mobile);

        if (image != null) {

            Picasso.with(Activity_UserProfile.this)
                    .load(Config.APP_BASE_URL + image)
                    .placeholder(R.drawable.contact_avatar)   // optional
                    .error(R.drawable.contact_avatar)         // optional
                    .resize(100, 100)                   // optional
                    .rotate(0)                          // optional
                    .into(profile_Image);
        }
        //Update_UserDetails();
    }


    private void DoYouWantToLogOutDiloge() {

        final Dialog dialog = new Dialog(Activity_UserProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reservation_bidding_dailog);
        dialog.setCancelable(true);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);

        TextView msgContentBidding = (TextView) dialog.findViewById(R.id.msgContentBidding);
        msgContentBidding.setText(R.string.want_to_logout);
        msgContentBidding.setTextColor(getResources().getColor(R.color.secondaryTextColor));

        Button btnActionSubmit = (Button) dialog.findViewById(R.id.btnActionSubmit);
        btnActionSubmit.setText(R.string.yes);
        btnActionSubmit.setTextColor(getResources().getColor(R.color.whiteColor));
        btnActionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                methodCallLogoutApi();

            }
        });
        Button btnActionCancel = (Button) dialog.findViewById(R.id.btnActionCancel);

        btnActionCancel.setText(R.string.not_now);
        btnActionCancel.setTextColor(getResources().getColor(R.color.whiteColor));
        btnActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }


        });

        dialog.show();


    }

    private void Update_UserDetails() {

        HashMap<String, String> userDataList = session.getUserDetails();
        UserID = userDataList.get(SessionManager.KEY_ID);

        String URL_GET_PROFILE = Config.APP_BASE_URL + Config.GET_USER_PROFILE + UserID;
//        if (NetworkUtils.isNetworkAvailable(Activity_UserProfile.this)) {
        CustomRequest req = new CustomRequest(Request.Method.GET, URL_GET_PROFILE, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseObject) {
                        Log.e("Response 1st API", responseObject.toString());


                        try {
                            boolean error = responseObject.getBoolean("error");

                            if (!error) {

                                try {

                                    Log.e("JSON DATA :: ", responseObject.toString());

                                    USER_NAME = responseObject.getJSONObject("User").getString("fullname");
                                    USER_Email = responseObject.getJSONObject("User").getString("email");
                                    USER_COUNTRY_CODE = responseObject.getJSONObject("User").getString("country_code");
                                    USER_MOBILE = responseObject.getJSONObject("User").getString("mobile_number");
                                    String USER_PROFILE_IMAGE = responseObject.getJSONObject("User").getString("user_pp");
                                    USER_REFERRAL_CODE = responseObject.getJSONObject("User").getString("referral_code");
                                    USER_PASSWORD = responseObject.getJSONObject("User").getString("password");

                                    if (!responseObject.get("Fav_home").toString().equals("null") && !responseObject.get("Fav_work").toString().equals("null") &&
                                            responseObject.get("Fav_home") != null && responseObject.get("Fav_work") != null) {
                                        String HOME_TITLE = responseObject.getJSONObject("Fav_home").getJSONObject("favourite").getString("title");
                                        String HOME_ADDRESS = responseObject.getJSONObject("Fav_home").getJSONObject("favourite").getString("address");

                                        String WORK_TITLE = responseObject.getJSONObject("Fav_work").getJSONObject("favourite").getString("title");
                                        String WORK_ADDRESS = responseObject.getJSONObject("Fav_work").getJSONObject("favourite").getString("address");
                                        USER_FAV_HOME = HOME_TITLE + " , " + HOME_ADDRESS;
                                        USER_FAV_WORK = WORK_TITLE + " , " + WORK_ADDRESS;

                                        txt_user_home.setText(USER_FAV_HOME);
                                        txt_user_work.setText(USER_FAV_WORK);
                                    }
                                    // set data to reference fields
                                    txt_user_name.setText(USER_NAME);
                                    txt_user_email.setText(USER_Email);
                                    txt_user_mobile.setText(USER_COUNTRY_CODE + USER_MOBILE);
                                    txt_user_passcode.setText("XXXXX");

                                 /*   txt_referral_value.setText(USER_REFERRAL_CODE);*/
                                    session.createupdateuserdata(USER_NAME, USER_Email, USER_MOBILE, USER_PROFILE_IMAGE);
                                    //setImage
                                    Picasso.with(Activity_UserProfile.this)
                                            .load(Config.APP_BASE_URL + USER_PROFILE_IMAGE)
                                            .placeholder(R.drawable.contact_avatar)   // optional
                                            .error(R.drawable.contact_avatar)         // optional
                                            .resize(100, 100)                   // optional
                                            .rotate(0)                          // optional
                                            .into(profile_Image);

                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                            } else
                                application.showToastMessageFunction("not able to get User Detail");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
            }
        }) {


            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.e("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);
                return headers;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        application.addToRequestQueue(req, "high");

        /*} else {
            NetworkUtils.showNoConnectionDialog(Activity_UserProfile.this);
        }*/
    }

    //checking if user touches spinner or not .......
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        isSpinnerTouched = true;

        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.transparent));

        if (isSpinnerTouched) {
            if (position == 0) {
                //called method to change Language
            } else if (position == 1) {
            }
        }
        isSpinnerTouched = false;


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        Update_UserDetails();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.Edit:
                Intent edit_intent = new Intent(Activity_UserProfile.this, Update_USerProfile.class);


                startActivity(edit_intent);
                finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                break;

            case R.id.img_back_arrow:
                Intent intent = new Intent(Activity_UserProfile.this, Activity_UserMain.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                break;
          /*  case R.id.nameLayoutView:

                Intent nameIntent = new Intent(Activity_UserProfile.this, Update_UserName.class);
                nameIntent.putExtra("IDENTIFIER", txt_user_name.getText());
                startActivity(nameIntent);
                break;*/

            case R.id.emailLayoutView:

                Intent emailIntent = new Intent(Activity_UserProfile.this, Update_USerProfile.class);
                emailIntent.putExtra("IDENTIFIER", txt_user_email.getText());
                startActivity(emailIntent);
                break;

            case R.id.changeProfilePic:
                showImageChooserAlertDialog();


                break;

            case R.id.changeLanguage:
                String next_lang = "";
                String current_lang = session.getAppLanguage();

                if (current_lang.equalsIgnoreCase("Thai")) {
                    next_lang = "en";
                } else {
                    next_lang = "np";
                }
                methodChangeLanguageApi(next_lang);

                break;

            default:
                break;
        }
    }

    private void methodChangeLanguageApi(String nextLanguage) {
        if (NetworkUtils.isNetworkAvailable(Activity_UserProfile.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserProfile.this);
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            CustomProgressBarDialog.progressDialog.show();

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", UserID);
            params.put("language", nextLanguage);

            Log.e("parameters", params.toString());

            CustomRequest changeLanguageRequest = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.CHANGE_LANGUAGE, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("response>>", response.toString());

                            try {
                                boolean error = response.getBoolean("error");

                                newLanguage = response.getString("language");
                                if (!error) {


                                    String setLanguage;
                                    if (newLanguage.equalsIgnoreCase("en")) {
                                        language_text.setText(getResources().getString(R.string.nepali_nepali));
                                        setLanguage = Config.LANG_ENG;
                                    } else

                                    {
                                        language_text.setText(getResources().getString(R.string.english));
                                        setLanguage = Config.LANG_THAI;
                                    }

                                    session.createLanguageSession(setLanguage);

                                    application.setLang(newLanguage);

                                    Intent refresh = new Intent(Activity_UserProfile.this, Activity_UserProfile.class);
                                    startActivity(refresh);//Start the same Activity
                                    finish(); //finish Activity.

                                } else {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            CustomProgressBarDialog.progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EApplication.getInstance().showToastMessageFunction("Error Response on Changing Language");
                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }
            };

            changeLanguageRequest.setRetryPolicy(new DefaultRetryPolicy(
                    60000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(changeLanguageRequest, "high");

        } else {
            NetworkUtils.showNoConnectionDialog(Activity_UserProfile.this);
        }

    }

    private void methodCallLogoutApi() {
        if (NetworkUtils.isNetworkAvailable(Activity_UserProfile.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserProfile.this);
            CustomProgressBarDialog.progressDialog.show();

            CustomRequest customRequest = new CustomRequest(Request.Method.GET,
                    Config.APP_BASE_URL + Config.USER_LOGOUT + UserID, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Response", response.toString());
                    try {
                        boolean error = response.getBoolean("error");
                        String message = response.getString("message");
                        if (!error) {
                            EApplication.getInstance().showToastMessageFunction(message);
                            CustomProgressBarDialog.progressDialog.dismiss();
                            session.clearUserDetails();
                            session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                            session.clearReservationType();

                            Intent intent = new Intent(Activity_UserProfile.this, SelectLanguage.class);
                            startActivity(intent);
                            finish();
                        } else {
                            EApplication.getInstance().showToastMessageFunction(message);
                            CustomProgressBarDialog.progressDialog.dismiss();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EApplication.getInstance().showToastMessageFunction("errorResponse on Logout");
                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {


                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            EApplication.getInstance().addToRequestQueue(customRequest, "high");


        } else {
            NetworkUtils.showNoConnectionDialog(Activity_UserProfile.this);
        }

    }

    // Show Alert Dialog chooser
    public void showImageChooserAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.choose_picturemethod))
                .setCancelable(true)
                .setPositiveButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, 103);
                            }
                        })
                .setNegativeButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, 101);
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    // For Activity Result
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        Uri selectedImage = null;
        String iconsStoragePath = "";
        File sdIconStorageDir = null;


        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {


            case 101:
                if (resultCode == RESULT_OK) {

                    selectedImage = imageReturnedIntent.getData();

                    // Get the path
                    try {
                        profileImgPath_selected = FileUtils.getPath(this, selectedImage);

                        if (profileImgPath_selected != null) {
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                            iconsStoragePath = Environment.getExternalStorageDirectory() + "/img/";

                            sdIconStorageDir = new File(iconsStoragePath);
                            if (sdIconStorageDir.exists())

                            {
                                sdIconStorageDir.delete();
                            }
                            // create storage directories, if they don't exist
                            sdIconStorageDir.mkdir();

                            profileImgPath = iconsStoragePath + "imageProfile_user.png";
                            File sFile = new File(profileImgPath_selected);

                            File dFile = new File(profileImgPath);
                            // Copy File one place to another
                            try {
                                FileUtils.copyFile(sFile, dFile);
                                callUploadImageClass();
                                //profileImgPath = iconsStoragePath;
                            } catch (Exception e) {
                                Log.e("Image Directory  error", e + "");

                            }


                        } else {

                            Toast.makeText(Activity_UserProfile.this, getResources().getString(R.string.profile_is_empty), Toast.LENGTH_SHORT).show();
                        }


                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                break;

            case 103:
                try {

                    bm = (Bitmap) imageReturnedIntent.getExtras().get("data");


                    if (bm != null) {    // profile_Image.setImageBitmap(bm);

                        boolean status = storeImage(bm, "imageProfile_user.png");
                        iconsStoragePath = Environment.getExternalStorageDirectory() + "/img/";

                        sdIconStorageDir = new File(iconsStoragePath);

                        // create storage directories, if they don't exist
                        sdIconStorageDir.mkdirs();


                        profileImgPath = iconsStoragePath + "imageProfile_user.png";
                        callUploadImageClass();
                    } else {

                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.pathnotfound));
                    }

                } catch (Exception e) {

                }
                break;

        }


    }


    private void callUploadImageClass() {
        Log.e("Image Directory :: ", profileImgPath);

        if (NetworkUtils.isNetworkAvailable(Activity_UserProfile.this))
            UploadFileToServer();
        else
            NetworkUtils.showNoConnectionDialog(Activity_UserProfile.this);
    }

    private void UploadFileToServer() {
        if (profileImgPath == null)

        {
            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));

        } else {
            HashMap<String, String> userDataList = session.getUserDetails();
            UserID = userDataList.get(SessionManager.KEY_ID);
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(this);
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            CustomProgressBarDialog.progressDialog.show();

            try {
                Ion.with(this)
                        .load("POST", Config.APP_BASE_URL + Config.UPDATE_USER_PROFILE_CHANGE_PROFILE_PIC)
                        .setTimeout(60 * 60 * 1000)
                        .setMultipartParameter("user_id", UserID)
                        .setMultipartFile("user_pp", "application/zip", new File(profileImgPath))
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                CustomProgressBarDialog.progressDialog.dismiss();

                                if ((e != null) && (result == null)) {
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                                } else {
                                    try {


                                        JSONObject resultObject = new JSONObject(result);
                                        // showing the server response in an alert dialog
                                        boolean error = resultObject.getBoolean("error");
                                        if (!error) {
                                            Update_UserDetails();
                                        } else {
                                            EApplication.getInstance().showToastMessageFunction(resultObject.getString("message"));
                                        }
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                }


                            }
                        });

            } catch (Exception e) {
                e.printStackTrace();
                CustomProgressBarDialog.progressDialog.dismiss();
            }


        }


    }

    /**
     * Uploading the file to server
     */


    // Save Image on SD card
    private boolean storeImage(Bitmap imageData, String filename) {
        // get path to external storage (SD card)
        String iconsStoragePath = Environment.getExternalStorageDirectory() + "/img/" + filename;
        File sdIconStorageDir = new File(iconsStoragePath);

        // create storage directories, if they don't exist
        //   sdIconStorageDir.mkdirs();

        try {
            String filePath = sdIconStorageDir.toString() + filename;
            FileOutputStream fileOutputStream = new FileOutputStream(iconsStoragePath);
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            // choose another format if PNG doesn't suit you
            imageData.compress(Bitmap.CompressFormat.PNG, 100, bos);
            bos.flush();
            bos.close();

        } catch (FileNotFoundException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Intent intent = new Intent(Activity_UserProfile.this, Activity_UserMain.class);
                startActivity(intent);
                this.finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Activity_UserProfile.this, Activity_UserMain.class);
        startActivity(intent);
        this.finish();
        overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
    }
}
