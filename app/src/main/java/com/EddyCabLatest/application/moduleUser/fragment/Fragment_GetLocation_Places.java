package com.EddyCabLatest.application.moduleUser.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.activity.Activity_PickupLocation;
import com.EddyCabLatest.application.moduleUser.adapter.AdapterFrequentSearch;
import com.EddyCabLatest.application.moduleUser.adapter.ExpandableListAdapter;
import com.EddyCabLatest.application.moduleUser.modal.ExpandableListChild;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.NewGPSTracker;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_FREQUENT_SEARCH;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME;


/**
 * Created by newimac on 7/6/15.
 */
public class Fragment_GetLocation_Places extends Fragment {
    private EApplication app;
    private NewGPSTracker gpsTracker;
    private int radius = 6000;
    private String type = "";
    EditText searchEditText;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<ExpandableListChild> recentPlaces;
    HashMap<String, List<ExpandableListChild>> listDataChild;
    ArrayList<HashMap<String, String>> searchDataList;
    List<ExpandableListChild> nearByPlaces;
    private ListView listView;
    private TextView emptyMessage;
    boolean visible = false;
    String currentLocationLatitude;
    String currentLocationLongitude;
    private ArrayList<HashMap<String, String>> dataListArrayFormPlaces;
    private SessionManager session;
    private String language;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.getlocation_fragment_places, container, false);
        setupUI(rootView.findViewById(R.id.main_parent));
        listView = (ListView) rootView.findViewById(R.id.googleSearchList);
        emptyMessage = (TextView) rootView.findViewById(android.R.id.empty);
        searchEditText = (EditText) rootView.findViewById(R.id.searchEditText);
        expListView = (ExpandableListView) rootView.findViewById(R.id.expandableListView);
        ExpandableDataListPrepare();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        searchEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        session = new SessionManager(getActivity());

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("===", "Character sequence is :" + s.toString());
                gpsTracker = new NewGPSTracker(getActivity());
                if (gpsTracker.canGetLocation()) {
                    if (count > 0) {
                        // performSearch(s.toString());
                        expListView.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        currentLocationLatitude = Double.toString(gpsTracker.getLatitude());
                        currentLocationLongitude = Double.toString(gpsTracker.getLongitude());
                        performPlacesSearch(s.toString(), currentLocationLatitude, currentLocationLongitude);
                    } else {
                        listView.setVisibility(View.GONE);
                        expListView.setVisibility(View.VISIBLE);
                        ExpandableDataListPrepare();
                    }
                } else {
                    gpsTracker.showSettingsAlert();
                    Log.e("====", "get location places onTextChage");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ((Activity_PickupLocation) getActivity()).setRequestAddress(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).getName(),
                        listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).getAddress(),
                        listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).getLatitude(),
                        listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).getLongitude());
                return false;
            }
        });

        return rootView;
    }

    private void performPlacesSearch(String query, String currentLocationLatitude, String currentLocationLongitude) {
        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            String placesRequestUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=" + Config.GOOGLE_MAP_API_KEY + "&region=np" + "&language=hi" + "&radius=" + radius + "&sensor=false&location=" + currentLocationLatitude + "," + currentLocationLongitude + "&keyword=" + query;
            Log.e("===", "PlaceSearchUrl : " + placesRequestUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, placesRequestUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dataListArrayFormPlaces = new ArrayList<>();
                    try {
                        JSONObject objectData = new JSONObject(response);
                        Log.e("", "Response is : " + response.toString());
                        JSONArray dataArray = objectData.getJSONArray("results");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject arrayObjectData = dataArray.getJSONObject(i);
                            String name = arrayObjectData.getString("name");
                            String vicinity = arrayObjectData.getString("vicinity");
                            JSONObject geoLocation = arrayObjectData.getJSONObject("geometry");
                            String geometry_lat = geoLocation.getJSONObject("location").getString("lat");
                            String geometry_lng = geoLocation.getJSONObject("location").getString("lng");
                            /* Fields Param */


                            String correctAddress = "", correctName = "", correctLat = "", correctLang = "";
                            if (name != null) correctName = name;
                            if (vicinity != null) correctAddress = vicinity;
                            if (geometry_lat != null) correctLat = geometry_lat;
                            if (geometry_lng != null) correctLang = geometry_lng;
                            HashMap<String, String> mapData = new HashMap<>();
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES, "icon_favourite_incative");
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, correctName);
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, correctAddress);
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, correctLat);
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, correctLang);
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, "0 KM");
                            dataListArrayFormPlaces.add(mapData);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("===", "Total data is " + dataListArrayFormPlaces.toString());

                    AdapterFrequentSearch adapter = new AdapterFrequentSearch(getActivity(), dataListArrayFormPlaces);
                    listView.setAdapter(adapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ((Activity_PickupLocation) getActivity()).setRequestAddress(
                                    dataListArrayFormPlaces.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME),
                                    dataListArrayFormPlaces.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS),
                                    dataListArrayFormPlaces.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT),
                                    dataListArrayFormPlaces.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));
                        }
                    });
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            })  {


                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            EApplication.getInstance().addToRequestQueue(stringRequest, "High");
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        }
    }

    @Override
    public void onResume() {
        visible = true;
        super.onResume();
    }

    @Override
    public void onPause() {
        visible = false;
        searchEditText.setText("");

        super.onPause();
    }

    private void performSearch(String query) {
        expListView.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        if (NetworkUtils.isNetworkAvailable(getActivity()) && query.length() > 0) {
            String requestURL = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + query + "&key=" + Config.GOOGLE_MAP_API_KEY;
            Log.e("===", "Request Url is :" + requestURL);
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("===", "Response is: " + response);
                    searchDataList = new ArrayList<>();
                    try {
                        JSONObject objectData = new JSONObject(response);
                        JSONArray dataArray = objectData.getJSONArray("results");
                        for (int i = 0; i < dataArray.length(); i++) {
                            String name = dataArray.getJSONObject(i).getString("name");
                            String formattedAddress = dataArray.getJSONObject(i).getString("formatted_address");
                            String geometry_lat = dataArray.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getString("lat");
                            String geometry_lng = dataArray.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getString("lng");
                            HashMap<String, String> mapData = new HashMap<>();
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, name);
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, formattedAddress);
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, geometry_lat);
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, geometry_lng);
                            mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, "1.0 KM");
                            searchDataList.add(mapData);
                        }

                        Log.e("Array Data", searchDataList.toString());
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }


                    AdapterFrequentSearch adapter = new AdapterFrequentSearch(getActivity(), searchDataList);
                    listView.setAdapter(adapter);
                    listView.setEmptyView(emptyMessage);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ((Activity_PickupLocation) getActivity()).setRequestAddress(
                                    searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME),
                                    searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS),
                                    searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT),
                                    searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));
                        }
                    });

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error: ", error + "");
                }
            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Add the request to the RequestQueue.
            EApplication.getInstance().addToRequestQueue(stringRequest, "High");
        }

    }
    /*

     private void performSearch(String query) {
     expListView.setVisibility(View.GONE);
     listView.setVisibility(View.VISIBLE);
     Log.e("where:::", "here");
     searchDataList = new ArrayList<>();
     if (nearByPlaces.size() > 0) {
     for (int i = 0; i < nearByPlaces.size(); i++) {
     if (nearByPlaces.get(i).getName().toLowerCase().contains(query.toLowerCase())) {
     HashMap<String, String> mapData = new HashMap<>();
     mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES, "icon_favourite_incative");
     mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, nearByPlaces.get(i).getName());
     mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, nearByPlaces.get(i).getAddress());
     mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, nearByPlaces.get(i).getLatitude());
     mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, nearByPlaces.get(i).getLongitude());
     mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, "0 KM");

     searchDataList.add(mapData);
     Log.e("where:::", "here1");
     }
     }
     }
     AdapterFrequentSearch adapter = new AdapterFrequentSearch(getActivity(), searchDataList);
     listView.setAdapter(adapter);
     listView.setEmptyView(emptyMessage);
     listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
     @Override
     public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

     ((Activity_PickupLocation) getActivity()).setRequestAddress(searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME),
     searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS),
     searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT),
     searchDataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));
     }
     });
     Log.e("where:::", "here2");

     }
     */

    private void ExpandableDataListPrepare() {
        expListView.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        /*
         * Preparing the list data
         */
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding child data
        listDataHeader.add("Recent Places");
        listDataHeader.add("Nearby Places");

        recentPlaces = new ArrayList<>();
        ArrayList<HashMap<String, String>> recentDataList = EApplication.getInstance().getAllToList(DBTableFields.TABLE_RECENT_SEARCH);
        for (int i = 0; i < recentDataList.size(); i++) {
            String image;
            String name = recentDataList.get(i).get(DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_NAME);
            String address = recentDataList.get(i).get(DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_ADDRESS);
            String latitude = recentDataList.get(i).get(DBTableFields.TABLE_RECENT_SEARCH_LAT);
            String longitude = recentDataList.get(i).get(DBTableFields.TABLE_RECENT_SEARCH_LANG);
            if (EApplication.getInstance().CheckIsDataAlreadyInDBorNotWithNameAndAddress(TABLE_FREQUENT_SEARCH, TABLE_FREQUENT_SEARCH_ADDRESS_NAME,
                    TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, name, address)) {
                image = "icon_favourite_active";
            } else {
                image = "icon_favourite_incative";
            }
            recentPlaces.add(new ExpandableListChild(image, name, address, latitude, longitude, ""));
        }

        nearByPlaces = new ArrayList<ExpandableListChild>();

        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, requestUrl(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Response is: ", response);

                    try {
                        JSONObject objectData = new JSONObject(response);
                        JSONArray dataArray = objectData.getJSONArray("results");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject arrayObjectData = dataArray.getJSONObject(i);
                            String name = arrayObjectData.getString("name");
                            String vicinity = arrayObjectData.getString("vicinity");
                            JSONObject geoLocation = arrayObjectData.getJSONObject("geometry");
                            String geometry_lat = geoLocation.getJSONObject("location").getString("lat");
                            String geometry_lng = geoLocation.getJSONObject("location").getString("lng");
                            String image = "icon_favourite_incative";
                            if (EApplication.getInstance().CheckIsDataAlreadyInDBorNotWithNameAndAddress(TABLE_FREQUENT_SEARCH, TABLE_FREQUENT_SEARCH_ADDRESS_NAME, TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, name, vicinity)) {
                                image = "icon_favourite_active";
                            } else {
                                image = "icon_favourite_incative";
                            }
                            nearByPlaces.add(new ExpandableListChild(image, name, vicinity, geometry_lat, geometry_lng, ""));
                        }

                        listDataChild.put(listDataHeader.get(0), recentPlaces);
                        listDataChild.put(listDataHeader.get(1), nearByPlaces);

                        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
                        expListView.setAdapter(listAdapter);

                        expListView.expandGroup(0);
                        expListView.expandGroup(1);
                        //                        expListView.setItemChecked(1, true);
                        //                        expListView.setSelectedChild(1, 1, true);


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error: ", error + "");
                }
            }) {
                @Override
                public Priority getPriority() {
                    return Priority.HIGH;
                }
            };
            // Add the request to the RequestQueue.
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(stringRequest, "High");


        } else {
            Toast.makeText(getActivity(), "No Network, Couldn't load nearby location !", Toast.LENGTH_LONG).show();
        }


    }

    private String requestUrl() {
        app = EApplication.getInstance();
        gpsTracker = new NewGPSTracker(getActivity());
        String NEARBY_URL = app.getNearByURL(gpsTracker.getLatitude(), gpsTracker.getLongitude(), radius, type);
        Log.e("Response:", NEARBY_URL);
        return NEARBY_URL;
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    EApplication.getInstance().hideSoftKeyboard(getActivity());
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

}
