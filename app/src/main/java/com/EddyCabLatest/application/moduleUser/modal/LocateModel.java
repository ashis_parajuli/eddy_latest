package com.EddyCabLatest.application.moduleUser.modal;

/**
 * Created by bikram on 4/7/16.
 */
public class LocateModel {


public static String  driver_name;
public static String  driver_profile;
public static String  driver_redg;
public static String  driver_id;
public static String  driver_mobile;

    public LocateModel() {

    }

    public static String getDriver_name() {
        return driver_name;
    }

    public static void setDriver_name(String driver_name) {
        LocateModel.driver_name = driver_name;
    }

    public static String getDriver_profile() {
        return driver_profile;
    }

    public static void setDriver_profile(String driver_profile) {
        LocateModel.driver_profile = driver_profile;
    }

    public static String getDriver_redg() {
        return driver_redg;
    }

    public static void setDriver_redg(String driver_redg) {
        LocateModel.driver_redg = driver_redg;
    }

    public static String getDriver_id() {
        return driver_id;
    }

    public static void setDriver_id(String driver_id) {
        LocateModel.driver_id = driver_id;
    }

    public static String getDriver_mobile() {
        return driver_mobile;
    }

    public static void setDriver_mobile(String driver_mobile) {
        LocateModel.driver_mobile = driver_mobile;
    }
}
