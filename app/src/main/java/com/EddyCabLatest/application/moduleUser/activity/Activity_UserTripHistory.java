package com.EddyCabLatest.application.moduleUser.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CircularImageView;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_UserTripHistory extends AppCompatActivity implements View.OnClickListener, RoutingListener,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    EApplication app;
    SessionManager session;
    public String jsonObject;

    TextView txt_status;
    TextView txt_total_fare;
    /*TextView fee_bhat;*/
    TextView eta_time;
    ImageView txt_need_help;
    TextView txt_driver_no;
    TextView txt_reg_no;
    TextView txt_driver_name;
    CircularImageView img_driver_image;
    Toolbar app_toolBar;
    ImageView img_cancel_reservation;


    //variables added to draw google map and Route
    private MapView mapView;
    private GoogleMap map;
    private LatLng startPoint;
    private LatLng endPoint;
    private Polyline polyline;

    String status;
    String ridefare;
    String totalfare;
    String driverName;
    String driverId;
    String userId;
    String reservationId;
    String driverImage;


    float cameraZoom = 15;
    int padding;
    String null_status = "";
    String updated_driver_id = "";
    String TAG = "===";
    private int eta_timevalue = 0;
    private TextView address_txt;
    private String address;
    private String userMobileNumber;
    private boolean visible = false;
    private Dialog dialog;
    private TextView text_eta_time;
    private String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_user_profile);

        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);

        //Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);

        // Initializing Application Instances
        session = new SessionManager(Activity_UserTripHistory.this);
        app = EApplication.getInstance();


        //getting intent from User_FragmentJobHistory Fragment
        Intent intent = getIntent();
        String date_time = intent.getStringExtra("trip_time");
        String pickup_location = intent.getStringExtra("from");
        String dropoff_location = intent.getStringExtra("to");
        status = intent.getStringExtra("status");
        Log.e(TAG, "Status:" + status);
        ridefare = intent.getStringExtra("ride_fare");

        eta_timevalue = intent.getIntExtra("eta-time", 0);
        driverName = intent.getStringExtra("driver_name");
        driverId = intent.getStringExtra("driver_id");
        userId = intent.getStringExtra("user_id");
        address = intent.getStringExtra("address");
        userMobileNumber = intent.getStringExtra("mobile");
        Log.e("driverno", driverId);

        if (!TextUtils.isEmpty(status) && !TextUtils.equals(status, "null")) {
            null_status = status;
        } else {
            null_status = getResources().getString(R.string.not_null);
            eta_time.setText(getResources().getString(R.string.not_null));

        }


        if (!TextUtils.isEmpty(driverId) && !TextUtils.equals(driverId, "null")) {

            updated_driver_id = driverId;

        } else {
            updated_driver_id = getResources().getString(R.string.not_null);

        }

        reservationId = intent.getStringExtra("reservation_id");
        driverImage = intent.getStringExtra("driver_image");

        String pickupLat = intent.getStringExtra("pickup_lat");
        Double pickLat = Double.parseDouble(pickupLat);

        String pickupLong = intent.getStringExtra("pickup_long");
        Double pickLong = Double.parseDouble(pickupLong);

        startPoint = new LatLng(pickLat, pickLong);

        String dropoffLat = intent.getStringExtra("dropoff_lat");
        Double dropLat = Double.parseDouble(dropoffLat);

        String dropOffLong = intent.getStringExtra("dropoff_long");
        Double dropLang = Double.parseDouble(dropOffLong);
        endPoint = new LatLng(dropLat, dropLang);


        // Map view Init Section
        mapView = (MapView) findViewById(R.id.content_map_view);
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        map = mapView.getMap();
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        MapsInitializer.initialize(Activity_UserTripHistory.this);

        Routing routing = new Routing(Routing.TravelMode.WALKING);
        routing.registerListener(Activity_UserTripHistory.this);
        routing.execute(startPoint, endPoint);

        //initializing Views from layout
        initViews();

    }

    private void initViews() {
        txt_status = (TextView) findViewById(R.id.txt_status);
        txt_total_fare = (TextView) findViewById(R.id.txt_total_fare);
        eta_time = (TextView) findViewById(R.id.txt_text1);
 /*       fee_bhat = (TextView) findViewById(R.id.txt_ridefare);*/
        txt_need_help = (ImageView) findViewById(R.id.txt_need_help);
        txt_driver_no = (TextView) findViewById(R.id.txt_driver_no);
        txt_reg_no = (TextView) findViewById(R.id.txt_reg_no);
        address_txt = (TextView) findViewById(R.id.address_driver);
        txt_driver_name = (TextView) findViewById(R.id.txt_driver_name);
        text_eta_time = (TextView) findViewById(R.id.txt_eta_time);
        img_driver_image = (CircularImageView) findViewById(R.id.img_driver_image);
        img_cancel_reservation = (ImageView) findViewById(R.id.img_cancel_reservation);
        img_cancel_reservation.setOnClickListener(this);

        if (status.equalsIgnoreCase("cancelled") || status.equalsIgnoreCase("completed")) {
            img_cancel_reservation.setVisibility(View.GONE);
        } else {
            img_cancel_reservation.setVisibility(View.VISIBLE);
        }

        txt_need_help.setOnClickListener(this);
        txt_status.setText(null_status);
        if (null_status.equalsIgnoreCase("cancelled")) {
            txt_status.setTextColor(getResources().getColor(R.color.redColor));

        }
        if (null_status.equalsIgnoreCase("completed")) {
            txt_status.setTextColor(getResources().getColor(R.color.green));
        }
        if (null_status.equalsIgnoreCase("driver_confirmed")) {

            img_cancel_reservation.setVisibility(View.INVISIBLE);
            txt_status.setTextColor(getResources().getColor(R.color.purple));
        }
        if (null_status.equalsIgnoreCase("onroute")) {

            img_cancel_reservation.setVisibility(View.INVISIBLE);
            txt_status.setTextColor(getResources().getColor(R.color.purple));
        }


        txt_driver_name.setText(driverName);

        txt_reg_no.setText(reservationId);
        address_txt.setText(address);
/*eta_time.setText(eta_timevalue);*/
        //need  to change
        if (!TextUtils.isEmpty(driverImage)) {

            Picasso.with(this)
                    .load(Config.APP_BASE_URL + driverImage)
                    .placeholder(R.drawable.contact_avatar)   // optional
                    .error(R.drawable.contact_avatar)         // optional
                    .resize(80, 80)                        // optional
                    .rotate(0)                             // optional
                    .into(img_driver_image);
        }
    }

    @Override
    public void onResume() {
        visible = true;
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        visible = false;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_cancel_reservation:
                confirmReservationCancelDialog();
                break;
            case R.id.txt_need_help:
                methodcall();
                break;

            default:
                break;
        }
    }

    private void methodcall() {
        String number = "tel:" + userMobileNumber.trim();

        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        startActivity(callIntent);

    }

    private void confirmReservationCancelDialog() {
        dialog = new Dialog(Activity_UserTripHistory.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reservation_cance_dialog_cancel);
        dialog.setCancelable(true);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);


        TextView txtMessage = (TextView) dialog.findViewById(R.id.txt_message);
        txtMessage.setText(getResources().getString(R.string.wanto_cancelReservation));


        Button btnOk = (Button) dialog.findViewById(R.id.btnOK);
        Button btnno = (Button) dialog.findViewById(R.id.no);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cancelUserReservationApi();
            }
        });
        if (visible) dialog.show();


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                cancelUserReservationApi();


            }
        });

        btnno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void cancelUserReservationApi() {

        if (NetworkUtils.isNetworkAvailable(Activity_UserTripHistory.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserTripHistory.this);
            CustomProgressBarDialog.progressDialog.show();

            final HashMap<String, String> params = new HashMap<>();
            params.put("user_id", userId);
            params.put("reservation_id", reservationId);

            Log.e("cancel", params.toString());
            CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.API_RESERVATION_CANCEL, params,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject responseObject) {
                            Log.e("Response 1st API", responseObject.toString());

                            try {
                                boolean error = responseObject.getBoolean("error");

                                if (!error) {
                                    if (dialog != null) {
                                        dialog.dismiss();
                                    }
                                    session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);

                                    Intent intentCancelReservation = new Intent(Activity_UserTripHistory.this, Activity_UserMain.class);
                                    EApplication.getInstance().setHistryCancelledStatus(true);
                                    startActivity(intentCancelReservation);
                                    finish();


                                } else {
                                    EApplication.getInstance().showToastMessageFunction("Reservation Already Cancelled");
                                    Intent intentCancelReservation = new Intent(Activity_UserTripHistory.this, Activity_UserMain.class);
                                    EApplication.getInstance().setHistryCancelledStatus(true);
                                    startActivity(intentCancelReservation);
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            CustomProgressBarDialog.progressDialog.dismiss();

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EApplication.getInstance().showToastMessageFunction(error.toString());
                    Log.e("error_volley", error.toString() + "");
                    CustomProgressBarDialog.progressDialog.dismiss();
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null) {
                        Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                    }
                    if (error instanceof TimeoutError) {
                        Log.e("Driver_New_JObs_Volley", "TimeoutError");
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                    } else if (error instanceof NoConnectionError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                    } else if (error instanceof AuthFailureError) {
                        Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                    } else if (error instanceof ServerError) {
                        Log.e("Driver_New_JObs_Volley", "ServerError");
                    } else if (error instanceof NetworkError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "NetworkError");
                    } else if (error instanceof ParseError) {
                        Log.e("Driver_New_JObs_Volley", "ParseError");
                    }

                }
            })  {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            req.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(req, "high");
        } else {
            Toast.makeText(Activity_UserTripHistory.this, getResources().getString(R.string.no_network), Toast.LENGTH_LONG).show();
            NetworkUtils.showNoConnectionDialog(Activity_UserTripHistory.this);
        }

    }

    @Override
    public void onRoutingFailure() {
        Log.e("===", "Sorry Route not Found");
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(startPoint);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(cameraZoom);

        map.moveCamera(center);
        map.animateCamera(zoom);


        if (polyline != null)
            polyline.remove();
        polyline = null;
        //adds route to the map.
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.primary_dark));
        polyOptions.width(10);
        polyOptions.addAll(mPolyOptions.getPoints());
        polyline = map.addPolyline(polyOptions);

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(startPoint);
        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));
        map.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(endPoint);
        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_driverpin));
        map.addMarker(options);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        padding = ((width * 10) / 100);
        EApplication.getInstance().setMapPadding(padding);
        //Animating Camera as passing

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(startPoint);
        builder.include(endPoint);
        final LatLngBounds bounds = builder.build();
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                map.animateCamera(cu);
            }
        });

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("===", "connectionFailed");
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.e("===", "Internet Connected");

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("===", "Connection Suspended");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(Activity_UserTripHistory.this, Activity_UserMain.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
