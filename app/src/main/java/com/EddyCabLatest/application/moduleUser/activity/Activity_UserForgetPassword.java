package com.EddyCabLatest.application.moduleUser.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by indraimac on 9/17/15.
 */
public class Activity_UserForgetPassword extends AppCompatActivity {

    private Toolbar app_toolBar;
    SessionManager session;

    LinearLayout layout_enterPassword;
    EditText EDT_EmailAddress;
    Button actionSubmitEmail;

    LinearLayout layout_EnterVerificationCode;
    EditText EDT_verificationCode;
    Button actionSubmitVerificationCode;

    LinearLayout layout_changePassword;
    EditText EDT_newPassword, EDT_confirmPassword;
    Button actionSubmitChangePassword;

    private JSONObject Response_Verification;

    String language = "";
    private String user_type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user_forget_password);

          /*ToolBar SetUp*/
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        app_toolBar.setVisibility(View.VISIBLE);

        /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        session = new SessionManager(Activity_UserForgetPassword.this);

        // singleton class initialization
        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            user_type = extras.getString("type");


        }

        /* References Initialization */
        layout_enterPassword = (LinearLayout) findViewById(R.id.layout_enterPassword);
        layout_changePassword = (LinearLayout) findViewById(R.id.layout_changePassword);
        layout_EnterVerificationCode = (LinearLayout) findViewById(R.id.layout_EnterVerificationCode);

        layout_enterPassword.setVisibility(View.VISIBLE);
        layout_changePassword.setVisibility(View.GONE);
        layout_EnterVerificationCode.setVisibility(View.GONE);


        EDT_EmailAddress = (EditText) findViewById(R.id.EDT_EmailAddress);
        actionSubmitEmail = (Button) findViewById(R.id.actionSubmitEmail);

        EDT_verificationCode = (EditText) findViewById(R.id.EDT_verificationCode);
        EDT_verificationCode = (EditText) findViewById(R.id.EDT_verificationCode);
        actionSubmitVerificationCode = (Button) findViewById(R.id.actionSubmitVerificationCode);

        EDT_newPassword = (EditText) findViewById(R.id.EDT_newPassword);
        EDT_confirmPassword = (EditText) findViewById(R.id.EDT_confirmPassword);
        actionSubmitChangePassword = (Button) findViewById(R.id.actionSubmitChangePassword);


        // =============== API FOR SEND MOBILE VERIFICATION CODE =============== //
        actionSubmitEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NetworkUtils.isNetworkAvailable(Activity_UserForgetPassword.this)) {

                    if (EDT_EmailAddress.getText().toString().trim().length() == 10) {

                        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserForgetPassword.this);
                        CustomProgressBarDialog.progressDialog.show();

                        CustomRequest request = new CustomRequest(Request.Method.GET,
                                Config.APP_BASE_URL + Config.FORGET_PASSWORD_SEND_MESSAGE + EDT_EmailAddress.getText().toString().trim() + "/" + user_type,
                                null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e("Response::", "" + response);
                                        Response_Verification = response;
                                        try {
                                            boolean error = response.getBoolean("error");
                                            if (!error) {

                                                layout_enterPassword.setVisibility(View.GONE);
                                                layout_changePassword.setVisibility(View.GONE);
                                                layout_EnterVerificationCode.setVisibility(View.VISIBLE);

                                            } else {
                                                String message = response.getString("message");
                                                EApplication.getInstance().showToastMessageFunction(message);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        CustomProgressBarDialog.progressDialog.dismiss();
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("Error::", "" + error);

                                CustomProgressBarDialog.progressDialog.dismiss();
                            }
                        }) {
                            @Override
                            public Request.Priority getPriority() {
                                return Request.Priority.HIGH;
                            }
                        };

                        EApplication.getInstance().addToRequestQueue(request, "high");


                    } else {
                        EDT_EmailAddress.setError(getResources().getString(R.string.mobilelesstendigit));
                    }
                } else {
                    NetworkUtils.showNoConnectionDialog(Activity_UserForgetPassword.this);
                }
            }
        });


        // =============== API FOR CONFIRM MOBILE VERIFICATION CODE =============== //
        actionSubmitVerificationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NetworkUtils.isNetworkAvailable(Activity_UserForgetPassword.this)) {

                    if (EDT_verificationCode.getText().toString().trim().length() == 4) {
                        final HashMap<String, String> params = new HashMap<>();
                        params.put("code", EDT_verificationCode.getText().toString().trim());
                        try {
                            String user_id = Response_Verification.getString("user_id");
                            String mobile_number = Response_Verification.getString("mobile_number");

                            params.put("user_id", user_id);
                            params.put("mobile_number", mobile_number);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.e("PARAMS ::", "" + params);

                        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserForgetPassword.this);
                        CustomProgressBarDialog.progressDialog.show();

                        StringRequest request = new StringRequest(Request.Method.POST,
                                Config.APP_BASE_URL + Config.CONFIRM_VERIFICATION_CODE,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String responseObject) {
                                        String newResponse = responseObject;


                                        if (responseObject.contains("ï»¿")) {
                                            newResponse = newResponse.substring(newResponse.indexOf("{"));
                                        }

                                        try {
                                            JSONObject response = new JSONObject(newResponse);
                                            Log.e("Response::", "" + response);
                                            boolean error = response.getBoolean("error");
                                            if (!error) {

                                                layout_enterPassword.setVisibility(View.GONE);
                                                layout_changePassword.setVisibility(View.VISIBLE);
                                                layout_EnterVerificationCode.setVisibility(View.GONE);


                                            } else {
                                                String message = response.getString("message");
                                                EApplication.getInstance().showToastMessageFunction(message);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        CustomProgressBarDialog.progressDialog.dismiss();
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("Error::", "" + error);

                                CustomProgressBarDialog.progressDialog.dismiss();
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                return params;
                            }

                            @Override
                            public Request.Priority getPriority() {
                                return Request.Priority.HIGH;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                String current_language = session.getAppLanguage();
                                Log.e("current_language", current_language);
                                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                    language = "en";
                                }
                                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                    language = "np";
                                }
                                headers.put("lang", language);
                                return headers;
                            }
                        };

                        EApplication.getInstance().addToRequestQueue(request, "high");


                    } else {
                        EDT_verificationCode.setError("Please Enter 3-Digit Valid Verification Code");
                    }
                } else {
                    NetworkUtils.showNoConnectionDialog(Activity_UserForgetPassword.this);
                }
            }
        });


        // =============== API FOR CHANGE PASSWORD EXECUTION =============== //
        actionSubmitChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NetworkUtils.isNetworkAvailable(Activity_UserForgetPassword.this)) {

                    if (EDT_newPassword.getText().toString().trim().equals(EDT_confirmPassword.getText().toString().trim())) {
                        final HashMap<String, String> params = new HashMap<>();
                        params.put("password", EDT_newPassword.getText().toString().trim());
                        try {
                            String user_id = Response_Verification.getString("user_id");

                            params.put("user_id", user_id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.e("PARAMS ::", "" + params);

                        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserForgetPassword.this);
                        CustomProgressBarDialog.progressDialog.show();

                        CustomRequest request = new CustomRequest(Request.Method.POST,
                                Config.APP_BASE_URL + Config.API_SET_NEW_PASSWORD,
                                params,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e("Response::", "" + response);

                                        try {
                                            boolean error = response.getBoolean("error");
                                            if (!error) {

                                                finish();
                                                String message = response.getString("message");
                                                EApplication.getInstance().showToastMessageFunction(message);

                                            } else {
                                                String message = response.getString("message");
                                                EApplication.getInstance().showToastMessageFunction(message);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        CustomProgressBarDialog.progressDialog.dismiss();
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("Error::", "" + error);

                                CustomProgressBarDialog.progressDialog.dismiss();
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                return params;
                            }

                            @Override
                            public Request.Priority getPriority() {
                                return Request.Priority.HIGH;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                String current_language = session.getAppLanguage();
                                Log.e("current_language", current_language);
                                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                    language = "en";
                                }
                                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                    language = "np";
                                }
                                headers.put("lang", language);
                                return headers;
                            }
                        };

                        EApplication.getInstance().addToRequestQueue(request, "high");

                    } else {
                        EDT_confirmPassword.setError("Confirm Password does not matched.");
                    }
                } else {
                    NetworkUtils.showNoConnectionDialog(Activity_UserForgetPassword.this);
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
