package com.EddyCabLatest.application.moduleUser.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;

import java.util.ArrayList;
import java.util.HashMap;

import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_FREQUENT_SEARCH;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME;

/**
 * Created by newimac on 7/7/15.
 */
public class AdapterFrequentSearch extends BaseAdapter {
    private ArrayList<HashMap<String, String>> dataList;
    private Context mContext;
    private int lastPosition = -1;

    public AdapterFrequentSearch(Context context, ArrayList<HashMap<String, String>> arrayList) {

        mContext = context;
        dataList = arrayList;
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        final LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.list_item_search_frequent, null);
            holder.location_name = (TextView) convertView.findViewById(R.id.Location_Name);
            holder.location_address = (TextView) convertView.findViewById(R.id.Location_address);
            holder.favorite_icon = (ImageView) convertView.findViewById(R.id.icon_favourite);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.location_name.setText(dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME));
        holder.location_address.setText(dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS));

        String Image_Name = dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES);
        if (!TextUtils.isEmpty(Image_Name)) {
            int res = mContext.getResources().getIdentifier(Image_Name, "drawable", mContext.getPackageName());
            holder.favorite_icon.setImageResource(res);
        }

        holder.favorite_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (EApplication.getInstance().CheckIsDataAlreadyInDBorNot(TABLE_FREQUENT_SEARCH, TABLE_FREQUENT_SEARCH_ADDRESS_NAME, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME))) {
                    Toast.makeText(mContext, "You have already referenced this address", Toast.LENGTH_LONG).show();
                } else {
                    HashMap<String, String> map = new HashMap<>();

                    map.put(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES, "icon_favourite_active");
                    map.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME));
                    map.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS));
                    map.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE));
                    map.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT));
                    map.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));



                    EApplication.getInstance().addToDB(DBTableFields.TABLE_FREQUENT_SEARCH, map);
                    Toast.makeText(mContext, "Successfully referenced this address", Toast.LENGTH_LONG).show();
                }
            }
        });


        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        return convertView;
    }


    class ViewHolder {
        TextView location_name;
        TextView location_address;
        ImageView favorite_icon;
    }

}
