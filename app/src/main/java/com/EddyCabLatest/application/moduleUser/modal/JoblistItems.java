package com.EddyCabLatest.application.moduleUser.modal;

public class JoblistItems {

    String from;
    String to;
    String date;
    Double pick_lat;
    Double pick_lang;
    Double drop_lat;
    Double drop_lang;
    String date_detail;
    String distance;
    String date_time;
    String note;
    String time;
    String bid_status;
    String ReservationID;
    String padding;

    public JoblistItems(String from,Double pick_lat, Double pick_lang,  String date_time, String note, String date, String time, String bid_status, String reservationID,String padding) {
        this.from = from;
        this.to = to;
        this.pick_lat = pick_lat;
        this.pick_lang = pick_lang;
        this.drop_lat = drop_lat;
        this.drop_lang = drop_lang;
        this.date_time = date_time;
        this.note = note;
        this.date = date;
        this.time = time;
        this.bid_status = bid_status;
        this.ReservationID = reservationID;
        this.padding=padding;
    }


    public String getPadding() {
        return padding;
    }

    public void setPadding(String padding) {
        this.padding = padding;
    }

    public String getReservationID() {
        return ReservationID;
    }

    public void setReservationID(String reservationID) {
        ReservationID = reservationID;
    }


    public JoblistItems(String from,String bid_status) {
        this.from = from;
        this.to = to;
        this.bid_status = bid_status;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    int bhatt;

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public Double getPick_lat() {
        return pick_lat;
    }

    public void setPick_lat(Double pick_lat) {
        this.pick_lat = pick_lat;
    }

    public Double getPick_lang() {
        return pick_lang;
    }

    public void setPick_lang(Double pick_lang) {
        this.pick_lang = pick_lang;
    }

    public Double getDrop_lat() {
        return drop_lat;
    }

    public void setDrop_lat(Double drop_lat) {
        this.drop_lat = drop_lat;
    }

    public Double getDrop_lang() {
        return drop_lang;
    }

    public void setDrop_lang(Double drop_lang) {
        this.drop_lang = drop_lang;
    }


    public String getDate_detail() {
        return date_detail;
    }

    public void setDate_detail(String date_detail) {
        this.date_detail = date_detail;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getBhatt() {
        return bhatt;
    }

    public void setBhatt(int bhatt) {
        this.bhatt = bhatt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getBid_status() {
        return bid_status;
    }

    public void setBid_status(String bid_status) {
        this.bid_status = bid_status;
    }
}