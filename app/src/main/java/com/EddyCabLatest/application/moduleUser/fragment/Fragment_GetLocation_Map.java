package com.EddyCabLatest.application.moduleUser.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleUser.activity.Activity_PickupLocation;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.gpsTrackerUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class Fragment_GetLocation_Map extends Fragment implements View.OnClickListener,
        GoogleMap.OnCameraChangeListener {

    private MapView mapView;
    private GoogleMap map;

    private gpsTrackerUser gps;
    private LatLng locationPoint;
    private double latitude;
    private double longitude;

    Geocoder geocoder;
    List<Address> addresses;

    TextView btn_set_location, layout_PickingLocation;
    TextView placeTitle;
    TextView placeDetails;
    ImageView img_marker_at_center;
    String LocationName;
    String LocationAddress;
    String LocationLat;
    String LocationLang;

    LinearLayout Bottom_Layout;
    private Context mcontext;
    private Bundle savedInstanceState;
    private String pickOrDestination;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;

        View rootView = inflater.inflate(R.layout.layout_fragment_search_map, container, false);

        mcontext = container.getContext();
        initializaMap(rootView, savedInstanceState);
        setupUI(rootView);
        Bottom_Layout = (LinearLayout) rootView.findViewById(R.id.bottom_ll);
/*if(savedInstanceState!=null)
{
   pickOrDestination = savedInstanceState.getString("pickOrDestination");
    if (pickOrDestination.equals("destinationLocation")) {
        img_marker_at_center.setImageDrawable(getResources().getDrawable(R.mipmap.eddy_driverpin));
    }
}*/
        //getBundle From Activity

        gps = new gpsTrackerUser(getActivity());
        geocoder = new Geocoder(getActivity(), Locale.getDefault());


        // Get References
        btn_set_location = (TextView) rootView.findViewById(R.id.btn_set_location);
        btn_set_location.setOnClickListener(this);

        placeTitle = (TextView) rootView.findViewById(R.id.place_title);
        placeDetails = (TextView) rootView.findViewById(R.id.place_details);
        layout_PickingLocation = (TextView) rootView.findViewById(R.id.layout_PickingLocation);
        img_marker_at_center = (ImageView) rootView.findViewById(R.id.img_marker_at_center);


        if (NetworkUtils.isNetworkAvailable(mcontext)) {

            // Gets to GoogleMap from the MapView and does initialization stuff

            map.getUiSettings().setZoomControlsEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.getUiSettings().setRotateGesturesEnabled(true);
            map.setMyLocationEnabled(true);
            map.setOnCameraChangeListener(this);

            // create class object

            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                if (latitude == 0.0 && longitude == 0.0) {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.gpsnotworking));
                } else {

            /* Implement Reverse Geo Fencing to get Address from Lat Lang  */

                    try {
                        addresses = geocoder.getFromLocation(gps.getLatitude(), gps.getLongitude(), 1);


                        if (addresses.size() > 0) {

                            String address = addresses.get(0).getAddressLine(0);
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String knownName = addresses.get(0).getFeatureName();


                            locationPoint = new LatLng(latitude, longitude);

                            String title = address + " " + city;
                            String snippet = country + " " + postalCode + " , " + state + " " + knownName;

                            // get Location Data for return purpose
                            LocationName = title;
                            LocationAddress = snippet;
                            LocationLat = String.valueOf(latitude);
                            LocationLang = String.valueOf(longitude);

                            layout_PickingLocation.setBackgroundColor(Color.BLACK);
                            layout_PickingLocation.setTextColor(Color.WHITE);
                            layout_PickingLocation.setText("Picked Location ...");
                            placeTitle.setText(title);
                            placeDetails.setText(snippet);

                            // addMarker(map, latitude, longitude, title, snippet);

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (locationPoint != null) {

                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(locationPoint, Config.CAMERA_FACTORY_UPDATE);
                                if (cameraUpdate != null) {

                                    map.animateCamera(cameraUpdate);
                                }

                                //  Toast.makeText(getActivity().getApplicationContext(),getResources(), Toast.LENGTH_SHORT).show();
                            } else {

                                // Toast.makeText(getActivity().getApplicationContext(), "Location is not set ", Toast.LENGTH_SHORT).show();
                            }


                        }
                    });


                }

            } else {
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.no_internet_availeble), Toast.LENGTH_LONG).show();
            NetworkUtils.showNoConnectionDialog(getActivity());
        }

        return rootView;
    }


    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_set_location:

                ((Activity_PickupLocation) getActivity()).setRequestAddress(LocationName, LocationAddress, LocationLat, LocationLang);

        }
    }

/*
    private void addMarker(GoogleMap map, double lat, double lon, String title, String snippet) {
        map.addMarker(new MarkerOptions().position(new LatLng(lat, lon))
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin))
                .draggable(true));
    }
*/


    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        map.clear();
        LocationLat = String.valueOf(map.getCameraPosition().target.latitude);
        LocationLang = String.valueOf(map.getCameraPosition().target.longitude);

        if (NetworkUtils.isNetworkAvailable(getActivity())) {

            try {

                addresses = geocoder.getFromLocation(map.getCameraPosition().target.latitude, map.getCameraPosition().target.longitude, 1);

                if (addresses.size() > 0) {

                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    String correctAddress = "", correctCity = "", correctState = "", correctCountry = "", correctPostalCode = "", correctKnownName = "";

                    if (address != null) correctAddress = address;
                    if (city != null) correctCity = city;
                    if (state != null) correctState = state;
                    if (country != null) correctCountry = country;
                    if (postalCode != null) correctPostalCode = postalCode;
                    if (knownName != null) correctKnownName = knownName;

                    LocationName = correctAddress + " " + correctCity;
                    LocationAddress = correctCountry + " " + correctState + " " + correctKnownName;


                    placeTitle.setText(LocationName);
                    placeDetails.setText(LocationAddress);
                    img_marker_at_center.setVisibility(View.VISIBLE);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            NetworkUtils.showNoConnectionDialog(getActivity());
        }

    }

    public void setupUI(View view) {

        Log.e("EVENT", "Called");

        //Set up touch listener for non-text box views to hide keyboard.
        if ((view instanceof MapView)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {

                        Bottom_Layout.setVisibility(View.GONE);

                        Bottom_Layout.animate()
                                .translationY(Bottom_Layout.getHeight())
                                .alpha(0.0f)
                                .setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        Bottom_Layout.setVisibility(View.GONE);
                                    }
                                });

                    } else if (event.getAction() == MotionEvent.ACTION_UP) {

                        Bottom_Layout.setVisibility(View.VISIBLE);

                        Bottom_Layout.animate()
                                .translationY(Bottom_Layout.getHeight())
                                .alpha(0.0f)
                                .setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        Bottom_Layout.setVisibility(View.VISIBLE);
                                    }
                                });
                    }


                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    private void initializaMap(View rootView, Bundle savedInstanceState) {

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(mcontext)) {
            case ConnectionResult.SUCCESS:

                mapView = (MapView) rootView.findViewById(R.id.content_map_view);
                mapView.onCreate(savedInstanceState);

                if (mapView != null) {
                    map = mapView.getMap();
                }
                MapsInitializer.initialize(mcontext);

                break;
            case ConnectionResult.SERVICE_MISSING:
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;
            default:

        }
    }

}

