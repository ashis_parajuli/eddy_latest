package com.EddyCabLatest.application.moduleUser.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.activity.Activity_PickupLocation;
import com.EddyCabLatest.application.moduleUser.adapter.AdapterFoursquare;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.NewGPSTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_RECENT_SEARCH;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_ADDRESS;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_NAME;

/**
 * Created by newimac on 7/6/15.
 */
public class Fragment_GetLocation_Foursquare extends Fragment {

    private EApplication app;
    private NewGPSTracker gpsTracker;
    private int radius = 6000;
    private String type = "embassy|shopping_mall|school|zoo|taxi_stand";
    private ArrayList<HashMap<String, String>> dataListArray;
    private ListView ListView_Foursquare;
    private TextView emptyMessage;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private Handler handler = new Handler();

    EditText searchEditText;
    ArrayList<HashMap<String, String>> searchDataList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.getlocation_fragment_foursquare, container, false);

        setupUI(rootView.findViewById(R.id.activity_main_swipe_refresh_layout));

        app = EApplication.getInstance();
        gpsTracker = new NewGPSTracker(getActivity());

        searchEditText = (EditText) rootView.findViewById(R.id.searchEditText);
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    // Hide Keyboard
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    performSearch(searchEditText.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });


        ListView_Foursquare = (ListView) rootView.findViewById(R.id.listItem_Frequent_GetLocation);
        emptyMessage = (TextView) rootView.findViewById(android.R.id.empty);

        methodTOLoadData();

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {

                    @Override
                    public void onRefresh() {
                         handler.post(refreshing);
                    }
                });

        mSwipeRefreshLayout.setColorSchemeResources(R.color.orange);


        ListView_Foursquare.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (ListView_Foursquare != null && ListView_Foursquare.getChildCount() > 0) {
                    boolean firstItemVisible = ListView_Foursquare.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = ListView_Foursquare.getChildAt(0).getTop() == 0;
                    boolean enable = firstItemVisible && topOfFirstItemVisible;
                    mSwipeRefreshLayout.setEnabled(enable);
                } else {
                    mSwipeRefreshLayout.setEnabled(true);
                }

            }
        });

        ListView_Foursquare.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                HashMap<String, String> map = new HashMap<>();

                map.put(DBTableFields.TABLE_RECENT_SEARCH_IMAGES, dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES));
                map.put(DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_NAME, dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME));
                map.put(DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_ADDRESS, dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS));
                map.put(DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_DISTANCE, dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE));
                map.put(DBTableFields.TABLE_RECENT_SEARCH_LAT, dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT));
                map.put(DBTableFields.TABLE_RECENT_SEARCH_LANG, dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));

                if (!EApplication.getInstance().CheckIsDataAlreadyInDBorNotWithNameAndAddress(TABLE_RECENT_SEARCH, TABLE_RECENT_SEARCH_ADDRESS_NAME, TABLE_RECENT_SEARCH_ADDRESS_ADDRESS, dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME), dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS))) {
                    EApplication.getInstance().addToDB(DBTableFields.TABLE_RECENT_SEARCH, map);
                    EApplication.getInstance().deleteRecent();
                }


                ((Activity_PickupLocation) getActivity()).setRequestAddress(dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME),
                        dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS),
                        dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT),
                        dataListArray.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));
            }
        });

        return rootView;
    }

    private void performSearch(String query) {
        Log.e("where:::","here");
        searchDataList = new ArrayList<>();
        if (dataListArray.size()>0){
            for (int i=0; i<dataListArray.size(); i++){
                if (dataListArray.get(i).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME).toLowerCase().contains(query.toLowerCase())){
                    HashMap<String, String> mapData = new HashMap<>();

                    mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES, "icon_favourite_incative");
                    mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, dataListArray.get(i).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME));
                    mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, dataListArray.get(i).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS));
                    mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, dataListArray.get(i).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT));
                    mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, dataListArray.get(i).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));
                    mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, "0 KM");

                    searchDataList.add(mapData);
                    Log.e("where:::", "here1");
                }
            }
        }
        AdapterFoursquare adapter = new AdapterFoursquare(getActivity(), searchDataList);
        ListView_Foursquare.setAdapter(adapter);
        ListView_Foursquare.setEmptyView(emptyMessage);
        Log.e("where:::", "here2");


    }

    private final Runnable refreshing = new Runnable(){
        public void run(){
            try {
                if(isRefreshing()) handler.postDelayed(this, 1000);
                else mSwipeRefreshLayout.setRefreshing(false);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private boolean isRefreshing() {
        methodTOLoadData();

        return false;
    }


    private void methodTOLoadData() {

        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, requestUrl(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Response is: ", response);

                    dataListArray = new ArrayList<>();

                    try {
                        JSONObject objectData = new JSONObject(response);

                        JSONArray dataArray = objectData.getJSONArray("results");

                        for (int i = 0; i < dataArray.length(); i++) {

                            JSONObject arrayObjectData = dataArray.getJSONObject(i);
                            String name = arrayObjectData.getString("name");
                            String vicinity = arrayObjectData.getString("vicinity");

                            JSONObject geoLocation = arrayObjectData.getJSONObject("geometry");
                            String geometry_lat = geoLocation.getJSONObject("location").getString("lat");
                            String geometry_lng = geoLocation.getJSONObject("location").getString("lng");

                             /* Fields Param */
                            String correctAddress = "", correctName = "", correctLat = "", correctLang = "";

                            if (name != null) correctName = name;
                            if (vicinity != null) correctAddress = vicinity;
                            if (geometry_lat != null) correctLat = geometry_lat;
                            if (geometry_lng != null) correctLang = geometry_lng;
                            if (!correctName.equals("") && !correctAddress.equals("") && !correctLang.equals("") && !correctLat.equals("")) {


                                HashMap<String, String> mapData = new HashMap<>();

                                mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES, "icon_favourite_incative");
                                mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, correctName);
                                mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, correctAddress);
                                mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, correctLat);
                                mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, correctLang);
                                mapData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, "0 KM");

                                dataListArray.add(mapData);
                            }

                        }


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }

                    AdapterFoursquare adapter = new AdapterFoursquare(getActivity(), dataListArray);
                    ListView_Foursquare.setAdapter(adapter);
                    ListView_Foursquare.setEmptyView(emptyMessage);



                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error", "That didn't work!");
                }
            });
            queue.add(stringRequest);

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.no_internet_availeble), Toast.LENGTH_LONG).show();
        }
    }


    //requests url of current location and returns values to arrays_lists variables
    private String requestUrl() {
        app = EApplication.getInstance();
        gpsTracker = new NewGPSTracker(getActivity());

        String NEARBY_URL = app.getNearByURL(gpsTracker.getLatitude(), gpsTracker.getLongitude(), radius, type);
        Log.e("Response:", NEARBY_URL);

        return NEARBY_URL;
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    EApplication.getInstance().hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }


        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

}
