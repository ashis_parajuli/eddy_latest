package com.EddyCabLatest.application.moduleUser.fragment;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_ActivityBidSuccess;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Driver_about_Fragment extends Fragment implements View.OnClickListener {
    ImageView img_shareFacebook;


    ImageView img_shareTwitter;
    private SessionManager session;
    private String UserId;
    private ChildEventListener eventlistener;
    private Context mcontext;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mcontext=container.getContext();
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.about_driver, container, false);
        img_shareFacebook = (ImageView) rootView.findViewById(R.id.img_shareFacebook);
     /*   img_shareInstagram = (ImageView) rootView.findViewById(R.id.img_shareInstagram);
        img_logo_banana = (ImageView) rootView.findViewById(R.id.img_logo_banana);*/
        img_shareTwitter = (ImageView) rootView.findViewById(R.id.img_shareTwitter);
    /*    img_shareGooglePlus = (ImageView) rootView.findViewById(R.id.img_shareGooglePlus);*/
        session = new SessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
         UserId = userDetails.get(SessionManager.KEY_ID);
        //event Handling
        img_shareFacebook.setOnClickListener(this);
       /* img_shareInstagram.setOnClickListener(this);
        img_logo_banana.setOnClickListener(this);*/
        img_shareTwitter.setOnClickListener(this);
       /* img_shareGooglePlus.setOnClickListener(this);*/


        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();

        getjoblistfirebase();
    }

    @Override
    public void onDestroy() {
        if(eventlistener!=null)
        {

            EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(eventlistener);
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.img_shareFacebook:
                String facebookUrl = "https://www.facebook.com/eddycab";
                linkUrl(facebookUrl);
                break;

          /*  case R.id.img_shareInstagram:
                String instagramUrl = "https://instagram.com/bananabike.co/";
                linkUrl(instagramUrl);
                break;

            case R.id.img_logo_banana:
                String url = "http://bananabike.co/";
                linkUrl(url);
                break;*/

            case R.id.img_shareTwitter:
                String twitterUrl = "https://twitter.com/eddycab";
                linkUrl(twitterUrl);
                break;

         /*   case R.id.img_shareGooglePlus:
                String gPlusUrl = "https://plus.google.com";
                linkUrl(gPlusUrl);
                break;*/
            default:
                break;

        }

    }

    public void getjoblistfirebase() {
        eventlistener = EApplication.ref.child(UserId + "/new_reservations/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    if ((dataSnapshot.child("accepted").getValue() .equals(false))&&(dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("new"))) {
                        Intent intent= new Intent(mcontext,Driver_MainActivity.class);
                        if(isAdded())
                        startActivity(intent);

                        if (isAdded()) {
                            EApplication.getInstance().showNotification(mcontext, Driver_MainActivity.class, 011, getResources().getString(R.string.new_job), getResources().getString(R.string.new_job), getResources().getString(R.string.new_job_avaiable));

                        }
                    }


                    Log.e("data_joblist", dataSnapshot + "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (isAdded()) {
                    if (dataSnapshot.getValue() != null) {

                        if (dataSnapshot.child("accepted").exists()) {
                            if (dataSnapshot.child("accepted").getValue() .equals(true)) {
                                //EApplication.ref.child(userID + "/new_reservations/" + dataSnapshot.getKey() + "/accepted/").removeValue();
                                EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                if (eventlistener != null) {
                                    EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                    if (eventlistener != null) {

                                        EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                    }
                                }
                                try {
                                    if (dataSnapshot.getValue() != null) {

                                        Intent intent_bid_succes = new Intent(mcontext, Driver_ActivityBidSuccess.class);

                                        Map<String, String> usersMap_data = (Map<String, String>) dataSnapshot.getValue();
                                        JSONObject usersJSON = null;
                                        usersJSON = new JSONObject(usersMap_data);

                                        String selected_id = dataSnapshot.getKey();
                                        String user_fullname = dataSnapshot.child("user_fullname").getValue().toString();
                                        String distance_value = dataSnapshot.child("distance").getValue().toString();
                                        String user_pp = dataSnapshot.child("user_pp").getValue().toString();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("id", selected_id);

                                        bundle.putString("user_fullname", user_fullname);
                                        bundle.putString("user_pp", user_pp);
                                        bundle.putString("distance_value", distance_value);
                                        bundle.putString("user_data_for", usersJSON.toString());
if(isAdded())
                                        intent_bid_succes.putExtras(bundle);


                                        mcontext.startActivity(intent_bid_succes);
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            } else {
                                //jobListRequest1();

                            }

                        }

                    }
                }


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // jobListRequest1();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }
    //implement link url
    private void linkUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        startActivity(intent);

    }


}
