package com.EddyCabLatest.application.moduleUser.fragment;


import android.Manifest;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserMain;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class UserDrawerLogoutHelp extends Fragment implements View.OnClickListener {


    Button about_btn_call_customer;
    Button about_btn_Submit;
    EditText edt_about_feedback;
    SessionManager session;
    EApplication app;
    String UserId;
    private EditText username_feedback;
    private EditText mobile_feedback;
    private Context mcontext;
    private String language;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.layout_user_help, container, false);
        // Initializing Application Instances
        mcontext = container.getContext();
        session = new SessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        UserId = userDetails.get(SessionManager.KEY_ID);

        about_btn_call_customer = (Button) rootView.findViewById(R.id.about_btn_call_customer);
        about_btn_call_customer.setOnClickListener(this);

        about_btn_Submit = (Button) rootView.findViewById(R.id.about_btn_Submit);
        about_btn_Submit.setOnClickListener(this);


        mobile_feedback = (EditText) rootView.findViewById(R.id.phone_feedback_person);

        username_feedback = (EditText) rootView.findViewById(R.id.email_feedback);


        edt_about_feedback = (EditText) rootView.findViewById(R.id.edt_about_feedback);
        edt_about_feedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edt_about_feedback.getText().toString().trim().length() > 5) {
                    about_btn_Submit.setBackgroundColor(getResources().getColor(R.color.primary));
                    about_btn_Submit.setEnabled(true);
                } else {
                    about_btn_Submit.setBackgroundColor(getResources().getColor(R.color.light_gray));
                    about_btn_Submit.setEnabled(false);
                }

            }
        });


        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_btn_call_customer:
                String number = "tel:" + "+97714410178";
                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
                if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {


                    return;
                }

                startActivity(callIntent);

                break;

            case R.id.about_btn_Submit:

                postFeedBackApi();


                break;
        }
    }


    private void postFeedBackApi() {
        String user_feedback_value = username_feedback.getText().toString().trim();
        String mobile_feedback_value = mobile_feedback.getText().toString().trim();
        if (user_feedback_value.equalsIgnoreCase("") || mobile_feedback_value.equalsIgnoreCase("")) {
            if (user_feedback_value.equalsIgnoreCase("")) {
                username_feedback.setError(getResources().getString(R.string.email_address));

            }
            if (mobile_feedback_value.equalsIgnoreCase("")) {
                mobile_feedback.setError(getResources().getString(R.string.mobile_no));
            }

        }
        if (user_feedback_value.equalsIgnoreCase("") && mobile_feedback_value.equalsIgnoreCase("")) {
            username_feedback.setError(getResources().getString(R.string.email_address));
            mobile_feedback.setError(getResources().getString(R.string.mobile_no));
        }


        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            CustomProgressBarDialog.progressDialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("feedback", "my email:" + username_feedback.getText().toString() + "My phone :" + mobile_feedback.getText().toString() + edt_about_feedback.getText().toString().trim());


            Log.e("Parameters", params.toString());

            String feedback_url = Config.APP_BASE_URL + Config.SUPPORT_FEEDBACK_POST_URL;
            Log.e("feedback_url ::", feedback_url);
            CustomRequest req = new CustomRequest(Request.Method.POST, feedback_url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("response", response.toString());
                            try {
                                boolean error = response.getBoolean("error");
                                Log.e("ERROR", String.valueOf(error));
                                if (!error) {
                                    CustomProgressBarDialog.progressDialog.setCancelable(true);
                                    CustomProgressBarDialog.progressDialog.hide();
                                    showSucessDialouge();
                                } else {
                                    CustomProgressBarDialog.progressDialog.setCancelable(true);
                                    CustomProgressBarDialog.progressDialog.hide();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomProgressBarDialog.progressDialog.setCancelable(true);
                    CustomProgressBarDialog.progressDialog.hide();
                }
            }) {


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }
            };
            req.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(req, "high");

        } else {
            NetworkUtils.showNoConnectionDialog(getActivity());
        }


    }




    private void showSucessDialouge() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.thanks_feedback);
        dialog.setCancelable(true);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(mcontext, Activity_UserMain.class);
                startActivity(intent);

            }
        });
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();

//Handling Back pressed
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {


                }

                return false;
            }
        });
    }

}
