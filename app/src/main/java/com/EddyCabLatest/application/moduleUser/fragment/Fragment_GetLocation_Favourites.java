package com.EddyCabLatest.application.moduleUser.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.moduleUser.activity.Update_USerProfile;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.activity.Activity_PickupLocation;
import com.EddyCabLatest.application.moduleUser.adapter.AdapterFavourites;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_RECENT_SEARCH;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_ADDRESS;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_NAME;


public class Fragment_GetLocation_Favourites extends Fragment {

    private EApplication app;
    private SessionManager session;

    private ListView ListView_Favourites;
    private ArrayList<HashMap<String, String>> dataList;
    private HashMap<String, String> mapHomeData;
    private HashMap<String, String> mapWorkData;

    private TextView Home_Address_Title;
    private TextView Home_Address_Address;
    private TextView Work_Address_Title;
    private TextView Work_Address_Address;

    private View user_favourite_home_divider;
    private RelativeLayout user_favourite_home;

    private View user_favourite_work_divider;
    private RelativeLayout user_favourite_work;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Handler handler = new Handler();
    private Context mcontext;
    private String language;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.getlocation_fragment_frequent, container, false);

        app = EApplication.getInstance();
        session = new SessionManager(getActivity());
        mcontext=container.getContext();
        ListView_Favourites = (ListView) rootView.findViewById(R.id.listItem_Frequent_GetLocation);

        Home_Address_Title = (TextView) rootView.findViewById(R.id.favourite_home_address_title);
        Home_Address_Address = (TextView) rootView.findViewById(R.id.favourite_home_address_address);
        Work_Address_Title = (TextView) rootView.findViewById(R.id.favourite_work_address_title);
        Work_Address_Address = (TextView) rootView.findViewById(R.id.favourite_work_address_address);

        user_favourite_home_divider = (View) rootView.findViewById(R.id.user_favourite_home_divider);
        user_favourite_work_divider = (View) rootView.findViewById(R.id.user_favourite_work_divider);

        user_favourite_home =(RelativeLayout) rootView.findViewById(R.id.user_favourite_home);
        user_favourite_work =(RelativeLayout) rootView.findViewById(R.id.user_favourite_work);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {

                    @Override
                    public void onRefresh() {
                        handler.post(refreshing);
                    }
                });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.orange);


        ListView_Favourites.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                mSwipeRefreshLayout.setEnabled(true);
            }
        });


        ListView_Favourites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mapWorkData!=null) {
                    HashMap<String, String> map = new HashMap<>();

                    map.put(DBTableFields.TABLE_RECENT_SEARCH_IMAGES, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES));
                    map.put(DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_NAME, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME));
                    map.put(DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_ADDRESS, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS));
                    map.put(DBTableFields.TABLE_RECENT_SEARCH_ADDRESS_DISTANCE, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE));
                    map.put(DBTableFields.TABLE_RECENT_SEARCH_LAT, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT));
                    map.put(DBTableFields.TABLE_RECENT_SEARCH_LANG, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));

                    if (!EApplication.getInstance().CheckIsDataAlreadyInDBorNotWithNameAndAddress(TABLE_RECENT_SEARCH, TABLE_RECENT_SEARCH_ADDRESS_NAME, TABLE_RECENT_SEARCH_ADDRESS_ADDRESS, dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME), dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS))) {
                        EApplication.getInstance().addToDB(DBTableFields.TABLE_RECENT_SEARCH, map);
                        EApplication.getInstance().deleteRecent();
                    }


                    ((Activity_PickupLocation) getActivity()).setRequestAddress(dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME),
                            dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS),
                            dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT),
                            dataList.get(position).get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));
                }

                else
                {
                    Intent intent=new Intent(mcontext, Update_USerProfile.class);
                    startActivity(intent);

                    //Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.favourite_place_are_not_set_please_set), Toast.LENGTH_SHORT).show();

                }



            }



        });

        user_favourite_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(mapHomeData!=null) {
                        ((Activity_PickupLocation) getActivity()).setRequestAddress(mapHomeData.get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME),
                                mapHomeData.get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS),
                                mapHomeData.get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT),
                                mapHomeData.get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));
                    }
                    else
                        Toast.makeText(getActivity().getApplicationContext(),getResources().getString(R.string.favourite_place_are_not_set_please_set), Toast.LENGTH_SHORT).show();

                }catch (Exception e)
                {

                    e.printStackTrace();
                }
                finally {
                }

            }
        });

        user_favourite_work.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

if(mapWorkData!=null) {
    ((Activity_PickupLocation) getActivity()).setRequestAddress(mapWorkData.get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME),
            mapWorkData.get(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS),
            mapWorkData.get(DBTableFields.TABLE_FREQUENT_SEARCH_LAT),
            mapWorkData.get(DBTableFields.TABLE_FREQUENT_SEARCH_LANG));
}

                else
{
    Intent intent=new Intent(mcontext, Update_USerProfile.class);
    startActivity(intent);
}
    //Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.favourite_place_are_not_set_please_set), Toast.LENGTH_SHORT).show();

            }
        });



        getFavouritesPlaces();

        setupUI(rootView.findViewById(R.id.activity_main_swipe_refresh_layout));

        return rootView;
    }

    private final Runnable refreshing = new Runnable() {
        public void run() {
            try {
                if (isRefreshing()) handler.postDelayed(this, 1000);
                else mSwipeRefreshLayout.setRefreshing(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private boolean isRefreshing() {
        getFavouritesPlaces();
        return false;
    }

    private void getFavouritesPlaces() {

        HashMap<String, String> userDataList = session.getUserDetails();
        String UserID = userDataList.get(SessionManager.KEY_ID);

        String URL_GET_PROFILE = Config.APP_BASE_URL + Config.GET_USER_PROFILE + UserID;

        if (NetworkUtils.isNetworkAvailable(getActivity())) {

            CustomRequest req = new CustomRequest(Request.Method.GET, URL_GET_PROFILE, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject responseObject) {
                            Log.e("Response 1st API", responseObject.toString());


                            try {
                                boolean error = responseObject.getBoolean("error");

                                if (!error) {

                                    try {
                                        Log.e("JSON DATA :: ", responseObject.toString());

                                        if (responseObject.getJSONObject("Fav_home") != JSONObject.NULL) {
                                            mapHomeData = new HashMap<>();

                                            String HOME_TITLE = responseObject.getJSONObject("Fav_home").getJSONObject("favourite").getString("title");
                                            String HOME_ADDRESS = responseObject.getJSONObject("Fav_home").getJSONObject("favourite").getString("address");
                                            String HOME_LATITUDE = responseObject.getJSONObject("Fav_home").getJSONObject("favourite").getString("latitude");
                                            String HOME_LONGITUDE = responseObject.getJSONObject("Fav_home").getJSONObject("favourite").getString("longitude");

                                            mapHomeData.put(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES, "icon_home");
                                            mapHomeData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, HOME_TITLE);
                                            mapHomeData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, HOME_ADDRESS);
                                            mapHomeData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, HOME_LATITUDE);
                                            mapHomeData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, HOME_LONGITUDE);
                                            mapHomeData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, "0");

                                            Home_Address_Title.setText(HOME_TITLE);
                                            Home_Address_Address.setText(HOME_ADDRESS);

                                        } else {
                                            user_favourite_home.setVisibility(View.GONE);
                                            user_favourite_home_divider.setVisibility(View.GONE);
                                        }

                                        if (responseObject.getJSONObject("Fav_work") != JSONObject.NULL) {

                                            mapWorkData = new HashMap<>();

                                            String WORK_TITLE = responseObject.getJSONObject("Fav_work").getJSONObject("favourite").getString("address");
                                            String WORK_ADDRESS = responseObject.getJSONObject("Fav_work").getJSONObject("favourite").getString("address");
                                            String WORK_LATITUDE = responseObject.getJSONObject("Fav_work").getJSONObject("favourite").getString("latitude");
                                            String WORK_LONGITUDE = responseObject.getJSONObject("Fav_work").getJSONObject("favourite").getString("longitude");

                                            mapWorkData.put(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES, "icon_work_place");
                                            mapWorkData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, WORK_TITLE);
                                            mapWorkData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, WORK_ADDRESS);
                                            mapWorkData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, WORK_LATITUDE);
                                            mapWorkData.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, WORK_LONGITUDE);
                                            mapWorkData.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, "0");

                                            Work_Address_Title.setText(WORK_TITLE);
                                            Work_Address_Address.setText(WORK_ADDRESS);
                                        } else {
                                            user_favourite_work.setVisibility(View.GONE);
                                            user_favourite_work_divider.setVisibility(View.GONE);
                                        }

                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }


                                }else {
                                    user_favourite_home.setVisibility(View.GONE);
                                    user_favourite_home_divider.setVisibility(View.GONE);
                                    user_favourite_work.setVisibility(View.GONE);
                                    user_favourite_work_divider.setVisibility(View.GONE);
                                }

                                dataList = EApplication.getInstance().getAllToList(DBTableFields.TABLE_FREQUENT_SEARCH);

                                ListView_Favourites.setAdapter(new AdapterFavourites(getActivity(), dataList));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error", "Error: " + error.getMessage());
                }
            }) {


                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            app.addToRequestQueue(req,"low");

        } else {
            NetworkUtils.showNoConnectionDialog(getActivity());
        }
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    EApplication.getInstance().hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

}
