package com.EddyCabLatest.application.moduleUser.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.EddyCabLatest.application.moduleDriver.activity.Driver_Activity_Login;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_SmsVerification extends AppCompatActivity implements View.OnClickListener {
    TextView textSendto;
    EditText edt_sms_code;
    String phoneNumber = "";
    String userId = "";
    SessionManager session;
    ImageView img_back_arrow;
    TextView txt_resendcode;
    String userDetail;
    boolean visible = false;
    private Button submit_verification;
    private String acces_token;
    private String language;

    @Override
    protected void onDestroy() {
        visible = false;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        visible = true;
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_verification_sms);
        phoneNumber = getIntent().getStringExtra("phoneNo");
        userDetail = getIntent().getStringExtra("user_detail");
        userId = getIntent().getStringExtra("user_id");
        acces_token = getIntent().getStringExtra("acces_token");

        session = new SessionManager(Activity_SmsVerification.this);

     /*   img_back_arrow = (ImageView) findViewById(R.id.img_back_arrow);
        img_back_arrow.setOnClickListener(this);*/
        txt_resendcode = (TextView) findViewById(R.id.txt_resendcode);
        txt_resendcode.setOnClickListener(this);
       /* textSendto = (TextView) findViewById(R.id.textSendto);*/
       /* textSendto.setText(getResources().getText(R.string.sms_to) + " " + phoneNumber);*/
        edt_sms_code = (EditText) findViewById(R.id.edt_sms_code);
        submit_verification = (Button) findViewById(R.id.submit_verification);

        submit_verification.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity__sms_verification, menu);

        return true;
    }
    @Override
    public void onBackPressed() {


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_arrow:
                //  onBackPressed();
                break;
            case R.id.txt_resendcode:
                methodResendCode(phoneNumber);
                break;
            case R.id.submit_verification:
                if(!edt_sms_code.getText().toString().trim().equals(""))
                { methodsendCode();

                }
                else
                {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_enter_code));

                }


                break;

            default:
                break;
        }
    }

    private void methodResendCode(String userPhoneNumber) {
        if (NetworkUtils.isNetworkAvailable(Activity_SmsVerification.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_SmsVerification.this);
            CustomProgressBarDialog.progressDialog.show();
            CustomRequest customRequest = new CustomRequest(Request.Method.GET,
                    Config.APP_BASE_URL + Config.URL_RESEEND_CODE_GET + userPhoneNumber, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CustomProgressBarDialog.progressDialog.dismiss();
//                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.entercode));
                    EApplication.getInstance().showToastMessageFunction(getResources().getText(R.string.sms_to) + " " + phoneNumber);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            EApplication.getInstance().addToRequestQueue(customRequest, "high");
        } else {
            NetworkUtils.showNoConnectionDialog(Activity_SmsVerification.this);
        }


    }
    private void methodsendCode() {
        if (!edt_sms_code.getText().toString().trim().equals("")) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_SmsVerification.this);
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            CustomProgressBarDialog.progressDialog.show();

            if (NetworkUtils.isNetworkAvailable(Activity_SmsVerification.this)) {
                final HashMap<String, String> params = new HashMap<>();
                params.put("mobile_number", phoneNumber);
                params.put("code", edt_sms_code.getText().toString().trim());
                params.put("user_id", userId);
                Log.e("parameters", params.toString());
                String URL_VERIFICATION_CODE = Config.APP_BASE_URL + Config.CONFIRM_VERIFICATION_CODE;
                CustomRequest req = new CustomRequest(Request.Method.POST, URL_VERIFICATION_CODE, params,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                Log.e("response", response.toString());
                                boolean error = false;
                                try {
                                    error = response.getBoolean("error");
                                    if (!error) {
                                        if (session.getAppUserType().equals(Config.USER_TYPE_DRIVER)) {
                                            edt_sms_code.setEnabled(true);
                                            Intent intent = new Intent(Activity_SmsVerification.this, Driver_Activity_Login.class);
                                            startActivity(intent);
                                            finish();
// loginDriver(userDetail);
                                        } else {
                                            edt_sms_code.setEnabled(true);
                                            Intent intent = new Intent(Activity_SmsVerification.this, Activity_UserLogin.class);
                                            startActivity(intent);
                                            finish();
//f
                                        }
                                    } else {
                                        edt_sms_code.setEnabled(true);
                                        String message = response.getString("message");
                                        EApplication.getInstance().showToastMessageFunction(message);
                                        txt_resendcode.setVisibility(View.VISIBLE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        edt_sms_code.setEnabled(true);
                        CustomProgressBarDialog.progressDialog.dismiss();
                    }
                })  {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return params;
                    }

                    @Override
                    public Request.Priority getPriority() {
                        return Request.Priority.HIGH;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                        String current_language = session.getAppLanguage();
                        Log.e("current_language", current_language);
                        if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                            language = "en";
                        }
                        if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                            language = "np";
                        }
                        headers.put("lang", language);
                        return headers;
                    }
                };
                int socketTimeout = 20000; //30 seconds
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                req.setRetryPolicy(policy);
                EApplication.getInstance().addToRequestQueue(req, "high");
            } else {
                NetworkUtils.showNoConnectionDialog(Activity_SmsVerification.this);
            }
        } else {

            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_enter_code));
        }


    }

    public void loginUser(String userDetail) {
        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_SmsVerification.this);

        if (visible) CustomProgressBarDialog.progressDialog.show();
        if (userDetail.contains("ï»¿")) {
            userDetail = userDetail.substring(userDetail.indexOf("{"));
        }
        try {
            String lastReservationId = "none";
            JSONObject responseObject = new JSONObject(userDetail);
            boolean error = responseObject.getBoolean("error");

            if (!error) {
                try {
                    EApplication.getInstance().showToastMessageFunction("No error");

                    CustomProgressBarDialog.progressDialog.dismiss();
                    JSONObject user_details = responseObject.getJSONObject("user");

                    int user_id = user_details.getInt("id");

                    String user_name = user_details.getString("fullname");
                    String user_email = user_details.getString("email");

                    String user_ProfilePic = user_details.getString("user_pp");
                    String user_referralcode = user_details.getString("referral_code");
                    String total_credit = user_details.getString("total_credit");
                    String corrected_rating = "0.0";
                                                        /* Save User Details to the Shared Preferences*/

                    session.createLoginSession(String.valueOf(user_id), user_name, user_email, user_ProfilePic,
                            corrected_rating, user_details.toString(), user_referralcode, "mobile_no", "license_no", lastReservationId, acces_token);
                                                            /* Save banana credit */

                    //session.setUserBananaCredit(total_credit);
                    Intent intent = new Intent(Activity_SmsVerification.this, Activity_CreateReservation.class);
                    EApplication.getInstance().setPreviousReservationStatus(true);
                    startActivity(intent);
                    Activity_SmsVerification.this.finish();
                } catch (Exception ex) {
                }

            } else {
                CustomProgressBarDialog.progressDialog.dismiss();

            }


        } catch (JSONException e) {
            e.printStackTrace();
            CustomProgressBarDialog.progressDialog.dismiss();
        }

        CustomProgressBarDialog.progressDialog.dismiss();
    }

    public void loginDriver(String userDetail) {

        if (userDetail.contains("ï»¿")) {
            userDetail = userDetail.substring(userDetail.indexOf("{"));
        }

        try {
            JSONObject response = new JSONObject(userDetail);

            boolean error = response.getBoolean("error");
            String message = response.getString("message");
            if (!error) {
                JSONObject user_details = response.getJSONObject("user");
                JSONObject driver_details = response.getJSONObject("driver");
                String user_id = user_details.getString("id");
                String user_name = user_details.getString("fullname");
                String driver_type = driver_details.getString("driver_type");

                String user_email = user_details.getString("email");
                String total_credit = user_details.getString("total_credit");

                String user_picture = response.getJSONObject("driver").getString("profile_picture");
                String mobile_no = user_details.getString("mobile_number");
                String liscense_no = response.getJSONObject("driver").getString("license_no");
                String referral_code = user_details.getString("referral_code");

                String corrected_rating = "0.0";


                String user_referral_code = "";
                if (!TextUtils.equals(referral_code, "null") && !TextUtils.isEmpty(referral_code))
                    user_referral_code = referral_code;
                else user_referral_code = "xxxx";



                                            /* Save User Details to the Shared Preferences*/
                session.createLoginSession(user_id, user_name, user_email, user_picture, corrected_rating, user_details.toString(), user_referral_code, mobile_no, liscense_no, driver_type, acces_token);

                                            /* Save user banana Credit */
                //session.setUserBananaCredit(total_credit);


                                            /* Dismiss the activity and resume Main Activity */
                Intent in = new Intent(Activity_SmsVerification.this, Driver_MainActivity.class);
                startActivity(in);
                finish();

            } else {
                EApplication.getInstance().showToastMessageFunction(message);
            }

        } catch (JSONException e) {
            EApplication.getInstance().showToastMessageFunction("inside catch   " + e.getMessage());
        }

        CustomProgressBarDialog.progressDialog.dismiss();

    }
}
