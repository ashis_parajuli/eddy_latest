package com.EddyCabLatest.application.moduleUser.modal;

/**
 * Created by User on 6/23/2015.
 */
public class NearbyPlaces {
    private String title;
    private String description;
    private String kms;
    private int imageStar;

    public NearbyPlaces(String title, String description, String kms, int imageStar) {
        this.title = title;
        this.description = description;
        this.kms = kms;
        this.imageStar = imageStar;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public int getImageStar() {
        return imageStar;
    }


    public void setImageStar(int imageStar) {
        this.imageStar = imageStar;
    }
}
