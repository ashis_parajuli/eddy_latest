package com.EddyCabLatest.application.moduleUser.fragment;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserTripHistory;
import com.EddyCabLatest.application.moduleUser.adapter.JobHistoryAdapter;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class User_FragmentJobHistory extends Fragment {
    private ListView listViewUserHistory;
    private TextView emptyListViewMsg;
    private SessionManager session;
    String UserID;
    int totalJobs;
    int loadNoOfJob = 20;
    View footerView;
    String toLocationAddress;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    private String language;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_layout_jobhistory, container, false);

        session = new SessionManager(getActivity());
        emptyListViewMsg = (TextView) rootView.findViewById(android.R.id.empty);
        listViewUserHistory = (ListView) rootView.findViewById(R.id.user_history_listView);


        if (NetworkUtils.isNetworkAvailable(getActivity())) {

            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
            CustomProgressBarDialog.progressDialog.show();

            HashMap<String, String> userDetails = session.getUserDetails();
            UserID = userDetails.get(SessionManager.KEY_ID);
            Log.e("user_id", UserID);
            apiCallForHistory(loadNoOfJob);


            //For See More View
            //If all data is loded then see more should not be displated
            footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.user_job_history_footer, null, false);
            listViewUserHistory.addFooterView(footerView);
            footerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadNoOfJob += 20;
                    apiCallForHistory(loadNoOfJob);
                }
            });
        } else {
            EApplication.getInstance().showToastMessageFunction("No network Connected !!!");
        }

        listViewUserHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getActivity(), Activity_UserTripHistory.class);
//                intent.putExtra("trip_time", dataList.get(position).get(DBTableFields.USER_TRIP_DATE_TIME));
                intent.putExtra("from", dataList.get(position).get(DBTableFields.USER_TRIP_FROM_LOCATION));
                intent.putExtra("to", dataList.get(position).get(DBTableFields.USER_TRIP_TO_LOCATION));
                intent.putExtra("status", dataList.get(position).get(DBTableFields.USER_TRIP_STATUS));
                intent.putExtra("ride_fare", dataList.get(position).get(DBTableFields.USER_TRIP_AMOUNT));
                intent.putExtra("user_id", dataList.get(position).get(DBTableFields.USER_TRIP_USER_ID));
                intent.putExtra("address", toLocationAddress);

                intent.putExtra("driver_name", dataList.get(position).get(DBTableFields.USER_TRIP_DRIVER_FULLNAME));
                intent.putExtra("mobile", dataList.get(position).get(DBTableFields.USER_TRIP_MOBILE));
                intent.putExtra("driver_id", dataList.get(position).get(DBTableFields.USER_TRIP_DRIVER_ID));
                intent.putExtra("reservation_id", dataList.get(position).get(DBTableFields.USER_TRIP_RESERVATION_ID));
                intent.putExtra("driver_image", dataList.get(position).get(DBTableFields.USER_TRIP_DRIVER_PROFILE));
                intent.putExtra("pickup_lat", dataList.get(position).get(DBTableFields.USER_TRIP_PICKUP_LAT));
                intent.putExtra("pickup_long", dataList.get(position).get(DBTableFields.USER_TRIP_PICKUP_LONG));
                intent.putExtra("dropoff_lat", dataList.get(position).get(DBTableFields.USER_TRIP_DROPOFF_LAT));
                intent.putExtra("dropoff_long", dataList.get(position).get(DBTableFields.USER_TRIP_DROPOFF_LONG));
                startActivity(intent);
            }
        });
        return rootView;
    }

    public void apiCallForHistory(int no) {

        String url = Config.APP_BASE_URL + Config.USER_TRIP_HISTORY_URL + UserID + "/" + no;
        Log.e("url", url);
        CustomRequest req = new CustomRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseObject) {
                        dataList = new ArrayList<>();
                        Log.e("Response1", "User Job History Response Objcet");
                        Log.e("Response2", "User Job History Response Objcet" + responseObject.toString());

                        try {
                            boolean error = responseObject.getBoolean("error");
                            totalJobs = Integer.parseInt(responseObject.getString("count").toString());
                            if (!error) {
                                JSONArray objectArray = responseObject.getJSONArray("reservations");

                                if (objectArray.length() < 22) {
                                    footerView.setVisibility(View.INVISIBLE);
                                }
                                else
                                {
                                    footerView.setVisibility(View.VISIBLE);

                                }
                                for (int i = 0; i < objectArray.length(); i++) {

                                    HashMap<String, String> mapList = new HashMap<>();
                                    JSONObject dataObject = (JSONObject) objectArray.get(i);
                                    String datetime = dataObject.getString("datetime");
                                    String fromLocationTitle = dataObject.getJSONObject("pickup_location").getString("title");
                                    String fromLocationAddress = dataObject.getJSONObject("pickup_location").getString("address");
                                    String toLocationTitle = dataObject.getJSONObject("dropoff_location").getString("title");
                                    toLocationAddress = dataObject.getJSONObject("dropoff_location").getString("address");
                                    String statusTrip = dataObject.getString("status");
                                    String amountTrip = dataObject.getString("ride_fare");
                                    String fullName = dataObject.getJSONObject("oneuser").getString("fullname");
                                    String mobile_number = dataObject.getJSONObject("oneuser").getString("mobile_number");
                                    String driverId = dataObject.getString("driver_id");
                                    String userId = dataObject.getString("user_id");
                                    String reservationId = dataObject.getString("id");
                                    String driverProfile = "";
                                    if (dataObject.get("driver") != JSONObject.NULL)
                                        driverProfile = dataObject.getJSONObject("driver").getString("profile_picture");
                                    String pickupLat = dataObject.getJSONObject("pickup_location").getString("latitude");
                                    String pickupLong = dataObject.getJSONObject("pickup_location").getString("longitude");
                                    String dropoffLat = dataObject.getJSONObject("dropoff_location").getString("latitude");
                                    String dropoffLong = dataObject.getJSONObject("dropoff_location").getString("longitude");
                                    String UserName = "";

                                    if (!TextUtils.equals(fullName, "null"))
                                        UserName = fullName;
                                    if (amountTrip == null) {
                                        amountTrip = "0";
                                    }

                                    mapList.put(DBTableFields.USER_TRIP_DATE_TIME, datetime);
                                    mapList.put(DBTableFields.USER_TRIP_FROM_LOCATION, fromLocationTitle + ", " + fromLocationAddress);
                                    mapList.put(DBTableFields.USER_TRIP_TO_LOCATION, toLocationTitle + ", " + toLocationAddress);
                                    mapList.put(DBTableFields.USER_TRIP_STATUS, statusTrip);
                                    mapList.put(DBTableFields.USER_TRIP_AMOUNT, amountTrip);
                                    mapList.put(DBTableFields.USER_TRIP_DRIVER_FULLNAME, UserName);
                                    mapList.put(DBTableFields.USER_TRIP_DRIVER_ID, driverId);
                                    mapList.put(DBTableFields.USER_TRIP_USER_ID, userId);
                                    mapList.put(DBTableFields.USER_TRIP_RESERVATION_ID, reservationId);
                                    mapList.put(DBTableFields.USER_TRIP_DRIVER_PROFILE, driverProfile);
                                    mapList.put(DBTableFields.USER_TRIP_PICKUP_LAT, pickupLat);
                                    mapList.put(DBTableFields.USER_TRIP_PICKUP_LONG, pickupLong);
                                    mapList.put(DBTableFields.USER_TRIP_DROPOFF_LAT, dropoffLat);
                                    mapList.put(DBTableFields.USER_TRIP_DROPOFF_LONG, dropoffLong);
                                    mapList.put(DBTableFields.USER_TRIP_MOBILE, mobile_number);
                                    dataList.add(mapList);
                                    Log.i("Data ::", dataList.toString());

                                }

                                JobHistoryAdapter adapter = new JobHistoryAdapter(getActivity(), dataList);
                                listViewUserHistory.setAdapter(adapter);
                                listViewUserHistory.setEmptyView(emptyListViewMsg);

                            } else
                                EApplication.getInstance().showToastMessageFunction("Reservation history not found !!!");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        CustomProgressBarDialog.progressDialog.dismiss();
                        if (dataList.size() == totalJobs) {
                            listViewUserHistory.removeFooterView(footerView);
                        } else if (dataList.size() == 0) {
                            listViewUserHistory.removeFooterView(footerView);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                }

                if (error instanceof TimeoutError) {
                    Log.e("Volley", "TimeoutError");
                } else if (error instanceof NoConnectionError) {
                    Log.e("Volley", "NoConnectionError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("Volley", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("Volley", "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.e("Volley", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.e("Volley", "ParseError");
                }




                Log.e("onErrorResponse", "Error: " + error + "");

                CustomProgressBarDialog.progressDialog.dismiss();

            }
        }) {


            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.e("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);
                return headers;
            }
        };;
        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        EApplication.getInstance().addToRequestQueue(req, "high");

    }


}
