package com.EddyCabLatest.application.moduleUser.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 7/10/2015.
 */
public class AdapterUserWallet extends BaseAdapter {

    private Context context;
    private ArrayList<HashMap<String, String>> dListItems;


    public AdapterUserWallet(Context context, ArrayList<HashMap<String, String>> dListItems) {
        this.context = context;
        this.dListItems = dListItems;
    }



    @Override
    public int getCount() {
        return dListItems.size();
    }

    @Override
    public Object getItem(int i) {
        return dListItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if(convertView==null){
            LayoutInflater dInflater=(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView=dInflater.inflate(R.layout.single_list_item_wallet,null);
        }

        TextView txt_date=(TextView)convertView.findViewById(R.id.txt_date);
        TextView txt_buy_banana=(TextView)convertView.findViewById(R.id.txt_buy_banana);
        TextView txt_free_banana=(TextView)convertView.findViewById(R.id.txt_free_banana);
        TextView txt_used_banana=(TextView)convertView.findViewById(R.id.txt_used_banana);

        txt_date.setText(dListItems.get(position).get(DBTableFields.USER_WALLET_DATE_TIME));
        txt_buy_banana.setText(dListItems.get(position).get(DBTableFields.USER_WALLET_BUY_BANANA));
        txt_free_banana.setText(dListItems.get(position).get(DBTableFields.USER_WALLET_FREE_BANANA));
        txt_used_banana.setText(dListItems.get(position).get(DBTableFields.USER_WALLET_USE_BANANA));

        return convertView;
    }
}
