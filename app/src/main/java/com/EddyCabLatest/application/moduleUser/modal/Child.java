package com.EddyCabLatest.application.moduleUser.modal;

/**
 * Created by User on 6/12/2015.
 */
public class Child {

    private String title;
    private String description;
    private String kms;
    private int Image;
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKms() {
        return kms;
    }


    public void setKms(String kms) {
        this.kms = kms;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int Image) {
        this.Image = Image;
    }
}
