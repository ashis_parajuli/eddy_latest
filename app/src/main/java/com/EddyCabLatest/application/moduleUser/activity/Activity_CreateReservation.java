package com.EddyCabLatest.application.moduleUser.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.utilities.gpsTrackerUser;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Activity_CreateReservation extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    EApplication app;
    SessionManager session;
    Toolbar app_toolBar;
    gpsTrackerUser gpsTracker;
    TextView appTitle, userBananaCredit;
    RelativeLayout userBananaCreditLayout;
    public String strReservationId1;
    TextView pickup_location_source;
    Button btn_pay_by_banana, btn_pay_cash;
    EditText im_here_notes;
    EditText edt_voucher_code;

    private MapView mapView;
    private GoogleMap map;

    private LatLng startPoint;

    private Polyline polyline;
    /*
    private int Banana_Credit;*/
    private String user_ID;
    public String ReservationID;
    private int SESSION_RESERVATION_ID;
    private String sessionReservationId;
    private int RESERVATION_ID;

    private int distance;
    private int padding = 40;
    private String reservationType;

    StringRequest stringRequest;
    private boolean visible = false;

    String language = "";


    HashMap<String, String> sourceLocation;

    private CountDownTimer myCountdownTimer;
    Bundle savedInstanceState;
    private Dialog dialog_main_cancel;

    private LinearLayout pick_up_click;
    private gpsTrackerUser gps_tracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_create_reservation);

        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);

        appTitle = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
        appTitle.setText(getResources().getString(R.string.newbooking));

        // Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);

        // Initializing Application Instances
        app = EApplication.getInstance();
        session = new SessionManager(Activity_CreateReservation.this);
        reservationType = session.getReservationType();

        if (reservationType.equals("none")) {
            reservationType = "bike";
        }

        // References Init
 /*   btn_pay_by_banana = (Button) findViewById(R.id.btn_pay_by_banana);*/
        btn_pay_cash = (Button) findViewById(R.id.btn_pay_cash);
        pickup_location_source = (TextView) findViewById(R.id.pickup_location_source);
        //pickup_location_destination = (TextView) findViewById(R.id.pickup_location_destination);
        pick_up_click = (LinearLayout) findViewById(R.id.pick_up_click);

/*    addCreditLayout = (TextView) findViewById(R.id.addCreditLayout);*/
        im_here_notes = (EditText) findViewById(R.id.im_here_notes);
    /*edt_voucher_code = (EditText) findViewById(R.id.edt_voucher_code);*/

        userBananaCreditLayout = (RelativeLayout) findViewById(R.id.userBananaCreditLayout);
        userBananaCredit = (TextView) app_toolBar.findViewById(R.id.user_banana_credit);
  /*  if (session.isLoggedIn()) {
       *//* userBananaCredit.setText(session.getUserBananaCredit());*//*
    } else userBananaCreditLayout.setVisibility(View.GONE);
    userBananaCredit
    Layout.setOnClickListener(this);
*/



        gpsTracker = new gpsTrackerUser(Activity_CreateReservation.this);
        sourceLocation = session.getSourceLocation();
        if (session.isSourceLocationSet()) {
            if (sourceLocation == null) {
                if (gpsTracker.canGetLocation()) {
                    startPoint = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                    MarkerOptions options = new MarkerOptions();
                    options.position(startPoint);
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));
                    if (startPoint != null) {
                        map.addMarker(options);

                        gpsTrackerUser gps = new gpsTrackerUser(Activity_CreateReservation.this);

                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(startPoint, 10);
                        map.animateCamera(cameraUpdate);

                    }
                } else {
                    startPoint = new LatLng(27, 85);
                }

            }


            startPoint = new LatLng(Double.parseDouble(sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LAT)),
                    Double.parseDouble(sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LANG)));


            Log.i("===", "start point is" + startPoint);


            pickup_location_source.setText(sourceLocation.get(SessionManager.SOURCE_LOCATION_NAME) + "\n " + sourceLocation.get(SessionManager.SOURCE_LOCATION_ADDRESS));
        }


        if (session.isSourceLocationSet() && session.isDestinationLocationSet()) {
            calculateLocationDistance();
        }

  /*  String userCredit = session.getUserBananaCredit();
    Banana_Credit = Integer.parseInt(userCredit);*/

        initializaMap(savedInstanceState);
      /*  // Map view Init Section
        mapView = (MapView) findViewById(R.id.content_map_view);
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        map = mapView.getMap();
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(false);*/
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    /*    map.setMyLocationEnabled(true);*/


        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
       /* MapsInitializer.initialize(Activity_CreateReservation.this);*/


        /* Check For Route Draw */
        if (session.isSourceLocationSet() && session.isDestinationLocationSet()) {

            Log.e("Message", "Draw route ....");


        } else {

            if (session.isSourceLocationSet()) {

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(startPoint, Config.CAMERA_FACTORY_UPDATE);
                map.animateCamera(cameraUpdate);
            }

        }


        String RESERVATION_STATE = session.getReservationByUser();

        if (!TextUtils.isEmpty(RESERVATION_STATE) && !TextUtils.equals(RESERVATION_STATE, null)) {

            switch (RESERVATION_STATE) {

                case DBTableFields.RESERVATION_STATE_NEW:


                    break;

                case DBTableFields.RESERVATION_STATE_PENDING:
                    Intent intentPending = new Intent(Activity_CreateReservation.this, Activity_UserSelectDriver.class);
                    startActivity(intentPending);
                    this.finish();
                    break;

                case DBTableFields.RESERVATION_STATE_WAITING_DRIVER_RESPONSE:
                    break;

                case DBTableFields.RESERVATION_STATE_ON_ROUTE:
                    break;

                case DBTableFields.RESERVATION_STATE_CANCELLED:
                case DBTableFields.RESERVATION_STATE_COMPLETED:
                    break;

            }
        }


        // apply click events
    /*btn_pay_by_banana.setOnClickListener(this);*/
        pick_up_click.setOnClickListener(this);

   /* addCreditLayout.setOnClickListener(this);*/
        btn_pay_cash.setOnClickListener(this);

    }

    private void calculateLocationDistance() {

        Location loc1 = new Location("");
        loc1.setLatitude(startPoint.latitude);
        loc1.setLongitude(startPoint.longitude);


//        EApplication.getInstance().showToastMessageFunction("distance from Location is.:" + distance);
    }

    @Override
    protected void onPause() {
        visible = false;
        super.onPause();

    }


    @Override
    protected void onStop() {

        visible = false;
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        reservationType = session.getReservationType();
        if (reservationType.equals("none")) {
            reservationType = "bike";
        }
        visible = true;
checkStatus(false);

    }

    @Override
    public void onDestroy() {
        if (myCountdownTimer != null) {
            myCountdownTimer.cancel();
            myCountdownTimer = null;
        }

        visible = false;
        mapView.onDestroy();
        super.onDestroy();


    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.pick_up_click:


                String RESERVATION_STATE = session.getReservationByUser();


                if (RESERVATION_STATE .equalsIgnoreCase(DBTableFields.RESERVATION_STATE_PENDING)) {

                    Intent intentPending_waiting = new Intent(Activity_CreateReservation.this, Activity_UserSelectDriver.class);
                    Log.e("state", "pending");

                    startActivity(intentPending_waiting);


                } else if ((RESERVATION_STATE .equalsIgnoreCase( DBTableFields.RESERVATION_STATE_DRIVER_CONFIRMED))||(RESERVATION_STATE .equalsIgnoreCase(DBTableFields.RESERVATION_STATE_ON_ROUTE))){
                    Intent intent_locate = new Intent(Activity_CreateReservation.this, Activity_UserLocateDriver.class);
                    intent_locate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_locate);

                } else {
                    Intent sourceIntent = new Intent(Activity_CreateReservation.this, Activity_PickupLocation.class);
                    sourceIntent.putExtra("ADDRESS_REQUEST", "sourceLocation");
                    EApplication.getInstance().fromCreateReservation = true;
                    EApplication.getInstance().fromFragmentIntent = false;
                    startActivity(sourceIntent);

                    finish();
                }




                break;

            /*case R.id.drop_off_click:
                Intent destinationIntent = new Intent(Activity_CreateReservation.this, Activity_PickupLocation.class);
                destinationIntent.putExtra("ADDRESS_REQUEST", "destinationLocation");
                EApplication.getInstance().fromCreateReservation = true;
                EApplication.getInstance().fromFragmentIntent = false;
                startActivity(destinationIntent);
                finish();
                break;*/


            case R.id.btn_pay_cash:
                checkStatus(true);


                break;

               /* Intent buyBananaIntent = new Intent(Activity_CreateReservation.this, BuyBananaCredit.class);
                startActivity(buyBananaIntent);
                break;*/


        }
    }


    private void checkStatus(boolean move) {

        if (session.isLoggedIn()) {
            String RESERVATION_STATE = session.getReservationByUser();


            if (RESERVATION_STATE .equalsIgnoreCase(DBTableFields.RESERVATION_STATE_PENDING)) {

                Intent intentPending_waiting = new Intent(Activity_CreateReservation.this, Activity_UserSelectDriver.class);
                Log.e("state", "pending");

                startActivity(intentPending_waiting);


            } else if ((RESERVATION_STATE .equalsIgnoreCase( DBTableFields.RESERVATION_STATE_DRIVER_CONFIRMED) )|| (RESERVATION_STATE .equalsIgnoreCase( DBTableFields.RESERVATION_STATE_ON_ROUTE))) {
                Intent intent_locate = new Intent(Activity_CreateReservation.this, Activity_UserLocateDriver.class);
                intent_locate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent_locate);

            } else {
                if(move)
                {

                    sourceLocation = session.getSourceLocation();
                    if (sourceLocation != null) {

                        gps_tracker = new gpsTrackerUser(Activity_CreateReservation.this);
                        session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);

                        createReservation_ByUser_FirstTime("CASH");


                    } else {

                        Toast.makeText(Activity_CreateReservation.this, getResources().getString(R.string.please_chosse_pic_up_location), Toast.LENGTH_LONG).show();

                    }

                }

            }

        } else {
            Intent intent = new Intent(Activity_CreateReservation.this, Activity_UserLogin.class);
            startActivity(intent);
        }

    }



    private void methodgoForLogin() {
        if (session.isSourceLocationSet()) {
            //&& !TextUtils.isEmpty(im_here_notes.getText().toString().trim())

            session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);
            createReservation_ByUser_FirstTime("CASH");

        } else {
            if (!session.isSourceLocationSet())
                Toast.makeText(Activity_CreateReservation.this, getResources().getString(R.string.validate_locations), Toast.LENGTH_LONG).show();


        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        EApplication.getInstance().showToastMessageFunction("Internet Connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        EApplication.getInstance().showToastMessageFunction("Connection Suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("LOG", connectionResult.toString());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(Activity_CreateReservation.this, Activity_UserMain.class);
                startActivity(intent);
                this.finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /*
    * createReservation_ByUser_FirstTime
    * */

    private void createReservation_ByUser_FirstTime(final String Reservation_Type) {

        if (NetworkUtils.isNetworkAvailable(Activity_CreateReservation.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_CreateReservation.this);
            if (visible) CustomProgressBarDialog.progressDialog.show();
            CustomProgressBarDialog.progressDialog.setCancelable(true);
            HashMap<String, String> userDetails = session.getUserDetails();
            user_ID = userDetails.get(SessionManager.KEY_ID);

            final HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_ID);
            params.put("use_credit", "0");
            params.put("pickup_lat", String.valueOf(startPoint.latitude) + "");
            params.put("pickup_long", String.valueOf(startPoint.longitude) + "");

            params.put("notes", im_here_notes.getText().toString().trim() + "");
            params.put("pickup_title", sourceLocation.get(SessionManager.SOURCE_LOCATION_NAME) + "");
            params.put("pickup_address", sourceLocation.get(SessionManager.SOURCE_LOCATION_ADDRESS) + "");

            params.put("reservation_type", reservationType);//tuktuk

            Log.e("Parameters", params.toString());
            String REQUEST_URL = Config.APP_BASE_URL + Config.USER_RESERVATION_URL;
            Log.e("REQUEST_URL ::", REQUEST_URL);
            StringRequest req = new StringRequest(Request.Method.POST, REQUEST_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String responseObject) {
                            CustomProgressBarDialog.progressDialog.dismiss();
                            String newResponse = responseObject;
                            if (responseObject.contains("Ã¯Â»Â¿")) {
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }
                            try {
                                JSONObject response = new JSONObject(newResponse);
                                boolean error = response.getBoolean("error");
                                Log.e("ERROR", String.valueOf(error));
                                String message = response.getString("message");
                                if (!error) {
                                    EApplication.getInstance().setPreviousReservationStatus(false);
                                    ReservationID = response.getString("Reservation");
                                    RequestPushNotificationToDriver(ReservationID);
                                    session.createReservationNewUser(ReservationID, Reservation_Type);
                                    reservationCreateSuccess(ReservationID);
                                } else if (error) {
                                    Log.e("errr", "onResponse: " + response.toString());
                                    wantToCancelDialog(getResources().getString(R.string.alert), response.getString("message"), response.getString("reservation_id"));


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            CustomProgressBarDialog.progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    VolleyLog.d("Error", "Error: " + error);
                    EApplication.getInstance().showToastMessageFunction("" + error);
                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };

            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(req, "high");
        } else NetworkUtils.showNoConnectionDialog(Activity_CreateReservation.this);


    }


    //dialog to cancel new Reservation

    private void wantToCancelDialog(final String alertTitle, String message, final String reservId) {

        final Dialog dialog = new Dialog(Activity_CreateReservation.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_of_transparent_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        dialog_title.setText(alertTitle);
        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(message);
        if (visible) dialog.show();
        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        dialog_action.setText(getResources().getString(R.string.ok_cancel));
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservationCancelAPIExecute(reservId);
                dialog.dismiss();
            }
        });

        Button dialog_dismiss = (Button) dialog.findViewById(R.id.dialog_dismiss);
        dialog_dismiss.setText(getResources().getString(R.string.dismiss_cancel));
        dialog_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
     /*   if (visible) dialog.show();*/

    }

    private void errorOnCreateReservationDialog(String message, String timeleft) {

        final Dialog dialog = new Dialog(Activity_CreateReservation.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_success_failure_dialog);
        dialog.setCancelable(true);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        dialog_title.setText(message);

        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(timeleft);

        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (visible) dialog.show();

    }

    private void noBiddersFoundDialog(final String reservation_id) {
        final Dialog dialog_no_bidder = new Dialog(Activity_CreateReservation.this);
        dialog_no_bidder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_no_bidder.setContentView(R.layout.custom_success_failure_dialog);
        dialog_no_bidder.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog_no_bidder.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog_no_bidder.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog_no_bidder.findViewById(R.id.dialog_title);
        dialog_title.setText(getResources().getString(R.string.alert));
        TextView dialog_message = (TextView) dialog_no_bidder.findViewById(R.id.dialog_message);
        dialog_message.setText(getResources().getString(R.string.no_SELECTders_found));
        Button dialog_action = (Button) dialog_no_bidder.findViewById(R.id.dialog_action);
        if (visible) dialog_no_bidder.show();
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_no_bidder.dismiss();
                reservationCancelAPIExecute(reservation_id);
            }
        });
    }

    private void reservationCreateSuccess(final String ReservationID) {
        dialog_main_cancel = new Dialog(Activity_CreateReservation.this);
        dialog_main_cancel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_main_cancel.setContentView(R.layout.custom_dailog_reservation_waiting);
        dialog_main_cancel.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog_main_cancel.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog_main_cancel.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog_main_cancel.getWindow().setAttributes(paramLayout);
        final TextView txt_count_down = (TextView)
                dialog_main_cancel.findViewById(R.id.txt_count_down);
        myCountdownTimer = new CountDownTimer(Config.COUNTDOWN_TIME, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_count_down.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                if (myCountdownTimer != null) {
                    myCountdownTimer.cancel();
                    myCountdownTimer = null;
                    //LoadDialogForCancelReservation(ReservationID);

                }

                if (dialog_main_cancel != null)

                    LoadBiddingResponse(ReservationID);

            }
        }.start();
        if ((dialog_main_cancel != null) && (!dialog_main_cancel.isShowing())) {
            if (visible) dialog_main_cancel.show();
            ImageView action_btn_quiet = (ImageView) dialog_main_cancel.findViewById(R.id.action_btn_quiet);
            action_btn_quiet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //  myCountdownTimer.cancel();
                    LoadDialogForCancelReservation(ReservationID);
                }
            });
        }


    }

    private void LoadDialogForCancelReservation(final String reservationID) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getResources().getString(R.string.warningTitle));
        builder.setMessage(getResources().getString(R.string.reservation_cancel_message));
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //  myCountdownTimer.start();

            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_CreateReservation.this);
                if (visible) CustomProgressBarDialog.progressDialog.show();
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_ID);
                params.put("reservation_id", reservationID);

                Log.e("Parameters", params.toString());
                CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.API_RESERVATION_CANCEL, params,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject responseObject) {
                                // myCountdownTimer.cancel();
                                Log.e("Response Cancelled", responseObject.toString());
                                try {

                                    boolean error = responseObject.getBoolean("error");
                                    if (!error) {
                                        dialog_main_cancel.dismiss();
                                        if (myCountdownTimer != null) {
                                            myCountdownTimer.cancel();
                                            myCountdownTimer = null;

                                            //LoadDialogForCancelReservation(ReservationID);

                                        }
                                        CustomProgressBarDialog.progressDialog.dismiss();
                                        EApplication.getInstance().showToastMessageFunction(responseObject.getString("message"));
                                        session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                                        session.clearPickUPDropOffLocationAddress();

                                        Intent intent = new Intent(Activity_CreateReservation.this, Activity_UserMain.class);
                                        startActivity(intent);
                                       /* finish();*/
                                    } else
                                        EApplication.getInstance().showToastMessageFunction("" + responseObject.getString("message"));
                                    CustomProgressBarDialog.progressDialog.dismiss();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Error", "Error: " + error.getMessage());

                        CustomProgressBarDialog.progressDialog.dismiss();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                            Log.e("Driver_New_JObs_Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Driver_New_JObs_Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.e("Driver_New_JObs_Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Driver_New_JObs_Volley", "ParseError");
                        }
                    }
                }) {


                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                        String current_language = session.getAppLanguage();
                        Log.i("current_language", current_language);
                        if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                            language = "en";
                        }
                        if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                            language = "np";
                        }
                        headers.put("lang", language);
                        return headers;
                    }


                    @Override
                    public Request.Priority getPriority() {
                        return Request.Priority.HIGH;
                    }
                };

                EApplication.getInstance().addToRequestQueue(req, "high");
            }
        });


        if (visible) builder.show();
    }

    /*
    * Methods for Send push notification to reservation id
    * it may call recursive while failure to send push notification
    * */
    private void RequestPushNotificationToDriver(final String reservationID) {

        CustomRequest req = new CustomRequest(Request.Method.GET, Config.APP_BASE_URL + Config.GET_PUSH_NOTIFICATION_DRIVER + reservationID, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseObject) {
                        Log.e("Reservation Response", responseObject.toString());
                        try {

                            boolean error = responseObject.getBoolean("error");
                            if (error) {
                                RequestPushNotificationAgainMethods(reservationID);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }
        };

        int socketTimeout = 9000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);

        EApplication.getInstance().addToRequestQueue(req, "high");
    }

    /* This methods execute only when failed to sed push notification*/
    private void RequestPushNotificationAgainMethods(String reservationID) {
        RequestPushNotificationToDriver(reservationID);
    }
    /*
   * DIALOG TO HANDLE
   * CONTINUE OR CANCEL RESERVATION
   * Plus update shared preferences Reservation State
   * */


    /*
    * RESERVATION CANCEL EXECUTION
    * SET SHARED PREFERENCES TO STATE CANCELED TO PERMIT
    * NEW RESERVATION CREATION
    * */
    private void reservationCancelAPIExecute(String reservation_id) {

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_CreateReservation.this);
        if (visible) CustomProgressBarDialog.progressDialog.show();

        final HashMap<String, String> params = new HashMap<>();
        params.put("user_id", user_ID);
        params.put("reservation_id", reservation_id);

        Log.e("Parameters", params.toString());

        String REquestUrl = Config.APP_BASE_URL + Config.API_RESERVATION_CANCEL;

        StringRequest req = new StringRequest(Request.Method.POST, REquestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String responseObject) {
                        String newResponse = responseObject;
                        CustomProgressBarDialog.progressDialog.dismiss();

                        if (responseObject.contains("Ã¯Â»Â¿")) {
                            newResponse = newResponse.substring(newResponse.indexOf("{"));
                        }

                        try {
                            JSONObject response = new JSONObject(newResponse);
                            boolean error = response.getBoolean("error");
                            if (!error) {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);
                                //EApplication.getInstance().showToastMessageFunction(response.getString("message"));
                               /* session.clearPickUPDropOffLocationAddress();*/
                                EApplication.getInstance().setPreviousReservationStatus(false);
                               /* reservationCancelSuccessDialog();*/
                                Toast.makeText(Activity_CreateReservation.this, getResources().getString(R.string.canceled), Toast.LENGTH_SHORT).show();


                            } else

                            {
                                EApplication.getInstance().showToastMessageFunction("" + response.getString("message"));
                                CustomProgressBarDialog.progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            CustomProgressBarDialog.progressDialog.dismiss();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());

                CustomProgressBarDialog.progressDialog.dismiss();

            }
        }) {
            @Override

            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.i("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);
                return headers;
            }
        };

        EApplication.getInstance().addToRequestQueue(req, "high");
    }


    /*
    * API TO GET BIDDING RESPONSE
    * USER CHECK IT WHEN TIMER LOOP COMPLETE
    * GENERATE NEW DIALOG WITH RESPECT OF REW API RESPONSE
    * CONTINUE OR CANCEL // Continue if Bidding not found
    * */
    private void LoadBiddingResponse(final String reservation_id) {
        Log.e("Methods:", "Get Bidding Result");

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_CreateReservation.this);
        dialog_main_cancel.dismiss();
        if (visible) CustomProgressBarDialog.progressDialog.show();

//        EApplication.getInstance().showToastMessageFunction("reservation id inside loadbidding response" + reservation_id);
        String BIDDING_URL = Config.APP_BASE_URL + Config.GET_BIDDING_RESULT_URL + reservation_id;

        CustomRequest req = new CustomRequest(Request.Method.GET, BIDDING_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("===", "Response bidding API" + response.toString());

                        try {
                            boolean error = response.getBoolean("error");
                            if (!error) {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_PENDING);
                                Intent bidResponseIntent = new Intent(Activity_CreateReservation.this, Activity_UserSelectDriver.class);
                                startActivity(bidResponseIntent);
                                finish();
                            } else {
                                //reservationCancelAPIExecute(reservation_id);
                                //reservationFailureToGetBidDialog(reservation_id);
                                CustomProgressBarDialog.progressDialog.dismiss();
                                noBiddersFoundDialog(reservation_id);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        CustomProgressBarDialog.progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadTryAgainDialog(reservation_id);
                CustomProgressBarDialog.progressDialog.dismiss();
            }
        });


        int socketTimeout = 9000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);

        EApplication.getInstance().addToRequestQueue(req, "");
    }

    private void loadTryAgainDialog(final String reservation_id) {
        final Dialog try_again_dialog = new Dialog(Activity_CreateReservation.this);
        try_again_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        try_again_dialog.setContentView(R.layout.custom_two_button_dialog);
        try_again_dialog.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(try_again_dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        try_again_dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) try_again_dialog.findViewById(R.id.dialog_title);
        dialog_title.setText(getResources().getString(R.string.alert));
        TextView dialog_message = (TextView) try_again_dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(getResources().getString(R.string.no_internet_availeble_please_try_again));
        Button dialog_action = (Button) try_again_dialog.findViewById(R.id.dialog_action);
        Button dialog_action_no = (Button) try_again_dialog.findViewById(R.id.dialog_action_no);
        if (visible) try_again_dialog.show();
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try_again_dialog.dismiss();
                LoadBiddingResponse(reservation_id);
            }
        });


        dialog_action_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try_again_dialog.dismiss();
                // reservationCancelAPIExecute(reservation_id);
            }
        });
    }




    private void initializaMap(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
        MapsInitializer.initialize(Activity_CreateReservation.this);
        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(Activity_CreateReservation.this)) {
            case ConnectionResult.SUCCESS:
                mapView = (MapView) findViewById(R.id.content_map_view);
                mapView.onCreate(this.savedInstanceState);
                if (mapView != null) {
                    map = mapView.getMap();
                    map.getUiSettings().setMyLocationButtonEnabled(true);
                    map.getUiSettings().setRotateGesturesEnabled(false);
                    map.setMyLocationEnabled(true);
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    map.setMyLocationEnabled(true);
                    MarkerOptions options = new MarkerOptions();
                    options.position(startPoint);
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));
                    if (startPoint != null) {

                        map.addMarker(options);
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(startPoint, Config.CAMERA_FACTORY_UPDATE);
                        map.animateCamera(cameraUpdate);

                    }

                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;
            default:

        }
    }

    @Override
    public void onBackPressed() {

    }
}