package com.EddyCabLatest.application.moduleUser.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.modal.ExpandableListChild;

import java.util.HashMap;
import java.util.List;

import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_FREQUENT_SEARCH;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS;
import static com.EddyCabLatest.application.constant.DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	private HashMap<String, List<ExpandableListChild>> _listDataChild; // child data

	public ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<ExpandableListChild>> listChildData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
	}


	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon).getName() + " , "
                + this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon).getAddress();

	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_item_search_frequent, null);
		}

		TextView txtListChildName = (TextView) convertView.findViewById(R.id.Location_Name);
		TextView txtListChildAddress = (TextView) convertView.findViewById(R.id.Location_address);
        ImageView favorite_icon = (ImageView) convertView.findViewById(R.id.icon_favourite);

        txtListChildName.setText(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getName());
        txtListChildAddress.setText(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getAddress());

		if (EApplication.getInstance().CheckIsDataAlreadyInDBorNotWithNameAndAddress(TABLE_FREQUENT_SEARCH, TABLE_FREQUENT_SEARCH_ADDRESS_NAME, TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getName(), _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getAddress())) {
			_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).setImage("icon_favourite_active");
		}

        int res = _context.getResources().getIdentifier(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getImage(), "drawable", _context.getPackageName());
        favorite_icon.setImageResource(res);

		favorite_icon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (EApplication.getInstance().CheckIsDataAlreadyInDBorNotWithNameAndAddress(TABLE_FREQUENT_SEARCH, TABLE_FREQUENT_SEARCH_ADDRESS_NAME, TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getName(), _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getAddress())) {
					EApplication.getInstance().removeFavourite(TABLE_FREQUENT_SEARCH, TABLE_FREQUENT_SEARCH_ADDRESS_NAME, TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getName(), _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getAddress());
					_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).setImage("icon_favourite_incative");
					notifyDataSetChanged();
				} else {
					HashMap<String, String> map = new HashMap<>();

					map.put(DBTableFields.TABLE_FREQUENT_SEARCH_IMAGES, "icon_favourite_active");
					map.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_NAME, _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getName());
					map.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_ADDRESS, _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getAddress());
					map.put(DBTableFields.TABLE_FREQUENT_SEARCH_ADDRESS_DISTANCE, _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getDistance());
					map.put(DBTableFields.TABLE_FREQUENT_SEARCH_LAT, _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getLatitude());
					map.put(DBTableFields.TABLE_FREQUENT_SEARCH_LANG, _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getLongitude());



					EApplication.getInstance().addToDB(DBTableFields.TABLE_FREQUENT_SEARCH, map);
					Toast.makeText(_context, "Successfully referenced this address", Toast.LENGTH_LONG).show();

					_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).setImage("icon_favourite_active");
					notifyDataSetChanged();
				}
			}
		});

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.group_item, null);
		}

		TextView lblListHeader = (TextView) convertView.findViewById(R.id.group_name);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
