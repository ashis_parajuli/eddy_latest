package com.EddyCabLatest.application.moduleUser.modal;

/**
 * Created by indraimac on 7/17/15.
 */
public class ExpandableListChild {

    String Image;
    String Name;
    String Address;
    String Latitude;
    String Longitude;
    String Distance;


    public ExpandableListChild(String image, String name, String address, String latitude, String longitude, String distance) {
        Image = image;
        Name = name;
        Address = address;
        Latitude = latitude;
        Longitude = longitude;
        Distance = distance;
    }


    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }
}
