package com.EddyCabLatest.application.moduleUser.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.EddyCabLatest.application.R;


public class Fragment_UserAbout extends Fragment implements View.OnClickListener {
    ImageView img_shareFacebook;


    ImageView img_shareTwitter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.layout_fragment_user_about, container, false);
        img_shareFacebook = (ImageView) rootView.findViewById(R.id.img_shareFacebook);
     /*   img_shareInstagram = (ImageView) rootView.findViewById(R.id.img_shareInstagram);
        img_logo_banana = (ImageView) rootView.findViewById(R.id.img_logo_banana);*/
        img_shareTwitter = (ImageView) rootView.findViewById(R.id.img_shareTwitter);
    /*    img_shareGooglePlus = (ImageView) rootView.findViewById(R.id.img_shareGooglePlus);*/

        //event Handling
        img_shareFacebook.setOnClickListener(this);
       /* img_shareInstagram.setOnClickListener(this);
        img_logo_banana.setOnClickListener(this);*/
        img_shareTwitter.setOnClickListener(this);
       /* img_shareGooglePlus.setOnClickListener(this);*/


        return rootView;

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.img_shareFacebook:
                String facebookUrl = "https://www.facebook.com/eddycab";
                linkUrl(facebookUrl);
                break;

          /*  case R.id.img_shareInstagram:
                String instagramUrl = "https://instagram.com/bananabike.co/";
                linkUrl(instagramUrl);
                break;

            case R.id.img_logo_banana:
                String url = "http://bananabike.co/";
                linkUrl(url);
                break;*/

            case R.id.img_shareTwitter:
                String twitterUrl = "https://twitter.com/eddycab";
                linkUrl(twitterUrl);
                break;

         /*   case R.id.img_shareGooglePlus:
                String gPlusUrl = "https://plus.google.com";
                linkUrl(gPlusUrl);
                break;*/
            default:
                break;

        }

    }

    //implement link url
    private void linkUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        startActivity(intent);

    }


}
