package com.EddyCabLatest.application.moduleUser.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.utilities.Validator;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by indraimac on 7/27/15.
 */
public class Update_USerProfile extends AppCompatActivity {

    private Toolbar app_toolBar;
    private RelativeLayout changeNameLayout;
    private LinearLayout ll_password;

    private TextView ethome, etwork;

    private ImageView etpic;
    SessionManager session;
    EApplication application;
    String IDENTIFIER;
    private String etNameValue, etEmailValue, etPasswordValue;
    private EditText etname, etphone, etemail, etpassword;
    private String Name, PHONE, EMAIL, PASSWORD;
    private String HOME;
    private String WORK;
    String USER_NAME;
    String USER_Email;
    String USER_MOBILE;
    String USER_FAV_HOME;
    String USER_FAV_WORK;
    String USER_REFERRAL_CODE;
    String USER_COUNTRY_CODE;
    String profileImgPath = "";
    private String UserID;
    private String USER_PASSWORD;
    private TextView etpasswordconfirm;
    private ImageView etpencil;
    private EditText etpasswordold;
    private Button action_save;
    private ImageView navBackPress;
    private String name;
    private String email;
    private String mobile;
    private String license;
    private String image;
    private String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user_profile_edit);
        etname = (EditText) findViewById(R.id.etName);
        etemail = (EditText) findViewById(R.id.etEmail);
        ethome = (TextView) findViewById(R.id.etHomeAddress);
        etphone = (EditText) findViewById(R.id.etPhone);
        etpassword = (EditText) findViewById(R.id.etPassword);
        etwork = (TextView) findViewById(R.id.etOfficeAddress);
        etpic = (ImageView) findViewById(R.id.circularView);
        navBackPress = (ImageView) findViewById(R.id.img_back_arrow);
        navBackPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Update_USerProfile.this, Activity_UserProfile.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
            }
        });
        session = new SessionManager(Update_USerProfile.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        name = userDetails.get("name");
        email = userDetails.get("email");
        mobile = userDetails.get("mobile_no");
        license = userDetails.get("license_no");
        image = userDetails.get("picture");
        etname.setText(name);
        etemail.setText(email);
        etphone.setText(mobile);


        etpasswordconfirm = (TextView) findViewById(R.id.etpasswordconfirm);
        etpasswordold = (EditText) findViewById(R.id.etPasswordold);
        action_save = (Button) findViewById(R.id.action_save);
        action_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateUser_Email();

            }
        });
        etwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateUser_Email_WithoutIntent();
                Intent intentwork = new Intent(Update_USerProfile.this, Activity_PickupLocation.class);
                intentwork.putExtra("ADDRESS_REQUEST", "WORK_ADDRESS");
                startActivity(intentwork);

            }
        });


        ethome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateUser_Email_WithoutIntent();
                Intent intenthome = new Intent(Update_USerProfile.this, Activity_PickupLocation.class);
                intenthome.putExtra("ADDRESS_REQUEST", "HOME_ADDRESS");
                startActivity(intenthome);
            }
        });
        //ToolBar SetUp
    /*app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
    setSupportActionBar(app_toolBar);
	getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	getSupportActionBar().setHomeButtonEnabled(true);
	app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);*/
        /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        application = EApplication.getInstance();


        Update_UserDetails();


    }

    private void UpdateUser_Email_WithoutIntent() {


       /* if (IDENTIFIER.equals(etemail.getText().toString().trim()))
            Toast.makeText(Update_USerProfile.this, getResources().getString(R.string.same_email_doesnot), Toast.LENGTH_LONG).show();
        else {*/

        if (etpassword.getText().toString().trim().isEmpty() && etpasswordold.getText().toString().trim().isEmpty() && etpasswordconfirm.getText().toString().trim().isEmpty()) {
            if (Validator.isEmailValid(etemail.getText().toString().trim())) {

                if (NetworkUtils.isNetworkAvailable(Update_USerProfile.this)) {

                    HashMap<String, String> userDetails = session.getUserDetails();
                    String userID = userDetails.get(SessionManager.KEY_ID);


                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", userID);
                    params.put("email", etemail.getText().toString().trim());
                    params.put("mobile_number", etphone.getText().toString().trim());
                    params.put("fullname", etname.getText().toString().trim());
                    Log.e("Parameters", params.toString());

                    CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Update_USerProfile.this);
                    CustomProgressBarDialog.progressDialog.show();
                    CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.EDIT_PROFILE, params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject responseObject) {
                                    Log.e("Response 1st API", responseObject.toString());

                                    try {
                                        boolean error = responseObject.getBoolean("error");

                                        if (!error) {

                                            // Dismiss the activity and resume Main Activity
                                            Update_USerProfile.this.finish();

                                            Toast.makeText(Update_USerProfile.this, getResources().getString(R.string.sucess_update), Toast.LENGTH_LONG).show();
                                    /*	Intent intent = new Intent(Update_USerProfile.this, Activity_UserProfile.class);
										startActivity(intent);
										finish();*/
                                        } else {
                                            CustomProgressBarDialog.progressDialog.dismiss();
                                            String message = responseObject.getString("message");
                                            showErrorDialog(message);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    CustomProgressBarDialog.progressDialog.dismiss();

                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("Error", "Error: " + error.getMessage());
                            Log.d("Error", "JSONErrorMSG: " + error.getMessage());
                            Log.d("Error", "JSONErrorStack: " + error.getStackTrace());

                            CustomProgressBarDialog.progressDialog.dismiss();

                        }
                    }) {
                        @Override
                        public Request.Priority getPriority() {
                            return Request.Priority.HIGH;
                        }
                    };
                    EApplication.getInstance().addToRequestQueue(req, "high");
                } else
                    NetworkUtils.showNoConnectionDialog(Update_USerProfile.this);


            } else {
                EApplication.getInstance().showToastMessageFunction("Email is Invalid");
            }

        } else {
            if (Validator.isEmailValid(etemail.getText().toString().trim())) {

                if (NetworkUtils.isNetworkAvailable(Update_USerProfile.this)) {

                    HashMap<String, String> userDetails = session.getUserDetails();
                    String userID = userDetails.get(SessionManager.KEY_ID);


                    if ((TextUtils.isEmpty(etpasswordold.getText().toString().trim()) || TextUtils.isEmpty(etpassword.getText().toString().trim()) || TextUtils.isEmpty(etpasswordconfirm.getText().toString().trim()))) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.fields_required));
                    } else {

                        if (!etpassword.getText().toString().trim().equalsIgnoreCase(etpasswordconfirm.getText().toString().trim())) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.password_donot_match));
                        } else {
                            final HashMap<String, String> params = new HashMap<>();
                            params.put("user_id", userID);
                            params.put("email", etemail.getText().toString().trim());
                            params.put("old_password", etpasswordold.getText().toString().trim());
                            params.put("new_password", etpasswordconfirm.getText().toString().trim());
                            params.put("mobile_number", etphone.getText().toString().trim());
                            params.put("fullname", etname.getText().toString().trim());
                            Log.e("Parameters", params.toString());

                            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Update_USerProfile.this);
                            CustomProgressBarDialog.progressDialog.show();
                            CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.EDIT_PROFILE, params,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject responseObject) {
                                            Log.e("Response 1st API", responseObject.toString());

                                            try {
                                                boolean error = responseObject.getBoolean("error");

                                                if (!error) {
                                                  /*  session.createLoginSession(String.valueOf(user_id), user_name, user_email, user_ProfilePic,
                                                            corrected_rating, user_details.toString(), user_referralcode, "mobile_no", "license_no", lastReservationId);*/
                                                    // Dismiss the activity and resume Main Activity
                                                    Update_USerProfile.this.finish();

                                                    Toast.makeText(Update_USerProfile.this, getResources().getString(R.string.sucess_update), Toast.LENGTH_LONG).show();
											/*Intent intent = new Intent(Update_USerProfile.this, Activity_UserProfile.class);
											startActivity(intent);
											finish();*/
                                                } else {
                                                    CustomProgressBarDialog.progressDialog.dismiss();
                                                    String message = responseObject.getString("message");
                                                    showErrorDialog(message);
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            CustomProgressBarDialog.progressDialog.dismiss();

                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.d("Error", "Error: " + error.getMessage());
                                    Log.d("Error", "JSONErrorMSG: " + error.getMessage());
                                    Log.d("Error", "JSONErrorStack: " + error.getStackTrace());

                                    CustomProgressBarDialog.progressDialog.dismiss();

                                }
                            })  {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    return params;
                                }

                                @Override
                                public Request.Priority getPriority() {
                                    return Request.Priority.HIGH;
                                }

                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                    String current_language = session.getAppLanguage();
                                    Log.e("current_language", current_language);
                                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                        language = "en";
                                    }
                                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                        language = "np";
                                    }
                                    headers.put("lang", language);
                                    return headers;
                                }
                            };
                            EApplication.getInstance().addToRequestQueue(req, "high");
                        }


                    }
                }


            } else {
                NetworkUtils.showNoConnectionDialog(Update_USerProfile.this);
            }

        }

    }


    private void Update_UserDetails() {

        HashMap<String, String> userDataList = session.getUserDetails();
        UserID = userDataList.get(SessionManager.KEY_ID);

        String URL_GET_PROFILE = Config.APP_BASE_URL + Config.GET_USER_PROFILE + UserID;

//        if (NetworkUtils.isNetworkAvailable(Activity_UserProfile.this)) {

        CustomRequest req = new CustomRequest(Request.Method.GET, URL_GET_PROFILE, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseObject) {
                        Log.e("Response 1st API", responseObject.toString());


                        try {
                            boolean error = responseObject.getBoolean("error");

                            if (!error) {

                                try {

                                    Log.e("JSON DATA :: ", responseObject.toString());

                                    USER_NAME = responseObject.getJSONObject("User").getString("fullname");
                                    USER_Email = responseObject.getJSONObject("User").getString("email");
                                    USER_COUNTRY_CODE = responseObject.getJSONObject("User").getString("country_code");
                                    USER_MOBILE = responseObject.getJSONObject("User").getString("mobile_number");
                                    String USER_PROFILE_IMAGE = responseObject.getJSONObject("User").getString("user_pp");
                                    USER_REFERRAL_CODE = responseObject.getJSONObject("User").getString("referral_code");
                                    USER_PASSWORD = responseObject.getJSONObject("User").getString("password");

                                    if (!responseObject.get("Fav_home").toString().equals("null") && !responseObject.get("Fav_work").toString().equals("null") &&
                                            responseObject.get("Fav_home") != null && responseObject.get("Fav_work") != null) {

                                        String HOME_TITLE = responseObject.getJSONObject("Fav_home").getJSONObject("favourite").getString("title");
                                        String HOME_ADDRESS = responseObject.getJSONObject("Fav_home").getJSONObject("favourite").getString("address");

                                        String WORK_TITLE = responseObject.getJSONObject("Fav_work").getJSONObject("favourite").getString("title");
                                        String WORK_ADDRESS = responseObject.getJSONObject("Fav_work").getJSONObject("favourite").getString("address");
                                        USER_FAV_HOME = HOME_TITLE + " , " + HOME_ADDRESS;
                                        USER_FAV_WORK = WORK_TITLE + " , " + WORK_ADDRESS;
                                        ethome.setText(USER_FAV_HOME);
                                        etwork.setText(USER_FAV_WORK);

                                    }


                                    // set data to reference fields
                                    etname.setText(USER_NAME);
                                    etemail.setText(USER_Email);
                                    etphone.setText(USER_MOBILE);
                                    etpassword.setText("");
                                    etpasswordconfirm.setText("");
                                    etpasswordold.setText("");
	                             /*   txt_referral_value.setText(USER_REFERRAL_CODE);*/

                                    //setImage
                                    Picasso.with(Update_USerProfile.this)
                                            .load(Config.APP_BASE_URL + USER_PROFILE_IMAGE)
                                            .placeholder(R.drawable.contact_avatar)   // optional
                                            .error(R.drawable.contact_avatar)         // optional
                                            .resize(100, 100)                   // optional
                                            .rotate(0)                          // optional
                                            .into(etpic);


                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                            } else
                                application.showToastMessageFunction("not able to get User Detail");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }
        };
        application.addToRequestQueue(req, "high");

        /*} else {
            NetworkUtils.showNoConnectionDialog(Activity_UserProfile.this);
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_update, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;

            case R.id.action_save:

                UpdateUser_Email();


                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void UpdateUser_Email() {





       /* if (IDENTIFIER.equals(etemail.getText().toString().trim()))
            Toast.makeText(Update_USerProfile.this, getResources().getString(R.string.same_email_doesnot), Toast.LENGTH_LONG).show();
        else {*/

        if (etpassword.getText().toString().trim().isEmpty() && etpasswordold.getText().toString().trim().isEmpty() && etpasswordconfirm.getText().toString().trim().isEmpty()) {
            if (Validator.isEmailValid(etemail.getText().toString().trim())) {

                if (NetworkUtils.isNetworkAvailable(Update_USerProfile.this)) {

                    HashMap<String, String> userDetails = session.getUserDetails();
                    String userID = userDetails.get(SessionManager.KEY_ID);


                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", userID);
                    params.put("email", etemail.getText().toString().trim());
                    params.put("mobile_number", etphone.getText().toString().trim());
                    params.put("fullname", etname.getText().toString().trim());
                    Log.e("Parameters", params.toString());

                    CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Update_USerProfile.this);
                    CustomProgressBarDialog.progressDialog.show();
                    CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.EDIT_PROFILE, params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject responseObject) {
                                    Log.e("Response 1st API", responseObject.toString());

                                    try {
                                        boolean error = responseObject.getBoolean("error");

                                        if (!error) {

                                            // Dismiss the activity and resume Main Activity
                                            Update_USerProfile.this.finish();

                                            Toast.makeText(Update_USerProfile.this, getResources().getString(R.string.sucess_update), Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(Update_USerProfile.this, Activity_UserProfile.class);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            CustomProgressBarDialog.progressDialog.dismiss();
                                            String message = responseObject.getString("message");
                                            showErrorDialog(message);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    CustomProgressBarDialog.progressDialog.dismiss();

                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("Error", "Error: " + error.getMessage());
                            Log.d("Error", "JSONErrorMSG: " + error.getMessage());
                            Log.d("Error", "JSONErrorStack: " + error.getStackTrace());

                            CustomProgressBarDialog.progressDialog.dismiss();

                        }
                    }) {
                        @Override
                        public Request.Priority getPriority() {
                            return Request.Priority.HIGH;
                        }
                    };
                    EApplication.getInstance().addToRequestQueue(req, "high");
                } else
                    NetworkUtils.showNoConnectionDialog(Update_USerProfile.this);


            } else {
                EApplication.getInstance().showToastMessageFunction("Email is Invalid");
            }

        } else {
            if (Validator.isEmailValid(etemail.getText().toString().trim())) {

                if (NetworkUtils.isNetworkAvailable(Update_USerProfile.this)) {

                    HashMap<String, String> userDetails = session.getUserDetails();
                    String userID = userDetails.get(SessionManager.KEY_ID);


                    if ((TextUtils.isEmpty(etpassword.getText().toString().trim()) || TextUtils.isEmpty(etpasswordconfirm.getText().toString().trim()) || TextUtils.isEmpty(etpasswordconfirm.getText().toString().trim()))) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.fields_required));
                    } else {

                        final HashMap<String, String> params = new HashMap<>();
                        params.put("user_id", userID);
                        params.put("email", etemail.getText().toString().trim());
                        params.put("password", etpassword.getText().toString().trim());
                        params.put("password_confirmation", etpasswordconfirm.getText().toString().trim());
                        params.put("mobile_number", etphone.getText().toString().trim());
                        params.put("fullname", etname.getText().toString().trim());
                        Log.e("Parameters", params.toString());

                        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Update_USerProfile.this);
                        CustomProgressBarDialog.progressDialog.show();
                        CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.EDIT_PROFILE, params,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject responseObject) {
                                        Log.e("Response 1st API", responseObject.toString());

                                        try {
                                            boolean error = responseObject.getBoolean("error");

                                            if (!error) {

                                                // Dismiss the activity and resume Main Activity
                                                Update_USerProfile.this.finish();

                                                Toast.makeText(Update_USerProfile.this, getResources().getString(R.string.sucess_update), Toast.LENGTH_LONG).show();
                                                Intent intent = new Intent(Update_USerProfile.this, Activity_UserProfile.class);
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                CustomProgressBarDialog.progressDialog.dismiss();
                                                String message = responseObject.getString("message");
                                                showErrorDialog(message);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        CustomProgressBarDialog.progressDialog.dismiss();

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d("Error", "Error: " + error.getMessage());
                                Log.d("Error", "JSONErrorMSG: " + error.getMessage());
                                Log.d("Error", "JSONErrorStack: " + error.getStackTrace());

                                CustomProgressBarDialog.progressDialog.dismiss();

                            }
                        })  {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                return params;
                            }

                            @Override
                            public Request.Priority getPriority() {
                                return Request.Priority.HIGH;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                String current_language = session.getAppLanguage();
                                Log.e("current_language", current_language);
                                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                    language = "en";
                                }
                                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                    language = "np";
                                }
                                headers.put("lang", language);
                                return headers;
                            }
                        };
                        EApplication.getInstance().addToRequestQueue(req, "high");
                    }
                }


            } else {
                NetworkUtils.showNoConnectionDialog(Update_USerProfile.this);
            }

        }
    }


    private void showErrorDialog(String errorMessage) {

        final Dialog dialog = new Dialog(Update_USerProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_success_failure_dialog);
        dialog.setCancelable(false);


        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_title.setText(getResources().getString(R.string.warningTitle));
        dialog_message.setText(errorMessage);
        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


}


