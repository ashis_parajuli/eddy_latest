package com.EddyCabLatest.application.moduleUser.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.adapter.AdapterQuotes;
import com.EddyCabLatest.application.moduleUser.modal.LocateModel;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.utilities.gpsTrackerUser;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Activity_UserSelectDriver extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    //TextView txt_distance_n_time;
    // TextView txt_eta_time;


    private MapView mapView;
    private GoogleMap map;


    private LatLng startPoint;

    private Polyline polyline;

    HashMap<String, String> sourceLocation;
    HashMap<String, String> destinationLocation;

    private EApplication app;
    private Toolbar app_toolBar;
    private SessionManager session;
    public static String RESERVATION_ID;
    String language = "";
    String TAG = "===";
    ArrayList<HashMap<String, String>> dataAllDriverList = new ArrayList<>();
    String time_distacne;
    int distance;
    int padding;
    private ListView selectDriverListView;
    private TextView emptyMessage;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String user_ID;
    private String reservation_id;
    private AdapterQuotes adapter;
    private String DRIVER_NAME;
    private String MOBILE;
    private String PROFILE;
    private String REDG;
    private String Driver_ID;
    private String Reservation_ID;
    private String from_waiting;
    private String to_waiting;
    private int position_value;
    private Handler handler = new Handler();
    private boolean visible = false;
    private boolean isset = false;
    private CountDownTimer myCountdownTimer;
    private Dialog dialog_main;
    private String Driver_Mobile;
    private String profile;
    private String driver_name;
    private String driver_redg_no;

    private String driver_id;
    private ValueEventListener accepted_listener;

    private String driver_mobile;
    private Dialog dialog_main_cancel_driver;
    private TextView text_cancel_button;
    private boolean iscall = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user_reservation_response);
        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        text_cancel_button = (TextView) findViewById(R.id.cancel_driver_userselect);

        padding = EApplication.getInstance().getMapPadding();
        initializaMap(savedInstanceState);

        setSupportActionBar(app_toolBar);
        session = new SessionManager(Activity_UserSelectDriver.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_ID = userDetails.get(SessionManager.KEY_ID);
        HashMap<String, String> getDriverWithReservation = session.getReservationIdWithDriver();
        reservation_id = getDriverWithReservation.get(SessionManager.RESERVATION_ID);

        app_toolBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EApplication.getInstance().setverifyStatus(false);
            }
        });
        // Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        // Initializing Application Instances
        app = EApplication.getInstance();

        // Reference allocating
        //txt_distance_n_time = (TextView) findViewById(R.id.txt_distance_n_time);
        //txt_eta_time = (TextView) findViewById(R.id.txt_eta_time);
        //get source location to plot in map from session
        sourceLocation = session.getSourceLocation();


        selectDriverListView = (ListView) findViewById(R.id.selectDriverListView);
        emptyMessage = (TextView) findViewById(android.R.id.empty);
        emptyMessage = (TextView) findViewById(android.R.id.empty);
        emptyMessage = (TextView) findViewById(android.R.id.empty);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.orange);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        selectDriverListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (selectDriverListView != null && selectDriverListView.getChildCount() > 0) {
                    boolean firstItemVisible = selectDriverListView.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = selectDriverListView.getChildAt(0).getTop() == 0;
                    boolean enable = firstItemVisible && topOfFirstItemVisible;
                    mSwipeRefreshLayout.setEnabled(enable);
                } else
                    mSwipeRefreshLayout.setEnabled(true);
            }
        });

        reservation_id = getDriverWithReservation.get(SessionManager.RESERVATION_ID);
        /* eventHandling for ListView */
        selectDriverListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Animation animation1 = new AlphaAnimation(0.3f, 1.0f);
                animation1.setDuration(4000);
                view.startAnimation(animation1);
                Driver_ID = dataAllDriverList.get(position).get(DBTableFields.DRIVER_ID);
                Reservation_ID = dataAllDriverList.get(position).get(DBTableFields.TABLE_BID_RESERVATION_ID);
                from_waiting = dataAllDriverList.get(position).get(DBTableFields.USER_TRIP_FROM_LOCATION);
                to_waiting = dataAllDriverList.get(position).get(DBTableFields.USER_TRIP_TO_LOCATION);
                position_value = position;
                showSetBookingDialog(position, Driver_ID, Reservation_ID);
            }
        });

        if (session.isSourceLocationSet()) {
            Log.e("Source Details", sourceLocation.toString());
            startPoint = new LatLng(Double.parseDouble(sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LAT)),
                    Double.parseDouble(sourceLocation.get(SessionManager.SOURCE_LOCATION_GEO_POINT_LANG)));
        }

        //get destination location to plot in map from session


        // call api to load data of driver bidding response
        HashMap<String, String> reservationData = session.getReservationNewData();
        RESERVATION_ID = reservationData.get(SessionManager.RESERVATION_NEW_ID);
/*    LoadDriverBiddingResponse(RESERVATION_ID);*/
        Log.e("user_selecg\t", RESERVATION_ID);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
          /* Check For Route Draw */
        if (session.isSourceLocationSet()) {
            Log.e("Message", "Draw route ....");

        }
       /* calculateDistance(startPoint, endPoint);*/
        //FunctionLoadDriverList();
        text_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wantToCancelDialog();
            }
        });
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                gpsTrackerUser gps_tracker = new gpsTrackerUser(Activity_UserSelectDriver.this);


                LatLngBounds.Builder builder_data = new LatLngBounds.Builder();


                if ((startPoint != null)) {
                    builder_data.include(startPoint);

                    if (gps_tracker.canGetLocation()) {


                        MarkerOptions options = new MarkerOptions();
                        options.position(new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude()));
                        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));
                        map.addMarker(options);
                        // builder_data.include(new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude()));

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude())).zoom(15).build();

                        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    }
                }

                map.setPadding(12, 20, 30, 30);


            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                EApplication.getInstance().setfromuserselect(true);
                Intent intent = new Intent(Activity_UserSelectDriver.this, Activity_UserMain.class);
                startActivity(intent);
                finish();

                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        session = new SessionManager(Activity_UserSelectDriver.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_ID = userDetails.get(SessionManager.KEY_ID);
        HashMap<String, String> getDriverWithReservation = session.getReservationIdWithDriver();
        reservation_id = getDriverWithReservation.get(SessionManager.RESERVATION_ID);

        if (iscall) {
            if (dialog_main_cancel_driver != null) {
                if (dialog_main_cancel_driver.isShowing()) {
                    dialog_main_cancel_driver.dismiss();
                }
            }
            iscall = false;
            ServerAPICallForDriverConfirmFirstTime();
        }

        visible = true;
        // FunctionLoadDriverList();
        FunctionLoadDriverList1();
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        mapView.onResume();
        padding = EApplication.getInstance().getMapPadding();
    }

    @Override
    protected void onPause() {
       /* if (dialog_main_cancel_driver != null) {
            if (dialog_main_cancel_driver.isShowing())
                dialog_main_cancel_driver.dismiss();
        }*/


        if (CustomProgressBarDialog.progressDialog != null) {

            CustomProgressBarDialog.progressDialog.dismiss();
        }

        super.onPause();
    }

    private void FunctionLoadDriverList1() {

        if (NetworkUtils.isNetworkAvailable(this)) {
            final ArrayList<HashMap<String, String>> serverDataList = new ArrayList<>();
            final HashMap<String, String> reservationData = session.getReservationNewData();
            RESERVATION_ID = reservationData.get(SessionManager.RESERVATION_NEW_ID);
            String REQUEST_URL = Config.APP_BASE_URL + Config.USER_RESERVATION_RESPONSE_URL + RESERVATION_ID;
            Log.e("closest", Activity_UserSelectDriver.RESERVATION_ID);
            CustomRequest request = new CustomRequest(Request.Method.GET, REQUEST_URL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                boolean error = response.getBoolean("error");
                                if (!error) {
                               /* EApplication.getInstance().showNotification(this, Activity_UserSelectDriver.class, 001, "Goto Track your user", "Click Here", "Go to track your user");*/
                                    if (!dataAllDriverList.isEmpty()) {
                                        dataAllDriverList.clear();
                                        adapter.notifyDataSetChanged();
                                    }
                                    Log.e("all_driver", response.toString());

                                    JSONArray all_driversArray = response.getJSONArray("all_drivers");
                                    for (int i = 0; i < all_driversArray.length(); i++) {
                                        HashMap<String, String> mapData = new HashMap<>();
                                        JSONObject parentObject = all_driversArray.getJSONObject(i);
                                        String RESERVATION_ID = parentObject.getString("reservation_id");
                                        String DRIVER_ID = parentObject.getString("driver_id");
                                        DRIVER_NAME = parentObject.getJSONObject("oneuser").getString("fullname");
                                        MOBILE = parentObject.getJSONObject("oneuser").getString("mobile_number");
                                        PROFILE = parentObject.getJSONObject("onedriver").getString("profile_picture");
                                        REDG = parentObject.getJSONObject("onedriver").getString("license_no");
                                        String NEAREST_DISTANCE = parentObject.getString("distance");
                                        String BEST_RATING = parentObject.getString("ratings");
                                        if (BEST_RATING.equals("null") || TextUtils.isEmpty(BEST_RATING))
                                            BEST_RATING = "0.0";

                                        LocateModel locateModel = new LocateModel();
                                        locateModel.setDriver_id(DRIVER_ID);
                                        locateModel.setDriver_name(DRIVER_NAME);
                                        locateModel.setDriver_mobile(MOBILE);
                                        locateModel.setDriver_profile(PROFILE);
                                        locateModel.setDriver_redg(REDG);

                                        mapData.put("DRIVER_NAME", DRIVER_NAME);
                                        mapData.put("RESERVATION_ID", RESERVATION_ID);
                                        mapData.put("DRIVER_ID", DRIVER_ID);
                                        mapData.put("NEAREST_DISTANCE", NEAREST_DISTANCE);
                                        mapData.put("BEST_RATING", BEST_RATING);
                                        mapData.put("DRIVER_REDG", REDG);
                                        mapData.put("DRIVER_MOBILE", MOBILE);
                                        mapData.put("DRIVER_PROFILE", PROFILE);
                                        serverDataList.add(mapData);
                                    }
                           /*     Log.e(" ARRAY ListItems>>", serverDataList.toString());*/
                                    // clear data from database
                                    EApplication.getInstance().deleteAllInDB(DBTableFields.TABLE_DRIVER_BID);

                                    Log.e("::", "Data Cleared");

                                    // insert new data to table
                                    EApplication.getInstance().addAllToDB(DBTableFields.TABLE_DRIVER_BID, serverDataList);
                                    Log.e("::", "Data Inserted" + serverDataList.toString());
//                                CustomProgressBarDialog.progressDialog.dismiss();

                                    dataAllDriverList = EApplication.getInstance().getAllToSortedList(DBTableFields.TABLE_DRIVER_BID, new String[]{DBTableFields.TABLE_BID_NEAREST_DRIVER}, DBTableFields.ASC);
                                    adapter = new AdapterQuotes(Activity_UserSelectDriver.this, dataAllDriverList);
                                    selectDriverListView.setAdapter(adapter);
                                    selectDriverListView.setEmptyView(emptyMessage);
                                    adapter.notifyDataSetChanged();
                                } else {


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
//                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                @Override
                public Request.Priority getPriority() {
                    return Priority.HIGH;
                }


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }

            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    2,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            EApplication.getInstance().addToRequestQueue(request, "high");

        } else {
            NetworkUtils.showNoConnectionDialog(this);
        }
    }

    @Override
    public void onDestroy() {
        if (dialog_main_cancel_driver != null) {
            if (dialog_main_cancel_driver.isShowing())
                dialog_main_cancel_driver.dismiss();
        }
        visible = false;
        super.onDestroy();
        handler.removeCallbacks(refreshing);
        visible = false;
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void showSetBookingDialog(final int position, final String driver_ID, final String Reservation_ID) {
        if (NetworkUtils.isNetworkAvailable(this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(this);
            if (visible) CustomProgressBarDialog.progressDialog.show();
            // Store reservation + driver id to pref.
            session.createLastReservation(Reservation_ID, driver_ID);
            final HashMap<String, String> params = new HashMap<>();
            params.put("reservation_id", Reservation_ID);
            params.put("driver_id", driver_ID);
            Log.e("Parameters>>", params.toString());

            CustomRequest jsonObjectRequestSetBooking = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.USER_SET_BOOKING_URL, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response_json) {


                    CustomProgressBarDialog.progressDialog.dismiss();

                    String newResponse = response_json.toString();

                    try {

                        JSONObject response = new JSONObject(newResponse);
                        Log.e("response>>", response.toString());
                        boolean error = response.getBoolean("error");
                        if (!error) {
                            // EApplication.getInstance().setBookingStatus(true);
                            // set user state to preferences
                            session.createReservationByUser(DBTableFields.RESERVATION_STATE_WAITING_DRIVER_RESPONSE);
                                /*    dataAllDriverList.clear();
                                    adapter.notifyDataSetChanged();*/
                            isset = true;
                            ServerAPICallForDriverConfirmFirstTime();
                            confirmAccept();

                        } else {

                            int code = response.getInt("code");
                            if (code == 201) {
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_WAITING_DRIVER_RESPONSE);
                                /*    dataAllDriverList.clear();
                                    adapter.notifyDataSetChanged();*/
                                isset = true;

                                confirmAccept();

                            } else {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                String error_msg = response.getString("message");
                                EApplication.getInstance().showToastMessageFunction(error_msg);
                                reservationWarningNotificationDialog(error_msg, code);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error", "Error: " + error.getMessage());

                    CustomProgressBarDialog.progressDialog.dismiss();
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null) {
                        Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                    }
                    if (error instanceof TimeoutError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "TimeoutError");
                    } else if (error instanceof NoConnectionError) {
                        Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                    } else if (error instanceof AuthFailureError) {
                        Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                    } else if (error instanceof ServerError) {
                        Log.e("Driver_New_JObs_Volley", "ServerError");
                    } else if (error instanceof NetworkError) {
                        Log.e("Driver_New_JObs_Volley", "NetworkError");
                    } else if (error instanceof ParseError) {
                        Log.e("Driver_New_JObs_Volley", "ParseError");
                    }

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequestSetBooking.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(jsonObjectRequestSetBooking, "high");

        } else {
            Toast.makeText(this, getResources().getString(R.string.no_internet_availeble), Toast.LENGTH_SHORT);
            NetworkUtils.showNoConnectionDialog(this);
        }

    }

    private void LoadDialogForBidder() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(Activity_UserSelectDriver.this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.warningTitle));
        builder.setMessage(getResources().getString(R.string.want_to_cancel));
        builder.setNegativeButton(getResources().getString(R.string.cancel_SELECTder), null);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelDriverApiCall();
            }
        });
        if (visible) builder.show();
    }

    private void cancelDriverApiCall() {
        if (NetworkUtils.isNetworkAvailable(this)) {


            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserSelectDriver.this);
            if (visible) CustomProgressBarDialog.progressDialog.show();

            HashMap<String, String> reservationWithDriver = session.getReservationIdWithDriver();
            String RESERVATION_ID = reservationWithDriver.get(SessionManager.RESERVATION_ID);
            String DRIVER_ID = reservationWithDriver.get(SessionManager.DRIVER_ID);

            final HashMap<String, String> params = new HashMap<>();
            params.put("reservation_id", RESERVATION_ID);
            params.put("driver_id", DRIVER_ID);
            Log.e("Parameters>>", params.toString());

            String REquestUrl = Config.APP_BASE_URL + Config.API_BIDDER_CANCEL_BY_USER;

            CustomRequest req = new CustomRequest(Request.Method.POST, REquestUrl, params,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject responseObject) {


                            String newResponse = responseObject.toString();
                            CustomProgressBarDialog.progressDialog.dismiss();

                            if (newResponse.contains("Ã¯Â»Â¿")) {
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }

                            try {
                                JSONObject response = new JSONObject(newResponse);
                                boolean error = response.getBoolean("error");
                                if (!error) {
                                    if (dialog_main_cancel_driver != null) {
                                        dialog_main_cancel_driver.dismiss();

                                    }


                                    if (myCountdownTimer != null) {
                                        myCountdownTimer.cancel();
                                        myCountdownTimer = null;
                                    }
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                    EApplication.getInstance().showToastMessageFunction(Activity_UserSelectDriver.this.getResources().getString(R.string.canceled));
                                    // set user state to preferences
                                    EApplication.getInstance().callBookingStatus = false;
                                    FunctionLoadDriverList();
                                    /*Intent setBookingIntent = new Intent(Activity_UserSelectDriver.this, Activity_UserSelectDriver.class);
                                    startActivity(setBookingIntent);
*/

                                } else {
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                    EApplication.getInstance().showToastMessageFunction(Activity_UserSelectDriver.this.getResources().getString(R.string.failed_to_cancel));
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                CustomProgressBarDialog.progressDialog.dismiss();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error", "Error: " + error.getMessage());

                    CustomProgressBarDialog.progressDialog.dismiss();

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            req.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            EApplication.getInstance().addToRequestQueue(req, "high");

        }


    }


    private void methodCall() {

        // DriverNumber = session.getDriverNumber();
        String number = "tel:" + "+977" + MOBILE;
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
        if (ActivityCompat.checkSelfPermission(Activity_UserSelectDriver.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        iscall = true;
        if (myCountdownTimer != null) {
            myCountdownTimer.cancel();
            myCountdownTimer = null;
        }
        if (dialog_main_cancel_driver != null) {

            dialog_main_cancel_driver.dismiss();
        }

        startActivity(callIntent);
    }

    private void ServerAPICallForDriverConfirmFirstTime() {
        iscall = false;
        dialog_main_cancel_driver = new Dialog(Activity_UserSelectDriver.this);
        dialog_main_cancel_driver.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_main_cancel_driver.setContentView(R.layout.call_driver_waiting);
        dialog_main_cancel_driver.setCancelable(false);
        AlphaAnimation alpha = new AlphaAnimation(0.5F, 0.5F);
        alpha.setDuration(0); // Make animation instant
        alpha.setFillAfter(true); // Tell it to persist after the animation ends
        dialog_main_cancel_driver.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog_main_cancel_driver.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog_main_cancel_driver.getWindow().setAttributes(paramLayout);
        TextView destination = (TextView) dialog_main_cancel_driver.findViewById(R.id.pickup_location_destination);
        TextView source = (TextView) dialog_main_cancel_driver.findViewById(R.id.pickup_location_source);
        LinearLayout calllayout = (LinearLayout) dialog_main_cancel_driver.findViewById(R.id.call_layout);
        calllayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCountdownTimer.cancel();
                dialog_main_cancel_driver.dismiss();
                methodCall();
            }
        });

        TextView txtMessage = (TextView) dialog_main_cancel_driver.findViewById(R.id.txt_sorry);
        txtMessage.setText(getResources().getString(R.string.waiting_driver));
       /* source.setText(from_waiting);
        destination.setText(to_waiting);*/
        final TextView txt_count_down = (TextView) dialog_main_cancel_driver.findViewById(R.id.txt_count_down);
        myCountdownTimer = new CountDownTimer(Config.COUNTDOWN_TIME, 1000) {
            public void onTick(long millisUntilFinished) {
                txt_count_down.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                dialog_main_cancel_driver.dismiss();
                cancelDriverApiCall();

                if (accepted_listener != null) {
                    EApplication.ref.child(user_ID + "/reservations/" + reservation_id).removeEventListener(accepted_listener);
                }
                //handler.removeCallbacks(runable);
                EApplication.getInstance().setBookingStatus(false);
                //dialog_main.dismiss();
            }
        }.start();
        ImageView action_btn_quiet = (ImageView) dialog_main_cancel_driver.findViewById(R.id.action_btn_quiet);
        action_btn_quiet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadDialogForBidder();
            }
        });
        if (visible) dialog_main_cancel_driver.show();
    }

    private void confirmAccept() {
        HashMap<String, String> userDetails = session.getUserDetails();
        user_ID = userDetails.get(SessionManager.KEY_ID);
        HashMap<String, String> getDriverWithReservation = session.getReservationIdWithDriver();
        reservation_id = getDriverWithReservation.get(SessionManager.RESERVATION_ID);
        accepted_listener = EApplication.ref.child(user_ID + "/reservations/" + reservation_id).addValueEventListener(new ValueEventListener() {


            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (CustomProgressBarDialog.progressDialog.isShowing()) {
                    CustomProgressBarDialog.progressDialog.dismiss();

                }

                Log.e("confirm", "" + dataSnapshot.toString());
                if (dataSnapshot.child("status").exists()) {
                    if ((dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("ignore"))) {
                        if (dialog_main_cancel_driver != null) {

                            dialog_main_cancel_driver.dismiss();
                        }
                        dialogIgnoredByDriver();

                        //   EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.ignored_by_driver));

                        EApplication.ref.child(user_ID + "/reservations/" + reservation_id).removeEventListener(this);


                    } else if (dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("driver_confirmed")) {
                        EApplication.ref.child(user_ID + "/reservations/" + reservation_id).removeEventListener(this);
                        session.createReservationByUser(DBTableFields.RESERVATION_STATE_DRIVER_CONFIRMED);

                        if (myCountdownTimer != null) {
                            //EApplication.ref.child(user_ID+"/reservations/"+reservation_id+"accepted").setValue(false);
                            myCountdownTimer.cancel();
                            myCountdownTimer = null;
                        }
                        Intent intent_main = new Intent(Activity_UserSelectDriver.this, Activity_UserLocateDriver.class);
                        profile = dataAllDriverList.get(position_value).get(DBTableFields.DRIVER_PROFILE);
                        driver_name = dataAllDriverList.get(position_value).get(DBTableFields.DRIVER_NAME);
                        driver_mobile = dataAllDriverList.get(position_value).get(DBTableFields.DRIVER_MOBILE);
                        driver_redg_no = dataAllDriverList.get(position_value).get(DBTableFields.DRIVER_REDG + "");
                        driver_id = dataAllDriverList.get(position_value).get(DBTableFields.DRIVER_ID);
                        //i need to put the mobile num
                        intent_main.putExtra("name", driver_name);
                        intent_main.putExtra("profile", profile);
                        intent_main.putExtra("mobile", driver_mobile);
                        intent_main.putExtra("redg", driver_redg_no);
                        intent_main.putExtra("id", driver_id);

                        startActivity(intent_main);


                        finish();
                    }
                }
            }


        });
    }

    private void dialogIgnoredByDriver() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.eddy_arrived_notification);
        dialog.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView txt_message = (TextView) dialog.findViewById(R.id.txt_message);
        txt_message.setText(getResources().getString(R.string.ignored_by_driver));
        ImageView btnOK = (ImageView) dialog.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();


                if (myCountdownTimer != null) {
                    //EApplication.ref.child(user_ID+"/reservations/"+reservation_id+"accepted").setValue(false);
                    myCountdownTimer.cancel();
                    myCountdownTimer = null;

                }
            }
        });
        if (visible) dialog.show();
    }


    private void wantToCancelDialog() {

        final Dialog dialog = new Dialog(Activity_UserSelectDriver.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_of_transparent_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        dialog_title.setText(getResources().getString(R.string.alert));
        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(getResources().getString(R.string.wanto_cancelReservation));
        if (visible) dialog.show();
        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        dialog_action.setText(getResources().getString(R.string.ok_cancel));
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> userDetails = session.getUserDetails();
                user_ID = userDetails.get(SessionManager.KEY_ID);
                HashMap<String, String> getDriverWithReservation = session.getReservationIdWithDriver();
                reservation_id = getDriverWithReservation.get(SessionManager.RESERVATION_ID);
                reservationCancelAPIExecute(reservation_id);
                dialog.dismiss();


            }
        });

        Button dialog_dismiss = (Button) dialog.findViewById(R.id.dialog_dismiss);
        dialog_dismiss.setText(getResources().getString(R.string.dismiss_cancel));
        dialog_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
     /*   if (visible) dialog.show();*/

    }


    private void reservationCancelAPIExecute(String reservation_id) {

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserSelectDriver.this);
        if (visible) CustomProgressBarDialog.progressDialog.show();

        final HashMap<String, String> params = new HashMap<>();
        params.put("user_id", user_ID);
        params.put("reservation_id", reservation_id);

        Log.e("Parameters", params.toString());

        String REquestUrl = Config.APP_BASE_URL + Config.API_RESERVATION_CANCEL;

        CustomRequest req = new CustomRequest(Request.Method.POST, REquestUrl, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject responseObject) {
                        String newResponse = responseObject.toString();
                        CustomProgressBarDialog.progressDialog.dismiss();

                        if (newResponse.contains("Ã¯Â»Â¿")) {
                            newResponse = newResponse.substring(newResponse.indexOf("{"));
                        }

                        try {
                            JSONObject response = new JSONObject(newResponse);
                            boolean error = response.getBoolean("error");
                            if (!error) {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                                session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);
                                //EApplication.getInstance().showToastMessageFunction(response.getString("message"));
                               /* session.clearPickUPDropOffLocationAddress();*/
                                if (myCountdownTimer != null) {
                                    myCountdownTimer.cancel();
                                    myCountdownTimer = null;
                                }

                                //EApplication.getInstance().setPreviousReservationStatus(false);
                                Intent intent = new Intent(Activity_UserSelectDriver.this, Activity_UserMain.class);
                                EApplication.getInstance().setfromuserselect(true);
                                startActivity(intent);
                                finish();
                               /* reservationCancelSuccessDialog();*/
                                Toast.makeText(Activity_UserSelectDriver.this, getResources().getString(R.string.reservation_cancelled), Toast.LENGTH_SHORT).show();

                            } else

                            {
                                EApplication.getInstance().showToastMessageFunction("" + response.getString("message"));
                                CustomProgressBarDialog.progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            CustomProgressBarDialog.progressDialog.dismiss();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());

                CustomProgressBarDialog.progressDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.i("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);
                return headers;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        EApplication.getInstance().addToRequestQueue(req, "high");
    }


    private void reservationWarningNotificationDialog(String Message, final int code) {

        final Dialog dialog = new Dialog(Activity_UserSelectDriver.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reservation_notification_dailog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView txt_message = (TextView) dialog.findViewById(R.id.txt_message);
        txt_message.setText(Message);
        Button btnOK = (Button) dialog.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (code != 201) {
                    visible = false;
                    session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);
                    EApplication.getInstance().setHistryCancelledStatus(true);
                    session.clearReservationData();
                    Intent mainIntent = new Intent(Activity_UserSelectDriver.this, Activity_UserMain.class);
                    EApplication.getInstance().setfromuserselect(true);


                    startActivity(mainIntent);
                    finish();
                    // FragmentManager fragmentManager = getFragmentManager();
                    //  fragmentManager.beginTransaction().replace(R.id.content_frame, new Fragment_WaitingDriverResponse()).commit();
                    //  finish();
                }
            }
        });
        if (visible) dialog.show();

    }


    private String timeConversion(int seconds) {
        String h = " hrs ";
        String d = " days ";
        String m = getResources().getString(R.string.mins);
        String s = " secs ";


        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);

        if (day <= 1) {
            d = " day ";
        }
        if (hours <= 1) {
            h = " hr ";
        }
        if (minute <= 1) {
            m = getResources().getString(R.string.min);
        }
        if (second <= 1) {
            s = " sec ";
        }

        if (day == 0) {

            if (hours == 0) {
                if (minute == 0) {
                    if (second == 0) {
                        return getResources().getString(R.string.not_null);
                    } else {
                        return second + s;
                    }

                } else {
                    return minute + m;
                }

            } else {
                return hours + h + minute + m + second + s;
            }

        } else {
            return day + d + hours + h + minute + m + second + s;
        }
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_cancel_or_selectDriver));
        // EApplication.getInstance().setfromuserselect(true);
       /* Intent intent = new Intent(Activity_UserSelectDriver.this, Activity_UserMain.class);
        startActivity(intent);
        //
        finish();
        overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);*/
    }

    public void FunctionLoadDriverList() {
//        Toast.makeText(getActivity(), "load driver list method", Toast.LENGTH_LONG).show();
        if (NetworkUtils.isNetworkAvailable(this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(this);
            if (visible) CustomProgressBarDialog.progressDialog.show();
            CustomProgressBarDialog.progressDialog.setCancelable(true);
          /*  HashMap<String, String> reservationData = session.getReservationNewData();
            RESERVATION_ID = reservationData.get(SessionManager.RESERVATION_NEW_ID);*/
            final ArrayList<HashMap<String, String>> serverDataList = new ArrayList<>();
            HashMap<String, String> reservationData = session.getReservationNewData();
            RESERVATION_ID = reservationData.get(SessionManager.RESERVATION_NEW_ID);
            if (RESERVATION_ID == null) {
                Intent intent = new Intent(Activity_UserSelectDriver.this, Activity_UserMain.class);
                startActivity(intent);
                finish();

            }
            String REQUEST_URL = Config.APP_BASE_URL + Config.USER_RESERVATION_RESPONSE_URL + RESERVATION_ID;
            Log.e("closest", Activity_UserSelectDriver.RESERVATION_ID);
            CustomRequest request = new CustomRequest(Request.Method.GET, REQUEST_URL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {

                                if (!response.getBoolean("error")) {

                                    CustomProgressBarDialog.progressDialog.dismiss();
                               /* EApplication.getInstance().showNotification(this, Activity_UserSelectDriver.class, 001, "Goto Track your user", "Click Here", "Go to track your user");*/
                                    if (!dataAllDriverList.isEmpty()) {
                                        dataAllDriverList.clear();
                                        adapter.notifyDataSetChanged();
                                    }
                                    Log.e("all_driver", response.toString());

                                    JSONArray all_driversArray = response.getJSONArray("all_drivers");
                                    for (int i = 0; i < all_driversArray.length(); i++) {
                                        HashMap<String, String> mapData = new HashMap<>();
                                        JSONObject parentObject = all_driversArray.getJSONObject(i);
                                        String RESERVATION_ID = parentObject.getString("reservation_id");
                                        String DRIVER_ID = parentObject.getString("driver_id");
                                        DRIVER_NAME = parentObject.getJSONObject("oneuser").getString("fullname");
                                        MOBILE = parentObject.getJSONObject("oneuser").getString("mobile_number");
                                        PROFILE = parentObject.getJSONObject("onedriver").getString("profile_picture");
                                        REDG = parentObject.getJSONObject("onedriver").getString("license_no");
                                        String NEAREST_DISTANCE = parentObject.getString("distance");
                                        String BEST_RATING = parentObject.getString("ratings");
                                        if (BEST_RATING.equals("null") || TextUtils.isEmpty(BEST_RATING))
                                            BEST_RATING = "0.0";

                                        LocateModel locateModel = new LocateModel();
                                        locateModel.setDriver_id(DRIVER_ID);
                                        locateModel.setDriver_name(DRIVER_NAME);
                                        locateModel.setDriver_mobile(MOBILE);
                                        locateModel.setDriver_profile(PROFILE);
                                        locateModel.setDriver_redg(REDG);

                                        mapData.put("DRIVER_NAME", DRIVER_NAME);
                                        mapData.put("RESERVATION_ID", RESERVATION_ID);
                                        mapData.put("DRIVER_ID", DRIVER_ID);
                                        mapData.put("NEAREST_DISTANCE", NEAREST_DISTANCE);
                                        mapData.put("BEST_RATING", BEST_RATING);
                                        mapData.put("DRIVER_REDG", REDG);
                                        mapData.put("DRIVER_MOBILE", MOBILE);
                                        mapData.put("DRIVER_PROFILE", PROFILE);
                                        serverDataList.add(mapData);
                                    }
                           /*     Log.e(" ARRAY ListItems>>", serverDataList.toString());*/

                                    // clear data from database
                                    EApplication.getInstance().deleteAllInDB(DBTableFields.TABLE_DRIVER_BID);

                                    Log.e("::", "Data Cleared");

                                    // insert new data to table
                                    EApplication.getInstance().addAllToDB(DBTableFields.TABLE_DRIVER_BID, serverDataList);
                                    Log.e("::", "Data Inserted" + serverDataList.toString());
//                                CustomProgressBarDialog.progressDialog.dismiss();


                                    dataAllDriverList = EApplication.getInstance().getAllToSortedList(DBTableFields.TABLE_DRIVER_BID, new String[]{DBTableFields.TABLE_BID_NEAREST_DRIVER}, DBTableFields.ASC);
                                    adapter = new AdapterQuotes(Activity_UserSelectDriver.this, dataAllDriverList);
                                    selectDriverListView.setAdapter(adapter);
                                    selectDriverListView.setEmptyView(emptyMessage);
                                    adapter.notifyDataSetChanged();
                                } else

                                {/*
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                  session = new SessionManager(Activity_UserSelectDriver.this);
                                    HashMap<String, String> userDetails = session.getUserDetails();
                                   CustomProgressBarDialog.progressDialog.dismiss();
                                    user_ID = userDetails.get(SessionManager.KEY_ID);
                                    HashMap<String, String> getDriverWithReservation = session.getReservationIdWithDriver();
                                    reservation_id = getDriverWithReservation.get(SessionManager.RESERVATION_ID);
                                    //reservationCancelAPIExecute(reservation_id);
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.nomore_drivers));
                                    EApplication.getInstance().deleteAllInDB(DBTableFields.TABLE_DRIVER_BID);
                                    session.createReservationByUser(DBTableFields.RESERVATION_STATE_NEW);
                                    session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                                    Intent intent = new Intent(Activity_UserSelectDriver.this, Activity_UserMain.class);
                                    reservationCancelApiExecuteWithoutToast(reservation_id);
                                    EApplication.getInstance().setfromuserselect(true);
                                    startActivity(intent);
                                    finish();*/
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }


//
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
//                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                @Override
                public Request.Priority getPriority() {
                    return Priority.HIGH;
                }


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }

            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(request, "high");

        } else {
            NetworkUtils.showNoConnectionDialog(this);
        }

    }

    private final Runnable refreshing = new Runnable() {
        public void run() {
            try {
                if (isRefreshing()) handler.postDelayed(this, 1000);
                else mSwipeRefreshLayout.setRefreshing(false);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private boolean isRefreshing() {
        FunctionLoadDriverList();


        return false;
    }

    private void initializaMap(Bundle savedInstanceState) {
        MapsInitializer.initialize(Activity_UserSelectDriver.this);
        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(Activity_UserSelectDriver.this)) {
            case ConnectionResult.SUCCESS:
                mapView = (MapView) findViewById(R.id.content_map_view);
                mapView.onCreate(savedInstanceState);
                if (mapView != null) {
                    map = mapView.getMap();
                    map.getUiSettings().setMyLocationButtonEnabled(true);
                    map.getUiSettings().setRotateGesturesEnabled(false);
                    map.setMyLocationEnabled(true);
                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;
            default:

        }
    }

    @Override
    public void onRefresh() {

        handler.post(refreshing);
    }
}
