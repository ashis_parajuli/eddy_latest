package com.EddyCabLatest.application.moduleUser.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.EddyCabLatest.application.R;


public class Activity_UserProfileUpdate extends ActionBarActivity implements View.OnClickListener {
    RelativeLayout rl_name;
    RelativeLayout rl_email;
    RelativeLayout rl_mobile;

    Toolbar app_toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user_profile_edit);

        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Update Profile");

        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);



        /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);


        initViews();

        intentFrmFragProfile();
    }

    private void initViews() {
       /* rl_name = (RelativeLayout) findViewById(R.id.changeNameLayout);
        rl_email = (RelativeLayout) findViewById(R.id.changeEmailLayout);
        rl_mobile = (RelativeLayout) findViewById(R.id.changeMobileLayout);*/
    }

    private void intentFrmFragProfile() {
        String identifier = getIntent().getExtras().getString("IDENTIFIER");
        if (identifier.equals("Name")) {
            rl_name.setVisibility(View.VISIBLE);

        } else if (identifier.equals("Email")) {
            rl_email.setVisibility(View.VISIBLE);

        } else if (identifier.equals("Mobile")) {
            rl_mobile.setVisibility(View.VISIBLE);

        } else if (identifier.equals("PassCode")) {

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_update, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_save:
                Toast.makeText(Activity_UserProfileUpdate.this," work on save here ",Toast.LENGTH_LONG).show();

                return true;

            case R.id.home:
                this.finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            default:
                break;
        }
    }
}
