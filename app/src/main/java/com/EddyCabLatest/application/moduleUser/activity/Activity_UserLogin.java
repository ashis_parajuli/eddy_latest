package com.EddyCabLatest.application.moduleUser.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.SelectUserType;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.gcmNotification.GCMClientManager;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Activity_UserLogin extends AppCompatActivity implements View.OnClickListener {

    Toolbar app_toolBar;
    SessionManager session;
    EditText user_login_password, user_login_email;

    private GCMClientManager pushClientManager;
    String PROJECT_NUMBER = Config.GCM_SENDER_KEY;
    String GCM_REG_DEVICE_ID;
    String language = "";
    String TAG = "===";

    private TextView apptitle_toolbar_driver_help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

         /*ToolBar SetUp*/
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        apptitle_toolbar_driver_help = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
        apptitle_toolbar_driver_help.setText(getResources().getString(R.string.user_login));
        /* Session Init */
        session = new SessionManager(Activity_UserLogin.this);
        /* References Init */
        user_login_email = (EditText) findViewById(R.id.user_email_or_phone);
        user_login_password = (EditText) findViewById(R.id.user_enter_pass);

        TextView forget_password = (TextView) findViewById(R.id.forget_password);
        forget_password.setText(Html.fromHtml("<u>" + getResources().getString(R.string.forget_pass) + "</u>"));

        forget_password.setOnClickListener(this);

        Button btn_Submit = (Button) findViewById(R.id.btn_Submit);
        btn_Submit.setOnClickListener(this);

        Button btn_register = (Button) findViewById(R.id.btn_action_joinFree);
        btn_register.setOnClickListener(this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Intent intent = new Intent(Activity_UserLogin.this, SelectUserType.class);
                startActivity(intent);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(Activity_UserLogin.this, SelectUserType.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_Submit:

                if (user_login_email.getText().toString().trim().length() < 10) {

                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.enter_the_10_digit_mobile));
                    user_login_email.setError(getResources().getString(R.string.enter_the_10_digit_mobile));

                } else {
                    methodLoginCall();
                }
                break;

            case R.id.forget_password:

                methodForgetPassword();
                break;

            case R.id.btn_action_joinFree:

                Intent intent = new Intent(Activity_UserLogin.this, Activity_UserRegister.class);
                intent.putExtra("type", "user");
                startActivity(intent);
                Activity_UserLogin.this.finish();
                break;
        }
    }

    private void methodForgetPassword() {

        Intent intent = new Intent(Activity_UserLogin.this, Activity_UserForgetPassword.class);
        startActivity(intent);
    }

    private void methodLoginCall() {
        if (NetworkUtils.isNetworkAvailable(Activity_UserLogin.this)) {

            if (!user_login_email.getText().toString().trim().equals("") && !user_login_password.getText().toString().trim().equals("")) {
                CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Activity_UserLogin.this);
                CustomProgressBarDialog.progressDialog.show();

                pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
                pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                    @Override
                    public void onSuccess(String registrationId, boolean isNewRegistration) {
                        GCM_REG_DEVICE_ID = registrationId;
                        Log.e("===", "GCM Value is :" + GCM_REG_DEVICE_ID);
                        Log.e("===", "Device successfully registered for GCM Notification !!!");


                        if (NetworkUtils.isNetworkAvailable(Activity_UserLogin.this)) {

                            if (!TextUtils.isEmpty(GCM_REG_DEVICE_ID)) {
                                final HashMap<String, String> params = new HashMap<>();
                                params.put("mobile_number", user_login_email.getText().toString().trim());
                                params.put("password", user_login_password.getText().toString().trim());
                                params.put("type", "user");
                                params.put("device_type", Config.DEVICE_TYPE);
                                params.put("device_id", GCM_REG_DEVICE_ID);
                                Log.e("Parameters", params.toString());
                                Log.e(TAG, "LOginUrl" + Config.APP_BASE_URL + Config.USER_LOGIN_URL);
                                StringRequest req = new StringRequest(Request.Method.POST, Config.APP_BASE_URL + Config.USER_LOGIN_URL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String oldResponseObject) {
                                                String newJsonResponse = oldResponseObject;
                                                if (newJsonResponse.contains("Ã¯Â»Â¿")) {
                                                    newJsonResponse = newJsonResponse.substring(newJsonResponse.indexOf("{"));
                                                }
                                                try {
                                                    String lastReservationId = "";
                                                    JSONObject responseObject = new JSONObject(newJsonResponse);
                                                    Log.e(TAG, "Response is :" + responseObject.toString());
                                                    boolean error = responseObject.getBoolean("error");
                                                    Log.e("===", responseObject.getString("message") + "error" + error);
                                                    String message = responseObject.getString("message");
                                                    Log.e("===", "message is:" + message);
                                                    if (!error) {
                                                        CustomProgressBarDialog.progressDialog.dismiss();
                                                        JSONObject user_details = responseObject.getJSONObject("User");
                                              /*      try {
                                                        JSONObject lastReservation = responseObject.getJSONObject("last_reservation_id");
                                                        if (lastReservation != JSONObject.NULL) {
                                                            lastReservationId = lastReservation.getString("id");
                                                        } else {
                                                            lastReservationId = "none";
                                                        }
                                                    } catch (Exception ex) {
                                                        String lastReservationString = responseObject.getString("last_reservation_id");
                                                        if (lastReservationString.equals("null")) {
                                                            lastReservationId = "none";
                                                        }
                                                    }*/
                                                        Log.e("User Details", user_details.toString());
                                                        int user_id = user_details.getInt("id");
                                                        Log.e("User_id", String.valueOf(user_id));

                                                        String user_name = user_details.getString("fullname");
                                                        String user_email = user_details.getString("email");
                                                        String user_rating = user_details.getString("rating");
                                                        String user_ProfilePic = user_details.getString("user_pp");
                                                        String user_referralcode = user_details.getString("referral_code");
                                                        String acces_token = user_details.getString("access_token");

                                                        /*  String total_credit = user_details.getString("total_credit");*/
                                                        String corrected_rating = "";
                                                        if (!TextUtils.equals(user_rating, "0") && !TextUtils.isEmpty(user_rating)) {
                                                            corrected_rating = user_rating;
                                                        } else corrected_rating = "0.0";
                                                        /* Save User Details to the Shared Preferences*/

                                                        session.createLoginSession(String.valueOf(user_id), user_name, user_email, user_ProfilePic,
                                                                corrected_rating, user_details.toString(), user_referralcode, "mobile_no", "license_no", lastReservationId, acces_token);
                                                            /* Save banana credit */
                                                        //   session.setUserBananaCredit("0");
                                                        Intent intent = new Intent(Activity_UserLogin.this, Activity_UserMain.class);
                                                        EApplication.getInstance().setPreviousReservationStatus(true);
                                                        startActivity(intent);
                                                        Activity_UserLogin.this.finish();

                                                    } else {
                                                        EApplication.getInstance().showToastMessageFunction(message);
                                                        CustomProgressBarDialog.progressDialog.dismiss();

                                                    }


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                    CustomProgressBarDialog.progressDialog.dismiss();
                                                }

                                                CustomProgressBarDialog.progressDialog.dismiss();

                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e(TAG, "Error: " + error);
                                        CustomProgressBarDialog.progressDialog.dismiss();


                                    }
                                }) {

                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        return params;
                                    }

                                    @Override
                                    public Request.Priority getPriority() {
                                        return Request.Priority.HIGH;
                                    }

                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        HashMap<String, String> headers = new HashMap<String, String>();

                                        String current_language = session.getAppLanguage();
                                        if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                            language = "en";
                                        }
                                        if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                            language = "np";
                                        }
                                        headers.put("lang", language);
                                        return headers;
                                    }
                                };
                                int socketTimeout = 20000; //30 seconds
                                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                req.setRetryPolicy(policy);
                                EApplication.getInstance().addToRequestQueue(req, "high");
                                req.setRetryPolicy(new DefaultRetryPolicy(
                                        15000,
                                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                            }
                            //else
                            //EApplication.getInstance().showToastMessageFunction("Please wait a while device registering to GCM !!!");

                        } else {
                            Toast.makeText(Activity_UserLogin.this, "No Network Connected ", Toast.LENGTH_SHORT);
                            NetworkUtils.showNoConnectionDialog(Activity_UserLogin.this);
                        }

                    }

                    @Override
                    public void onFailure(String ex) {
                        CustomProgressBarDialog.progressDialog.dismiss();

                        runOnUiThread(new Runnable() {
                            public void run() {
                                EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                            }
                        });
                        super.onFailure(ex);


                    }
                });
            } else {

                if (user_login_email.getText().toString().trim().equals("")) {
                    user_login_email.setError(getResources().getString(R.string.correct_mobile));
                }
                if (user_login_password.getText().toString().trim().equals("")) {
                    user_login_password.setError(getResources().getString(R.string.please_enter_password));
                }
            }
        } else NetworkUtils.showNoConnectionDialog(Activity_UserLogin.this);
    }
}

