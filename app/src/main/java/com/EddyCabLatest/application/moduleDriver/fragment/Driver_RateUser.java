package com.EddyCabLatest.application.moduleDriver.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.utilities.CustomRatingBar_with_rating;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Driver_RateUser extends Fragment {

    EditText feedback_message;
    Button submit_feedback;
    CustomRatingBar_with_rating ratingBar;
    String journeyFee;
    TextView txt_bhat_value;
    TextView txt_date;
    HashMap<String, String> requiredParam;
    String TAG = "===";
    String comment;
    public Context mcontext;

    float ratingValue;
    private SessionManager session;
    private String language;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_fragment_rate_user, container, false);
        mcontext = container.getContext();
        feedback_message = (EditText) rootView.findViewById(R.id.user_comment_message);
        submit_feedback = (Button) rootView.findViewById(R.id.btn_submit_rating);
        ratingBar = (CustomRatingBar_with_rating) rootView.findViewById(R.id.ratingBar);
        txt_bhat_value = (TextView) rootView.findViewById(R.id.txt_bhat_value);
        txt_date = (TextView) rootView.findViewById(R.id.txt_date);
session= new SessionManager(mcontext);
        //getting current date of system
        SimpleDateFormat dfDate_day = new SimpleDateFormat("MMM dd, yyyy");
        String dt = "";
        Calendar c = Calendar.getInstance();
        dt = dfDate_day.format(c.getTime());
        Log.e(TAG, "date:" + dt);
        txt_date.setText(dt);
        requiredParam = EApplication.getUserRateData;
        Log.e("requiredParam", "" + requiredParam);
        journeyFee = requiredParam.get("ride_fare");
        Log.e(TAG, "rating value :" + ratingValue);
        txt_bhat_value.setText(journeyFee);
    /*    ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingValue = rating;
                Log.e(TAG, "RatingValue is :" + rating);
                if (rating >= 4) {
                    feedback_message.setVisibility(View.GONE);
                } else {
                    feedback_message.setVisibility(View.VISIBLE);
                }

            }
        });*/
        ratingBar.setOnScoreChanged(new CustomRatingBar_with_rating.IRatingBarCallbacks() {
            @Override
            public void scoreChanged(float score) {
                ratingValue = score;
                Log.e(TAG, "RatingValue is :" + score);
                if (score >= 4) {
                    feedback_message.setVisibility(View.GONE);
                } else {
                    feedback_message.setVisibility(View.VISIBLE);
                }
            }
        });

        submit_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EApplication.ref.child(requiredParam.get("driver_id")).removeValue();
                if (ratingValue >= 3) {
                    feedback_message.setVisibility(View.GONE);
                    comment = "no comment inserted";
                } else {
                    comment = feedback_message.getText().toString().trim();
                }
                
                if (ratingValue != 0) {
                    if (NetworkUtils.isNetworkAvailable(mcontext)) {
                        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(mcontext);
                        CustomProgressBarDialog.progressDialog.show();
                        HashMap<String, String> params = new HashMap<>();
                        params.put("reservation_id", requiredParam.get("reservation_id"));
                        params.put("from_id", requiredParam.get("driver_id"));
                        params.put("to_id", requiredParam.get("user_id"));
                        params.put("rating", String.valueOf(ratingValue));
                        params.put("comments", comment);

                        Log.e("Response Param", "" + params);

                        CustomRequest request = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.RATE_USER_BY_DRIVER, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                CustomProgressBarDialog.progressDialog.dismiss();
                      /*          EApplication.getInstance().openFragment1 = true;*/
                                if (isAdded()) {
                                    Intent intentGoToMain = new Intent(mcontext, Driver_MainActivity.class);
                                    startActivity(intentGoToMain);
                                    EApplication.getInstance().setUserRateByDriver(false);
                                    EApplication.getInstance().showToastMessageFunction("User rating sucess");
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                // EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));

                                NetworkResponse networkResponse = error.networkResponse;
                                if (networkResponse != null) {
                                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                                }
                                if (error instanceof TimeoutError) {
                                    Log.e("Driver_New_JObs_Volley", "TimeoutError");
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                                } else if (error instanceof NoConnectionError) {
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                                    Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                                } else if (error instanceof AuthFailureError) {
                                    Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                                } else if (error instanceof ServerError) {
                                    Log.e("Driver_New_JObs_Volley", "ServerError");
                                } else if (error instanceof NetworkError) {
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                                    Log.e("Driver_New_JObs_Volley", "NetworkError");
                                } else if (error instanceof ParseError) {
                                    Log.e("Driver_New_JObs_Volley", "ParseError");
                                }

                            }
                        }) {
                            @Override
                            public Request.Priority getPriority() {
                                return Request.Priority.HIGH;
                            }



                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                String current_language = session.getAppLanguage();
                                Log.e("current_language", current_language);
                                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                    language = "en";
                                }
                                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                    language = "np";
                                }
                                headers.put("lang", language);


                                return headers;
                            }
                        };
                        int socketTimeout = 20000; //30 seconds
                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        request.setRetryPolicy(policy);
                        EApplication.getInstance().addToRequestQueue(request, "high");


                    } else {
                        NetworkUtils.showNoConnectionDialog(mcontext);
                    }
                } else {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_rate_user));


                }

            }
        });

        setupUI(rootView);

        return rootView;
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    EApplication.getInstance().hideSoftKeyboard((Activity) mcontext);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }


}
