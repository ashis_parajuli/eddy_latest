package com.EddyCabLatest.application.moduleDriver.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.moduleUser.modal.JoblistItems;

import java.util.ArrayList;


public class Driver_JobListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<JoblistItems> dataList = new ArrayList<>();

    public Driver_JobListAdapter(Context context, ArrayList<JoblistItems> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataList.get(i);

    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater dInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = dInflater.inflate(R.layout.job_list_item, null);

        }
        TextView txt_from_place = (TextView) convertView.findViewById(R.id.txt_from_place);
        /*TextView txt_to_place = (TextView) convertView.findViewById(R.id.txt_to_place);*/
        TextView txt_job_payment = (TextView) convertView.findViewById(R.id.txt_bid);
        LinearLayout txt_job_payment_layout = (LinearLayout) convertView.findViewById(R.id.layout_roundedView);

        txt_from_place.setText(dataList.get(position).getFrom());
   /*     txt_to_place.setText(dataList.get(position).getTo());*/
        //TODo change here BID NOW according to the language selection
        switch (dataList.get(position).getBid_status()) {


            case "0":
                convertView.setEnabled(true);
                txt_job_payment.setText(context.getResources().getString(R.string.proceed));
                txt_job_payment.setTextColor(context.getResources().getColor(R.color.whiteColor));
                txt_job_payment_layout.setBackgroundColor(context.getResources().getColor(R.color.joblistgreen));
                break;
            case "1":
                convertView.setEnabled(true);
                txt_job_payment.setText(context.getResources().getString(R.string.proceed_nepali));
                txt_job_payment.setTextColor(context.getResources().getColor(R.color.whiteColor));
                txt_job_payment_layout.setBackgroundColor(context.getResources().getColor(R.color.joblistgreen));
                break;

            case "2":
                txt_job_payment.setText(context.getResources().getString(R.string.proceeded));
                txt_job_payment.setTextColor(context.getResources().getColor(R.color.blackColor));
                txt_job_payment_layout.setBackgroundColor(context.getResources().getColor(R.color.grayColor));
                convertView.setEnabled(false);
                // convertView.setOnClickListener(null);
                break;

            case "3":
                txt_job_payment.setText(context.getResources().getString(R.string.proceeded_nepali));
                txt_job_payment_layout.setBackgroundColor(context.getResources().getColor(R.color.grayColor));
                txt_job_payment.setTextColor(context.getResources().getColor(R.color.blackColor));
                convertView.setEnabled(false);
                //convertView.setOnClickListener(null);


                break;


        }
        return convertView;
    }
}
