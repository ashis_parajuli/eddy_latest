package com.EddyCabLatest.application.moduleDriver.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.EddyCabLatest.application.R;

import java.util.ArrayList;
import java.util.HashMap;



public class MessageAdapter extends BaseAdapter {

    private ArrayList<HashMap<String, String>> messageList;
    private Context context;

    public MessageAdapter(ArrayList<HashMap<String, String>> messageList, Context context) {
        this.messageList = messageList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Object getItem(int i) {
        return messageList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if(convertView==null){
            LayoutInflater dInflater=(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView=dInflater.inflate(R.layout.list_item_messages,null);
            TextView txt_message=(TextView)convertView.findViewById(R.id.txt_message);
            TextView txt_time=(TextView)convertView.findViewById(R.id.txt_time);

            txt_message.setText(messageList.get(position).get("message"));
            txt_time.setText(messageList.get(position).get("time"));
        }


        return convertView;
    }
}