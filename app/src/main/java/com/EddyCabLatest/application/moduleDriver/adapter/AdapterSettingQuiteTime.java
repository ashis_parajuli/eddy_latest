package com.EddyCabLatest.application.moduleDriver.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.views.customSwitch.SwitchButton;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterSettingQuiteTime extends BaseAdapter {

    private Context mContext;
    private ArrayList<HashMap<String, String>> dataList;


    public AdapterSettingQuiteTime(Context context, ArrayList<HashMap<String, String>> dataList) {
        this.mContext = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            LayoutInflater dInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = dInflater.inflate(R.layout.item_list_driver_quitetime, null);
        }

        TextView periodName = (TextView) convertView.findViewById(R.id.textPeriodHeader);
        TextView startTime = (TextView) convertView.findViewById(R.id.startTime);
        TextView endTime = (TextView) convertView.findViewById(R.id.endTime);
        SwitchButton actionSwitch = (SwitchButton) convertView.findViewById(R.id.actionSwitch);


        periodName.setText(dataList.get(position).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME));
        startTime.setText(dataList.get(position).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM));
        endTime.setText(dataList.get(position).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO));

        String status = dataList.get(position).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS).trim();
        if (status.equals("1")) actionSwitch.setChecked(true);
        else  actionSwitch.setChecked(false);

        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("IDENTIFIER ::", "StartTime" + position);
                ((Driver_MainActivity) mContext).getTimeFromPicker("Start Time", position, dataList);
            }
        });

        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Driver_MainActivity) mContext).getTimeFromPicker("End Time", position, dataList);
            }
        });

        actionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    ((Driver_MainActivity) mContext).getSwitchEventFromPicker(isChecked, position);

                }else{

                    ((Driver_MainActivity) mContext).getSwitchEventFromPicker(isChecked, position);
                }

            }
        });



        return convertView;
    }
}
