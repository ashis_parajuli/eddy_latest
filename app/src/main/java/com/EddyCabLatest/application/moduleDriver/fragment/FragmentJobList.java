package com.EddyCabLatest.application.moduleDriver.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_ActivityBidSuccess;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_NewJobActivity;
import com.EddyCabLatest.application.moduleDriver.adapter.Driver_JobListAdapter;
import com.EddyCabLatest.application.moduleUser.modal.JoblistItems;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.NewGPSTracker;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class FragmentJobList extends Fragment {

    public JSONObject usersJSON_route_data;
    public String language;
    public boolean enter = true;
    public Activity mActivity;
    public String credit;
    public CoordinatorLayout coordinatorLayout;
    ListView newJobListView;
    TextView textViewDate;
    Driver_JobListAdapter jobListAdapter;
    ArrayList<JoblistItems> dListitems;
    ArrayList<JoblistItems> pickup_item;
    String reservationDistance;
    String TAG = "===";
    int noOfBids = 0;
    View rootView;
    ArrayList<JoblistItems> driver_new_job;
    ArrayList<JoblistItems> driver_new_job_pList;
    private Handler handler = new Handler();
    private SessionManager session;
    private boolean visible = false;
    private Context mcontext;
    private String userID;
    private Runnable runnable;
    private TextView totalCredit_txt;
    private boolean notification = false;
    private ChildEventListener eventlistener;
    passCredittomain passcredittomain;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_job_list, container, false);
        mcontext = container.getContext();
        notification = false;
        session = new SessionManager(mcontext);
        newJobListView = (ListView) rootView.findViewById(R.id.newJobListView);
        totalCredit_txt = (TextView) rootView.findViewById(R.id.total_cr);
        this.rootView = rootView;
        coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.coordinate_joblist);
        HashMap<String, String> userDetails = session.getUserDetails();
        userID = userDetails.get(SessionManager.KEY_ID);
        //Taking references of view from fragment layout
        textViewDate = (TextView) rootView.findViewById(R.id.txt_date);

        updateDriverLocation();
//EApplication.getInstance().setfromdrivernewjob(false);
        newJobListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (newJobListView != null && newJobListView.getChildCount() > 0) {
                    boolean firstItemVisible = newJobListView.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = newJobListView.getChildAt(0).getTop() == 0;
                    boolean enable = firstItemVisible && topOfFirstItemVisible;
                }
            }
        });
//

        return rootView;
    }

    private void updateDriverLocation() {

        runnable = new Runnable() {
            @Override
            public void run() {
                NewGPSTracker gps_tracker = new NewGPSTracker(mcontext);
                if (gps_tracker.canGetLocation())

                {
                    EApplication.geofire.setLocation(userID, new GeoLocation(gps_tracker.getLatitude(), gps_tracker.getLongitude()), new GeoFire.CompletionListener() {

                        @Override
                        public void onComplete(String key, FirebaseError error) {
                            if (error != null) {
                                System.err.println("There was an error saving the location to GeoFire: " + error);
                            } else {
                                System.out.println("Location saved on server successfully!");
                            }
                        }
                    });
                }
                handler.postDelayed(runnable, 30 * 1000);
            }


        };
        handler.postDelayed(runnable, 5 * 1000);
        Log.e("===", "updateDriverLocation");


    }


    @Override
    public void onResume() {
        super.onResume();
        visible = true;
        notification = false;

        if (eventlistener != null) {
            EApplication.ref.child(userID + "/new_reservations/").removeEventListener(eventlistener);
            eventlistener = null;

            getjoblistfirebase();
        } else {

            getjoblistfirebase();
        }

        //  jobListRequest1();
    }

    @Override
    public void onDestroy() {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) mcontext.getSystemService(ns);
        nMgr.cancel(011);



            if(eventlistener!=null)
            {

                EApplication.ref.child(userID + "/new_reservations/").removeEventListener(eventlistener);
            }


        visible = false;
        if ((handler != null) && (runnable != null)) {

            handler.removeCallbacks(runnable);
        }
        super.onDestroy();
    }

    @Override
    public void onPause() {
        visible = false;
        notification = true;
        super.onPause();
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        passcredittomain = (passCredittomain) a;
    }

    public void getjoblistfirebase() {
        eventlistener = EApplication.ref.child(userID + "/new_reservations/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    if(dataSnapshot.child("accepted").exists())
                    {
                        if ((dataSnapshot.child("accepted").getValue() .equals(false))&&(dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("new"))) {
                            jobListRequest1();
                            if (isAdded()) {
                                EApplication.getInstance().showNotification(mcontext, Driver_MainActivity.class, 011, getResources().getString(R.string.new_job), getResources().getString(R.string.new_job), getResources().getString(R.string.new_job_avaiable));

                            }
                        }
                    }



                    Log.e("data_joblist", dataSnapshot + "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (isAdded()) {
                    if (dataSnapshot.getValue() != null) {

                        if (dataSnapshot.child("accepted").exists()) {
                            if (dataSnapshot.child("accepted").getValue() .equals(true)) {
                                //EApplication.ref.child(userID + "/new_reservations/" + dataSnapshot.getKey() + "/accepted/").removeValue();
                                EApplication.ref.child(userID + "/new_reservations/").removeEventListener(this);
                                if (eventlistener != null) {
                                    EApplication.ref.child(userID + "/new_reservations/").removeEventListener(this);
                                    if (eventlistener != null) {

                                        EApplication.ref.child(userID + "/new_reservations/").removeEventListener(this);
                                    }
                                }
                                try {
                                    if (dataSnapshot.getValue() != null) {

                                        Intent intent_bid_succes = new Intent(mcontext, Driver_ActivityBidSuccess.class);

                                        Map<String, String> usersMap_data = (Map<String, String>) dataSnapshot.getValue();
                                        JSONObject usersJSON = null;
                                        usersJSON = new JSONObject(usersMap_data);

                                        String selected_id = dataSnapshot.getKey();
                                        String user_fullname = dataSnapshot.child("user_fullname").getValue().toString();
                                        String distance_value = dataSnapshot.child("distance").getValue().toString();
                                        String user_pp = dataSnapshot.child("user_pp").getValue().toString();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("id", selected_id);

                                        bundle.putString("user_fullname", user_fullname);
                                        bundle.putString("user_pp", user_pp);
                                        bundle.putString("distance_value", distance_value);
                                        bundle.putString("user_data_for", usersJSON.toString());

                                        intent_bid_succes.putExtras(bundle);


                                        mcontext.startActivity(intent_bid_succes);
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            } else {
                                //jobListRequest1();

                            }

                        }

                    }
                }


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // jobListRequest1();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }

    public void passData(int data) {
        passcredittomain.oncreditpass(data);
    }

    private void jobListRequest1() {
        Log.e("Methods Called", "Proceed");
        if (NetworkUtils.isNetworkAvailable(mcontext)) {

            HashMap<String, String> userDetails = session.getUserDetails();
            final String userID = userDetails.get(SessionManager.KEY_ID);
            String jobListUrl = Config.APP_BASE_URL + Config.GET_NEW_RESERVATIONS + userID;
            Log.e("jobnListUrl", jobListUrl);
            CustomRequest customRequest = new CustomRequest(Request.Method.GET,
                    jobListUrl, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Response", response.toString());
                    try {

                        boolean error = response.getBoolean("error");
                        int total_credit = response.getInt("total_credit");

                        int total_bonus = response.getInt("total_bonus_credit");
                        int credit_only = total_credit - total_bonus;
                        session.setUserBananaCredit(String.valueOf(total_credit), String.valueOf(total_bonus), String.valueOf(credit_only));
                        passcredittomain.oncreditpass(total_credit);
                        passData(total_credit);
                        if (!error) {
                            session.setUserBananaCredit("0", "0", "0");
                            dListitems = new ArrayList<>();
                            pickup_item = new ArrayList<>();
                            JSONArray all_reservation_array = response.getJSONArray("all");
                            for (int i = 0; i < all_reservation_array.length(); i++) {

                                String title_pickup = all_reservation_array.getJSONObject(i).getJSONObject("pickup_location").getString("title");
                                Double pick_lat = all_reservation_array.getJSONObject(i).getJSONObject("pickup_location").getDouble("latitude");
                                Double pick_lng = all_reservation_array.getJSONObject(i).getJSONObject("pickup_location").getDouble("longitude");
                                String date_time = all_reservation_array.getJSONObject(i).getString("datetime");
                                String notes = all_reservation_array.getJSONObject(i).getString("notes");
                                String date = all_reservation_array.getJSONObject(i).getString("date");
                                String time = all_reservation_array.getJSONObject(i).getString("time");
                                String reservationID = all_reservation_array.getJSONObject(i).getString("id");
                                reservationDistance = all_reservation_array.getJSONObject(i).getString("reservation_distance");
                                Log.e(TAG, "reservationDistance is :" + reservationDistance);

                                String bidding_status;
                                String current_language = session.getAppLanguage();
                                if ((all_reservation_array.getJSONObject(i).getString("onebidding")).equals("null") /*!= JSONObject.NULL*/) {

                                    Log.e("current_language", current_language);
                                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                        bidding_status = "0";
                                    } else {

                                        bidding_status = "1";
                                    }
                                } else {
                                    noOfBids += 1;
                                    EApplication.noOfJobsBidded = noOfBids;
                                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                        bidding_status = "2";

                                    } else {
                                        bidding_status = "3";
                                    }
                                }

                                Log.e("Bidding Status", bidding_status);
                                dListitems.add(new JoblistItems(title_pickup, bidding_status));
                                /*String from, String to, Double pick_lat, Double pick_lang, Double drop_lat, Double drop_lang, String date_time, String note, String date, String time, String bid_status, String reservationID,String padding*/
                                pickup_item.add(new JoblistItems(title_pickup, pick_lat, pick_lng, date_time, notes, date, time, bidding_status, reservationID, "0"));
                            }
                            Log.e("Data After Added", dListitems.toString());
                            Log.e("PickUp after added", pickup_item.get(0).getBid_status() + "\n");

                            jobListAdapter = new Driver_JobListAdapter(mcontext, dListitems);
                            newJobListView.setAdapter(jobListAdapter);
                            jobListAdapter.notifyDataSetChanged();
                            newJobListView.setEmptyView(textViewDate);

                            newJobListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                    Animation animation1 = new AlphaAnimation(0.3f, 1.0f);
                                    animation1.setDuration(4000);
                                    view.startAnimation(animation1);
                                    Driver_NewJobActivity.setRequiredData(pickup_item);
                                    EApplication.getInstance().setDriverNewjobdata(pickup_item, dListitems);
                                    if ((pickup_item.get(position).getBid_status().toString() == "0") || (pickup_item.get(position).getBid_status().toString() == "1")) {
                                        ((Driver_MainActivity) mcontext).checkISDriverLogin(position);
                                    }
                                }
                            });
                        } else {
                            newJobListView.setEmptyView(textViewDate);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    CustomProgressBarDialog.progressDialog.dismiss();
                    VolleyLog.e("Error: ", error.getMessage());



                   EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_internet_availeble_please_try_again));

                }
            }) {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);


                    return headers;
                }
            };

            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            customRequest.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(customRequest, "high");
        } else {
            NetworkUtils.showNoConnectionDialog(mcontext);
        }
    }


    private void jobListRequest() {
        Log.e("Methods Called", "Proceed");
        if (NetworkUtils.isNetworkAvailable(mcontext)) {


            HashMap<String, String> userDetails = session.getUserDetails();
            final String userID = userDetails.get(SessionManager.KEY_ID);
            String jobListUrl = Config.APP_BASE_URL + Config.GET_NEW_RESERVATIONS + userID;
            Log.e("jobnListUrl", jobListUrl);
            CustomRequest customRequest = new CustomRequest(Request.Method.GET,
                    jobListUrl, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("Response", response.toString());
                    try {
                        session.setUserBananaCredit("0", "0", "0");
                        boolean error = response.getBoolean("error");
                        int total_credit = response.getInt("total_credit");

                        int total_bonus = response.getInt("total_bonus_credit");
                        int credit_only = total_credit - total_bonus;
                        session.setUserBananaCredit(String.valueOf(total_credit), String.valueOf(total_bonus), String.valueOf(credit_only));


                        if (!error) {
                            dListitems = new ArrayList<>();
                            pickup_item = new ArrayList<>();

                            JSONArray all_reservation_array = response.getJSONArray("all");
                            for (int i = 0; i < all_reservation_array.length(); i++) {
                                String title_pickup = all_reservation_array.getJSONObject(i).getJSONObject("pickup_location").getString("title");
                                Double pick_lat = all_reservation_array.getJSONObject(i).getJSONObject("pickup_location").getDouble("latitude");
                                Double pick_lng = all_reservation_array.getJSONObject(i).getJSONObject("pickup_location").getDouble("longitude");

                                String date_time = all_reservation_array.getJSONObject(i).getString("datetime");
                                String notes = all_reservation_array.getJSONObject(i).getString("notes");
                                String date = all_reservation_array.getJSONObject(i).getString("date");
                                String time = all_reservation_array.getJSONObject(i).getString("time");
                                String reservationID = all_reservation_array.getJSONObject(i).getString("id");
                                reservationDistance = "0";
                                Log.e(TAG, "reservationDistance is :" + reservationDistance);

                                String bidding_status;
                                String current_language = session.getAppLanguage();
                                if ((all_reservation_array.getJSONObject(i).getString("onebidding")).equals("null") /*!= JSONObject.NULL*/) {

                                    Log.e("current_language", current_language);
                                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                        bidding_status = "0";
                                    } else {

                                        //
                                        bidding_status = "1";
                                    }
                                } else {
                                    noOfBids += 1;
                                    EApplication.noOfJobsBidded = noOfBids;
                                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                        bidding_status = "2";

                                    } else {
                                        bidding_status = "3";
                                    }
                                }
                                Log.e("Bidding Status", bidding_status);
                                dListitems.add(new JoblistItems(title_pickup, bidding_status));
                                /*String from, String to, Double pick_lat, Double pick_lang, Double drop_lat, Double drop_lang, String date_time, String note, String date, String time, String bid_status, String reservationID,String padding*/
                                pickup_item.add(new JoblistItems(title_pickup, pick_lat, pick_lng, date_time, notes, date, time, bidding_status, reservationID, "0"));
                            }
                            Log.e("Data After Added", dListitems.toString());
                            Log.e("PickUp after added", pickup_item.get(0).getBid_status() + "\n");

                            jobListAdapter = new Driver_JobListAdapter(mcontext, dListitems);
                            newJobListView.setAdapter(jobListAdapter);
                            jobListAdapter.notifyDataSetChanged();
                            newJobListView.setEmptyView(textViewDate);
                            newJobListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                    if (pickup_item != null) {


                                        if ((pickup_item.get(position).getBid_status() == "0") || (pickup_item.get(position).getBid_status() == "1")) {
                                            Driver_NewJobActivity.setRequiredData(pickup_item);
                                            EApplication.getInstance().setDriverNewjobdata(pickup_item, dListitems);
                                            ((Driver_MainActivity) mcontext).checkISDriverLogin(position);
                                        }
                                    }
                                }
                            });
                        } else {


                            // EApplication.ref.child(userID).removeValue();
                        }
                        //EApplication.getInstance().showToastMessageFunction(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_internet_availeble_please_try_again));
                    VolleyLog.e("Error: ", error.getMessage());

                }
            }) {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);


                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            customRequest.setRetryPolicy(policy);

            EApplication.getInstance().addToRequestQueue(customRequest, "high");
        } else {

            NetworkUtils.showNoConnectionDialog(mcontext);
        }

    }

    private void showsnakbar() {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }


    public interface passCredittomain {
        public void oncreditpass(int data);
    }

}










