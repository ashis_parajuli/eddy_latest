package com.EddyCabLatest.application.moduleDriver.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.modal.data_history;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class FragmentCredit extends android.support.v4.app.Fragment {
    public Context mcontext;

    private TableLayout myLInearLayout;

    private ListView listView;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private CustomAdapter mAdapter;
    private SessionManager session;
    private String UserID, bonus_created_date, bonus_server, bonus_created_by, bonus_agent_name;
    public ArrayList<Object> dataList;
    ArrayList<data_history> created_date_Array;

    public String created_date_value;
    String created_date, credit_server, created_by, agent_name, server_total_credit;
    private EditText recharge_num;
    private Button submit_recharge;
    OnHeadlineSelectedListener mCallback;
    private String recharge_num_value;
    private String server_total_bonus;
    private TextView toal_txt;
    private TextView empty_listP_credit;
    private String language;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tableview, container, false);

        mcontext = container.getContext();
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        EventBus.getDefault().register(this);
        recharge_num = (EditText) rootView.findViewById(R.id.card_num);
        toal_txt = (TextView) rootView.findViewById(R.id.total_cr);
        empty_listP_credit = (TextView) rootView.findViewById(R.id.empty_list);
        submit_recharge = (Button) rootView.findViewById(R.id.submit_recharge);
        session = new SessionManager(container.getContext());
        submit_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("recharge_card_num", "recharge card num" + recharge_num.getText().toString().trim());

                if(!recharge_num.getText().toString().trim().equals(""))
                {
                    recharge_card(recharge_num.getText().toString().trim());
                }
                else
                {

                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_enter_credit_num));
                }



            }
        });
        HashMap<String, String> userDetails = session.getUserDetails();
        UserID = userDetails.get(SessionManager.KEY_ID);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(container.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        apiCallForHistory();
        toal_txt.setText(getResources().getString(R.string.Total_credit) + session.getUserBananaCredit());
        return rootView;
    }

    public interface OnHeadlineSelectedListener {
        public void onArticleSelected(String credit, String bonus);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Subscribe
    public void onEvent(Credit_Details event) {/* Do something */}

    ;

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {
        private static final String TAG = "CustomAdapter";

        private ArrayList<data_history> mDataSet;

        /**
         * Provide a reference to the type of views that you are using (custom ViewHolder)
         */
        public class ViewHolder extends RecyclerView.ViewHolder {
            private final TextView textView, textView1, textView2;

            public ViewHolder(View v) {
                super(v);
                // Define click listener for the ViewHolder's View.
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "Element " + getPosition() + " clicked.");
                    }
                });
                textView = (TextView) v.findViewById(R.id.text);
                textView1 = (TextView) v.findViewById(R.id.text1);
                textView2 = (TextView) v.findViewById(R.id.text2);


            }


            public TextView getTextView() {
                return textView;
            }
        }

        /**
         * Initialize the dataset of the Adapter.
         *
         * @param listener
         * @param dataSet  String[] containing the data to populate views to be used by RecyclerView.
         */
        public CustomAdapter(Response.Listener<JSONObject> listener, ArrayList<data_history> dataSet) {
            this.mDataSet = dataSet;
        }

        // Create new views (invoked by fthe layout manager)
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            // Create a new view.Create a new view.
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.text_row_item, viewGroup, false);

            return new ViewHolder(v);
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            Log.d(TAG, "Element " + position + " set");
            // Get element from your dataset at this position and replace the contents of the view
            // with that element
            viewHolder.textView.setText(mDataSet.get(position).date);
            viewHolder.textView1.setText(mDataSet.get(position).credit);
            viewHolder.textView2.setText(mDataSet.get(position).agent_name);
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mDataSet.size();
        }
    }

    public void apiCallForHistory() {

        String url = Config.APP_BASE_URL + Config.DRIVER_RECHARGE_HISTORY + UserID;
        Log.e("url", url);
        CustomRequest req = new CustomRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {


                    @Override
                    public void onResponse(JSONObject responseObject) {

                        Log.e("Response1", "User Job History Response Objcet");

                        try {
                            boolean error = responseObject.getBoolean("error");


                            if (!error) {
                                created_date_Array = new ArrayList<>();
                                try {
                                    JSONObject total_credit = responseObject.getJSONObject("message");
                                    server_total_credit = total_credit.getString("total_credit");
                                    server_total_bonus = total_credit.getString("total_bonus_credit");
                                    JSONArray objectArray = total_credit.getJSONArray("recharge_history");
                                    if (objectArray != null) {
                                        for (int i = 0; i < objectArray.length(); i++) {
                                            empty_listP_credit.setVisibility(View.INVISIBLE);
                                            JSONObject dataObject = (JSONObject) objectArray.get(i);
                                            created_date = dataObject.getString("created_at");
                                            credit_server = dataObject.getString("credit");
                                            created_by = dataObject.getString("created_by");
                                            agent_name = dataObject.getString("agent_name");
                                            String date_before = created_date;
                                            String date_after = formateDateFromstring("yyyy-MM-dd", "dd/MM/yyyy", date_before);
                                            // EventBus.getDefault().post(new CreditEvent(server_total_credit));
                                            created_date_Array.add(new data_history(date_after, credit_server, agent_name));
                                        }

                                        //toal_txt.setText(getResources().getString(R.string.total_credit)+" : "+total_text+"");
                                        // toal_txt.setText(Integer.parseInt(server_total_credit)+Integer.parseInt(server_total_bonus));
                                        mCallback.onArticleSelected(server_total_credit, server_total_bonus);
                                        mAdapter = new CustomAdapter(this, created_date_Array);
                                        mRecyclerView.setAdapter(mAdapter);
                                    } else {

                                        empty_listP_credit.setVisibility(View.VISIBLE);
                                        mRecyclerView.setAdapter(null);
                                    }

                                } catch (Exception e) {

                                    e.printStackTrace();
                                }

                            } else {

                                EApplication.getInstance().showToastMessageFunction("Reservation history not found !!!");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                           /* CustomProgressBarDialog.progressDialog.dismiss();*/
                        }

                    }

                    public String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

                        Date parsed = null;
                        String outputDate = "";

                        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
                        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

                        try {
                            parsed = df_input.parse(inputDate);
                            outputDate = df_output.format(parsed);
                            Log.e("parse", " dateFormat" + outputDate);
                        } catch (ParseException e) {

                        }

                        return outputDate;

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onErrorResponse", "Error: " + error + "");


            }
        }){
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.e("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);


                return headers;
            }
        };
        int socketTimeout = 20000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        EApplication.getInstance().addToRequestQueue(req, "low");

    }


    private void recharge_card(String recharge_card_num) {

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(mcontext);
        CustomProgressBarDialog.progressDialog.show();
        if (recharge_card_num == "") {
            Toast.makeText(mcontext, getResources().getString(R.string.please_enter_credit_num), Toast.LENGTH_SHORT).show();
        } else {
            final HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", UserID);
            params.put("recharge_card_number", recharge_card_num);
            Log.e("Parameters", params.toString());

            String REquestUrl = Config.APP_BASE_URL + Config.RECHARGE_CARD;

            StringRequest req = new StringRequest(Request.Method.POST, REquestUrl,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String responseObject) {
                            String newResponse = responseObject;


                            if (responseObject.contains("Ã¯Â»Â¿")) {
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }


                            try {
                                JSONObject response = new JSONObject(newResponse);
                                boolean error = response.getBoolean("error");
                                if (!error) {
                                    apiCallForHistory();
                                    String message = response.getString("message");
                                    EApplication.getInstance().showToastMessageFunction(response.getString(message));


                                } else
                                    EApplication.getInstance().showToastMessageFunction("" + response.getString("message"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            CustomProgressBarDialog.progressDialog.dismiss();

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error", "Error: " + error.getMessage());
                    CustomProgressBarDialog.progressDialog.dismiss();

                }
            }) {
                public String language;

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(req, "high");
        }

    }


}

