package com.EddyCabLatest.application.moduleDriver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.moduleDriver.fragment.FragmentCredit;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.MaterialTabCredit;
import com.EddyCabLatest.application.views.MaterialTab_Credit_host;
import com.EddyCabLatest.application.views.Material_Credit_Listener;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Credit_Activity extends AppCompatActivity implements Material_Credit_Listener, FragmentCredit.OnHeadlineSelectedListener {

    MaterialTab_Credit_host tabHost;
    ViewPager pager;
    ViewPagerAdapter adapter;
    private String credit_value;
    private SessionManager session;
    private Toolbar app_toolBar;
    private TextView appTitle;
    private String event_credit_bonus;
    private TextView credit_bonus_details;
    private ArrayList<Object> created_date_Array;
    private String total_credit;
    private String total_bonus;
    private TextView credit_value_toolbar;
    private ChildEventListener eventlistener;
    private String UserId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        tabHost = (MaterialTab_Credit_host) this.findViewById(R.id.tabHost);
        pager = (ViewPager) this.findViewById(R.id.pager);
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        appTitle = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
        credit_bonus_details = (TextView) app_toolBar.findViewById(R.id.credit_bonus_details);
        credit_value_toolbar = (TextView) app_toolBar.findViewById(R.id.user_banana_credit);
        appTitle.setText(getResources().getString(R.string.credit_title));
        tabHost.setMinimumHeight(10);

        session = new SessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        UserId = userDetails.get(SessionManager.KEY_ID);
        // init view pager
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);

            }
        });

        // insert all tabs from pagerAdapter data
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(tabHost.newTab().setText(adapter.getPageTitle(i)).setTabListener((Material_Credit_Listener) this)
            );

        }
    }



    public void getjoblistfirebase() {

        eventlistener = EApplication.ref.child(UserId + "/new_reservations/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    if ((dataSnapshot.child("accepted").getValue().equals(false))&&(dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("new"))) {
                        Intent intent= new Intent(Credit_Activity.this,Driver_MainActivity.class);
                        startActivity(intent);


                            EApplication.getInstance().showNotification(Credit_Activity.this, Driver_MainActivity.class, 011, getResources().getString(R.string.new_job), getResources().getString(R.string.new_job), getResources().getString(R.string.new_job_avaiable));


                    }


                    Log.e("data_joblist", dataSnapshot + "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    if (dataSnapshot.getValue() != null) {

                        if (dataSnapshot.child("accepted").exists()) {
                            if (dataSnapshot.child("accepted").getValue() .equals(true)) {
                                //EApplication.ref.child(userID + "/new_reservations/" + dataSnapshot.getKey() + "/accepted/").removeValue();
                                EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                if (eventlistener != null) {
                                    EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                    if (eventlistener != null) {

                                        EApplication.ref.child(UserId + "/new_reservations/").removeEventListener(this);
                                    }
                                }
                                try {
                                    if (dataSnapshot.getValue() != null) {

                                        Intent intent_bid_succes = new Intent(Credit_Activity.this, Driver_ActivityBidSuccess.class);

                                        Map<String, String> usersMap_data = (Map<String, String>) dataSnapshot.getValue();
                                        JSONObject usersJSON = null;
                                        usersJSON = new JSONObject(usersMap_data);

                                        String selected_id = dataSnapshot.getKey();
                                        String user_fullname = dataSnapshot.child("user_fullname").getValue().toString();
                                        String distance_value = dataSnapshot.child("distance").getValue().toString();
                                        String user_pp = dataSnapshot.child("user_pp").getValue().toString();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("id", selected_id);

                                        bundle.putString("user_fullname", user_fullname);
                                        bundle.putString("user_pp", user_pp);
                                        bundle.putString("distance_value", distance_value);
                                        bundle.putString("user_data_for", usersJSON.toString());

                                        intent_bid_succes.putExtras(bundle);

startActivity(intent_bid_succes);
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            } else {
                                //jobListRequest1();

                            }

                        }

                    }



            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // jobListRequest1();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        getjoblistfirebase();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();
    }

    @Override
    public void onTabSelected(MaterialTabCredit tab) {
        pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTabCredit tab) {


    }

    @Override
    public void onTabUnselected(MaterialTabCredit tab) {

    }

    @Override
    public void onArticleSelected(String credit, String bonus) {

        total_credit = credit;
        total_bonus = bonus;


        if (total_credit.trim().equals("") && total_bonus.trim().equals(""))

        {
            int total = Integer.parseInt(total_credit.trim()) + Integer.parseInt(total_bonus.trim());
        }
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        public android.support.v4.app.Fragment getItem(int num) {

            if (num == 0) {
                //credit_bonus_details.setText("");
                return new FragmentCredit();
            } else if (num == 1) {
                // credit_bonus_details.setText(getResources().getString(R.string.bonus_details));
                return new FragmentBonus();
            } else

                return new FragmentCredit();

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            session = new SessionManager(Credit_Activity.this);
            if (position == 0) {
                return (getResources().getString(R.string.credit) + session.getUserBananaCreditOnly());
            } else if (position == 1) {
                return getResources().getString(R.string.bonus_credit) + session.getUserBananaBonus();
            } else {
                return "Section " + position;
            }
        }

    }


}






