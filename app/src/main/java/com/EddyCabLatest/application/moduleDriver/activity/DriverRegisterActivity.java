package com.EddyCabLatest.application.moduleDriver.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.gcmNotification.GCMClientManager;
import com.EddyCabLatest.application.moduleUser.activity.Activity_SmsVerification;
import com.EddyCabLatest.application.utilities.FileUtils;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;

public class DriverRegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = DriverRegisterActivity.class.getSimpleName();
    private static final String str_text_terms = "<a href=http://www.eddycab.com/terms-and-conditions/ >Terms and condition</a>";
    private static final String str_text_privacy = "<a href=http://www.eddycab.com/privacy-policy/ >Privacy policy</a>";

    EditText edt_driver_register_name;
    EditText edt_driver_register_email;
    EditText edt_driver_register_country_code;
    EditText edt_driver_register_mobile_number;
    EditText edt_driver_register_pass_code;
    EditText edt_driver_register_referral_code;
    EditText edt_profile_pic;
    EditText edt_driver_id;
    EditText edt_drivers_license;
    /*   EditText edt_bike_liscence;*/
    EditText edt_driver_license_no;
    CheckBox register_driver_checkbox;
    Button btn_driver_profile_pic;
    Button btn_driver_id;
    Button btn_drivers_license;
    Button btn_bike_liscence;
    Button btn_driver_register_sign_up;
    Toolbar app_toolBar;


    String edtNameValue;
    String edtEmailValue;
    String edtDriverLicenseValue;
    String edtPasswordValue;
    String edtReferralValue;
    String edtProfileValue;
    String edtDriverIdValue;
    /* String edtMotortLicenseValue;*/
    String edtCountryCodeValue;
    String edtMobileValue;
    String edtLicenseValue;


    private GCMClientManager pushClientManager;
    String PROJECT_NUMBER = Config.GCM_SENDER_KEY;
    String GCM_REG_DEVICE_ID;

    String profileImgPath = "";
    long totalSize = 0;
    SessionManager session;

    private boolean BROWSER_PROFILE_PIC = false;
    private boolean BROWSER_DRIVER_PIC_LICENCE = false;
    private boolean BROWSER_TAXI_BLUE_BOOK = false;
    private boolean BROWSER_MOTOR_LICENSE_PIC = false;
    private boolean visible = false;

    private String PROMO_OFFER;
    private TextView user_register_terms_condition;
    private TextView privacy_policy;
    private EditText driving_licence;
    private EditText blue_book;
    private Button btn_driver_licence;
    private Button btn_drivers_blue_book;
    private String edt_driver_licence_image;
    private String edt_driver_profile_pic;
    private String edt_driver_blue_book;
    private EditText edt_driver_register_pass_code_confirm;
    private String edt_driver_register_pass_code_confirm_value;
    private TextView apptitle_toolbar_driver_help;
    private String edt_driver_register_referral_code_value;
    private String profileImgPath_selected;
    private Bitmap bm;
    private String iconsStoragePath;
    private String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_register);


        // Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);

        /* ToolBar SetUp */
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //app_toolBar.setTitle(getResources().getString(R.string.driver_register));
        apptitle_toolbar_driver_help = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
        apptitle_toolbar_driver_help.setText(getResources().getString(R.string.driver_registeration));
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);

        //taking reference of Views  from Layout
        edt_driver_register_name = (EditText) findViewById(R.id.edt_driver_register_name);
        edt_driver_register_email = (EditText) findViewById(R.id.edt_driver_register_email);
        edt_driver_register_country_code = (EditText) findViewById(R.id.edt_driver_register_country_code);
        edt_driver_register_mobile_number = (EditText) findViewById(R.id.edt_driver_register_mobile_number);
        edt_driver_register_pass_code = (EditText) findViewById(R.id.edt_driverr_register_pass_code);
        edt_driver_register_pass_code_confirm = (EditText) findViewById(R.id.edt_driverr_register_pass_code_confirm);
        edt_driver_register_referral_code = (EditText) findViewById(R.id.edt_driver_register_referral_code);
        edt_driver_license_no = (EditText) findViewById(R.id.edt_driver_license_no);

        edt_profile_pic = (EditText) findViewById(R.id.edt_profile_pic);
        driving_licence = (EditText) findViewById(R.id.edt_driver_licence_img);
        blue_book = (EditText) findViewById(R.id.edt_drivers_blue_book);
      /*  edt_bike_liscence = (EditText) findViewById(R.id.edt_bike_liscence);*/


        btn_driver_profile_pic = (Button) findViewById(R.id.btn_driver_profile_pic);
        btn_driver_licence = (Button) findViewById(R.id.btn_driver_licence);
        btn_drivers_blue_book = (Button) findViewById(R.id.btn_drivers_bluebook);
        btn_driver_register_sign_up = (Button) findViewById(R.id.btn_driver_register_sign_up);
      /*  btn_bike_liscence = (Button) findViewById(R.id.btn_bike_liscence);*/
        session = new SessionManager(DriverRegisterActivity.this);

        //eventListener section
        btn_driver_profile_pic.setOnClickListener(this);
        btn_driver_licence.setOnClickListener(this);
        btn_drivers_blue_book.setOnClickListener(this);
        btn_driver_register_sign_up.setOnClickListener(this);
        /*btn_bike_liscence.setOnClickListener(this);*/


        user_register_terms_condition = (TextView) findViewById(R.id.terms_condition);
        privacy_policy = (TextView) findViewById(R.id.privacy_policy);
        user_register_terms_condition.setOnClickListener(this);
      /*  privacy_policy.setOnClickListener(this);*/

   /*     user_register_sign_up.setOnClickListener(this);*/
        user_register_terms_condition.setMovementMethod(LinkMovementMethod.getInstance());
        privacy_policy.setMovementMethod(LinkMovementMethod.getInstance());

        user_register_terms_condition.setLinkTextColor(Color.BLUE);
        if ((session.getAppLanguage()).equalsIgnoreCase(Config.LANG_ENG)) {

            user_register_terms_condition.setText(Html.fromHtml(Config.TERMS_DRIVER_ENGLISH+">Terms and condition</a>"));
            privacy_policy.setText(Html.fromHtml(Config.PRIVACY_POLICY_DRIVER_ENGLISH+">Privacy Policy</a>"));
        }
        else
        {
            user_register_terms_condition.setText(Html.fromHtml(Config.TERMS_DRIVER_NEPALI+">सेवाशर्त र यसका प्रावधानहरु</a>"));



            privacy_policy.setText(Html.fromHtml(Config.PRIVACY_POLICY_DRIVER_NEPALI+">गोपनीयता नीति</a>"));

        }

        String current_language = session.getAppLanguage();
        Log.e("current_language", current_language);
        if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
            language = "en";
        }
        if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
            language = "np";
        }





        privacy_policy.setLinkTextColor(Color.BLUE);

    }

    @Override
    public void onResume() {

        super.onResume();

        visible = true;

    }

    @Override
    public void onDestroy() {
        visible = false;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        visible = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;
        super.onStop();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                Intent intent = new Intent(DriverRegisterActivity.this, DriverLoginScreenActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_driver_profile_pic:
                showImageChooserAlertDialog();
                BROWSER_PROFILE_PIC = true;
                BROWSER_DRIVER_PIC_LICENCE = false;
                BROWSER_TAXI_BLUE_BOOK = false;

                break;
            case R.id.btn_driver_licence:
                showImageChooserAlertDialog();
                BROWSER_PROFILE_PIC = false;
                BROWSER_DRIVER_PIC_LICENCE = true;
                BROWSER_TAXI_BLUE_BOOK = false;

                break;
            case R.id.btn_drivers_bluebook:
                showImageChooserAlertDialog();
                BROWSER_PROFILE_PIC = false;
                BROWSER_DRIVER_PIC_LICENCE = false;
                BROWSER_TAXI_BLUE_BOOK = true;

                break;


            case R.id.btn_driver_register_sign_up:
                getValuesFromEditText();
                break;

        }
    }


    private void getValuesFromEditText() {

        edtNameValue = edt_driver_register_name.getText().toString().trim();
        edtEmailValue = edt_driver_register_email.getText().toString().trim();
        edtCountryCodeValue = edt_driver_register_country_code.getText().toString().trim();
        edtPasswordValue = edt_driver_register_pass_code.getText().toString().trim();
        edt_driver_register_pass_code_confirm_value = edt_driver_register_pass_code_confirm.getText().toString().trim();
        edtMobileValue = edt_driver_register_mobile_number.getText().toString().trim();
        edtLicenseValue = edt_driver_license_no.getText().toString().trim();
        edt_driver_profile_pic = edt_profile_pic.getText().toString().trim();
        Log.e("profilepath", edt_driver_profile_pic);
        edt_driver_licence_image = driving_licence.getText().toString().trim();
        edt_driver_blue_book = blue_book.getText().toString().trim();

        edt_driver_register_referral_code_value = edt_driver_register_referral_code.getText().toString().trim();

        if (!blue_book.getText().toString().trim().equals("")
                && !edt_driver_register_name.getText().toString().trim().equals("")

                && !edt_driver_register_mobile_number.getText().toString().trim().equals("")
                && !edt_driver_register_pass_code.getText().toString().trim().equals("")
                && !edt_driver_license_no.getText().toString().trim().equals("")
                && !driving_licence.getText().toString().trim().equals("")
                && !edt_profile_pic.getText().toString().trim().equals("")
                && !edt_driver_register_pass_code_confirm.getText().toString().trim().equals("")
                && !edt_driver_register_referral_code.getText().toString().trim().equals("")
                ) {
            if (edt_driver_register_pass_code_confirm_value.equals(edtPasswordValue)) {
                if (edtMobileValue.length() < 10) {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.enter_the_10_digit_mobile));
                    edt_driver_register_mobile_number.setError(getResources().getString(R.string.enter_the_10_digit_mobile));
                }
                else
                {
                    methodRegisterDriver();
                }

            } else {
                edt_driver_register_pass_code_confirm.setError(getResources().getString(R.string.password_donot_match));

            }

        } else {
            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_enter_all_fields));

            if (edt_driver_profile_pic.equals("")) {
                edt_profile_pic.setError(getResources().getString(R.string.add_your_profile));
            }
            if (edtNameValue.equals("")) {
                edt_driver_register_name.setError(getResources().getString(R.string.enterfullname));
            }

            if (edtPasswordValue.equals("")) {
                edt_driver_register_pass_code.setError(getResources().getString(R.string.passcode_required));
            }
            if (edtMobileValue.equals("")) {
                edt_driver_register_mobile_number.setError(getResources().getString(R.string.enter_mobile_no));
            }
            if (edt_driver_licence_image.equals("")) {
                driving_licence.setError(getResources().getString(R.string.driver_blue_book));
            }
            if (edt_driver_blue_book.equals("")) {
                blue_book.setError(getResources().getString(R.string.enter_license_no));
            }
            if (edt_driver_register_pass_code_confirm_value.equals("")) {
                edt_driver_register_pass_code_confirm.setError(getResources().getString(R.string.confirm_pass));
            }

            if (edtLicenseValue.equals("")) {
                edt_driver_license_no.setError(getResources().getString(R.string.license_no));
            }

            if (edt_driver_register_referral_code_value.equals("")) {
                edt_driver_register_referral_code.setError(getResources().getString(R.string.vehicle_no));
            }
        }
    }

    private void methodRegisterDriver() {
        if (NetworkUtils.isNetworkAvailable(DriverRegisterActivity.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(DriverRegisterActivity.this);


            CustomProgressBarDialog.progressDialog.show();

          /* Get GCM Device Registration ID */
            pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
            pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                @Override
                public void onSuccess(String registrationId, boolean isNewRegistration) {
                    CustomProgressBarDialog.progressDialog.dismiss();
                    GCM_REG_DEVICE_ID = registrationId;
                    Log.e("===", "gcm id is :" + GCM_REG_DEVICE_ID);
                    if (NetworkUtils.isNetworkAvailable(DriverRegisterActivity.this))
                        UploadFileToServer();
                    else
                        NetworkUtils.showNoConnectionDialog(DriverRegisterActivity.this);
                }

                @Override
                public void onFailure(String ex) {

                    CustomProgressBarDialog.progressDialog.dismiss();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                        }
                    });
                    super.onFailure(ex);


                }
            });

        } else {
            NetworkUtils.showNoConnectionDialog(DriverRegisterActivity.this);
        }

    }


    /* RegisterDriver Method */

    // Show Alert Dialog chooser
    public void showImageChooserAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage("Choose picture method")
                .setCancelable(true)
                .setPositiveButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, 103);

                            }
                        })
                .setNegativeButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, 101);


                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        if (visible) alertDialog.show();
    }

    // For Activity Result
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {

        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        String fileName = "";
        Uri selectedImage = null;
        String iconsStoragePath = "";
        File sdIconStorageDir = null;


        if (BROWSER_PROFILE_PIC) {
            edt_profile_pic.setText(profileImgPath);

            fileName = "profilePic.png";
        } else if (BROWSER_DRIVER_PIC_LICENCE) {
            driving_licence.setText(profileImgPath);
            fileName = "drivingLicence.png";

        } else if (BROWSER_TAXI_BLUE_BOOK) {
            fileName = "BlueBook.png";
            blue_book.setText(profileImgPath);

        }

        switch (requestCode) {
            case 101:
                if (resultCode == RESULT_OK) {

                    selectedImage = imageReturnedIntent.getData();


                    try {
                        profileImgPath_selected = FileUtils.getPath(this, selectedImage);

                        if (profileImgPath_selected != null) {
                            if (true) {
                                rotateImage(profileImgPath_selected, fileName);
                            } else {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                                iconsStoragePath = Environment.getExternalStorageDirectory() + "/img/";

                                sdIconStorageDir = new File(iconsStoragePath);

                                sdIconStorageDir.mkdir();
                                File file = new File(iconsStoragePath, fileName);
                                if (file.exists()) file.delete();
                                profileImgPath = iconsStoragePath + fileName;
                                File sFile = new File(profileImgPath_selected);
                                File dFile = new File(profileImgPath);
                                // Copy File one place to another
                                try {
                                    FileUtils.copyFile(sFile, dFile);
                                    //profileImgPath = iconsStoragePath;
                                } catch (Exception e) {

                                    Log.e("Image Directory  error", e + "");


                                }

                            }

                        } else {

                            Toast.makeText(DriverRegisterActivity.this, getResources().getString(R.string.profile_is_empty), Toast.LENGTH_SHORT).show();
                        }


                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                break;
            case 103:
                try {

                    bm = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    if (bm != null) {    // profile_Image.setImageBitmap(bm);

                        boolean status = storeImage(bm, fileName, false);
                        iconsStoragePath = Environment.getExternalStorageDirectory() + "/img/";

                        sdIconStorageDir = new File(iconsStoragePath);

                        // create storage directories, if they don't exist
                        sdIconStorageDir.mkdirs();


                   /* String iconsStoragePath = Environment
                            .getExternalStorageDirectory()
                            + "/android/data/" + DriverRegisterActivity.this.getPackageName() + "/imgprofileImage.png";*/
// + "profileImage.png";
                        profileImgPath = iconsStoragePath + fileName;

                    } else {

                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.pathnotfound));
                    }

                } catch (Exception e) {

                }

                break;

        }
        if (profileImgPath != null) {

            if (BROWSER_DRIVER_PIC_LICENCE)
                driving_licence.setText(profileImgPath);
            if (BROWSER_PROFILE_PIC)
                edt_profile_pic.setText(profileImgPath);
            if (BROWSER_TAXI_BLUE_BOOK)
                blue_book.setText(profileImgPath);

        } else {

            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
        }



        /*    edt_bike_liscence.setText(profileImgPath);*/


        Log.e("Image Directory :: ", profileImgPath);

    }


    private void rotateImage(String imagePath, String filename) {
        try {
            int rotate = 0;
            String imgPath = imagePath;

            ExifInterface exif = new ExifInterface(imgPath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
            Log.i("orientation", "Exif orientation: " + orientation);

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = false;
            opts.inPreferredConfig = Bitmap.Config.RGB_565;
            opts.inDither = true;


            try {


                Bitmap initialBitmap = BitmapFactory.decodeFile(imgPath, opts);
                Matrix matrix = new Matrix();
                matrix.postRotate(rotate);
                Bitmap bm = Bitmap.createBitmap(initialBitmap, 0, 0,
                        initialBitmap.getWidth(), initialBitmap.getHeight(), matrix, true);

                storeImage(bm, filename, true);
            } catch (OutOfMemoryError e) {
                Toast.makeText(DriverRegisterActivity.this, "Out of memory! Try uploading small size photo.", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Can not upload image", Toast.LENGTH_SHORT).show();
        }

    }


    private boolean storeImage(Bitmap imageData, String filename, boolean compress) {
        // get path to external storage (SD card)
        String folder = Environment.getExternalStorageDirectory() + "/img/";
        File folder_new_img = new File(folder);
        folder_new_img.mkdir();
        String iconsStoragePath = folder + filename;
        try {

            File file = new File(folder, filename);
            if (file.exists()) file.delete();

            FileOutputStream fileOutputStream = new FileOutputStream(iconsStoragePath);
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            // choose another format if PNG doesn't suit you
            if (compress) {
                imageData.compress(Bitmap.CompressFormat.JPEG, 30, bos);
            } else {

                imageData.compress(Bitmap.CompressFormat.JPEG, 30, bos);
            }

            bos.flush();
            bos.close();
            profileImgPath = iconsStoragePath;
            //  profileImgPath = iconsStoragePath + filename;
        } catch (FileNotFoundException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        }

        return true;
    }


    /**
     * Uploading the file to server
     */
    private void UploadFileToServer() {

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(DriverRegisterActivity.this);
        CustomProgressBarDialog.progressDialog.setCancelable(false);
        if (visible) CustomProgressBarDialog.progressDialog.show();


         /*   HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.APP_BASE_URL + Config.USER_REGISTER_URL);*/

        try {
            Ion.with(DriverRegisterActivity.this)
                    .load("POST", Config.APP_BASE_URL + Config.USER_REGISTER_URL)
                    .setTimeout(60 * 4 * 1000).setHeader("language",language)
                    .setMultipartParameter("fullname", edtNameValue)
                    .setMultipartParameter("email", edtEmailValue)
                    .setMultipartParameter("country_code", edtCountryCodeValue)
                    .setMultipartParameter("mobile_number", edtMobileValue)
                    .setMultipartParameter("license_no", edtLicenseValue)
                    .setMultipartParameter("password", edtPasswordValue)
                    .setMultipartParameter("type", Config.Driver_TYPE_USER_ID)
                    .setMultipartParameter("device_id", GCM_REG_DEVICE_ID)
                    .setMultipartParameter("device_type", Config.DEVICE_TYPE)
                    .setMultipartParameter("email_promo_offer", PROMO_OFFER)
                    .setMultipartFile("profile_picture", "application/zip", new File(edt_driver_profile_pic))
                    .setMultipartFile("blue_book_image", "application/zip", new File(edt_driver_blue_book))
                    .setMultipartFile("driving_licence_image", "application/zip", new File(edt_driver_licence_image))
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            CustomProgressBarDialog.progressDialog.dismiss();
                            Log.e("registration", e + result);

                            if ((e != null) && (result == null)) {
                                EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                            } else {

                                try {
                                    JSONObject resultObject = new JSONObject(result);
                                    // showing the server response in an alert dialog
                                    boolean error = resultObject.getBoolean("error");
                                    if (!error) {
                                        String driverPhone = resultObject.getJSONObject("user").getString("mobile_number");
                                        String driverid = resultObject.getJSONObject("user").getString("id");
                                        String acces_token = resultObject.getJSONObject("user").getString("access_token");
                                        Intent intent = new Intent(DriverRegisterActivity.this, Activity_SmsVerification.class);
                                        intent.putExtra("phoneNo", driverPhone);
                                        intent.putExtra("user_id", driverid);
                                        intent.putExtra("user_detail", result);
                                        intent.putExtra("acces_token", acces_token);
                                        startActivity(intent);
                                        finish();
                                    } else {

                                        EApplication.getInstance().showToastMessageFunction(resultObject.getString("message"));
                                    }
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            CustomProgressBarDialog.progressDialog.dismiss();
        }


/*
                MultiPartEntity entity = new MultiPartEntity(new MultiPartEntity.ProgressListener() {

                    @Override
                    public void transferred(long num) {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                    }
                });


                File sourceFile_DRIVER_ID = new File(edtDriverIdValue);
                File sourceFile_DRIVER_TAXI_LICENSE = new File(edtDriverLicenseValue);
              */
/*  File sourceFile_DRIVER_MOTOR_LICENSE = new File(edtMotortLicenseValue);*//*

                File sourceFile_DRIVER_PROFILE_PIC = new File(edtProfileValue);
                entity.addPart("blue_book_image", new FileBody(sourceFile_DRIVER_ID));
                // Adding file data to http body
           */
/*     entity.addPart("drivers_id", new FileBody(sourceFile_DRIVER_ID));*//*

                entity.addPart("profile_picture", new FileBody(sourceFile_DRIVER_TAXI_LICENSE));
              */
/*  entity.addPart("motorbike_license", new FileBody(sourceFile_DRIVER_MOTOR_LICENSE));
                entity.addPart("drivers_taxi_license", new FileBody(sourceFile_DRIVER_TAXI_LICENSE));*//*


                entity.addPart("driving_licence_image", new FileBody(sourceFile_DRIVER_TAXI_LICENSE));
                // Extra parameters if you want to pass to server
                entity.addPart("fullname", new StringBody(edtNameValue));
                entity.addPart("email", new StringBody(edtEmailValue));
                entity.addPart("country_code", new StringBody(edtCountryCodeValue));
                entity.addPart("mobile_number", new StringBody(edtMobileValue));
                entity.addPart("license_no", new StringBody(edtLicenseValue));
                entity.addPart("password", new StringBody(edtPasswordValue));
             */
/*   entity.addPart("driver_type", new StringBody("bike"));//tuktuk*//*


               */
/* if (!TextUtils.isEmpty(edtReferralValue)) {
                    entity.addPart("referral_code", new StringBody(edtReferralValue));
                }*//*

                entity.addPart("type", new StringBody(Config.Driver_TYPE_USER_ID));
                entity.addPart("device_id", new StringBody(GCM_REG_DEVICE_ID));
                entity.addPart("device_type", new StringBody(Config.DEVICE_TYPE));
                entity.addPart("email_promo_offer", new StringBody(PROMO_OFFER));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);

                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                    //ERROR
                }
*/


    }

       /* @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);
            try {
                JSONObject resultObject = new JSONObject(result);
                // showing the server response in an alert dialog

                boolean error = resultObject.getBoolean("error");
                if (!error) {
                    String driverPhone = resultObject.getJSONObject("user").getString("mobile_number");
                    String driverid = resultObject.getJSONObject("user").getString("id");
                    Intent intent = new Intent(DriverRegisterActivity.this, Activity_SmsVerification.class);
                    intent.putExtra("phoneNo", driverPhone);
                    intent.putExtra("user_id", driverid);
                    intent.putExtra("user_detail", result);
                    startActivity(intent);
                    finish();
                } else {
                    EApplication.getInstance().showToastMessageFunction(resultObject.getString("message"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            CustomProgressBarDialog.progressDialog.dismiss();
            super.onPostExecute(result);
        }*/
}

 /*   @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(DriverRegisterActivity.this, Driver_Activity_Login.class);
        startActivity(intent);
    }
*/


