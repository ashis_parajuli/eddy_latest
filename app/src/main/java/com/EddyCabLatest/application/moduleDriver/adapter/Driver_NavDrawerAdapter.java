package com.EddyCabLatest.application.moduleDriver.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.moduleDriver.modal.NavDrawerItem_Driver;

import java.util.ArrayList;

/**
 * Created by User on 6/30/2015.
 */
public class Driver_NavDrawerAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<NavDrawerItem_Driver> navDrawerItems;

    public Driver_NavDrawerAdapter(Context context, ArrayList<NavDrawerItem_Driver> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.driver_drawer_list_item, null);

        }
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(navDrawerItems.get(position).getTitle()+" ");
        return convertView;
    }
}
