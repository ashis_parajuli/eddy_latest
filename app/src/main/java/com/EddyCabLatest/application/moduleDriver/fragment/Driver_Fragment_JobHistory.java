package com.EddyCabLatest.application.moduleDriver.fragment;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_ActivityBidSuccess;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_ActivityTripHistry;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.moduleDriver.adapter.Adapter_DriverBookingHistory;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class Driver_Fragment_JobHistory extends Fragment implements AdapterView.OnItemClickListener, StickyListHeadersListView.OnStickyHeaderChangedListener,
        StickyListHeadersListView.OnStickyHeaderOffsetChangedListener {

    private ListView driverHistoryListView;
    private TextView emptyListMsg;
    private SessionManager session;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    private String address;
    private String full_name;
    private String user_id;
    public Matcher matcher;
    private String UserID;
    private int loadNoOfJob = 20;
    private View footerView;
    private Context mcontext;
    private String language;
    private ChildEventListener eventlistener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.driver_layout_jobhistory, container, false);
        mcontext = container.getContext();
        session = new SessionManager(getActivity());


        driverHistoryListView = (ListView) rootView.findViewById(R.id.driverHistoryListView);
        emptyListMsg = (TextView) rootView.findViewById(R.id.empty_job);

        if (NetworkUtils.isNetworkAvailable(mcontext)) {

            HashMap<String, String> userDetails = session.getUserDetails();
            UserID = userDetails.get(SessionManager.KEY_ID);

            Log.e("user_id", UserID);



            //For See More View
            //If all data is loded then see more should not be displated
            footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.user_job_history_footer, null, false);
            driverHistoryListView.addFooterView(footerView);
            footerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadNoOfJob += 20;
                    loadForHistory(loadNoOfJob);
                }
            });

        } else
            NetworkUtils.showNoConnectionDialog(getActivity());

        driverHistoryListView.setOnItemClickListener(this);


        driverHistoryListView.setFastScrollEnabled(true);
        driverHistoryListView.setFastScrollAlwaysVisible(false);


        driverHistoryListView.setEmptyView(emptyListMsg);
        return rootView;
    }

    @Override
    public void onResume() {

        super.onResume();
        getjoblistfirebase();
    }

    @Override
    public void onDestroy() {

        if(eventlistener!=null)
        {

            EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(eventlistener);
        }
        super.onDestroy();
    }


    public void getjoblistfirebase() {

        eventlistener = EApplication.ref.child(user_id + "/new_reservations/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    if ((dataSnapshot.child("accepted").getValue() .equals( false))&&(dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("new"))) {
                        Intent intent= new Intent(mcontext,Driver_MainActivity.class);
                        startActivity(intent);


                        EApplication.getInstance().showNotification(mcontext, Driver_MainActivity.class, 011, getResources().getString(R.string.new_job), getResources().getString(R.string.new_job), getResources().getString(R.string.new_job_avaiable));


                    }
                    else
                    {

                        loadForHistory(loadNoOfJob);
                    }


                    Log.e("data_joblist", dataSnapshot + "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.getValue() != null) {

                    if (dataSnapshot.child("accepted").exists()) {
                        if (dataSnapshot.child("accepted").getValue() .equals(true)) {
                            //EApplication.ref.child(userID + "/new_reservations/" + dataSnapshot.getKey() + "/accepted/").removeValue();
                            EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                            if (eventlistener != null) {
                                EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                if (eventlistener != null) {

                                    EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                }
                            }
                            try {
                                if (dataSnapshot.getValue() != null) {

                                    Intent intent_bid_succes = new Intent(mcontext, Driver_ActivityBidSuccess.class);

                                    Map<String, String> usersMap_data = (Map<String, String>) dataSnapshot.getValue();
                                    JSONObject usersJSON = null;
                                    usersJSON = new JSONObject(usersMap_data);

                                    String selected_id = dataSnapshot.getKey();
                                    String user_fullname = dataSnapshot.child("user_fullname").getValue().toString();
                                    String distance_value = dataSnapshot.child("distance").getValue().toString();
                                    String user_pp = dataSnapshot.child("user_pp").getValue().toString();

                                    Bundle bundle = new Bundle();
                                    bundle.putString("id", selected_id);

                                    bundle.putString("user_fullname", user_fullname);
                                    bundle.putString("user_pp", user_pp);
                                    bundle.putString("distance_value", distance_value);
                                    bundle.putString("user_data_for", usersJSON.toString());

                                    intent_bid_succes.putExtras(bundle);

                                    startActivity(intent_bid_succes);
                                }
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        } else {
                            //jobListRequest1();

                        }

                    }

                }



            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // jobListRequest1();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }
    private void loadForHistory(int loadNoOfJob) {

        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
        CustomProgressBarDialog.progressDialog.show();

        HashMap<String, String> userDetails = session.getUserDetails();
        String UserID = userDetails.get(SessionManager.KEY_ID);


        String url = Config.APP_BASE_URL + Config.Driver_BOOKING_HISTORY_URL + UserID + "/" + loadNoOfJob;
        Log.e("URL", url);
        CustomRequest req = new CustomRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {


                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response API", response.toString());

                        try {
                            boolean error = response.getBoolean("error");

                            if (!error) {


                                JSONObject responseData = response.getJSONObject("bookings");
                                Log.e("Response", "Driver JobHistory Response" + responseData.toString());


                                Iterator iterator = responseData.keys();
                                while (iterator.hasNext()) {
                                    String keyValue = (String) iterator.next();
                                    //  Log.e("KEY ::", keyValue);

                                    JSONArray objectArray = response.getJSONObject("bookings").getJSONArray(keyValue);

                                    if (objectArray.length() < 22) {
                                        footerView.setVisibility(View.INVISIBLE);
                                    } else {
                                        footerView.setVisibility(View.VISIBLE);

                                    }
                                    //  Log.e("ARRAY ::", objectArray.toString());
                                    for (int i = 0; i < objectArray.length(); i++) {


                                        HashMap<String, String> mapList = new HashMap<>();

                                        JSONObject rootObject = objectArray.getJSONObject(i);

                                        String datetime = rootObject.getString("datetime");
                                        String fromLocationTitle = rootObject.getJSONObject("pickup_location").getString("title");
                                        String fromLocationAddress = rootObject.getJSONObject("pickup_location").getString("address");
                                        String toLocationTitle = rootObject.getJSONObject("dropoff_location").getString("title");
                                        String toLocationAddress = rootObject.getJSONObject("dropoff_location").getString("address");
                                        String statusTrip = rootObject.getString("status");
                                        String amountTrip = rootObject.getString("ride_fare");
                                        String month_notation = rootObject.getString("month_notation");
                                        String monthname = rootObject.getString("monthname");
                                        String pickup_lat = rootObject.getJSONObject("pickup_location").getString("latitude");
                                        String pickup_long = rootObject.getJSONObject("pickup_location").getString("longitude");
                                        String dropoff_lat = rootObject.getJSONObject("dropoff_location").getString("latitude");
                                        String dropoff_long = rootObject.getJSONObject("dropoff_location").getString("longitude");
                                        address = rootObject.getJSONObject("dropoff_location").getString("address");
                                        JSONObject user_ppObject = rootObject.getJSONObject("user");
                                        String user_pp = user_ppObject.getString("user_pp");
                                        full_name = user_ppObject.getString("fullname");
                                        user_id = user_ppObject.getString("id");


                                        mapList.put(DBTableFields.USER_TRIP_LIST_HEADER, keyValue);
                                        mapList.put(DBTableFields.USER_TRIP_DATE_TIME, datetime);
                                        mapList.put(DBTableFields.USER_TRIP_FROM_LOCATION, fromLocationTitle + ", " + fromLocationAddress);
                                        mapList.put(DBTableFields.USER_TRIP_TO_LOCATION, toLocationTitle + ", " + toLocationAddress);
                                        mapList.put(DBTableFields.USER_TRIP_STATUS, statusTrip);
                                        mapList.put(DBTableFields.USER_TRIP_AMOUNT, amountTrip);
                                        mapList.put(DBTableFields.USER_TRIP_USER_PP, user_pp);
                                        mapList.put(DBTableFields.USER_TRIP_PICKUP_LAT, pickup_lat);
                                        mapList.put(DBTableFields.USER_TRIP_PICKUP_LONG, pickup_long);
                                        mapList.put(DBTableFields.USER_TRIP_DROPOFF_LAT, dropoff_lat);
                                        mapList.put(DBTableFields.USER_TRIP_DROPOFF_LONG, dropoff_long);
                                        mapList.put(DBTableFields.USER_TRIP_DROFF_NAME, full_name);
                                        mapList.put(DBTableFields.USER_TRIP_REFERAL_CODE, user_id);
                                        mapList.put(DBTableFields.USER_TRIP_MONTH, monthname);
                                        dataList.add(mapList);
                                        //Log.i("Data ::", dataList.toString());

                                    }
                                }

                                Adapter_DriverBookingHistory adapter = new Adapter_DriverBookingHistory(mcontext, dataList);
                                driverHistoryListView.setAdapter(adapter);

                                driverHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        Intent intent = new Intent(getActivity(), Driver_ActivityTripHistry.class);
                                        intent.putExtra("trip_time", dataList.get(position).get(DBTableFields.USER_TRIP_DATE_TIME));
                                        intent.putExtra("ride_fare", dataList.get(position).get(DBTableFields.USER_TRIP_AMOUNT));
                                        intent.putExtra("from", dataList.get(position).get(DBTableFields.USER_TRIP_FROM_LOCATION));
                                        intent.putExtra("to", dataList.get(position).get(DBTableFields.USER_TRIP_TO_LOCATION));
                                        intent.putExtra("status", dataList.get(position).get(DBTableFields.USER_TRIP_STATUS));
                                        intent.putExtra("user_profile", dataList.get(position).get(DBTableFields.USER_TRIP_USER_PP));
                                        intent.putExtra("pick_latitude", dataList.get(position).get(DBTableFields.USER_TRIP_PICKUP_LAT));
                                        intent.putExtra("pick_longitude", dataList.get(position).get(DBTableFields.USER_TRIP_PICKUP_LONG));
                                        intent.putExtra("drop_latitude", dataList.get(position).get(DBTableFields.USER_TRIP_DROPOFF_LAT));
                                        intent.putExtra("drop_longitude", dataList.get(position).get(DBTableFields.USER_TRIP_DROPOFF_LONG));
                                        intent.putExtra("user_id", user_id);
                                        intent.putExtra("reservation_id", user_id);
                                        intent.putExtra("address", address);
                                        intent.putExtra("full_name", full_name);
                                        startActivity(intent);
                                    }
                                });
                            } else
                                EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_history_item));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        CustomProgressBarDialog.progressDialog.dismiss();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());

                CustomProgressBarDialog.progressDialog.dismiss();
            }
        }) {
            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.e("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);


                return headers;
            }
        };
        int socketTimeout = 20000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        EApplication.getInstance().addToRequestQueue(req, "high");
    }


    private void apiCallForHistory(int loadNoOfJob) {


    }


    @Override
    public void onStickyHeaderChanged(StickyListHeadersListView stickyListHeadersListView, View view, int i, long l) {

    }

    @Override
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView stickyListHeadersListView, View view, int i) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
