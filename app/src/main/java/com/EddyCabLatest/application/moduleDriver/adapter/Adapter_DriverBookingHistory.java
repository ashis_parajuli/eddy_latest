package com.EddyCabLatest.application.moduleDriver.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.DBTableFields;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class Adapter_DriverBookingHistory extends BaseAdapter implements StickyListHeadersAdapter, SectionIndexer {
    private final ArrayList<HashMap<String, String>> mList;
    private Context mContext;
    private int[] mSectionIndices;
    private Character[] mSectionLetters;
    private LayoutInflater mInflater;
    private Date dateObj;

    public Adapter_DriverBookingHistory(Context context, ArrayList<HashMap<String, String>> list) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mList = list;
        if (mList.size() > 0) {
            mSectionIndices = getSectionIndices();
            mSectionLetters = getSectionLetters();
        }
    }


    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<>();
        String lastFirstChar = mList.get(0).get(DBTableFields.USER_TRIP_LIST_HEADER);
        sectionIndices.add(0);
        for (int i = 1; i < mList.size(); i++) {
            if (mList.get(i).get(DBTableFields.USER_TRIP_LIST_HEADER) != lastFirstChar) {
                lastFirstChar = mList.get(i).get(DBTableFields.USER_TRIP_LIST_HEADER);
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }


    private Character[] getSectionLetters() {
        Character[] letters = new Character[mSectionIndices.length];
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = mList.get(mSectionIndices[i]).get(DBTableFields.USER_TRIP_LIST_HEADER).charAt(0);
        }
        return letters;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.previous_job_single_item, parent, false);
            holder.tripSourceAddress = (TextView) convertView.findViewById(R.id.tripFromLocation);
            holder.tripDestinationAddress = (TextView) convertView.findViewById(R.id.tripToLocation);
            holder.date = (TextView) convertView.findViewById(R.id.month);
            holder.time = (TextView) convertView.findViewById(R.id.time_joblist);
            holder.tripStatus = (TextView) convertView.findViewById(R.id.tripStatus);
           /* holder.tripAmount = (TextView) convertView.findViewById(R.id.tripAmount);*/

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String dateTime = mList.get(position).get(DBTableFields.USER_TRIP_DATE_TIME);
        String time = formateDateFromstring("yyyy-MM-dd HH:mm", "H:mm a", dateTime);
        String date = formateDateFromstring("yyyy-MM-dd HH:mm", "dd MMM", dateTime);
        holder.time.setText(time);
        holder.date.setText(date);
        holder.tripSourceAddress.setText(mList.get(position).get(DBTableFields.USER_TRIP_FROM_LOCATION));
        holder.tripDestinationAddress.setText(mList.get(position).get(DBTableFields.USER_TRIP_TO_LOCATION));

        holder.tripStatus.setVisibility(View.GONE);

        return convertView;
    }


    public String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
            Log.e("parse", " dateFormat" + outputDate);
        } catch (ParseException e) {

        }

        return outputDate;

    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;


        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        // String headerText = "" + mList.get(position).get(DBTableFields.USER_TRIP_LIST_HEADER).subSequence(0, 1).charAt(0);
        holder.text.setText(mList.get(position).get(DBTableFields.USER_TRIP_LIST_HEADER));

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the name as ID because this is what headers are based upon
        return mList.get(position).get(DBTableFields.USER_TRIP_LIST_HEADER).subSequence(0, 1).charAt(0);
    }

    @Override
    public int getPositionForSection(int section) {
        if (section >= mSectionIndices.length) {
            section = mSectionIndices.length - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices[section];
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }

    @Override
    public Object[] getSections() {
        return mSectionLetters;
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView tripSourceAddress;
        TextView tripDestinationAddress;
        TextView date;
        TextView time;
        TextView tripStatus;
        TextView tripAmount;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


}
