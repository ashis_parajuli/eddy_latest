package com.EddyCabLatest.application.moduleDriver.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleDriver.adapter.Driver_NavDrawerAdapter;
import com.EddyCabLatest.application.moduleDriver.fragment.Driver_FragmentMessages;
import com.EddyCabLatest.application.moduleDriver.fragment.Driver_FragmentSetTime;
import com.EddyCabLatest.application.moduleDriver.fragment.Driver_Fragment_JobHistory;
import com.EddyCabLatest.application.moduleDriver.fragment.Driver_RateUser;
import com.EddyCabLatest.application.moduleDriver.fragment.FragmentJobList;
import com.EddyCabLatest.application.moduleDriver.modal.NavDrawerItem_Driver;
import com.EddyCabLatest.application.moduleUser.fragment.Driver_about_Fragment;
import com.EddyCabLatest.application.moduleUser.fragment.Fragment_DriverNewHelp;
import com.EddyCabLatest.application.utilities.CustomRatingBar;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.NewGPSTracker;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CircularImageView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class Driver_MainActivity extends AppCompatActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, FragmentJobList.passCredittomain, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    String TAG = "===";
    public String language;

    EApplication app;
    SessionManager session;

    NewGPSTracker gps_tracker;
    static Geocoder geocoder;
    static List<Address> addresses;
    static String LocationName, LocationAddress;

    static Location OLD_LOCATION;
    static String user_id;
    TextView txt_login_driver;
    Button btn_create_account;
    Toolbar app_toolBar;
    public static TextView appTitle;
    TextView userBananaCredit;
    RelativeLayout rl_clear;
    Button btn_about;
    Button btn_help;
    Button btn_tour_logout;
    Button btn_help_logout;
    CustomRatingBar txt_ratingCount;
    Driver_NavDrawerAdapter mAdapter;
    DrawerLayout mDrawerLayout;
    FrameLayout content_frame;
    ActionBarDrawerToggle mDrawerToggle;
    CharSequence mDrawerTitle;
    LinearLayout layout_Logout_State;
    ListView listview;

    //For checkResponse
    JSONObject returnData;
    int i = 0;
    Handler handler = null;
    Runnable runable;


    ArrayList<NavDrawerItem_Driver> mNavItems = new ArrayList<>();

    private int quiteTimeIndex = 0;
    private String quiteTimeUpdateField = "";

    private static int mInterval = 20000;
    public static Handler mHandler;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

    public static Context context;
    private boolean visible = false;
    private TextView rating_text;
    private NotificationCompat.Builder mBuilder;
    private int mNotificationId = 001;
    private String credit;
    private ArrayList<String> titles_nav = new ArrayList<>();
    private String bonus;
    private String acces_token;
    private int server_total_credit;
    private int server_total_bonus;
    private TextView credit_value_toolbar;
    private PowerManager.WakeLock mWakeLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = EApplication.getInstance();
        session = new SessionManager(Driver_MainActivity.this);
        geocoder = new Geocoder(Driver_MainActivity.this, Locale.getDefault());


        context = Driver_MainActivity.this;

        if (session.getAppLanguage().equals("Thai"))
            app.setLang("np");
        else app.setLang("en");

        setContentView(R.layout.activity_driver_main);

//Driver ID
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);


        /* ToolBar SetUp */
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mHandler = new Handler();


        appTitle = (TextView) app_toolBar.findViewById(R.id.applicationTitle);

        credit_value_toolbar = (TextView) app_toolBar.findViewById(R.id.user_banana_credit);
        //reference for login layout inside Adapter
        //getcreditandbonus();
        //credit_value_toolbar.setText(session.getUserBananaCredit());
        layout_Logout_State = (LinearLayout) findViewById(R.id.layout_Logout_State);
        btn_create_account = (Button) findViewById(R.id.create_account);
        txt_login_driver = (TextView) findViewById(R.id.login_user);
        btn_tour_logout = (Button) findViewById(R.id.btn_tour_logout);
        btn_help_logout = (Button) findViewById(R.id.btn_help_logout);

        /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);


        titles_nav.add(getResources().getString(R.string.new_job));
        titles_nav.add(getResources().getString(R.string.job_history));
        titles_nav.add(getResources().getString(R.string.buy_credit));
        titles_nav.add(getResources().getString(R.string.messages));
        titles_nav.add(getResources().getString(R.string.set_time));


/*int size= TITLES.length;*/
        getNavItem();
        /* References Init */
        mDrawerTitle = getTitle().toString();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.driver_drawer_layout);
        content_frame = (FrameLayout) findViewById(R.id.content_frame_driver);
        txt_login_driver = (TextView) findViewById(R.id.login_user);
        /*event handling for logout state views*/
        btn_create_account.setOnClickListener(this);
        txt_login_driver.setOnClickListener(this);
        btn_tour_logout.setOnClickListener(this);
        btn_help_logout.setOnClickListener(this);
        /*rl_clear.setOnClickListener(this);*/
         /*Drawer SetUp*/
        mAdapter = new Driver_NavDrawerAdapter(Driver_MainActivity.this, mNavItems);
        listview = (ListView) findViewById(R.id.Login_State);

        //code to add header and footer to list view
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.item_drawer_driver_header, listview, false);
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.item_drawer_driver_footer, listview, false);
        TextView update_details = (TextView) header.findViewById(R.id.update_details);

        btn_about = (Button) footer.findViewById(R.id.btn_about);
        btn_about.setOnClickListener(this);

        btn_help = (Button) footer.findViewById(R.id.btn_help);
        btn_help.setOnClickListener(this);

        txt_ratingCount = (CustomRatingBar) header.findViewById(R.id.ratingBar);
        rating_text = (TextView) header.findViewById(R.id.rating_value);


        listview.addHeaderView(header, null, false);
        listview.addFooterView(footer, null, false);

        update_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                Intent intent = new Intent(Driver_MainActivity.this, Driver_UpdateProfile.class);
                startActivity(intent);
                finish();
            }
        });


        listview.setAdapter(mAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mDrawerLayout.closeDrawers();
                if (position != 3) appTitle.setText(titles_nav.get(position - 1));
                LoadFragmentView(position);

            }
        });
        if ((session.getAppLanguage()) != "en") {
            appTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            btn_about.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            btn_help.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
        }
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, app_toolBar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        //code to control Navigation via DrawerToggle
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();

    }

    private void getcreditandbonus() {


        String url = Config.APP_BASE_URL + Config.CREDIT_BONUS + user_id;
        Log.e("url", url);
        CustomRequest req = new CustomRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject responseObject) {

                        Log.e("Response1", "User Job History Response Objcet");

                        try {
                            boolean error = responseObject.getBoolean("error");

                            if (!error) {
                                try {
                                    JSONObject total_credit = responseObject.getJSONObject("message");
                                    server_total_credit = total_credit.getInt("total_credit");
                                    server_total_bonus = total_credit.getInt("total_bonus_credit");
                                    EApplication.getInstance().setcreditandbonus(server_total_credit, server_total_bonus);
                                    Log.e("credit_main", "totoal credit: " + server_total_bonus + server_total_credit);
                                    credit_value_toolbar.setText(server_total_credit+"");
                                        session.setUserBananaCredit(String.valueOf(server_total_credit), String.valueOf(server_total_bonus),String.valueOf(server_total_credit-server_total_bonus));

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else
                                EApplication.getInstance().showToastMessageFunction("Reservation history not found !!!");
                        } catch (JSONException e) {
                            e.printStackTrace();
                           /* CustomProgressBarDialog.progressDialog.dismiss();*/
                        }

                    }

                    public String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

                        Date parsed = null;
                        String outputDate = "";

                        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
                        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

                        try {
                            parsed = df_input.parse(inputDate);
                            outputDate = df_output.format(parsed);
                            Log.e("parse", " dateFormat" + outputDate);
                        } catch (ParseException e) {

                        }

                        return outputDate;

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onErrorResponse", "Error: " + error + "");


            }
        });
        EApplication.getInstance().addToRequestQueue(req, "low");

    }

    private void updatDriverLocation() {
        gps_tracker = new NewGPSTracker(Driver_MainActivity.this);
        if (gps_tracker.canGetLocation()) {
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("current_lat", String.valueOf(gps_tracker.getLatitude()));
            params.put("current_long", String.valueOf(gps_tracker.getLongitude()));
            params.put("title", "assihs");
            params.put("address", "asiohisi");
            CustomRequest request = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.SET_USER_LOCATION_POST_URL, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {

                    try {
                        boolean error = jsonObject.getBoolean("error");
                        if (!error) {

                            Log.e("===", "Sucess on Updating Driver Lcoation" + jsonObject.getString("message"));
                            // EApplication.getInstance().showToastMessageFunction("Sucess updating Location");
                        } else
                            Log.e("===", "Failure on posting Driver Location:>>" + jsonObject.getString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });

            EApplication.getInstance().addToRequestQueue(request, "low");

        }
    }

    private void getNavItem() {
        mNavItems.add(new NavDrawerItem_Driver(titles_nav.get(0)));
        mNavItems.add(new NavDrawerItem_Driver(titles_nav.get(1)));
        mNavItems.add(new NavDrawerItem_Driver(titles_nav.get(2)));
        mNavItems.add(new NavDrawerItem_Driver(titles_nav.get(3)));
        mNavItems.add(new NavDrawerItem_Driver(titles_nav.get(4)));
    }

    @Override
    protected void onResume() {

        super.onResume();
        visible = true;
        if (EApplication.getInstance().getUserRateByDriver()) {
            LoadFragmentView(6);
            appTitle.setText(getResources().getString(R.string.feedback_user));
        } else {

            LoadFragmentView(1);
            appTitle.setText(getResources().getString(R.string.new_job));

        }


        if (session.isLoggedIn()) {
            layout_Logout_State.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
            setLoggedInUserDetails();
          /*  userBananaCredit.setText(session.getUserBananaCredit());*/

            // update driver update location ::
            if (!EApplication.isDriverLocationUpdateExecuted) {
                EApplication.getInstance().setDriverLocationUpdate(true);
                Log.e("Driver::", "Location update execute Now !!!");


            } else Log.e("Driver::", "Location update already executed !!!");
        } else {
            layout_Logout_State.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        }

        getcreditandbonus();
    }

    @Override
    public void onDestroy() {
        visible = false;
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        visible = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;
        super.onStop();
    }


    private void setLoggedInUserDetails() {

        /* get user details from session */
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);
        acces_token = userDetails.get(SessionManager.ACCES_TOKEN);
        EApplication.getInstance().setUserId(user_id);
        EApplication.getInstance().setAccesToken(acces_token);

        String userName = userDetails.get(SessionManager.KEY_NAME);
        String userRating = userDetails.get(SessionManager.KEY_RATING);
        String userProfilePicture = userDetails.get(SessionManager.KEY_PICTURE);
        String user_referral = userDetails.get(SessionManager.KEY_USER_REFERRAL);
        Log.e(TAG, user_referral);
        String user_LicenseNo = userDetails.get(SessionManager.KEY_USER_LICENSENO);
        String ImageURL = Config.APP_BASE_URL + userProfilePicture;
        rating_text.setText(Float.parseFloat(userRating) + "");
        TextView driverUserName = (TextView) findViewById(R.id.person_name);
        driverUserName.setText(userName);
        TextView driver_license_no = (TextView) findViewById(R.id.person_code);
        driver_license_no.setText(" : " + user_LicenseNo);

        String referral_value;
        if (!TextUtils.isEmpty(user_referral) && !TextUtils.equals(user_referral, "null"))
            referral_value = user_referral;
        else referral_value = "N/A";

        if (!EApplication.getInstance().getUserRateByDriver()) {


            CustomRatingBar ratingDriver = (CustomRatingBar) findViewById(R.id.ratingBar_raing);
            Float ratingCount = Float.parseFloat(userRating);

            Log.e("User Rating ::", "" + ratingCount);
            ratingDriver.setScore(ratingCount);
        }

        CircularImageView driverProfileImage = (CircularImageView) findViewById(R.id.driverProfileImage);

        Picasso.with(this)
                .load(ImageURL)
                .placeholder(R.drawable.contact_avatar)   // optional
                .error(R.drawable.contact_avatar)         // optional
                .rotate(0)                             // optional
                .into(driverProfileImage);
    }

    private void LoadFragmentView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 1:
                EApplication.getInstance().setNewReservationStatus(false);
                appTitle.setText(getResources().getString(R.string.new_job));
                fragment = new FragmentJobList();
                break;
            case 2:
                fragment = new Driver_Fragment_JobHistory();
                break;
               /* Intent intentBY_Credit = new Intent(Driver_MainActivity.this, BuyBananaCredit.class);
                startActivity(intentBY_Credit);
                break;*/
            case 3:
                Intent intentBY_Credit = new Intent(Driver_MainActivity.this, Credit_Activity.class);

                startActivity(intentBY_Credit);
                break;
              /*  Intent intentBY_Credit = new Intent(Driver_MainActivity.this, BuyBananaCredit.class);
                startActivity(intentBY_Credit);*/
               /* fragment = new Driver_FragmentMessages();*/


            case 4:
                fragment = new Driver_FragmentMessages();

                break;
            case 5:
                fragment = new Driver_FragmentSetTime();

                break;
            case 6:
                fragment = new Driver_RateUser();
                break;

            case 7:
                fragment = new Fragment_DriverNewHelp();
                break;
            case 8:
                fragment = new Driver_about_Fragment();
                break;


            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame_driver, fragment).commit();

        } else {
            Log.e("===", "MainActivity Error in creating fragment");
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawers();
            return;
        }
        //super.onBackPressed();
    }

    public void checkISDriverLogin(int position) {
        if (session.isLoggedIn()) {
            Intent intent = new Intent(Driver_MainActivity.this, Driver_NewJobActivity.class);
            intent.putExtra("position", position);
            startActivity(intent);
        } else {
            Intent intent = new Intent(Driver_MainActivity.this, Driver_Activity_Login.class);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account:
                Intent intent = new Intent(Driver_MainActivity.this, DriverRegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.login_user:
                Intent intentLogin = new Intent(Driver_MainActivity.this, Driver_Activity_Login.class);
                startActivity(intentLogin);
                break;

            case R.id.btn_about:
                mDrawerLayout.closeDrawers();
                appTitle.setText(getResources().getText(R.string.aboutUs));
                LoadFragmentView(8);


                break;

            case R.id.btn_help:

                mDrawerLayout.closeDrawers();
                appTitle.setText(getResources().getText(R.string.help_n_Support));
                LoadFragmentView(7);
                break;
        }
    }

    /*method to clear Driver messages*/
    private void clearMessageApi() {

        if (NetworkUtils.isNetworkAvailable(Driver_MainActivity.this)) {
            HashMap<String, String> userDetails = session.getUserDetails();
            final String user_id = userDetails.get(SessionManager.KEY_ID);
            Log.e("User_id", user_id);
            CustomRequest req = new CustomRequest(Request.Method.GET, Config.APP_BASE_URL + Config.DELETE_ALL_NOTIFICATION_MESSAGE + user_id, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                boolean error = response.getBoolean("error");
                                Log.i("response", "URL==" + Config.APP_BASE_URL + Config.DELETE_NOTIFICATION_MESSAGE + user_id + "==Response==" + response.toString());
                                if (!error) {
                                    LoadFragmentView(4);
                                    Log.e("response", response.toString());

                                } else {
                                    Log.i("===", "Error true: " + error);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                }
            }) {

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }
            };
            EApplication.getInstance().addToRequestQueue(req, "low");
        } else {
            NetworkUtils.showNoConnectionDialog(Driver_MainActivity.this);
        }

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        // String shift = hourOfDay > 12 ? "PM" : "AM";
        String time = hourString + ":" + minuteString + ":" + "00";
        Log.e("TIME ::", time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = sdf.parse("1970-01-01 " + time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Long newTime = date.getTime();
        int errorCount = 0;
        if (dataList.size() > 1) {
            for (int i = 0; i < dataList.size() - 1; i++) {
//                if (quiteTimeUpdateField.equals("Start Time")) {
                String fromTime = dataList.get(i).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM);
                String toTime = dataList.get(i).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO);

                Date date1 = null;
                Date date2 = null;
                try {
                    date1 = sdf.parse("1970-01-01 " + fromTime);
                    date2 = sdf.parse("1970-01-01 " + toTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Long fromTimeMillis = date1.getTime();
                Long toTimeMillis = date2.getTime();

                if (newTime >= fromTimeMillis && newTime <= toTimeMillis) {
                    errorCount++;
                }
                if (quiteTimeUpdateField.equals("End Time") && !dataList.get(dataList.size() - 1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM).equals(getResources().getString(R.string.from))) {
                    String newFromTime = dataList.get(dataList.size() - 1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM);
//                    String newToTime = dataList.get(dataList.size()-1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO);

                    Date date3 = null;
//                    Date date4 = null;
                    try {
                        date3 = sdf.parse("1970-01-01 " + newFromTime);
//                        date4 = sdf.parse("1970-01-01 " + newToTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Long newFromTimeMillis = date3.getTime();
                    Long newToTimeMillis = newTime;

                    if (fromTimeMillis >= newFromTimeMillis && fromTimeMillis <= newToTimeMillis)
                        errorCount++;
                    else if (toTimeMillis >= newFromTimeMillis && toTimeMillis <= newToTimeMillis)
                        errorCount++;
                } else if (quiteTimeUpdateField.equals("Start Time") && !dataList.get(dataList.size() - 1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO).equals(getResources().getString(R.string.to_setime))) {
//                    String newFromTime = dataList.get(dataList.size()-1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM);
                    String newToTime = dataList.get(dataList.size() - 1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO);

//                    Date date3 = null;
                    Date date4 = null;
                    try {
//                        date3 = sdf.parse("1970-01-01 " + newFromTime);
                        date4 = sdf.parse("1970-01-01 " + newToTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Long newFromTimeMillis = newTime;
                    Long newToTimeMillis = date4.getTime();

                    if (fromTimeMillis >= newFromTimeMillis && fromTimeMillis <= newToTimeMillis)
                        errorCount++;
                    else if (toTimeMillis >= newFromTimeMillis && toTimeMillis <= newToTimeMillis)
                        errorCount++;
                }
            }
        }

        if (errorCount > 0)
            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.time_overlapped));
        else Driver_FragmentSetTime.setQuiteTime(time, quiteTimeIndex, quiteTimeUpdateField);

    }

    public void getTimeFromPicker(String actionFrom, int position, ArrayList<HashMap<String, String>> dataList) {

        quiteTimeIndex = position;
        quiteTimeUpdateField = actionFrom;

        this.dataList = dataList;

        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                Driver_MainActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.vibrate(true);
        tpd.show(getFragmentManager(), "Choose " + actionFrom);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
    }

    public void getSwitchEventFromPicker(boolean isChecked, int position) {

        Driver_FragmentSetTime.setQuiteTimeSwitchAction(isChecked, position);
    }


    @Override
    public void oncreditpass(int data) {
        credit_value_toolbar.setText(data + "");
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
