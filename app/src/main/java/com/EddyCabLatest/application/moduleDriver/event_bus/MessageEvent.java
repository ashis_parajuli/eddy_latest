package com.EddyCabLatest.application.moduleDriver.event_bus;

public class MessageEvent {
    public final String message;

    public MessageEvent(String message)
    {
        this.message = message;
    }
}