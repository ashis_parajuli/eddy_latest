package com.EddyCabLatest.application.moduleDriver.modal;

/**
 * Created by User on 3/1/2016.
 */
public class credit_data
{
    public  String date,date_bonus;
    public  String credit;
    public String bonus_value;
    public String agent_name,agent_name_bonus;
    public String getDate() {
        return date;
    }

    public String getDate_bonus() {
        return date_bonus;
    }

    public String getCredit() {
        return credit;
    }

    public String getBonus_value() {
        return bonus_value;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public String getAgent_name_bonus() {
        return agent_name_bonus;
    }




    public credit_data(String date_bonus, String bonus_value, String agent_name_bonus) {

        this.bonus_value=bonus_value;
        this.agent_name_bonus=agent_name_bonus;

        this.date_bonus=date_bonus;
    }
}
