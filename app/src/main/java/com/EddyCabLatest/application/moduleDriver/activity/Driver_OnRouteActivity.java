package com.EddyCabLatest.application.moduleDriver.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.NewGPSTracker;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Driver_OnRouteActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener, RoutingListener {
    private static final int UPDATE_INTERVAL = 0;
    private static final int FASTEST_INTERVAL = 0;
    private static final int DSIPLACEMENT_UPDATES = 20;
    private static final int REQUEST_CHECK_SETTINGS = 0x01;
    ImageView img_cross_red, img_cancel_reservation, img_sms;
    ImageButton ll_pickup_passenger;
    public boolean pick_up = false;
    ImageView ll_btn_call;
    public double tips_value;
    public String distance;
    private float speed;
    public int user_useCredit;
    public MarkerOptions options2;
    public MarkerOptions options1;
    public double diffDistance;
    ImageView ll_btn_sms;
    public Route mPolyOptions;
    TextView txt_passenger_name, txt_place_of_passenger, txt_place_detail_passenger, txt_pickpassenger;
    Toolbar app_toolBar;
    private Double total_taxi_charge;
    //view inside Dialog
    TextView txt_wrong_location, txt_cannot_make_it, txt_emergency;
    ImageView image_cross_red;

    private double totalWaiting = 0;
    double i = 0;
    public TextView distance_txt;
    public Button btn_job_complete;

    public TextView txt_from_location;
    //variables declared
    private MapView mapView;
    private GoogleMap map;
    SessionManager session;
    NewGPSTracker gps;
    NewGPSTracker changingGps;
    String driver_id, reservation_id;
    private String RESPONSE_PASSED_DATA;

    //driver locations
    LatLng driver_fixedLocation;

    double driverChangingLatitude;
    double driverChangingLongitude;

    double user_pickupLatitude;
    double user_pickupLongitude;
    LatLng user_pickupLocation;
    Location location_pick;
    double dropoffLatitude;
    double dropoffLongitude;

    double distane_calculation = 0;
    Polyline polyline;

    String reason;


    String pickTitle;
    String pickAddress;

    String ride_fare;
    String driverType;
    String TAG = "===";
    String language = "";
    Handler handler = new Handler();

    Runnable runable;
    private boolean visible = false;
    boolean boolreservationCancelled;


    private TextView distancetext;
    private EditText tips;
    private TextView date;
    private TextView ride_fare_txt;
    private TextView total_txt_fee;
    private TextView booking_fee_value;
    private Handler handler1;
    private Runnable runable1;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRrequest;
    private double oldDistance1;
    private Runnable runnable_location;
    private Location mLastLocation;
    private ImageView img_call;
    private Location locationOld;
    private Handler handler_destination;
    private Runnable runable_destination;
    private boolean isShown = false;
    private CoordinatorLayout coordinatorLayout;
    private Routing routing;
    private LatLngBounds.Builder builder = new LatLngBounds.Builder();
    private AsyncTask<LatLng, Void, Route> prior;
    private com.github.clans.fab.FloatingActionButton time_to_reach;


    private String usersJSON;
    private JSONObject usersJSON_route_data;
    private boolean cangetlocation = false;
    private String user_id;
    private String user_phone_num;
    private Dialog dialog_cancel;


    private ValueEventListener canceled_remove_listener;
    private boolean isUserNear500 = false;
    private String user_id_for_rating;
    private TextView total_distance_route;
    private TextView waiting_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_driver_on_route);
        initializaMap(savedInstanceState);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();
        createLocationRequest();

        //ToolBar SetUp

        total_distance_route = (TextView) findViewById(R.id.total_distance_route);
        waiting_text = (TextView) findViewById(R.id.waiting_text);


        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        session = new SessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        /*referencing views inside layout_driver_on_route.xml*/
        img_cross_red = (ImageView) findViewById(R.id.img_cross_red);
        img_call = (ImageView) findViewById(R.id.txt_call);
        img_sms = (ImageView) findViewById(R.id.txt_sms);
       /* txt_fee_paid_unpaid = (TextView) findViewById(R.id.txt_fee_paid_unpaid);*/
        txt_passenger_name = (TextView) findViewById(R.id.txt_passenger_name);
        txt_place_of_passenger = (TextView) findViewById(R.id.txt_place_of_passenger);
        txt_place_detail_passenger = (TextView) findViewById(R.id.txt_place_detail_passenger);
        time_to_reach = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.time_to_reach);

       /* txt_sms = (TextView) findViewById(R.id.txt_sms);
        txt_call = (TextView) findViewById(R.id.txt_call);*/
        time_to_reach.setVisibility(View.VISIBLE);

        ll_pickup_passenger = (ImageButton) findViewById(R.id.ll_pickup_passenger);
        ll_btn_call = (ImageView) findViewById(R.id.txt_call);
        ll_btn_sms = (ImageView) findViewById(R.id.txt_sms);
        img_cancel_reservation = (ImageView) findViewById(R.id.img_cancel_reservation);
        /*txt_pickpassenger = (TextView) findViewById(R.id.ll_pickup_passenger);*/
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        //isUserNear500 = EApplication.getInstance().gethas_arrived();

        img_cross_red.setOnClickListener(this);
        ll_pickup_passenger.setOnClickListener(this);
        ll_pickup_passenger.setEnabled(true);
        ll_btn_call.setOnClickListener(this);
        ll_btn_sms.setOnClickListener(this);
        time_to_reach.setOnClickListener(this);
        try {
            usersJSON = getIntent().getStringExtra("PASSED_RESPONSE");
            usersJSON_route_data = new JSONObject(usersJSON.toString());

            reservation_id = usersJSON_route_data.getString("id");
            pickTitle = usersJSON_route_data.getString("location_pickup_title");

            pickAddress = usersJSON_route_data.getString("location_pickup_address");

            user_pickupLongitude = Double.parseDouble(usersJSON_route_data.getString("location_pickup_longitude"));
            user_pickupLatitude = Double.parseDouble(usersJSON_route_data.getString("location_pickup_latitude"));
            user_phone_num = usersJSON_route_data.getString("user_mobile_number");
            txt_place_of_passenger.setText(pickTitle);
            txt_place_detail_passenger.setText(pickAddress);
            user_pickupLocation = new LatLng(user_pickupLatitude, user_pickupLongitude);
            location_pick = new Location("provider");
            location_pick.setLatitude(user_pickupLatitude);
            location_pick.setLongitude(user_pickupLongitude);
            txt_passenger_name.setText(usersJSON_route_data.getString("user_fullname") + "");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        // checkforcancellation();

    }

    private void generateDriverCancelledDialog() {
        dialog_cancel = new Dialog(Driver_OnRouteActivity.this);
        dialog_cancel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_cancel.setContentView(R.layout.custom_dialog_user_cancelled);
        dialog_cancel.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog_cancel.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog_cancel.getWindow().setAttributes(paramLayout);
        if (!dialog_cancel.isShowing()) {

        }
        if (visible) dialog_cancel.show();
        ImageView btnOk = (ImageView) dialog_cancel.findViewById(R.id.action_cross);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_cancel.dismiss();
                if ((handler1 != null) && (runable1 != null))

                {
                    handler1.removeCallbacks(runable1);
                    handler1 = null;
                    runable1 = null;
                }
                if ((handler != null) && (runable != null)) {
                    handler.removeCallbacks(runable);
                    handler = null;
                    runable = null;

                }

                EApplication.ref.child(user_id).removeValue();
                // set user state to preferences
                Intent intentCancelReservation = new Intent(Driver_OnRouteActivity.this, Driver_MainActivity.class);
                startActivity(intentCancelReservation);

                finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
            }
        });
    }

    private void udatedriverlocation() {


        if (mLastLocation != null) {
            EApplication.geofire.setLocation(user_id, new GeoLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new GeoFire.CompletionListener() {
                @Override
                public void onComplete(String key, FirebaseError error) {
                    if (error != null) {
                        System.err.println("There was an error saving the location to GeoFire: " + error);
                    } else {
                        System.out.println("Location saved on server successfully!");
                    }
                }
            });
        }
    }

    private void createLocationRequest() {
        mLocationRrequest = new LocationRequest();
        mLocationRrequest.setInterval(UPDATE_INTERVAL);
        mLocationRrequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRrequest.setSmallestDisplacement(DSIPLACEMENT_UPDATES);

    }

    @Override
    protected void onStart() {

        super.onStart();


    }

    @Override
    public void onResume() {

        final Handler handler = new Handler();
        mapView.onResume();
        super.onResume();
        visible = true;

        runable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(runable, 60 * 1000);
                udatedriverlocation();

            }
        };
        handler.postDelayed(runable, 30 * 1000);
        checkCancelArrived();

    }

    @Override
    public void onDestroy() {
        visible = false;
        if (canceled_remove_listener != null) {
            EApplication.ref.child(user_id + "/new_reservations").removeEventListener(canceled_remove_listener);
        }


        if (handler1 != null) {
            handler1.removeCallbacks(runable1);
            handler1 = null;
            runable1 = null;
        }
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }


        if (handler != null) {
            handler.removeCallbacks(runable);

        }
        mapView.onDestroy();
        super.onDestroy();

    }


    // This method will be called when a SomeOtherEvent is posted
    @Override
    protected void onPause() {
        visible = false;
        mapView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;

        super.onStop();

    }

    @Override
    public void onLowMemory() {
        Toast.makeText(Driver_OnRouteActivity.this, "Low Memory ", Toast.LENGTH_SHORT).show();
        super.onLowMemory();
        mapView.onLowMemory();
    }


    private void calculate_distance_eta() {

        if (cangetlocation) {

            String URL_DISTANCE = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" + mLastLocation.getLatitude() + ","
                    + mLastLocation.getLongitude() + "&destinations=" + user_pickupLocation.latitude + "," + user_pickupLocation.longitude + "&mode=driving&language=en-EN&sensor=false";
            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DISTANCE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject responseObject = new JSONObject(response);
                                JSONArray objectArray = responseObject.getJSONArray("rows");
                                JSONArray arrayElements = objectArray.getJSONObject(0).getJSONArray("elements");
                                distance = arrayElements.getJSONObject(0).getJSONObject("duration").getString("text");
                                //  distance = arrayElements.getJSONObject(0).getJSONObject("distance").getInt("value");
                                Log.e("DISTANCE::", arrayElements.toString());
                                // EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.time_to_reach) + distance);
                                Toast toast = Toast.makeText(Driver_OnRouteActivity.this, getResources().getString(R.string.time_to_reach) + " : " + distance, Toast.LENGTH_SHORT);
                                TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                                v.setTextColor(getResources().getColor(R.color.primary));
                                toast.show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null) {
                        Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                    }
                    if (error instanceof TimeoutError) {
                        Log.e("Driver_New_JObs_Volley", "TimeoutError");
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                    } else if (error instanceof NoConnectionError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                    } else if (error instanceof AuthFailureError) {
                        Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                    } else if (error instanceof ServerError) {
                        Log.e("Driver_New_JObs_Volley", "ServerError");
                    } else if (error instanceof NetworkError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "NetworkError");
                    } else if (error instanceof ParseError) {
                        Log.e("Driver_New_JObs_Volley", "ParseError");
                    }

                }
            }) {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);


                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(stringRequest, "low");
        }
    }


    private void UserIsNearDialogue(String message) {
        final Dialog dialog = new Dialog(Driver_OnRouteActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_with_redbutton);
        dialog.setCancelable(true);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);

        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(message);
        ImageView dialog_action = (ImageView) dialog.findViewById(R.id.img_cross_red);

        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_cross_red.setVisibility(View.GONE);
                dialog.dismiss();

            }
        });

        if (visible) dialog.show();

    }

    private int converintodp(int dp) {
        int padding_in_dp = dp;  // 6 dps
        final float scale = getResources().getDisplayMetrics().density;
        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
        return padding_in_px;
    }

    public void checkCancelArrived() {

        canceled_remove_listener = EApplication.ref.child(user_id + "/new_reservations/" + reservation_id).addValueEventListener(new ValueEventListener() {


            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {
                    Log.e("driver_onroute", dataSnapshot + "");
                    if (dataSnapshot.child("status").exists()) {

                        if (dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("cancelled")) {

                            EApplication.ref.child(user_id + "/new_reservations/" + reservation_id).removeEventListener(this);

                            generateDriverCancelledDialog();


                        } else {
                            if (dataSnapshot.child("has_arrived").getValue() .equals(true)) {
                                EApplication.ref.child(user_id + "/new_reservations/" + reservation_id + "/has_arrived").setValue(false);
                                EApplication.ref.child(user_id + "/new_reservations/" + reservation_id).removeEventListener(this);
                                UserIsNearDialogue(getResources().getString(R.string.near_customer));
                                EApplication.getInstance().sethas_arrived(true);
                                isUserNear500 = true;
                            }
                        }
                    }
                    // /EApplication.ref.child(user_id + "/new_reservations/" + reservation_id).removeEventListener(this);

                }
            }


        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_cross_red:
                if (canceled_remove_listener != null) {
                    EApplication.ref.child(user_id + "/new_reservations/" + reservation_id).removeEventListener(canceled_remove_listener);
                }
                showDismissBidDialog();
                break;

            case R.id.ll_pickup_passenger:
                if (canceled_remove_listener != null) {
                    EApplication.ref.child(user_id + "/new_reservations/" + reservation_id).removeEventListener(canceled_remove_listener);
                }
                time_to_reach.setVisibility(View.INVISIBLE);


                pickPassengerApi();

                break;

            case R.id.txt_call:
                methodCall();
                break;
            case R.id.txt_sms:
                methodSms();
                break;

            case R.id.time_to_reach:
                calculate_distance_eta();
                break;

        }
    }

    private void pickPassengerApi() {
        if (NetworkUtils.isNetworkAvailable(Driver_OnRouteActivity.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_OnRouteActivity.this);
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            if (visible) CustomProgressBarDialog.progressDialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", user_id);
            params.put("reservation_id", reservation_id);
            Log.e("parameters", params.toString());
            String URL_PICK_PASSENGER = Config.APP_BASE_URL + Config.DRIVER_DRIVER_PICKUP_PASSENGER_POST;
            CustomRequest req = new CustomRequest(Request.Method.POST, URL_PICK_PASSENGER, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response_value) {
                            EApplication.ref.child(user_id).removeValue();
                            img_cross_red.setVisibility(View.INVISIBLE);

                            LatLngBounds.Builder builder = new LatLngBounds.Builder();


                            builder.include(user_pickupLocation);

                            builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));

                            if (mLastLocation != null) {
                                builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                            }
                            LatLngBounds bounds = builder.build();

                            int padding_in_dp = 30;  // 6 dps
                            final float scale = getResources().getDisplayMetrics().density;
                            int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
                            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding_in_px));


                            Log.e("response", response_value.toString());
                            LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View v = vi.inflate(R.layout.onroute_jobcomplete, null);
                            ViewGroup insertPoint = (ViewGroup) findViewById(R.id.replace_view);
                            insertPoint.removeAllViews();
                            insertPoint.clearFocus();
                            insertPoint.addView(v, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            txt_from_location = (TextView) insertPoint.findViewById(R.id.pickup_location_source);
                            btn_job_complete = (Button) insertPoint.findViewById(R.id.btn_job_complete);
                            btn_job_complete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (canceled_remove_listener != null) {
                                        EApplication.ref.child(user_id + "/new_reservations/" + reservation_id).removeEventListener(canceled_remove_listener);
                                    }
                                    API_JOB_COMPLETE_EXECUTE();
                                    if (mGoogleApiClient.isConnected()) {
                                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, Driver_OnRouteActivity.this);

                                    }


                                    if (handler1 != null) {
                                        handler1.removeCallbacks(runable1);
                                        handler1 = null;
                                        runable1 = null;
                                    }
                                }
                            });
                            txt_from_location.setText(pickTitle + ", " + pickAddress);

                            CustomProgressBarDialog.progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("===", "Error on Picking Passenger");
                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }
            };
            req.setRetryPolicy(new DefaultRetryPolicy(
                    9000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(req, "high");
        } else {
            NetworkUtils.showNoConnectionDialog(Driver_OnRouteActivity.this);
        }
    }


    private void API_JOB_COMPLETE_EXECUTE() {

        if (NetworkUtils.isNetworkAvailable(Driver_OnRouteActivity.this)) {

            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_OnRouteActivity.this);
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            if (visible) CustomProgressBarDialog.progressDialog.show();

            final HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", user_id);
            params.put("reservation_id", reservation_id);
            params.put("distance", String.valueOf(distane_calculation));
            params.put("waiting_numbers", String.valueOf(totalWaiting));
            Log.i("===", "ResponseDriverConfirmed: " + params.toString());
            Log.e("===", "Parameters" + params.toString());
            CustomRequest request = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.DRIVER_JOB_COMPLETE_URL, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responseObject) {


                    Log.i("===", "ResponseDriverConfirmed:" + responseObject.toString());
                    String newResponse = responseObject.toString();

                    if (newResponse.contains("ï»¿")) {
                        newResponse = newResponse.substring(newResponse.indexOf("{"));
                    }
                    try {
                        JSONObject response = new JSONObject(newResponse);
                        boolean error_ack = response.getBoolean("error");

                        if (!error_ack) {

                            if (handler != null) {

                                handler.removeCallbacks(runable);
                                handler = null;
                                runable = null;
                            }

                            if (mGoogleApiClient.isConnected()) {

                                mGoogleApiClient.disconnect();

                            }
                            ride_fare = response.getString("total_taxi_charge");
                            user_id_for_rating = response.getString("user_id");
                            total_taxi_charge = Double.parseDouble(ride_fare);
                            CustomProgressBarDialog.progressDialog.dismiss();

                            Log.e("ride_fare", ride_fare);


                            generateBillingDialogForPayment();

                        } else {

                            CustomProgressBarDialog.progressDialog.dismiss();
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                        }

                    } catch (JSONException e) {
                        CustomProgressBarDialog.progressDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "errorJobComplete:" + error);
                    CustomProgressBarDialog.progressDialog.dismiss();

                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null) {
                        Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                    }
                    if (error instanceof TimeoutError) {
                        Log.e("Driver_New_JObs_Volley", "TimeoutError");
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                    } else if (error instanceof NoConnectionError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                    } else if (error instanceof AuthFailureError) {
                        Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                    } else if (error instanceof ServerError) {
                        Log.e("Driver_New_JObs_Volley", "ServerError");
                    } else if (error instanceof NetworkError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "NetworkError");
                    } else if (error instanceof ParseError) {
                        Log.e("Driver_New_JObs_Volley", "ParseError");
                    }

                }

            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }
            };

            EApplication.getInstance().addToRequestQueue(request, "high");
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        } else {
            NetworkUtils.showNoConnectionDialog(Driver_OnRouteActivity.this);
        }
    }

    private void generateBillingDialogForPayment() {

        Log.e("where_am_I", "im here");
        final Dialog dialog = new Dialog(Driver_OnRouteActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_send_receipt);
        dialog.setCancelable(false);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);


        Button btn_send = (Button) dialog.findViewById(R.id.btn_send);
        Button btn_dnt_send = (Button) dialog.findViewById(R.id.btn_dont_send);

        tips = (EditText) dialog.findViewById(R.id.tips);
        date = (TextView) dialog.findViewById(R.id.date);
        Calendar c = Calendar.getInstance();
        date.setText("" + c.get(Calendar.DATE) + "-" + getMonth(c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.YEAR));
        ride_fare_txt = (TextView) dialog.findViewById(R.id.ride_fee);
        total_txt_fee = (TextView) dialog.findViewById(R.id.txt_total_fare);
        booking_fee_value = (TextView) dialog.findViewById(R.id.booking_distance);
        if (distane_calculation > 1000) {
            double distance_rounded = roundTwoDecimals(distane_calculation);
            booking_fee_value.setText((distance_rounded / 1000) + getResources().getString(R.string.km));
        } else {
            double distance_rounded2 = roundTwoDecimals(distane_calculation);
            booking_fee_value.setText(roundTwoDecimals(distance_rounded2) + getResources().getString(R.string.meter));
        }
        total_txt_fee.setText(roundTwoDecimals(Double.parseDouble(ride_fare)) + "");
        ride_fare_txt.setText(ride_fare);
        dialog.show();
        Log.e("where_am_I", "im here 0");
        tips.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
              /*  tips.setText("0.0");*/
                total_txt_fee.setText(ride_fare);
                ride_fare_txt.setText(ride_fare);
                total_taxi_charge = Double.parseDouble(ride_fare);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                total_taxi_charge = Double.parseDouble(ride_fare);
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string_value_tips = tips.getText().toString().trim();
                if (string_value_tips == null || string_value_tips.isEmpty()) {
                    total_txt_fee.setText("RS " + ride_fare + "");
                    total_taxi_charge = Double.parseDouble(ride_fare);

                } else {
                    total_taxi_charge = Double.parseDouble(string_value_tips) + Double.parseDouble(ride_fare);
                    total_txt_fee.setText("RS " + total_taxi_charge + "");
                }
            }
        });


        btn_dnt_send.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                dialog.dismiss();

                HashMap<String, String> data = new HashMap<>();
                data.put("driver_id", user_id);
                data.put("user_id", user_id_for_rating);
                data.put("reservation_id", reservation_id);
                data.put("ride_fare", String.valueOf(total_taxi_charge));
                EApplication.getInstance().setUserRateByDriver(true);
                //ride_fare_txt.setText(String.valueOf(ride_fare + ""));
                EApplication.putUserRateData(data);
                generateSuccessFailureDialog(data);

            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("where_am_I", "im here 1");
                dialog.dismiss();
                CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_OnRouteActivity.this);
                if (visible) CustomProgressBarDialog.progressDialog.show();
                CustomProgressBarDialog.progressDialog.setCancelable(false);
                HashMap<String, String> params = new HashMap<>();
               /* params.put("1", "10");*/
                params.put("ride_fare", String.valueOf(total_taxi_charge));
                //credit
                params.put("use_credit", String.valueOf(0));

                params.put("reservation_id", reservation_id);
             /*   params.put("ride_tips", tips_value);
*/
                Log.e("DRIVER_PAY_VOUCHER_URL", "" + params);

                CustomRequest requestVoucher = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.DRIVER_PAY_VOUCHER_URL, params, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Voucher Response::", response.toString());
                        try {
                            boolean error_ack = response.getBoolean("error");
                            if (!error_ack) {
                             /*   int journeyfee = Integer.parseInt(ride_fare) + 10;*/
                                Log.e(TAG, "journeyfee:" + ride_fare);
                                CustomProgressBarDialog.progressDialog.dismiss();
                                HashMap<String, String> data = new HashMap<>();
                                data.put("driver_id", response.getString("driver_id"));
                                data.put("user_id", response.getString("user_id"));
                                data.put("reservation_id", reservation_id);
                                data.put("ride_fare", String.valueOf(total_taxi_charge));
                                ride_fare_txt.setText(String.valueOf(ride_fare + ""));
                                EApplication.putUserRateData(data);
                                generateSuccessFailureDialog(data);
                            } else {
                               /* EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                                Intent intent = new Intent(Driver_OnRouteActivity.this, Driver_MainActivity.class);
                                CustomProgressBarDialog.progressDialog.dismiss();
                                startActivity(intent);
                                finish();*/
                            }

                        } catch (JSONException e) {
                            CustomProgressBarDialog.progressDialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                        CustomProgressBarDialog.progressDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.e("Driver_New_JObs_Volley", "TimeoutError");
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        } else if (error instanceof NoConnectionError) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                            Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Driver_New_JObs_Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                            Log.e("Driver_New_JObs_Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Driver_New_JObs_Volley", "ParseError");
                        }

                    }
                })


                {

                    @Override
                    public Request.Priority getPriority() {
                        return Request.Priority.HIGH;
                    }
                };


                requestVoucher.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                EApplication.getInstance().addToRequestQueue(requestVoucher, "high");
            }
        });


        /*Button btn_dont_send = (Button) v.findViewById(R.id.btn_dont_send);
        btn_dont_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_bill.dismiss();


               *//* EApplication.getInstance().setUserRateByDriver(true);*//*
                Intent intent = new Intent(Driver_OnRouteActivity.this, Driver_MainActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

    }

    double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#");
        return Double.valueOf(twoDForm.format(d));
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    private void generateSuccessFailureDialog(final HashMap<String, String> data) {
        EApplication.getInstance().setUserRateByDriver(true);

        //EApplication.setUserRateByDriver(true);
        EApplication.putUserRateData(data);
        Intent intent = new Intent(Driver_OnRouteActivity.this, DriverRateUserActivity.class);
        startActivity(intent);
    }


    //method declared to send sms
    private void methodSms() {
        //Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        //sendIntent.putExtra("sms_body", " ");
        //  sendIntent.setType("vnd.android-dir/mms-sms");
        //startActivity(sendIntent);
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
                + user_phone_num.trim())));
    }

    //method declared to call
    private void methodCall() {
        String number = "tel:" + "+977" + user_phone_num.trim();
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    private void showDismissBidDialog() {

        final Dialog dialog_dismiss = new Dialog(Driver_OnRouteActivity.this);
        dialog_dismiss.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_dismiss.setContentView(R.layout.custom_dialog_wrong_location);
        dialog_dismiss.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog_dismiss.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog_dismiss.show();
        dialog_dismiss.getWindow().setAttributes(lp);
        txt_wrong_location = (TextView) dialog_dismiss.findViewById(R.id.txt_wrong_location);
        txt_cannot_make_it = (TextView) dialog_dismiss.findViewById(R.id.txt_cannot_make_it);
        txt_emergency = (TextView) dialog_dismiss.findViewById(R.id.txt_emergency);
        image_cross_red = (ImageView) dialog_dismiss.findViewById(R.id.img_cross_red);
        txt_wrong_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reason = getResources().getString(R.string.wrong_location);
                dialog_dismiss.dismiss();
                methodCancelReservation(reason);
            }
        });
        txt_cannot_make_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reason = getResources().getString(R.string.cannot_make_it);
                dialog_dismiss.dismiss();
                methodCancelReservation(reason);
            }
        });
        txt_emergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reason = getResources().getString(R.string.emergency);
                dialog_dismiss.dismiss();
                methodCancelReservation(reason);
            }
        });
        image_cross_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_dismiss.dismiss();

            }
        });

    }

    private void methodCancelReservation(String reasons) {
        if (NetworkUtils.isNetworkAvailable(Driver_OnRouteActivity.this)) {

            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_OnRouteActivity.this);
            if (visible) CustomProgressBarDialog.progressDialog.show();
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            final HashMap<String, String> params = new HashMap<>();
            params.put("reservation_id", reservation_id);
            params.put("driver_id", user_id);
            params.put("reason", reasons);
            Log.e("Parameters", params.toString());
            String CANCEL_URL = Config.APP_BASE_URL + Config.DRIVER_RESERVATION_CANCEL_POST;
            Log.e("CANCEL_URL :", CANCEL_URL);

            CustomRequest req = new CustomRequest(Request.Method.POST, CANCEL_URL, params,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject responseObject) {


                            String newResponse = responseObject.toString();
                            if (newResponse.contains("ï»¿")) {
                                if (canceled_remove_listener != null) {
                                    EApplication.ref.child(user_id + "/new_reservations/" + reservation_id).removeEventListener(canceled_remove_listener);
                                }
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }
                            Log.e("===", "Reservation Response" + newResponse);
                            try {
                                JSONObject response = new JSONObject(newResponse);
                                boolean error = response.getBoolean("error");
                                String message = response.getString("message");
                                Log.e("===", "message" + message);
                                if (!error) {
                                    boolreservationCancelled = true;
                                    methodGoBackToMain();
                                } else {
                                    if (boolreservationCancelled) {
                                        methodGoBackToMain();
                                    }
                                    if (message.equals("Reservation Not Found")) {

                                        EApplication.getInstance().showToastMessageFunction("Not Found ");
                                        //methodGoBackToMain();
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, "error on Canceling Reservation:" + error);
                    CustomProgressBarDialog.progressDialog.dismiss();
                    // EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.try_again_to_cancel_reservation));


                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null) {
                        Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                    }
                    if (error instanceof TimeoutError) {
                        Log.e("Driver_New_JObs_Volley", "TimeoutError");
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                    } else if (error instanceof NoConnectionError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                    } else if (error instanceof AuthFailureError) {
                        Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                    } else if (error instanceof ServerError) {
                        Log.e("Driver_New_JObs_Volley", "ServerError");
                    } else if (error instanceof NetworkError) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                        Log.e("Driver_New_JObs_Volley", "NetworkError");
                    } else if (error instanceof ParseError) {
                        Log.e("Driver_New_JObs_Volley", "ParseError");
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            req.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(req, "high");
        } else

        {
            if (!isShown) {
                NetworkUtils.showNoConnectionDialog(Driver_OnRouteActivity.this);
                isShown = true;
            } else {

                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);

                snackbar.show();


            }


        }
    }

    private void methodGoBackToMain() {

        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.reservation_cancelled));
        Intent cancelIntent = new Intent(Driver_OnRouteActivity.this, Driver_MainActivity.class);
        startActivity(cancelIntent);
        finish();
        overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
    }


    private void generateWarningDialog() {
        final Dialog dialog = new Dialog(Driver_OnRouteActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reservation_bidding_dailog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView WarningMsg = (TextView) dialog.findViewById(R.id.msgTitleBidding);
        WarningMsg.setText(R.string.warningTitle);
        WarningMsg.setTextColor(getResources().getColor(R.color.orange));

        TextView msgContentBidding = (TextView) dialog.findViewById(R.id.msgContentBidding);
        msgContentBidding.setText(R.string.backPressWarningMsg);
        WarningMsg.setTextColor(getResources().getColor(R.color.secondaryTextColor));

        Button btnActionSubmit = (Button) dialog.findViewById(R.id.btnActionSubmit);
        btnActionSubmit.setText(R.string.actionContinue);
        btnActionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        Button btnActionCancel = (Button) dialog.findViewById(R.id.btnActionCancel);
        btnActionCancel.setText(R.string.cancelReservation);
        btnActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                showDismissBidDialog();
            }

        });
        if (visible) dialog.show();
    }

    private void initializaMap(Bundle savedInstanceState) {
        MapsInitializer.initialize(Driver_OnRouteActivity.this);
        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(Driver_OnRouteActivity.this)) {
            case ConnectionResult.SUCCESS:
                mapView = (MapView) findViewById(R.id.content_map_view);
                mapView.onCreate(savedInstanceState);
                if (mapView != null) {
                    map = mapView.getMap();
                    map.getUiSettings().setMyLocationButtonEnabled(true);
                    map.getUiSettings().setRotateGesturesEnabled(false);

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    map.setMyLocationEnabled(true);
                }
                break;


            case ConnectionResult.SERVICE_MISSING:
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;
            default:
        }
    }

    @Override
    public void onBackPressed() {


    }


    @Override
    public void onLocationChanged(final Location location) {

        driverChangingLatitude = location.getLatitude();
        driverChangingLongitude = location.getLongitude();
        Toast.makeText(Driver_OnRouteActivity.this, "location" + location, Toast.LENGTH_SHORT).show();
        if ((location.hasAccuracy()) && (location.getAccuracy() < 25) && mLastLocation.getAccuracy() < 25) {
            if (mLastLocation == null) {
                mLastLocation = location;
            }
            speed = location.getSpeed();
            distane_calculation += mLastLocation.distanceTo(location);

            DisplayLoggingInfo();
            map.addPolyline(new PolylineOptions()
                    .add(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new LatLng(location.getLatitude(), location.getLongitude()))
                    .width(10)
                    .color(Color.YELLOW));
            builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
            /*LatLngBounds bounds = builder.build();
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80));*/
            mLastLocation = location;
        }
        mLastLocation = location;
        cangetlocation = true;
    }


    private void DisplayLoggingInfo() {
        Log.d(TAG, "entered DisplayLoggingInfo");

        //distancetext.setText(roundTwoDecimals(distane_calculation) + 0 + "" + "and waiting" + totalWaiting);
        if (distane_calculation > 1000) {
            double distance_rounded = roundTwoDecimals(distane_calculation);
            total_distance_route.setText((distance_rounded / 1000) + getResources().getString(R.string.km));
        } else {
            double distance_rounded2 = roundTwoDecimals(distane_calculation);
            total_distance_route.setText(roundTwoDecimals(distance_rounded2) + getResources().getString(R.string.meter));
        }
        waiting_text.setText(totalWaiting * 2 + getResources().getString(R.string.mins));
        /*total_distance_route.setText(roundTwoDecimals(distane_calculation) + getResources().getString(R.string.km));
        waiting_text.setText(totalWaiting*2 +getResources().getString(R.string.mins));*/
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationSettingRequest();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;

        }
        locationOld = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (locationOld == null) {
            locationOld = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        routing = new Routing(Routing.TravelMode.DRIVING);
        routing.registerListener(Driver_OnRouteActivity.this);
        mLastLocation = locationOld;
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRrequest, this);

        // LatLngBounds.Builder builder = new LatLngBounds.Builder();


        // builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));


        if ((mLastLocation != null) && (user_pickupLocation != null)) {
            builder.include(user_pickupLocation);
            builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
            LatLngBounds bounds = builder.build();
            int padding_in_dp = 30;  // 6 dps
            final float scale = getResources().getDisplayMetrics().density;
            int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding_in_px));


            prior = routing.execute(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), user_pickupLocation);
        }

        methodToCalculateWaitingTime();

        //calcualteDistancetouser();
    }

    private void locationSettingRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRrequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates locationSettingsStates = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog` by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    (Activity) Driver_OnRouteActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.

                        break;
                }
            }
        });

    }


    private void methodToCalculateWaitingTime() {
        oldDistance1 = distane_calculation;
        handler1 = new Handler();
        Log.e("Here", "here1");
/*	oldDistance1=distance;*/
/*diffDistance= distance-diffDistance;*/
        runable1 = new Runnable() {


            public void run() {
                diffDistance = distane_calculation - oldDistance1;
                if (diffDistance < 200) {
                    increaseTime();
                    oldDistance1 = distane_calculation;

                }
                handler1.postDelayed(runable1, 2 * 60 * 1000);
                if (ActivityCompat.checkSelfPermission(Driver_OnRouteActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Driver_OnRouteActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //
                    //                                     int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                /*locationOld = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                mLastLocation = locationOld;*/

            }

            private void increaseTime() {

                totalWaiting++;
               /* EApplication.getInstance().showNotification(Driver_OnRouteActivity.this, Driver_OnRouteActivity.class, 001, "Distance and Waiting", "Distance and Waiting", "" + distane_calculation + "" + totalWaiting);
*/
                DisplayLoggingInfo();
                Log.e("waiting Time", "increaseTime: " + totalWaiting);
            }

        };

        handler1.postDelayed(runable1, 2 * 60000); //start the timer

    }

    @Override
    public void onConnectionSuspended(int i) {
        EApplication.getInstance().showToastMessageFunction("suspended connection");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onRoutingFailure() {

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(final PolylineOptions mPolyOptions, Route route) {
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                options1 = new MarkerOptions();
                options1.position(user_pickupLocation);
                options1.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));
                map.addMarker(options1);
                PolylineOptions polyOptions = new PolylineOptions();
                polyOptions.color(getResources().getColor(R.color.primary_dark));
                polyOptions.width(10);
                polyOptions.addAll(mPolyOptions.getPoints());
                polyline = map.addPolyline(polyOptions);
                builder.include(options1.getPosition());


                LatLngBounds.Builder builder = new LatLngBounds.Builder();


                if ((mLastLocation != null) && (user_pickupLocation != null)) {

                    builder.include(user_pickupLocation);


                    builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));

                    if (mLastLocation != null) {
                        builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                    }
                    LatLngBounds bounds = builder.build();

                    int padding_in_dp = 30;  // 6 dps
                    final float scale = getResources().getDisplayMetrics().density;
                    int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding_in_px));
                    map.setPadding(12, 20, 30, 30);
                }

            }
        });


    }
}


