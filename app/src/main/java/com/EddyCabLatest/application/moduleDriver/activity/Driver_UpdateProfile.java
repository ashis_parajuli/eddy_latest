package com.EddyCabLatest.application.moduleDriver.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.SelectLanguage;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.utilities.Validator;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Driver_UpdateProfile extends AppCompatActivity implements View.OnClickListener {

    SessionManager session;
    EApplication app;
    ImageView image_profile;
    TextView changeLanguage;
    TextView Logout;

    EditText edt_driver_name;
    EditText edt_driver_email;
    EditText edt_driver_country_code;
    EditText edt_driver_mobile_number;
    EditText edt_driver_license;
    EditText edt_driver_old_pass;
    EditText edt_driver_newpass, password_confirm;
    Button btn_update;
    ImageView img_back_arrow, changeProfilePic;


    String id, name, email, mobile, image, license, referralCode, rating;
    private String newLanguage;
    private String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_driver_update_profile);

        // Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);

        // Initializing Application Instances
        session = new SessionManager(Driver_UpdateProfile.this);
        app = EApplication.getInstance();

        initViews();
        HashMap<String, String> userDetails = session.getUserDetails();

        id = userDetails.get("id");
        name = userDetails.get("name");
        email = userDetails.get("email");
        mobile = userDetails.get("mobile_no");
        license = userDetails.get("license_no");
        image = userDetails.get("picture");
        referralCode = userDetails.get("referral_code");
        rating = userDetails.get("rating");

        edt_driver_name.setText(name);
        edt_driver_email.setText(email);
        edt_driver_mobile_number.setText(mobile);
        edt_driver_license.setText(license);
        String language;
        if (session.getAppLanguage().equalsIgnoreCase(Config.LANG_ENG))
            language = getResources().getString(R.string.nepali_language);
        else language = getResources().getString(R.string.english);
        changeLanguage.setText(language);

        Picasso.with(Driver_UpdateProfile.this)
                .load(Config.APP_BASE_URL + image)
                .placeholder(R.drawable.contact_avatar)   // optional
                .error(R.drawable.contact_avatar)         // optional
                .rotate(0).into(image_profile);


        btn_update.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                updateprofile();


            }
        });

    }

    private void updateprofile() {
        if (edt_driver_old_pass.getText().toString().trim().isEmpty() && edt_driver_newpass.getText().toString().trim().isEmpty() && password_confirm.getText().toString().trim().isEmpty()) {
            if (Validator.isEmailValid(edt_driver_email.getText().toString().trim())) {
                if (NetworkUtils.isNetworkAvailable(Driver_UpdateProfile.this)) {

                    HashMap<String, String> userDetails = session.getUserDetails();
                    String userID = userDetails.get(SessionManager.KEY_ID);


                    final HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", userID);
                    params.put("email", edt_driver_email.getText().toString().trim());
                    params.put("mobile_number", edt_driver_mobile_number.getText().toString().trim());
                    params.put("fullname", edt_driver_name.getText().toString().trim());

                    Log.e("Parameters", params.toString());

                    CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_UpdateProfile.this);
                    CustomProgressBarDialog.progressDialog.setCancelable(false);
                    CustomProgressBarDialog.progressDialog.show();

                    CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.EDIT_PROFILE, params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject responseObject) {
                                    Log.e("Response 1st API", responseObject.toString());

                                    try {
                                        boolean error = responseObject.getBoolean("error");

                                        if (!error) {
                                            //TODO put the data in session
                                                   /* session.createLoginSession(String.valueOf(user_id), user_name, user_email, user_ProfilePic,
                                                            corrected_rating, user_details.toString(), user_referralcode, "mobile_no", "license_no", lastReservationId);*/
                                            // Dismiss the activity and resume Main Activity

                                            session.createupdatedriverdata(edt_driver_name.getText().toString().trim(), edt_driver_email.getText().toString().trim(), edt_driver_mobile_number.getText().toString().trim(), edt_driver_license.getText().toString().trim());
                                            Driver_UpdateProfile.this.finish();

                                            Toast.makeText(Driver_UpdateProfile.this, getResources().getString(R.string.sucess_update), Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(Driver_UpdateProfile.this, Driver_UpdateProfile.class);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            CustomProgressBarDialog.progressDialog.dismiss();
                                            String message = responseObject.getString("message");
                                            showErrorDialog(message);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    CustomProgressBarDialog.progressDialog.dismiss();

                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("Error", "Error: " + error.getMessage());
                            Log.d("Error", "JSONErrorMSG: " + error.getMessage());
                            Log.d("Error", "JSONErrorStack: " + error.getStackTrace());

                            CustomProgressBarDialog.progressDialog.dismiss();

                        }
                    })  {
                        @Override
                        public Request.Priority getPriority() {
                            return Request.Priority.HIGH;
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                            String current_language = session.getAppLanguage();
                            Log.e("current_language", current_language);
                            if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                language = "en";
                            }
                            if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                language = "np";
                            }
                            headers.put("lang", language);


                            return headers;
                        }
                    };
                    int socketTimeout = 20000; //30 seconds
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req.setRetryPolicy(policy);
                    EApplication.getInstance().addToRequestQueue(req, "high");
                } else
                    NetworkUtils.showNoConnectionDialog(Driver_UpdateProfile.this);


            } else {
                EApplication.getInstance().showToastMessageFunction("Email is Invalid");
            }
        } else {
            if (Validator.isEmailValid(edt_driver_email.getText().toString().trim())) {

                if (NetworkUtils.isNetworkAvailable(Driver_UpdateProfile.this)) {

                    HashMap<String, String> userDetails = session.getUserDetails();
                    String userID = userDetails.get(SessionManager.KEY_ID);
                    if ((TextUtils.isEmpty(edt_driver_old_pass.getText().toString().trim()) || TextUtils.isEmpty(edt_driver_newpass.getText().toString().trim()) || TextUtils.isEmpty(password_confirm.getText().toString().trim()))) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.fields_required));
                    } else {

                        if (!edt_driver_newpass.getText().toString().trim().equalsIgnoreCase(password_confirm.getText().toString().trim())) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.password_donot_match));
                        } else {
                            final HashMap<String, String> params = new HashMap<>();
                            params.put("user_id", userID);
                            params.put("email", edt_driver_email.getText().toString().trim());
                            params.put("old_password", edt_driver_old_pass.getText().toString().trim());
                            params.put("new_password", password_confirm.getText().toString().trim());
                            params.put("mobile_number", edt_driver_mobile_number.getText().toString().trim());
                            params.put("fullname", edt_driver_name.getText().toString().trim());
                            Log.e("Parameters", params.toString());
                            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_UpdateProfile.this);
                            CustomProgressBarDialog.progressDialog.setCancelable(false);
                            CustomProgressBarDialog.progressDialog.show();
                            CustomRequest req = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.EDIT_PROFILE, params,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject responseObject) {
                                            Log.e("Response 1st API", responseObject.toString());
                                            try {
                                                boolean error = responseObject.getBoolean("error");

                                                if (!error) {
                                                    session.createupdatedriverdata(edt_driver_name.getText().toString().trim(), edt_driver_email.getText().toString().trim(), edt_driver_mobile_number.getText().toString().trim(), edt_driver_license.getText().toString().trim());
                                                    // Dismiss the activity and resume Main Activity
                                                    Driver_UpdateProfile.this.finish();

                                                    Toast.makeText(Driver_UpdateProfile.this, getResources().getString(R.string.sucess_update), Toast.LENGTH_LONG).show();
                                                    Intent intent = new Intent(Driver_UpdateProfile.this, Driver_UpdateProfile.class);
                                                    startActivity(intent);
                                                    finish();
                                                } else {
                                                    CustomProgressBarDialog.progressDialog.dismiss();
                                                    String message = responseObject.getString("message");
                                                    showErrorDialog(message);
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            CustomProgressBarDialog.progressDialog.dismiss();

                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.d("Error", "Error: " + error.getMessage());
                                    Log.d("Error", "JSONErrorMSG: " + error.getMessage());
                                    Log.d("Error", "JSONErrorStack: " + error.getStackTrace());

                                    CustomProgressBarDialog.progressDialog.dismiss();

                                }
                            }) {
                                @Override
                                public Request.Priority getPriority() {
                                    return Request.Priority.HIGH;
                                }

                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    return params;
                                }

                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                    String current_language = session.getAppLanguage();
                                    Log.e("current_language", current_language);
                                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                        language = "en";
                                    }
                                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                        language = "np";
                                    }
                                    headers.put("lang", language);


                                    return headers;
                                }
                            };
                            int socketTimeout = 20000; //30 seconds
                            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                            req.setRetryPolicy(policy);
                            EApplication.getInstance().addToRequestQueue(req, "high");
                        }

                    }
                }


            } else {
                NetworkUtils.showNoConnectionDialog(Driver_UpdateProfile.this);
            }

        }
    }

    private void showErrorDialog(String errorMessage) {

        final Dialog dialog = new Dialog(Driver_UpdateProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_success_failure_dialog);
        dialog.setCancelable(false);


        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_title.setText(getResources().getString(R.string.warningTitle));
        dialog_message.setText(errorMessage);
        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void initViews() {
        image_profile = (ImageView) findViewById(R.id.image_profile);
        changeLanguage = (TextView) findViewById(R.id.changeLanguage);
        Logout = (TextView) findViewById(R.id.logout);
        img_back_arrow = (ImageView) findViewById(R.id.img_back_arrow);

        edt_driver_name = (EditText) findViewById(R.id.edt_driver_name);
        edt_driver_name = (EditText) findViewById(R.id.edt_driver_name);
        edt_driver_email = (EditText) findViewById(R.id.edt_driver_email);
        edt_driver_country_code = (EditText) findViewById(R.id.edt_driver_country_code);
        edt_driver_mobile_number = (EditText) findViewById(R.id.edt_driver_mobile_number);
        edt_driver_license = (EditText) findViewById(R.id.edt_driver_license);
        edt_driver_old_pass = (EditText) findViewById(R.id.edt_driver_old);
        edt_driver_newpass = (EditText) findViewById(R.id.edt_driver_newpass);
        edt_driver_newpass = (EditText) findViewById(R.id.edt_driver_newpass);
        password_confirm = (EditText) findViewById(R.id.edt_driver_newpass_confirm);
     /*   changeProfilePic = (ImageView) findViewById(R.id.changeProfilePic);*/
        btn_update = (Button) findViewById(R.id.btn_update);
        img_back_arrow.setOnClickListener(this);
     /*   changeProfilePic.setOnClickListener(this);*/
        changeLanguage.setOnClickListener(this);
        Logout.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_update_profile, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_back_arrow:
                Intent intent = new Intent(Driver_UpdateProfile.this, Driver_MainActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                break;

            case R.id.logout:
                DoYouWantToLogOutDiloge();
                break;

            case R.id.changeLanguage:
                String current_lang = "";
                String next_lang = "";
                current_lang = session.getAppLanguage();

                if (current_lang.equalsIgnoreCase("Thai")) {
                    //  changeLanguage.setText(getResources().getString(R.string.english));
                    next_lang = "en";
                } else {
                    //  changeLanguage.setText(getResources().getString(R.string.nepali_nepali));
                    next_lang = "np";
                }
                methodChangeLanguageApi(next_lang);
                break;

            default:
                break;
        }

    }

    private void methodChangeLanguageApi(String nextLanguage) {
        if (NetworkUtils.isNetworkAvailable(Driver_UpdateProfile.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_UpdateProfile.this);
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            CustomProgressBarDialog.progressDialog.show();
            final HashMap<String, String> params = new HashMap<>();
            params.put("user_id", id);
            params.put("language", nextLanguage);

            Log.e("parameters", params.toString());

            CustomRequest changeLanguageRequest = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.CHANGE_LANGUAGE, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("response>>", response.toString());
                            try {
                                boolean error = response.getBoolean("error");
                                newLanguage = response.getString("language");
                                if (!error) {
                                    String setLanguage;
                                    if (newLanguage.equalsIgnoreCase("en")) {
                                        changeLanguage.setText(getResources().getString(R.string.nepali_nepali));
                                        setLanguage = Config.LANG_ENG;
                                    } else {
                                        changeLanguage.setText(getResources().getString(R.string.english));
                                        setLanguage = Config.LANG_THAI;
                                    }
                                    session.createLanguageSession(setLanguage);
                                    app.setLang(newLanguage);
                                    Intent intent = new Intent(Driver_UpdateProfile.this, Driver_UpdateProfile.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            CustomProgressBarDialog.progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EApplication.getInstance().showToastMessageFunction("Error Response on Changing Language");
                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);


                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            changeLanguageRequest.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(changeLanguageRequest, "high");

        } else {
            NetworkUtils.showNoConnectionDialog(Driver_UpdateProfile.this);
        }

    }

    private void DoYouWantToLogOutDiloge() {

        final Dialog dialog = new Dialog(Driver_UpdateProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reservation_bidding_dailog);
        dialog.setCancelable(true);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);

        TextView msgContentBidding = (TextView) dialog.findViewById(R.id.msgContentBidding);
        msgContentBidding.setText(R.string.want_to_logout);
        msgContentBidding.setTextColor(getResources().getColor(R.color.secondaryTextColor));

        Button btnActionSubmit = (Button) dialog.findViewById(R.id.btnActionSubmit);
        btnActionSubmit.setText(R.string.yes);
        btnActionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                methodCallLogoutApi();
            }
        });
        Button btnActionCancel = (Button) dialog.findViewById(R.id.btnActionCancel);
        btnActionCancel.setText(R.string.not_now);
        btnActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();


    }

    private void methodCallLogoutApi() {


        if (NetworkUtils.isNetworkAvailable(Driver_UpdateProfile.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_UpdateProfile.this);
            CustomProgressBarDialog.progressDialog.show();

            CustomRequest customRequest = new CustomRequest(Request.Method.GET,
                    Config.APP_BASE_URL + Config.USER_LOGOUT + id, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Response", response.toString());
                    try {
                        boolean error = response.getBoolean("error");
                        String message = response.getString("message");
                        if (!error) {
                            //  EApplication.getInstance().showToastMessageFunction(message);
                            CustomProgressBarDialog.progressDialog.dismiss();

                            EApplication.getInstance().setDriverLocationUpdate(false);
                          /*  Driver_MainActivity.removeHandlerCalls();*/
                            session.clearUserDetails();
                            session.clearDriverNumber();

                            Intent intent = new Intent(Driver_UpdateProfile.this, SelectLanguage.class);
                            startActivity(intent);
                            finish();
                            Log.e("Action", "Driver Update Exit");

                        } else {
                            EApplication.getInstance().showToastMessageFunction(message);
                            CustomProgressBarDialog.progressDialog.dismiss();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            })  {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }



                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);


                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            customRequest.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(customRequest, "high");


        } else {
            NetworkUtils.showNoConnectionDialog(Driver_UpdateProfile.this);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
