package com.EddyCabLatest.application.moduleDriver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.CustomRatingBar_with_rating;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DriverRateUserActivity extends AppCompatActivity {

    private EditText feedback_message;
    private Button submit_feedback;
    private CustomRatingBar_with_rating ratingBar;
    private TextView txt_bhat_value;
    private TextView txt_date;
    private HashMap<String, String> requiredParam;
    private float ratingValue;
    private String journeyFee;
    private String comment;
    private SessionManager session;
    private String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_rate_user);


        feedback_message = (EditText) findViewById(R.id.user_comment_message);
        submit_feedback = (Button) findViewById(R.id.btn_submit_rating);
        ratingBar = (CustomRatingBar_with_rating) findViewById(R.id.ratingBar);
        txt_bhat_value = (TextView) findViewById(R.id.txt_bhat_value);
        txt_date = (TextView) findViewById(R.id.txt_date);
        session = new SessionManager(this);
        //getting current date of system
        SimpleDateFormat dfDate_day = new SimpleDateFormat("MMM dd, yyyy");
        String dt = "";
        Calendar c = Calendar.getInstance();
        dt = dfDate_day.format(c.getTime());

        txt_date.setText(dt);
        requiredParam = EApplication.getUserRateData;
        Log.e("requiredParam", "" + requiredParam);
        journeyFee = requiredParam.get("ride_fare");

        txt_bhat_value.setText(journeyFee);
    /*    ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingValue = rating;
                Log.e(TAG, "RatingValue is :" + rating);
                if (rating >= 4) {
                    feedback_message.setVisibility(View.GONE);
                } else {
                    feedback_message.setVisibility(View.VISIBLE);
                }

            }
        });*/
        ratingBar.setOnScoreChanged(new CustomRatingBar_with_rating.IRatingBarCallbacks() {
            @Override
            public void scoreChanged(float score) {
                ratingValue = score;

                if (score >= 4) {
                    feedback_message.setEnabled(false);
                    feedback_message.setVisibility(View.GONE);
                } else {
                    feedback_message.setVisibility(View.VISIBLE);
                }
            }
        });

        submit_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EApplication.ref.child(requiredParam.get("driver_id")).removeValue();
                if (ratingValue >= 3) {
                    feedback_message.setVisibility(View.GONE);
                    comment = "no comment inserted";
                } else {
                    comment = feedback_message.getText().toString().trim();
                }

                if (ratingValue != 0) {
                    if (NetworkUtils.isNetworkAvailable(DriverRateUserActivity.this)) {
                        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(DriverRateUserActivity.this);
                        CustomProgressBarDialog.progressDialog.show();
                        final HashMap<String, String> params = new HashMap<>();
                        params.put("reservation_id", requiredParam.get("reservation_id"));
                        params.put("from_id", requiredParam.get("driver_id"));
                        params.put("to_id", requiredParam.get("user_id"));
                        params.put("rating", String.valueOf(ratingValue));
                        params.put("comments", comment);

                        Log.e("Response Param", "" + params);

                        CustomRequest request = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.RATE_USER_BY_DRIVER, params, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                CustomProgressBarDialog.progressDialog.dismiss();
                      /*          EApplication.getInstance().openFragment1 = true;*/

                                Intent intentGoToMain = new Intent(DriverRateUserActivity.this, Driver_MainActivity.class);
                                startActivity(intentGoToMain);
                                EApplication.getInstance().setUserRateByDriver(false);
                                //EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.));


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                // EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));

                                NetworkResponse networkResponse = error.networkResponse;
                                if (networkResponse != null) {
                                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                                }
                                if (error instanceof TimeoutError) {
                                    Log.e("Driver_New_JObs_Volley", "TimeoutError");
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                                } else if (error instanceof NoConnectionError) {
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                                    Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                                } else if (error instanceof AuthFailureError) {
                                    Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                                } else if (error instanceof ServerError) {
                                    Log.e("Driver_New_JObs_Volley", "ServerError");
                                } else if (error instanceof NetworkError) {
                                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                                    Log.e("Driver_New_JObs_Volley", "NetworkError");
                                } else if (error instanceof ParseError) {
                                    Log.e("Driver_New_JObs_Volley", "ParseError");
                                }

                            }
                        }) {
                            @Override
                            public Request.Priority getPriority() {
                                return Request.Priority.HIGH;
                            }

                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                String current_language = session.getAppLanguage();
                                Log.e("current_language", current_language);
                                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                    language = "en";
                                }
                                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                    language = "np";
                                }
                                headers.put("lang", language);


                                return headers;
                            }
                        };
                        int socketTimeout = 20000; //30 seconds
                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        request.setRetryPolicy(policy);
                        EApplication.getInstance().addToRequestQueue(request, "high");


                    } else {
                        NetworkUtils.showNoConnectionDialog(DriverRateUserActivity.this);
                    }
                } else {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_rate_user));


                }

            }
        });


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
