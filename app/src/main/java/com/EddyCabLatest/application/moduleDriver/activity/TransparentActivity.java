package com.EddyCabLatest.application.moduleDriver.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserMain;
import com.EddyCabLatest.application.utilities.SessionManager;

/**
 * Created by Santosh on 10/13/2015.
 */
public class TransparentActivity extends AppCompatActivity {
    SessionManager session;
    String newReservation;
    private Dialog dialog;
    private boolean visible=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        visible=true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transparent);
        session = new SessionManager(TransparentActivity.this);

        newReservation = getIntent().getStringExtra("newReservation");
        String message = getIntent().getStringExtra("message");
        if (message != null)
            if (message.equals("reservationcancelled")) {
                message = getResources().getString(R.string.reservation_cancelled);
            }
        if(dialog!=null)
        {
            dialog=null;

        }
        dialog = new Dialog(TransparentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_of_transparent_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        dialog_title.setText(getResources().getString(R.string.alert));
        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(message);

        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        if (newReservation != null) {
            if (newReservation.equals("newReservation")) {
                dialog_action.setText(getResources().getString(R.string.SELECT_now));
            }
        }



        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SessionManager session = new SessionManager(TransparentActivity.this);
                String user_type = session.getAppUserType();
                if (user_type.equalsIgnoreCase(Config.USER_TYPE_USER)) {
                    session.createReservationByUser(DBTableFields.RESERVATION_STATE_CANCELLED);
                    EApplication.getInstance().setBookingStatus(false);
                    EApplication.getInstance().setHistryCancelledStatus(true);
                    session.clearPickUPDropOffLocationAddress();
                    Intent intent_cancel_bid = new Intent(TransparentActivity.this, Activity_UserMain.class);
                    intent_cancel_bid.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent_cancel_bid);
                    finish();
                } else {
                    if (newReservation != null) {
                        if (newReservation.equals("newReservation")) {
                            EApplication.getInstance().setIsNewJobDialogShown(false);
                        }
                    }
                   /* EApplication.getInstance().openFragment1 = true;*/
                    Intent intent_cancel_bid = new Intent(TransparentActivity.this, Driver_MainActivity.class);
                    intent_cancel_bid.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent_cancel_bid);
                    finish();
                }


            }
        });
    dialog.show();

        Button dialog_dismiss = (Button) dialog.findViewById(R.id.dialog_dismiss);

        dialog_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                visible=false;
                if (newReservation != null) {
                    if (newReservation.equals("newReservation")) {
                        EApplication.getInstance().setIsNewJobDialogShown(false);
                    }
                }
                dialog.dismiss();
                finish();
            }
        });


    }

    @Override
    protected void onDestroy() {
        visible=false;
        if(dialog!=null)
        {
            dialog=null;
        }
        finish();
        super.onDestroy();
    }

    @Override
    protected void onPause() {

        super.onPause();




    }





    @Override
    public void onBackPressed() {
    }
}
