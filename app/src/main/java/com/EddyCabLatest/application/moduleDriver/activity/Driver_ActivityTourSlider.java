package com.EddyCabLatest.application.moduleDriver.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.utilities.CirclePageIndicator;
import com.EddyCabLatest.application.utilities.PageIndicator;

public class Driver_ActivityTourSlider extends AppCompatActivity {


    int[] imageDataList;
    PageIndicator mIndicator;
    private ViewPager viewPager;
    // ImageView actionSliderNext, actionSliderFinish;
    RelativeLayout rl_parent;
    String[] slidertexts;
    Button btn_create_account;
    TextView txt_signin;
    private String[] slidertexts_detail;
    private String[] slidertexts_title;
    private Toolbar app_toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_slider);
        // Activity Loading Animation
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);

        imageDataList = new int[]{
                R.drawable.driver_tour1,
                R.drawable.driver_tour2,
                R.drawable.driver_tour3,
                R.drawable.driver_tour4
        };
        slidertexts_detail = new String[]{
                getResources().getString(R.string.bidfor_newone_detail_driver),
                getResources().getString(R.string.wait_for_driver_bid_you_detail_driver_new),
                getResources().getString(R.string.select_driver_for_detaill_driver),
                getResources().getString(R.string.sit_n_trackdriver_detaill_driver)
        };

        slidertexts_title = new String[]{
                getResources().getString(R.string.welcome_to_eddycab),
                getResources().getString(R.string.welcome_to_eddycab),
                getResources().getString(R.string.welcome_to_eddycab),
                getResources().getString(R.string.welcome_to_eddycab)
        };


        rl_parent = (RelativeLayout) findViewById(R.id.parent_main);


        viewPager = (ViewPager) findViewById(R.id.pager_slider);
        CustomPagerAdapter adapter = new CustomPagerAdapter(Driver_ActivityTourSlider.this);

        viewPager.setAdapter(adapter);


        final CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator = indicator;
        indicator.setViewPager(viewPager);

        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(6 * density);
        indicator.setPageColor(0xFF333333);
        indicator.setFillColor(0xFFFFFFFF);
        indicator.setStrokeColor(0xFF333333);
        indicator.setStrokeWidth(2 * density);


//        actionSliderFinish.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.e("Action ::", "onPageScrolled");

            }

            @Override
            public void onPageSelected(int position) {
                Log.e("Action ::", "onPageSelected");
//
//
                /*if (position == 0) {
                    rl_parent.setBackgroundColor(getResources().getColor(R.color.primary));
                }
                if (position == 1) {
                    rl_parent.setBackgroundColor(getResources().getColor(R.color.purple));
                }
                if (position == 2) {
                    rl_parent.setBackgroundColor(getResources().getColor(R.color.blue_green));
                }
                if (position == 3) {
                    rl_parent.setBackgroundColor(getResources().getColor(R.color.blue_light));
                }*/


                indicator.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.e("Action ::", "onPageScrollStateChanged");
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Intent intent = new Intent(Driver_ActivityTourSlider.this, DriverLoginScreenActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return imageDataList.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.slider_single_view, container, false);


            ImageView imageView = (ImageView) itemView.findViewById(R.id.image_view);
            imageView.setImageDrawable(getResources().getDrawable(imageDataList[position]));
            TextView textdetail = (TextView) itemView.findViewById(R.id.textdetail);
            TextView welcome_text = (TextView) itemView.findViewById(R.id.welcome_text);
            Button close_tour = (Button) itemView.findViewById(R.id.button_tour);
            close_tour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            textdetail.setText(slidertexts_detail[position]);
            welcome_text.setText(slidertexts_title[position]);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
