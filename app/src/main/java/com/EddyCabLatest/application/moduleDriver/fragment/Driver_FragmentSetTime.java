package com.EddyCabLatest.application.moduleDriver.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.constant.DBTableFields;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_ActivityBidSuccess;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.moduleDriver.adapter.AdapterSettingQuiteTime;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Driver_FragmentSetTime extends Fragment {

    private SessionManager session;
    private ImageView actionAddQuiteTime;
    private ImageView actionSetQuiteTime;
    private Button actionDeleteQuiteTime;

    private static ListView quiteTimeListView;
    private static ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

    private static Activity mActivity;

    private Context mcontext;
    private String user_id;

    private ChildEventListener eventlistener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.driver_layout_set_time, container, false);

        session = new SessionManager(getActivity());


        quiteTimeListView = (ListView) rootView.findViewById(R.id.driverQuiteListView);
        actionAddQuiteTime = (ImageView) rootView.findViewById(R.id.actionAddQuiteTime);
        actionSetQuiteTime = (ImageView) rootView.findViewById(R.id.actionSetQuiteTime);
        actionDeleteQuiteTime = (Button) rootView.findViewById(R.id.actionDeleteQuiteTime);


        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
            CustomProgressBarDialog.progressDialog.show();

            HashMap<String, String> userDetails = session.getUserDetails();
            String userID = userDetails.get(SessionManager.KEY_ID);

            String REQUEST_URL = Config.APP_BASE_URL + Config.DRIVER_GET_QUITE_TIME_URL + userID;

            CustomRequest request = new CustomRequest(Request.Method.GET, REQUEST_URL, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Response", response.toString());

                    try {
                        boolean error = response.getBoolean("error");

                        if (!error) {

                            dataList.clear();

                            JSONArray arrayData = response.getJSONArray("setting");
                            for (int i = 0; i < arrayData.length(); i++) {
                                HashMap<String, String> mapData = new HashMap<>();
                                String from_time = arrayData.getJSONObject(i).getString("from_time");
                                String to_time = arrayData.getJSONObject(i).getString("to_time");
                                String period_name = arrayData.getJSONObject(i).getString("period_name");
                                String status = arrayData.getJSONObject(i).getString("status");

                                mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM, from_time + ":00");
                                mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO, to_time + ":00");
                                mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME, period_name);
                                mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS, status);

                                dataList.add(mapData);

                                Log.e("Data ::", dataList.toString());
                            }

                        } else {
                            HashMap<String, String> mapData = new HashMap<>();
                            dataList.add(mapData);
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.quiet_time_not_set));
                        }


                        AdapterSettingQuiteTime adapter = new AdapterSettingQuiteTime(getActivity(), dataList);
                        quiteTimeListView.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("Error: ", error.getMessage());

                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                public String language;


                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            EApplication.getInstance().addToRequestQueue(request, "low");
        } else NetworkUtils.showNoConnectionDialog(getActivity());

        actionAddQuiteTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dataList.size() > 0) {
                    actionSetQuiteTime.setEnabled(true);
                    if (dataList.get(dataList.size() - 1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM).equals("From")
                            || dataList.get(dataList.size() - 1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO).equals("To")) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.complete_previous_time));
                    } else {
                        HashMap<String, String> mapData = new HashMap<>();

                        mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM, getResources().getString(R.string.from));
                        mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO, getResources().getString(R.string.to_setime));
                        mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME, getResources().getString(R.string.period) + (dataList.size() + 1));
                        mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS, "1");

                        dataList.add(mapData);
                        Log.e("Data ::", dataList.toString());

                        setUpdatedDataToListView(dataList);

                    }
                } else {
                    actionSetQuiteTime.setEnabled(true);
                    HashMap<String, String> mapData = new HashMap<>();

                    mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM, getResources().getString(R.string.from));
                    mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO, getResources().getString(R.string.to_setime));
                    mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME, getResources().getString(R.string.period) + (dataList.size() + 1));
                    mapData.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS, "1");

                    dataList.add(mapData);
                    Log.e("Data ::", dataList.toString());

                    setUpdatedDataToListView(dataList);

                }

            }
        });

        actionSetQuiteTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataList.size() > 0) {
                    if (dataList.get(dataList.size() - 1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM).equals(getResources().getString(R.string.from))
                            || dataList.get(dataList.size() - 1).get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO).equals(getResources().getString(R.string.to_setime))) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.set_quiet_time_first_1));
                    } else {
                        SaveQuiteTimeTOServer();
                    }
                } else {

                    /*SaveQuiteTimeTOServer();*/
                }
            }
        });

        actionDeleteQuiteTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*ArrayList<HashMap<String, String>> tempDataList = new ArrayList<>();

                if (dataList.size() > 0) {

                    for (int i = 0; i < dataList.size() - 1; i++) {
                        HashMap<String, String> sameMapData = dataList.get(i);
                        tempDataList.add(sameMapData);
                    }

                    dataList = tempDataList;
                    setUpdatedDataToListView(dataList);
                }*/

                if (dataList.size() > 0) {
                    dataList.remove(dataList.size() - 1);
                    setUpdatedDataToListView(dataList);
                    dataList.clear();
                    SaveQuiteTimeTOServer1();

                }

                Log.e("Data ::", dataList.toString());

            }
        });

        return rootView;

    }


    @Override
    public void onDestroy() {

        if (eventlistener != null) {

            EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(eventlistener);
        }
        super.onDestroy();


    }

    @Override
    public void onResume() {
        super.onResume();
        getjoblistfirebase();
    }

    public void getjoblistfirebase() {
        eventlistener = EApplication.ref.child(user_id + "/new_reservations/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    if ((dataSnapshot.child("accepted").getValue().equals(false)) && (dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("new"))) {
                        Intent intent = new Intent(mcontext, Driver_MainActivity.class);
                        startActivity(intent);

                        if (isAdded()) {
                            EApplication.getInstance().showNotification(mcontext, Driver_MainActivity.class, 011, getResources().getString(R.string.new_job), getResources().getString(R.string.new_job), getResources().getString(R.string.new_job_avaiable));

                        }
                    }


                    Log.e("data_joblist", dataSnapshot + "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (isAdded()) {
                    if (dataSnapshot.getValue() != null) {

                        if (dataSnapshot.child("accepted").exists()) {
                            if (dataSnapshot.child("accepted").getValue().equals(true)) {
                                //EApplication.ref.child(userID + "/new_reservations/" + dataSnapshot.getKey() + "/accepted/").removeValue();
                                EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                if (eventlistener != null) {
                                    EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                    if (eventlistener != null) {

                                        EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                    }
                                }
                                try {
                                    if (dataSnapshot.getValue() != null) {

                                        Intent intent_bid_succes = new Intent(mcontext, Driver_ActivityBidSuccess.class);

                                        Map<String, String> usersMap_data = (Map<String, String>) dataSnapshot.getValue();
                                        JSONObject usersJSON = null;
                                        usersJSON = new JSONObject(usersMap_data);

                                        String selected_id = dataSnapshot.getKey();
                                        String user_fullname = dataSnapshot.child("user_fullname").getValue().toString();
                                        String distance_value = dataSnapshot.child("distance").getValue().toString();
                                        String user_pp = dataSnapshot.child("user_pp").getValue().toString();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("id", selected_id);

                                        bundle.putString("user_fullname", user_fullname);
                                        bundle.putString("user_pp", user_pp);
                                        bundle.putString("distance_value", distance_value);
                                        bundle.putString("user_data_for", usersJSON.toString());

                                        intent_bid_succes.putExtras(bundle);


                                        mcontext.startActivity(intent_bid_succes);
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            } else {
                                //jobListRequest1();

                            }

                        }

                    }
                }


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // jobListRequest1();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }

    /* Save data to server */
    private void SaveQuiteTimeTOServer() {

        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
            CustomProgressBarDialog.progressDialog.show();

            HashMap<String, String> userDetails = session.getUserDetails();
            String userID = userDetails.get(SessionManager.KEY_ID);

            /* make dataList array ready for send params */

            List<String> stringList = new ArrayList<>();

            if (dataList.size() > 0) {

                for (int i = 0; i < dataList.size(); i++) {

                    HashMap<String, String> mapData = dataList.get(i);

                    String periodName = mapData.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME);
                    String startTime = mapData.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM);
                    String toTime = mapData.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO);
                    String status = mapData.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS);

                    StringBuilder builder = new StringBuilder();
                    builder.append(periodName + ", ");
                    builder.append(startTime + ", ");
                    builder.append(toTime + ", ");
                    builder.append(status);

                    stringList.add(builder.toString());

                    Log.e("String List ::", stringList.toString());

                }
            }

            HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", userID);
            for (int i = 0; i < stringList.size(); i++) {
                params.put("items[" + i + "]", stringList.get(i));
            }

            Log.e("SEND PARAMS ::", params.toString());


            CustomRequest request = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.DRIVER_SET_QUITE_TIME_URL, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Response", response.toString());

                    try {
                        boolean error = response.getBoolean("error");

                        if (!error) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.sucess_set_time));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("Error: ", error.getMessage());

                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            });
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);
            EApplication.getInstance().addToRequestQueue(request, "low");


        } else NetworkUtils.showNoConnectionDialog(getActivity());

    }    /* Save data to server */

    private void SaveQuiteTimeTOServer1() {

        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
            CustomProgressBarDialog.progressDialog.show();

            HashMap<String, String> userDetails = session.getUserDetails();
            String userID = userDetails.get(SessionManager.KEY_ID);

            /* make dataList array ready for send params */

            List<String> stringList = new ArrayList<>();

            if (dataList.size() > 0) {

                for (int i = 0; i < dataList.size(); i++) {

                    HashMap<String, String> mapData = dataList.get(i);

                    String periodName = mapData.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME);
                    String startTime = mapData.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM);
                    String toTime = mapData.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO);
                    String status = mapData.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS);

                    StringBuilder builder = new StringBuilder();
                    builder.append(periodName + ", ");
                    builder.append(startTime + ", ");
                    builder.append(toTime + ", ");
                    builder.append(status);

                    stringList.add(builder.toString());

                    Log.e("String List ::", stringList.toString());

                }
            }

            HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", userID);
            for (int i = 0; i < stringList.size(); i++) {
                params.put("items[" + i + "]", stringList.get(i));
            }

            Log.e("SEND PARAMS ::", params.toString());


            CustomRequest request = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.DRIVER_SET_QUITE_TIME_URL, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Response", response.toString());

                    try {
                        boolean error = response.getBoolean("error");

                        if (!error) {
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.delete_quit_time));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("Error: ", error.getMessage());

                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }) {
                public String language;


                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            EApplication.getInstance().addToRequestQueue(request, "high");


        } else NetworkUtils.showNoConnectionDialog(getActivity());

    }

    /* Methods to set updated data to list view */
    private static void setUpdatedDataToListView(ArrayList<HashMap<String, String>> dataList) {

        AdapterSettingQuiteTime adapter = new AdapterSettingQuiteTime(mActivity, dataList);
        quiteTimeListView.setAdapter(adapter);

    }

    /* Callback Methods to update Clock Time in HashMap ArrayList */
    public static void setQuiteTime(String time, int position, String actionChangeField) {

//        ArrayList<HashMap<String, String>> tempDataList = new ArrayList<>();

        if (actionChangeField.equals("Start Time")) {

            /*for (int i = 0; i < dataList.size(); i++) {

                if (i == position) {

                    HashMap<String, String> mapDataChanged = dataList.get(position);

                    String endTime = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO);
                    String status = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS);
                    String periodName = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME);

                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM, time);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO, endTime);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME, periodName);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS, status);

                    tempDataList.add(mapDataChanged);

                } else {

                    HashMap<String, String> sameMapData = dataList.get(i);
                    tempDataList.add(sameMapData);
                }
            }*/

            dataList.get(position).put(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM, time);


        } else {

            /*for (int i = 0; i < dataList.size(); i++) {

                if (i == position) {

                    HashMap<String, String> mapDataChanged = dataList.get(position);

                    String startTime = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM);
                    String status = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS);
                    String periodName = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME);

                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM, startTime);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO, time);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME, periodName);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS, status);

                    tempDataList.add(mapDataChanged);

                } else {

                    HashMap<String, String> sameMapData = dataList.get(i);
                    tempDataList.add(sameMapData);
                }

            }*/

            dataList.get(position).put(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO, time);
        }

        /*dataList = tempDataList;*/
        setUpdatedDataToListView(dataList);
        Log.e("Data ::", dataList.toString());

    }

    /* Callback Methods to update Switch Event in HashMap ArrayList */
    public static void setQuiteTimeSwitchAction(boolean isChecked, int position) {
        ArrayList<HashMap<String, String>> tempDataList = new ArrayList<>();

        if (isChecked) {

            for (int i = 0; i < dataList.size(); i++) {

                if (i == position) {

                    HashMap<String, String> mapDataChanged = dataList.get(position);

                    String startTime = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM);
                    String endTime = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO);
                    String periodName = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME);

                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM, startTime);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO, endTime);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME, periodName);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS, "1");

                    tempDataList.add(mapDataChanged);

                } else {

                    HashMap<String, String> sameMapData = dataList.get(i);
                    tempDataList.add(sameMapData);
                }
            }

        } else {
            for (int i = 0; i < dataList.size(); i++) {

                if (i == position) {

                    HashMap<String, String> mapDataChanged = dataList.get(position);

                    String startTime = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM);
                    String endTime = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO);
                    String periodName = mapDataChanged.get(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME);

                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_FROM, startTime);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_TO, endTime);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_NAME, periodName);
                    mapDataChanged.put(DBTableFields.DRIVER_SETTING_QUITE_TIME_STATUS, "0");

                    tempDataList.add(mapDataChanged);

                } else {

                    HashMap<String, String> sameMapData = dataList.get(i);
                    tempDataList.add(sameMapData);
                }

            }
        }

        dataList = tempDataList;
        setUpdatedDataToListView(dataList);
        Log.e("Data ::", dataList.toString());
    }
}
