package com.EddyCabLatest.application.moduleDriver.modal;

/**
 * Created by User on 8/24/2015.
 */
public class MessageItms {
String id;
    String user_id;
    String message;
    String type;
    String sendon;
    String created_at;
    String updated_at;
    String timeSinceUpdate;

    public MessageItms(String message, String timeSinceUpdate) {
        this.message = message;
        this.timeSinceUpdate = timeSinceUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSendon() {
        return sendon;
    }

    public void setSendon(String sendon) {
        this.sendon = sendon;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getTimeSinceUpdate() {
        return timeSinceUpdate;
    }

    public void setTimeSinceUpdate(String timeSinceUpdate) {
        this.timeSinceUpdate = timeSinceUpdate;
    }
}
