package com.EddyCabLatest.application.moduleDriver.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dell on 9/14/2015.
 */
public class DriverHelpActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar app_toolBar;
    Button about_btn_call_customer;
    Button about_btn_Submit;
    EditText edt_about_feedback;
    SessionManager session;
    EApplication app;
    private boolean visible = false;
    private TextView apptitle_toolbar_driver_help;
    private EditText phone_feedback_person;
    private EditText email_feedback;
    private String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user_help);

        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        // Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);

        about_btn_call_customer = (Button) findViewById(R.id.about_btn_call_customer);
        about_btn_call_customer.setOnClickListener(this);

        about_btn_Submit = (Button) findViewById(R.id.about_btn_Submit);
        about_btn_Submit.setOnClickListener(this);
        apptitle_toolbar_driver_help = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
        apptitle_toolbar_driver_help.setText(getResources().getString(R.string.help_n_Support));
        phone_feedback_person = (EditText) findViewById(R.id.phone_feedback_person);
        email_feedback = (EditText) findViewById(R.id.email_feedback);
        edt_about_feedback = (EditText) findViewById(R.id.edt_about_feedback);
        edt_about_feedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edt_about_feedback.getText().toString().trim().length() >= 5) {
                    about_btn_Submit.setEnabled(true);
                    about_btn_Submit.setBackgroundColor(getResources().getColor(R.color.primary));
                } else {
                    about_btn_Submit.setEnabled(false);
                    about_btn_Submit.setBackgroundColor(getResources().getColor(R.color.light_gray));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onResume() {
        visible = true;
        super.onResume();
    }

    @Override
    public void onDestroy() {
        visible = false;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        visible = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_btn_call_customer:

                String number = "tel:" + "+97714410178";
                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                startActivity(callIntent);
                break;

            case R.id.about_btn_Submit:
                postFeedBackApi();
                break;
        }
    }

    boolean isEmailValid(CharSequence email_feedback) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email_feedback).matches();
    }

    private void postFeedBackApi() {


        String user_feedback_value = email_feedback.getText().toString();
        String mobile_feedback_value = phone_feedback_person.getText().toString();

        if (user_feedback_value.equalsIgnoreCase("") && mobile_feedback_value.equalsIgnoreCase("")) {

            phone_feedback_person.setError(getResources().getString(R.string.mobile_no));
            email_feedback.setError(getResources().getString(R.string.email_address));


        } else if (user_feedback_value.equalsIgnoreCase("")) {
            email_feedback.setError(getResources().getString(R.string.email_address));
        } else if (mobile_feedback_value.equalsIgnoreCase("")) {
            phone_feedback_person.setError(getResources().getString(R.string.mobile_no));
        } else if (!user_feedback_value.equalsIgnoreCase("") && (!mobile_feedback_value.equalsIgnoreCase(""))) {

            if (isEmailValid(user_feedback_value)) {

                if (NetworkUtils.isNetworkAvailable(DriverHelpActivity.this)) {
                    CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(DriverHelpActivity.this);
                    CustomProgressBarDialog.progressDialog.setCancelable(false);
                    if (visible) CustomProgressBarDialog.progressDialog.show();
                    final HashMap<String, String> params = new HashMap<>();
                    params.put("feedback", "my email:" + email_feedback.getText().toString() + "My phone :" + phone_feedback_person.getText().toString() + edt_about_feedback.getText().toString().trim());
                    Log.e("Parameters", params.toString());

                    String feedback_url = Config.APP_BASE_URL + Config.SUPPORT_FEEDBACK_POST_URL;
                    Log.e("feedback_url ::", feedback_url);
                    CustomRequest req = new CustomRequest(Request.Method.POST, feedback_url, params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.e("response", response.toString());
                                    try {
                                        boolean error = response.getBoolean("error");
                                        Log.e("ERROR", String.valueOf(error));
                                        if (!error) {
                                            CustomProgressBarDialog.progressDialog.setCancelable(true);
                                            CustomProgressBarDialog.progressDialog.hide();
                                            showSucessDialouge();

                                        } else {
                                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                                            CustomProgressBarDialog.progressDialog.setCancelable(true);
                                            CustomProgressBarDialog.progressDialog.hide();
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CustomProgressBarDialog.progressDialog.setCancelable(true);
                            CustomProgressBarDialog.progressDialog.hide();
                            EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                        }
                    }) {
                        @Override
                        public Request.Priority getPriority() {
                            return Request.Priority.HIGH;
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                            String current_language = session.getAppLanguage();
                            Log.e("current_language", current_language);
                            if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                language = "en";
                            }
                            if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                language = "np";
                            }
                            headers.put("lang", language);


                            return headers;
                        }
                    };
                    EApplication.getInstance().addToRequestQueue(req, "low");

                } else {
                    NetworkUtils.showNoConnectionDialog(DriverHelpActivity.this);
                }

            } else {

                email_feedback.setError(getResources().getString(R.string.error_email_address_not_valid));
            }
        }

    }

    private void showSucessDialouge() {
        final Dialog dialog = new Dialog(DriverHelpActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.thanks_feedback);
        dialog.setCancelable(true);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_about_feedback.setText("");
                phone_feedback_person.setText("");
                email_feedback.setText("");
                finish();

                dialog.dismiss();
            }
        });
        if (visible) dialog.show();
    }
}
