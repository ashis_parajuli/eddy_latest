package com.EddyCabLatest.application.moduleDriver.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.NewGPSTracker;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Driver_ActivityJobComplete extends AppCompatActivity implements View.OnClickListener {

    TextView txt_from_location;
    TextView txt_to_location;
    Button btn_job_complete;
    Toolbar app_toolBar;

    private MapView mapView;
    private GoogleMap map;
    NewGPSTracker gps;
    private Polyline polyline;
    private double distance;
    private LatLng user_pickupLocation;
    private LatLng user_dropOffLocation;
    private String driver_id;
    private String reservation_id;
    private String ride_fare;
    private String paid_unpaid;
    private int user_useCredit;
    String pickTitle;
    String pickAddress;
    double user_pickupLatitude;
    double user_pickupLongitude;
    double dropoffLatitude;
    double dropoffLongitude;
    String dropTitle;
    String dropAddress;
    SessionManager session;
    int mapPadding;
    private boolean visible = false;
    String TAG = "===";


    private Intent intent;
    private ServiceConnection mConnection;
    private float speed;
    private double totalWaiting;
    private EditText tips;
    private TextView ride_fare_txt;
    private TextView total_txt_fee, distance_txt;
    private Double total_taxi_charge;
    private Double tips_value = 0.0;
    private TextView date;
    private TextView booking_fee_value;
    private String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_arrived_destination);
        EApplication.getInstance().showNotification(this, Driver_OnRouteActivity.class, 001, "Goto Track your user", getResources().getString(R.string.Click_here), getResources().getString(R.string.jobComplete));
        initializaMap(savedInstanceState);
        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        session = new SessionManager(Driver_ActivityJobComplete.this);
        mapPadding = EApplication.getInstance().getMapPadding();

        mConnection = new ServiceConnection() {

            public void onServiceConnected(ComponentName className, IBinder binder) {

               /* Toast.makeText(Driver_ActivityJobComplete.this, "binded inside a job complete", Toast.LENGTH_SHORT).show();*/
                Log.e("binded", "binded from service");
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            }

            public void onServiceDisconnected(ComponentName className) {
                // Do something
              /*  mIsBound = false;*/
                //mIsBound = false;
            }
        };
      /*  intent = new Intent(Driver_ActivityJobComplete.this, MyService.class);

        startService(intent);*/

        LocalBroadcastManager.getInstance(Driver_ActivityJobComplete.this).registerReceiver(
                broadcastReceiver, new IntentFilter("speedExceeded"));


        // Activity Loading Animation
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        initView();

        Intent i = getIntent();
      /*  ride_fare = i.getStringExtra("intent_ridefare");*/
        user_pickupLatitude = i.getDoubleExtra("intent_picklat", 0.0);
        user_pickupLongitude = i.getDoubleExtra("intent_picklong", 0.0);
        dropoffLatitude = i.getDoubleExtra("intent_droplat", 0.0);
        dropoffLongitude = i.getDoubleExtra("intent_droplong", 0.0);
        driver_id = i.getStringExtra("intent_driver_id");
        reservation_id = i.getStringExtra("intent_reservation_id");
        pickTitle = i.getStringExtra("intent_pickuptitle");
        pickAddress = i.getStringExtra("intent_pickupAddress");
        dropTitle = i.getStringExtra("intent_drop_title");
        dropAddress = i.getStringExtra("intent_dropAddress");
        Log.e(TAG, "dropAddress:" + dropAddress);
        user_useCredit = i.getIntExtra("intent_usercredit", 0);
        oldlattitude = i.getDoubleExtra("old_lat", 0.0);
        oldlongitude = i.getDoubleExtra("old_long", 0.0);

    /*distance_travelled= i.getDoubleExtra("distance",0);
    totalWaitingTime= i.getDoubleExtra("waiting_time",0);*/
        Log.e(TAG, "user_useCredit:" + oldlattitude + "and" + oldlongitude);


        gps = new NewGPSTracker(Driver_ActivityJobComplete.this);

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(oldlattitude, oldlongitude), 20));
            }
        });
        btn_job_complete.setOnClickListener(this);
        txt_from_location.setText(pickTitle + ", " + pickAddress);

        txt_to_location.setText(dropTitle + ", " + dropAddress);
        user_pickupLocation = new LatLng(user_pickupLatitude, user_pickupLongitude);
        user_dropOffLocation = new LatLng(dropoffLatitude, dropoffLongitude);

       methodCheckArrival();


    }


    private double longitudeFromService;
    private double lattitudeFromService;
    private double oldlongitude;
    private double oldlattitude;


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {


        public double i = 0;

        @Override
        public void onReceive(Context context, Intent intent) {
            //intent.getAction();
    /*	Log.e("data", "onReceive: Received");
        oldlattitude = intent.getExtras().getDouble("latitudeold");
		oldlongitude = intent.getExtras().getDouble("longitudeold");*/
            //Log.e("intent data", "" + intent.getStringExtra("distance"));
            distance = intent.getExtras().getDouble("distance");
            speed = intent.getExtras().getFloat("speed");
            totalWaiting = intent.getExtras().getDouble("waitingtime");
            longitudeFromService = intent.getExtras().getDouble("longitude");
            lattitudeFromService = intent.getExtras().getDouble("latitude");
            Log.e("distance", distance + "and" + totalWaiting);

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lattitudeFromService, longitudeFromService), 10);
            map.animateCamera(cameraUpdate);


          /*  map.addPolyline(new PolylineOptions()

                    .add(new LatLng(oldlattitude, oldlongitude), new LatLng(lattitudeFromService, longitudeFromService))
                    .width(5)
                    .color(Color.RED));
            oldlattitude = lattitudeFromService;
            oldlongitude = longitudeFromService;*/
            distance_txt.setText(formatNumber(distance) + "");

            if (i < 2) {
                oldlattitude = lattitudeFromService;
                oldlongitude = longitudeFromService;
                i++;
            } else {
                map.addPolyline(new PolylineOptions()
                        .add(new LatLng(oldlattitude, oldlongitude), new LatLng(lattitudeFromService, longitudeFromService))
                        .width(5)
                        .color(Color.RED));
                oldlattitude = lattitudeFromService;
                oldlongitude = longitudeFromService;


		/*price = flag_down + distance * cost_per_200_meters + totalWaiting * waiting_charge;
        pricetxt.setText("" + price);*/
      /*  Log.e("intent locaition", "onReceive:" + totalWaiting);*/
       /* plotTheInitialMarkers();*/
       /* displayViews();*/


            }



        /*price = flag_down + distance * cost_per_200_meters + totalWaiting * waiting_charge;
        pricetxt.setText("" + price);*/
      /*  Log.e("intent locaition", "onReceive:" + totalWaiting);*/
       /* plotTheInitialMarkers();*/
       /* displayViews();*/
           /* distancetext.setText(formatNumber(distance));
            waitingtxt.setText(totalWaiting + "mins");*/
/*		pricetxt.setText(10 + "RS");*/
            //unbindService(mConnection);
        }

        private String formatNumber(double distance) {
            String unit = "m";
            if (distance < 1) {
                distance *= 100;
                unit = "mm";
            } else if (distance > 1000) {
                distance /= 1000;
                unit = "km";
            }

            return String.format("%4.3f%s", distance, unit);
        }


    };

    private void initView() {

        txt_from_location = (TextView) findViewById(R.id.pickup_location_source);
        txt_to_location = (TextView) findViewById(R.id.pickup_location_destination);
        btn_job_complete = (Button) findViewById(R.id.btn_job_complete);
        distance_txt = (TextView) findViewById(R.id.distance_txt);
    }

    @Override
    public void onResume() {
       /* EApplication.getInstance().showNotification(this, Driver_ActivityJobComplete.class, 001, "Goto Track your user", "Click Here", "Go to track your user");*/
        visible = true;
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        visible = false;
        mapView.onDestroy();
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        visible = false;
        mapView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    private void methodCheckArrival() {
        if (gps.canGetLocation()) {
            double driver_current_latitude = gps.getLatitude();
            double driver_currentLongitude = gps.getLongitude();

            Log.e("driver_current_latitude", String.valueOf(driver_current_latitude));
            Log.e("driver_currentLongitude", String.valueOf(driver_currentLongitude));

            String url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" + user_pickupLocation.latitude + ","
                    + user_pickupLocation.longitude + "&destinations=" + driver_current_latitude + "," + driver_currentLongitude + "&mode=driving&language=en-EN&sensor=false";

            Log.e("distanceMatrix", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.e("matrixResponse", response.toString());
                                JSONObject responseObject = new JSONObject(response);
                                JSONArray objectArray = responseObject.getJSONArray("rows");
                                JSONArray arrayElements = objectArray.getJSONObject(0).getJSONArray("elements");

                                distance = arrayElements.getJSONObject(0).getJSONObject("distance").getInt("value");
                                if (distance <= 50) {


                                    jobReachedDialouge();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {


                @Override
                public void onErrorResponse(VolleyError error) {

                }
            })

            {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }



                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);


                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);

            EApplication.getInstance().addToRequestQueue(stringRequest, "high");

        } else {
            gps.showSettingsAlert();
        }

    }

    private void jobReachedDialouge() {
        final Dialog dialog = new Dialog(Driver_ActivityJobComplete.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_success_failure_dialog);
        dialog.setCancelable(true);
        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(getResources().getString(R.string.job_complete));
        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        dialog_action.setBackgroundColor(getResources().getColor(R.color.green));
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        if (visible) dialog.show();


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_job_complete:
                API_JOB_COMPLETE_EXECUTE();

                break;
        }
    }

    private void API_JOB_COMPLETE_EXECUTE() {

        if (NetworkUtils.isNetworkAvailable(Driver_ActivityJobComplete.this)) {

            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_ActivityJobComplete.this);
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            if (visible) CustomProgressBarDialog.progressDialog.show();

            final HashMap<String, String> params = new HashMap<>();
            params.put("driver_id", driver_id);
            params.put("reservation_id", reservation_id);
            params.put("distance", String.valueOf(distance));
            params.put("waiting_numbers", String.valueOf(totalWaiting));
            Log.i("===", "ResponseDriverConfirmed: " + params.toString());
            Log.e("===", "Parameters" + params.toString());


            StringRequest request = new StringRequest(Request.Method.POST, Config.APP_BASE_URL + Config.DRIVER_JOB_COMPLETE_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String responseObject) {
                    Log.i("===", "ResponseDriverConfirmed:" + responseObject.toString());
                    String newResponse = responseObject;

                    if (responseObject.contains("ï»¿")) {
                        newResponse = newResponse.substring(newResponse.indexOf("{"));
                    }

                    try {
                        JSONObject response = new JSONObject(newResponse);
                        boolean error_ack = response.getBoolean("error");

                        if (!error_ack) {
                            ride_fare = response.getString("total_taxi_charge");

                            CustomProgressBarDialog.progressDialog.dismiss();
                            EApplication.getInstance().setUserIsNear500(false);
                            Log.e("ride_fare", ride_fare);
                         /*   if (user_useCredit == 0) {
                                int oldCredit = Integer.parseInt(session.getUserBananaCredit());
                                Log.e(TAG, "" + oldCredit);
                                String newCredit = String.valueOf(oldCredit - 1);
                                Log.e(TAG, "" + newCredit);
                                session.setUserBananaCredit(newCredit);
                            }*/
                            generateBillingDialogForPayment();

                        } else {
                            CustomProgressBarDialog.progressDialog.dismiss();
                            Intent intent = new Intent(Driver_ActivityJobComplete.this, Driver_MainActivity.class);
                            startActivity(intent);
                            finish();
                        }


                    } catch (JSONException e) {
                        CustomProgressBarDialog.progressDialog.dismiss();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "errorJobComplete:" + error);
                    CustomProgressBarDialog.progressDialog.dismiss();

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(request, "high");



        } else {
            NetworkUtils.showNoConnectionDialog(Driver_ActivityJobComplete.this);
        }
    }

    private void generateBillingDialogForPayment() {

        Log.e("where_am_I", "im here");

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_dialog_send_receipt, null);
        final Dialog dialog = new Dialog(Driver_ActivityJobComplete.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(v);
        tips = (EditText) v.findViewById(R.id.tips);
        date = (TextView) v.findViewById(R.id.date);

        String Datetime;
        Calendar c = Calendar.getInstance();
        date.setText("" + c.get(Calendar.DATE) + "-" + getMonth(c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.YEAR));

        ride_fare_txt = (TextView) v.findViewById(R.id.ride_fee);
        total_txt_fee = (TextView) v.findViewById(R.id.txt_total_fare);
        booking_fee_value = (TextView) v.findViewById(R.id.booking_distance);
        booking_fee_value.setText(distance + "");
        total_txt_fee.setText(ride_fare);
        ride_fare_txt.setText(ride_fare);
        Log.e("where_am_I", "im here 0");

        tips.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
              /*  tips.setText("0.0");*/
                total_txt_fee.setText(ride_fare);
                ride_fare_txt.setText(ride_fare);

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                String string_value_tips = tips.getText().toString();
                if (string_value_tips == null || string_value_tips.isEmpty()) {

                  /*  tips.setText(0);*/

                } else {

                    tips_value = Double.parseDouble(tips.getText().toString());

                }


                total_taxi_charge = tips_value + Double.parseDouble(ride_fare);
                total_txt_fee.setText("RS " + total_taxi_charge + "");
            }
        });
        Button btn_send = (Button) v.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Log.e("where_am_I", "im here 1");

                dialog.dismiss();
                CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_ActivityJobComplete.this);
                if (visible) CustomProgressBarDialog.progressDialog.show();
                CustomProgressBarDialog.progressDialog.setCancelable(false);
                tips_value = Double.parseDouble(tips.getText().toString());
                HashMap<String, String> params = new HashMap<>();
               /* params.put("1", "10");*/
                params.put("ride_fare", String.valueOf(total_taxi_charge));
                params.put("use_credit", String.valueOf(user_useCredit));
                params.put("reservation_id", reservation_id);
             /*   params.put("ride_tips", tips_value);
*/
                Log.e("DRIVER_PAY_VOUCHER_URL", "" + params);

                CustomRequest requestVoucher = new CustomRequest(Request.Method.POST, Config.APP_BASE_URL + Config.DRIVER_PAY_VOUCHER_URL, params, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Voucher Response::", response.toString());
                        try {
                            boolean error_ack = response.getBoolean("error");
                            if (!error_ack) {

                             /*   int journeyfee = Integer.parseInt(ride_fare) + 10;*/
                                Log.e(TAG, "journeyfee:" + ride_fare);
                                CustomProgressBarDialog.progressDialog.dismiss();
                                HashMap<String, String> data = new HashMap<>();
                                data.put("driver_id", response.getString("driver_id"));
                                data.put("user_id", response.getString("user_id"));
                                data.put("reservation_id", reservation_id);
                                data.put("ride_fare", String.valueOf(total_taxi_charge));
                                ride_fare_txt.setText(String.valueOf(ride_fare + ""));
                /*                EApplication.setUserRateByDriver(true);*/
                                EApplication.putUserRateData(data);
                               /* Intent intent = new Intent(Driver_ActivityJobComplete.this, Driver_MainActivity.class);
                                startActivity(intent);*/

                                generateSuccessFailureDialog(data);

                            } else {
                                EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                                Intent intent = new Intent(Driver_ActivityJobComplete.this, Driver_MainActivity.class);
                                CustomProgressBarDialog.progressDialog.dismiss();
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            CustomProgressBarDialog.progressDialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                        CustomProgressBarDialog.progressDialog.dismiss();

                    }
                })


                {

                    @Override
                    public Request.Priority getPriority() {
                        return Request.Priority.HIGH;
                    }
                };
                requestVoucher.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                EApplication.getInstance().addToRequestQueue(requestVoucher, "high");


            }
        });


        Button btn_dont_send = (Button) v.findViewById(R.id.btn_dont_send);
        btn_dont_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               /* EApplication.getInstance().setUserRateByDriver(true);*/
                Intent intent = new Intent(Driver_ActivityJobComplete.this, Driver_MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        if (visible) dialog.show();


    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    private void generateSuccessFailureDialog(final HashMap<String, String> data) {

        final Dialog dialog = new Dialog(Driver_ActivityJobComplete.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reservation_bidding_dailog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);

        TextView WarningMsg = (TextView) dialog.findViewById(R.id.msgTitleBidding);
        WarningMsg.setText(R.string.rate_user_dialog_title);
        WarningMsg.setTextColor(getResources().getColor(R.color.darkGreenColor));

        TextView msgContentBidding = (TextView) dialog.findViewById(R.id.msgContentBidding);
        msgContentBidding.setText(R.string.rate_user_dialog_message);
        WarningMsg.setTextColor(getResources().getColor(R.color.secondaryTextColor));

        Button btnActionSubmit = (Button) dialog.findViewById(R.id.btnActionSubmit);
        btnActionSubmit.setText(R.string.rate_now);
        btnActionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EApplication.getInstance().setUserRateByDriver(true);
               /* EApplication.setUserRateByDriver(true);*/
                EApplication.putUserRateData(data);
                Intent intent = new Intent(Driver_ActivityJobComplete.this, Driver_MainActivity.class);
                startActivity(intent);
            }
        });

        Button btnActionCancel = (Button) dialog.findViewById(R.id.btnActionCancel);
        btnActionCancel.setText(R.string.remind_later);
        btnActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                Intent intent = new Intent(Driver_ActivityJobComplete.this, Driver_MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

        if (visible) dialog.show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initializaMap(Bundle savedInstanceState) {

        MapsInitializer.initialize(Driver_ActivityJobComplete.this);
        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(Driver_ActivityJobComplete.this)) {
            case ConnectionResult.SUCCESS:
                mapView = (MapView) findViewById(R.id.content_map_view);
                mapView.onCreate(savedInstanceState);
                if (mapView != null) {
                    map = mapView.getMap();
                    map.getUiSettings().setMyLocationButtonEnabled(true);
                    map.getUiSettings().setRotateGesturesEnabled(false);
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    map.setMyLocationEnabled(true);
                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;
            default:

        }
    }


}
