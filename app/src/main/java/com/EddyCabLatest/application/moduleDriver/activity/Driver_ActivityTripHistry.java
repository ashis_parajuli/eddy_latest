package com.EddyCabLatest.application.moduleDriver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.utilities.NewGPSTracker;
import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Driver_ActivityTripHistry extends AppCompatActivity implements RoutingListener {

    //variables declared to load Map in Activity
    private MapView mapView;
    private GoogleMap map;
    Toolbar app_toolBar;
    TextView pickup_location_source, dropoff_location, txt_eta_time, txt_distance;
    TextView txt_status_value, txt_user_name, txt_user_address, txt_redg_no;
    /*    CircularImageView img_circ_driver;*/
    NewGPSTracker gps;
    String distance;
    String duration;

    Double latitudePickup;
    Double longitudePickup;
    Double latitiudeDropOff;
    Double longitudeDropOff;
    LatLng user_pickupLocation;
    LatLng user_dropOffLocation;
    Polyline polyline;


    String dateTime, ridefare, pickupLocation, dropoffLocation, tripStatus, profileUser, pickupLatitude, pickupLongitude, dropLatitude, dropLongitude;
    private String user_name;
    private String user_address;
    private String user_id;
    private ImageView cancel_Resercvation;
    private SessionManager session;
    private String language;
    private ChildEventListener eventlistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_drivertriphistry);

        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        gps = new NewGPSTracker(Driver_ActivityTripHistry.this);

        methodGetIntent();
session=new SessionManager(this);
       /* pickup_location_source = (TextView) findViewById(R.id.pickup_location_source);*/
  /*      dropoff_location = (TextView) findViewById(R.id.pickup_location_destination);*/
        txt_status_value = (TextView) findViewById(R.id.txt_status);
     /*   txt_total_fare = (TextView) findViewById(R.id.txt_total_fare);*/
        txt_eta_time = (TextView) findViewById(R.id.txt_text1);
 /*       txt_distance = (TextView) findViewById(R.id.txt_distance);*/
        cancel_Resercvation = (ImageView) findViewById(R.id.img_cancel_reservation);
        if (tripStatus.equalsIgnoreCase("cancelled") || tripStatus.equalsIgnoreCase("completed")) {
            cancel_Resercvation.setVisibility(View.GONE);
        } else {
            cancel_Resercvation.setVisibility(View.VISIBLE);
        }
        cancel_Resercvation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_user_address = (TextView) findViewById(R.id.txt_user_address);
        txt_redg_no = (TextView) findViewById(R.id.txt_reg_no);
/*        pickup_location_source.setText(pickupLocation);*/
      /*  dropoff_location.setText(dropoffLocation);*/
        if (tripStatus.equalsIgnoreCase("cancelled")) {
            txt_status_value.setTextColor(getResources().getColor(R.color.redColor));
            txt_status_value.setText(getResources().getString(R.string.cancelled));
        }
        if (tripStatus.equalsIgnoreCase("completed")) {
            txt_status_value.setTextColor(getResources().getColor(R.color.green));
            txt_status_value.setText(getResources().getString(R.string.completed));
        }
        if (tripStatus.equalsIgnoreCase("new")) {
            txt_status_value.setTextColor(getResources().getColor(R.color.green));
            txt_status_value.setText(getResources().getString(R.string.new_stat));
        }
        txt_status_value.setText(tripStatus);
        txt_redg_no.setText(user_id);
        txt_user_name.setText(user_name);
        txt_user_address.setText(user_address);
      /*  txt_total_fare.setText(ridefare);*/

        //values to plot route on Map
        latitudePickup = Double.parseDouble(pickupLatitude);
        Log.e("pLatitude", latitudePickup.toString());
        longitudePickup = Double.parseDouble(pickupLongitude);
        Log.e("pLatitude", longitudePickup.toString());
        user_pickupLocation = new LatLng(latitudePickup, longitudePickup);

        latitiudeDropOff = Double.parseDouble(dropLatitude);
        Log.e("pLatitude", latitiudeDropOff.toString());
        longitudeDropOff = Double.parseDouble(dropLongitude);
        Log.e("pLatitude", longitudeDropOff.toString());
        user_dropOffLocation = new LatLng(latitiudeDropOff, longitudeDropOff);

      /*  Picasso.with(Driver_ActivityTripHistry.this)
                .load(Config.APP_BASE_URL + profileUser)
                .placeholder(R.drawable.contact_avatar)   // optional
                .error(R.drawable.contact_avatar)         // optional
                .resize(100, 100)                   // optional
                .rotate(0)                          // optional
                .into(img_circ_driver);*/
        methodCalculateDistance();


        //map Init Section
        mapView = (MapView) findViewById(R.id.content_map_view);
        mapView.onCreate(savedInstanceState);

        //Gets map to mapView and other initializations for the map in MapView
        map = mapView.getMap();
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(false);
        map.setMyLocationEnabled(true);


        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        MapsInitializer.initialize(Driver_ActivityTripHistry.this);

        Routing routing = new Routing(Routing.TravelMode.DRIVING);
        routing.registerListener(Driver_ActivityTripHistry.this);
        routing.execute(user_pickupLocation, user_dropOffLocation);

    }

    private void methodCalculateDistance() {
        if (gps.canGetLocation()) {


            String URL_DISTANCE = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" + pickupLatitude + ","
                    + pickupLongitude + "&destinations=" + dropLatitude + "," + dropLongitude + "&mode=driving&language=en-EN&sensor=false";

            Log.e("URL", URL_DISTANCE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DISTANCE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject responseObject = new JSONObject(response);
                                JSONArray objectArray = responseObject.getJSONArray("rows");
                                JSONArray arrayElements = objectArray.getJSONObject(0).getJSONArray("elements");

                                distance = arrayElements.getJSONObject(0).getJSONObject("distance").getString("text");

                                Log.e("distance", distance);

                                duration = arrayElements.getJSONObject(0).getJSONObject("duration").getString("text");
                                Log.e("duration", duration);
                                Log.e("DISTANCE::", String.valueOf(distance));
                                txt_eta_time.setText(duration);
                             /*   txt_distance.setText(getResources().getString(R.string.dis) + " " + distance);*/

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            })

            {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }



                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {

                        language="np";
                    }
                    headers.put("lang", language);


                    return headers;
                }
            };
            EApplication.getInstance().addToRequestQueue(stringRequest, "low");

        } else {
            gps.showSettingsAlert();
        }


    }


    public void getjoblistfirebase() {

        eventlistener = EApplication.ref.child(user_id + "/new_reservations/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    if ((dataSnapshot.child("accepted").getValue() .equals(false))&&(dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("new"))) {
                        Intent intent= new Intent(Driver_ActivityTripHistry.this,Driver_MainActivity.class);
                        startActivity(intent);


                        EApplication.getInstance().showNotification(Driver_ActivityTripHistry.this, Driver_MainActivity.class, 011, getResources().getString(R.string.new_job), getResources().getString(R.string.new_job), getResources().getString(R.string.new_job_avaiable));


                    }


                    Log.e("data_joblist", dataSnapshot + "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.getValue() != null) {

                    if (dataSnapshot.child("accepted").exists()) {
                        if (dataSnapshot.child("accepted").getValue() .equals( true)) {
                            //EApplication.ref.child(userID + "/new_reservations/" + dataSnapshot.getKey() + "/accepted/").removeValue();
                            EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                            if (eventlistener != null) {
                                EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                if (eventlistener != null) {

                                    EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                }
                            }
                            try {
                                if (dataSnapshot.getValue() != null) {

                                    Intent intent_bid_succes = new Intent(Driver_ActivityTripHistry.this, Driver_ActivityBidSuccess.class);

                                    Map<String, String> usersMap_data = (Map<String, String>) dataSnapshot.getValue();
                                    JSONObject usersJSON = null;
                                    usersJSON = new JSONObject(usersMap_data);

                                    String selected_id = dataSnapshot.getKey();
                                    String user_fullname = dataSnapshot.child("user_fullname").getValue().toString();
                                    String distance_value = dataSnapshot.child("distance").getValue().toString();
                                    String user_pp = dataSnapshot.child("user_pp").getValue().toString();

                                    Bundle bundle = new Bundle();
                                    bundle.putString("id", selected_id);

                                    bundle.putString("user_fullname", user_fullname);
                                    bundle.putString("user_pp", user_pp);
                                    bundle.putString("distance_value", distance_value);
                                    bundle.putString("user_data_for", usersJSON.toString());

                                    intent_bid_succes.putExtras(bundle);

                                    startActivity(intent_bid_succes);
                                }
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        } else {
                            //jobListRequest1();

                        }

                    }

                }



            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // jobListRequest1();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }
    private void methodGetIntent() {
        dateTime = getIntent().getStringExtra("trip_time");
        Log.e("name", dateTime + "");
        if (TextUtils.isEmpty(dateTime) || TextUtils.equals(dateTime, "null")) {
            dateTime = getResources().getString(R.string.not_null);
        }

        ridefare = getIntent().getStringExtra("ride_fare");

        if (TextUtils.isEmpty(ridefare) || TextUtils.equals(ridefare, "null")) {
            ridefare = getResources().getString(R.string.not_null);
        }

        pickupLocation = getIntent().getStringExtra("from");
        Log.e("pickUpLocation", pickupLocation);
        if (TextUtils.isEmpty(pickupLocation) || TextUtils.equals(pickupLocation, "null")) {
            pickupLocation = getResources().getString(R.string.not_null);
        }

        dropoffLocation = getIntent().getStringExtra("to");
        Log.e("dropLocation", dropoffLocation);
        if (TextUtils.isEmpty(dropoffLocation) || TextUtils.equals(dropoffLocation, "null")) {
            dropoffLocation = getResources().getString(R.string.not_null);
        }

        tripStatus = getIntent().getStringExtra("status");
        Log.e("status", tripStatus);
        if (TextUtils.isEmpty(tripStatus) || TextUtils.equals(tripStatus, "null")) {
            tripStatus = getResources().getString(R.string.not_null);
        }

        profileUser = getIntent().getStringExtra("user_profile");
        Log.e("profile", profileUser);
        if (TextUtils.isEmpty(tripStatus) || TextUtils.equals(tripStatus, "null")) {
            profileUser = getResources().getString(R.string.not_null);
        }

        user_name = getIntent().getStringExtra("full_name");
        Log.e("name", user_name);
        if (TextUtils.isEmpty(tripStatus) || TextUtils.equals(tripStatus, "null")) {
            user_name = getResources().getString(R.string.not_null);
        }

        user_address = getIntent().getStringExtra("address");
        Log.e("address", user_name);
        if (TextUtils.isEmpty(tripStatus) || TextUtils.equals(tripStatus, "null")) {
            user_address = getResources().getString(R.string.not_null);
        }

        user_id = getIntent().getStringExtra("user_id");
        Log.e("use_id", user_name);
        if (TextUtils.isEmpty(tripStatus) || TextUtils.equals(tripStatus, "null")) {
            user_id = getResources().getString(R.string.not_null);
        }


        pickupLatitude = getIntent().getStringExtra("pick_latitude");
        Log.e("pickLatitude", pickupLatitude);

        pickupLongitude = getIntent().getStringExtra("pick_longitude");
        Log.e("pickUpLongitude", pickupLongitude);

        dropLatitude = getIntent().getStringExtra("drop_latitude");
        Log.e("dropUpLatitude", dropLatitude);

        dropLongitude = getIntent().getStringExtra("drop_longitude");
        Log.e("dropLongitude", dropLongitude);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
        getjoblistfirebase();

    }

    @Override
    public void onDestroy() {

        if(eventlistener!=null)
        {

            EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(eventlistener);
        }
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_driver_trip_histry, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRoutingFailure() {
        // The Routing request failed
        EApplication.getInstance().showToastMessageFunction("No Route found");
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(user_pickupLocation);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(5);

        map.moveCamera(center);
        map.animateCamera(zoom);
        if (polyline != null)
            polyline.remove();

        polyline = null;

        //adds route to the map.
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.primary_dark));
        polyOptions.width(20);
        polyOptions.addAll(mPolyOptions.getPoints());
        polyline = map.addPolyline(polyOptions);

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(user_pickupLocation);
        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));
        map.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(user_dropOffLocation);
        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_driverpin));
        map.addMarker(options);


        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {


                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(user_pickupLocation);
                builder.include(user_dropOffLocation);
                LatLngBounds bounds = builder.build();
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));


            }


        });

    }
}
