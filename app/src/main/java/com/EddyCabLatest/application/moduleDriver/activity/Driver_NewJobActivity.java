package com.EddyCabLatest.application.moduleDriver.activity;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleUser.modal.JoblistItems;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.NewGPSTracker;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.utilities.gpsTrackerUser;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Driver_NewJobActivity extends AppCompatActivity implements View.OnClickListener,
        RoutingListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static ArrayList<JoblistItems> modalDataList;

    TextView txt_distance, txt_date, txt_from_location, txt_driver_note;
    Button btn_bid_now;
    Toolbar app_toolBar;

    //Relative Layout for Note section
    TextView ll_driver_note;
    public String language;


    NewGPSTracker GPSLocation;
    SessionManager session;

    String reservation_ID, driver_ID;


    String to_location;

    Double pick_lat, pick_long;

    String note;
    String date, time, distance, paddingStr;
    private int mapPadding = 40;

    //variables declared to load Map in Activity
    private MapView mapView;
    private GoogleMap map;

    //for routing purpose
    LatLng pickupLatLang;

    private Polyline polyline;


    EApplication app;
    Geocoder geocoder;
    List<Address> addresses;
    private boolean visible = false;

    private String Location_Title = "", Location_Address = "";
    private int position;
    private Routing routing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_new_job_for_bid);


        //ToolBar SetUp
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        app = EApplication.getInstance();
        GPSLocation = new NewGPSTracker(Driver_NewJobActivity.this);
        geocoder = new Geocoder(Driver_NewJobActivity.this, Locale.getDefault());


        position = getIntent().getExtras().getInt("position");
        Log.e("modelcheck", position + "");

        to_location = modalDataList.get(position).getTo();
        pick_lat = Double.valueOf(modalDataList.get(position).getPick_lat());
        pick_long = Double.valueOf(modalDataList.get(position).getPick_lang());

        note = modalDataList.get(position).getNote();
        reservation_ID = modalDataList.get(position).getReservationID();
        Log.e("Reservation Id >>>>>:", (reservation_ID));
        session = new SessionManager(Driver_NewJobActivity.this);

        HashMap<String, String> userDetails = session.getUserDetails();
        driver_ID = userDetails.get(SessionManager.KEY_ID);
        /* referencing resource views + set value */
        txt_driver_note = (TextView) findViewById(R.id.txt_driver_note);
        txt_driver_note.setText(getResources().getString(R.string.notes) + " : " + note);

        //If note is unavailable then hide Note

        if (note == null) {
            ll_driver_note = (TextView) findViewById(R.id.txt_driver_note);
            ll_driver_note.setVisibility(View.GONE);    // doSomething
        }
        txt_from_location = (TextView) findViewById(R.id.pickup_location_source);


        btn_bid_now = (Button) findViewById(R.id.btn_bid_now);
        btn_bid_now.setOnClickListener(this);

        //map Init Section
        mapView = (MapView) findViewById(R.id.content_map_view);
        mapView.onCreate(savedInstanceState);
      /*      mapView.onCreate(savedInstanceState);*/
        map = mapView.getMap();
        //Gets map to mapView and other initializations for the map in MapView

        map.getUiSettings().setZoomControlsEnabled(true);

        map.getUiSettings().setRotateGesturesEnabled(false);
        map.setMyLocationEnabled(true);
        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        MapsInitializer.initialize(Driver_NewJobActivity.this);

        //method called to draw Route between source and destination
        pickupLatLang = new LatLng(pick_lat, pick_long);


        Log.e("Message", "Draw route ....");

        routing = new Routing(Routing.TravelMode.DRIVING);
        routing.registerListener(Driver_NewJobActivity.this);
        NewGPSTracker gpsTracker = new NewGPSTracker(Driver_NewJobActivity.this);
        if (gpsTracker.canGetLocation()) {


            routing.execute(pickupLatLang, new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()));
        }

         /* Methods call to calculate ETA */
        calcTimeDistance();


    }

    @Override
    public void onResume() {

        super.onResume();
        visible = true;
        mapView.onResume();
    }

    @Override
    public void onDestroy() {
        visible = false;

        mapView.onDestroy();

        super.onDestroy();

    }

    @Override
    protected void onPause() {
        visible = false;
        mapView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        visible = false;
        mapView.onLowMemory();
        super.onLowMemory();

    }

    private void calcTimeDistance() {
        NewGPSTracker gpsTracker = new NewGPSTracker(Driver_NewJobActivity.this);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(pickupLatLang);

        if (gpsTracker.canGetLocation()) {
            builder.include(new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()));
        }


        final LatLngBounds bounds = builder.build();

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                gpsTrackerUser gps_tracker = new gpsTrackerUser(Driver_NewJobActivity.this);


                LatLngBounds.Builder builder_data = new LatLngBounds.Builder();
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude())).zoom(10).build();

                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if ((pickupLatLang != null)) {
                    builder_data.include(pickupLatLang);

                    if (gps_tracker.canGetLocation()) {
                        builder_data.include(new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude()));

                    }

                }
                LatLngBounds bounds_data = builder_data.build();

                int padding_in_dp = 30;  // 6 dps
                final float scale = getResources().getDisplayMetrics().density;
                int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds_data, padding_in_px));
                map.setPadding(12, 20, 30, 30);

            }


        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_bid_now:


                if (NetworkUtils.isNetworkAvailable(Driver_NewJobActivity.this)) {
                    if (GPSLocation.canGetLocation()) {


                        HashMap<String, String> params = new HashMap<>();
                        params.put("reservation_id", String.valueOf(reservation_ID));
                        params.put("driver_id", driver_ID);
                        params.put("bidding_amount", "0");
                        params.put("location_lat", String.valueOf(GPSLocation.getLatitude()));
                        params.put("location_long", String.valueOf(GPSLocation.getLongitude()));
                        params.put("location_title", "kathmandu");
                        params.put("location_address", "Nepal");
                        Log.e("===", "Parameters" + params.toString());

                        methodsBiddingAPIExecute(params);
                    } else {
                        GPSLocation.showSettingsAlert();
                    }
                } else {
                    NetworkUtils.showNoConnectionDialog(Driver_NewJobActivity.this);
                }
                break;
        }
    }

    private void methodsBiddingAPIExecute(final HashMap<String, String> params) {
        Log.e("Required Params ::", params.toString());
        CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_NewJobActivity.this);
        CustomProgressBarDialog.progressDialog.setCancelable(false);
        if (!CustomProgressBarDialog.progressDialog.isShowing()) {
            if (visible) CustomProgressBarDialog.progressDialog.show();
        }

        StringRequest stringRequestMethod = new StringRequest(Request.Method.POST,
                Config.APP_BASE_URL + Config.DRIVER_BID_POST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        CustomProgressBarDialog.progressDialog.dismiss();
                        Log.e("Response ::", response);
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            Log.e("Converted JSON", "" + responseObject);

                            boolean error = responseObject.getBoolean("error");

                            if (!error) {
                                //EApplication.ref.child(driver_ID + "/new_reservations/" + reservation_ID + "/bidding").setValue(1);
                                biddingSuccessFailureResponse(getResources().getString(R.string.success), getResources().getString(R.string.SELECT_success));

                            } else {
                                String error_msg = responseObject.getString("message");
                                if (error_msg.equals("You have already bid for this reservation"))
                                    biddingSuccessFailureResponse("FAILURE", error_msg);
                                else if (error_msg.equals("Your credit is not enough. Please add banana"))
                                    biddingSuccessFailureResponse("FAILURE", error_msg);
                                else
                                    biddingSuccessFailureResponse("FAILURE", error_msg);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Response Error ::", "" + error);
                CustomProgressBarDialog.progressDialog.dismiss();
                Toast.makeText(Driver_NewJobActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                EApplication.getInstance().showToastMessageFunction("Error Occurred on sending post message to set Booking");
                VolleyLog.e("Error", error.toString());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                }
                if (error instanceof TimeoutError) {
                    Log.e("Driver_New_JObs_Volley", "TimeoutError");
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                } else if (error instanceof NoConnectionError) {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                    Log.e("Driver_New_JObs_Volley", "NoConnectionError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("Driver_New_JObs_Volley", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("Driver_New_JObs_Volley", "ServerError");
                } else if (error instanceof NetworkError) {
                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.no_network));
                    Log.e("Driver_New_JObs_Volley", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.e("Driver_New_JObs_Volley", "ParseError");
                }
            }
        }) {

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.HIGH;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> param = params;
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                String current_language = session.getAppLanguage();
                Log.i("current_language", current_language);
                if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                    language = "en";
                }
                if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                    language = "np";
                }
                headers.put("lang", language);
                return headers;
            }


        };
        int socketTimeout = 9000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequestMethod.setRetryPolicy(policy);
        app.addToRequestQueue(stringRequestMethod, "high");

    }


    private void biddingSuccessFailureResponse(String title, String message) {

        final Dialog dialog = new Dialog(Driver_NewJobActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_success_failure_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);

        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        dialog_title.setText(title);

        TextView txt_message = (TextView) dialog.findViewById(R.id.dialog_message);
        txt_message.setText(message);

        Button btnOK = (Button) dialog.findViewById(R.id.dialog_action);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // EApplication.ref.child(driver_ID + "/new_reservations/" + reservation_ID).removeValue();
                EApplication.getInstance().setfromdrivernewjob(true);
                String ns = Driver_NewJobActivity.this.NOTIFICATION_SERVICE;
                NotificationManager nMgr = (NotificationManager) Driver_NewJobActivity.this.getSystemService(ns);
                nMgr.cancel(011);
                dialog.dismiss();
                EApplication.getInstance().setfromdrivernewjob(true);
                Intent intent = new Intent(getApplicationContext(), Driver_MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                finish();
            }
        });
        if (!dialog.isShowing()) {
            if (visible) dialog.show();
        }
    }


    public static void setRequiredData(ArrayList<JoblistItems> dataList) {
        modalDataList = dataList;
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onRoutingFailure() {

        // The Routing request failed
        /*CustomProgressBarDialog.progressDialog.hide();*/
        Toast.makeText(this, "No Route found", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {


        if (polyline != null)
            polyline.remove();

        polyline = null;
        //adds route to the map.
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.primary_dark));
        polyOptions.width(10);
        polyOptions.addAll(mPolyOptions.getPoints());
        polyline = map.addPolyline(polyOptions);
        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(pickupLatLang);
        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.eddy_userpin));
        map.addMarker(options);
        // End marker


        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //calcTimeDistance();


                gpsTrackerUser gps_tracker = new gpsTrackerUser(Driver_NewJobActivity.this);


                LatLngBounds.Builder builder_data = new LatLngBounds.Builder();


                if ((pickupLatLang != null)) {
                    builder_data.include(pickupLatLang);

                    if (gps_tracker.canGetLocation()) {
                        builder_data.include(new LatLng(gps_tracker.getLatitude(), gps_tracker.getLongitude()));
                    }

                }

                LatLngBounds bounds_data = builder_data.build();
                int padding_in_dp = 40;  // 6 dps
                final float scale = getResources().getDisplayMetrics().density;
                int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds_data, padding_in_px));
                map.setPadding(12, 40, 30, 30);
            }


        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), Driver_MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

  /*  @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
    }*/

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("Connection Failed >>>", connectionResult.toString());
    }
}