package com.EddyCabLatest.application.moduleDriver.activity;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CircularImageView;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Driver_ActivityBidSuccess extends AppCompatActivity implements View.OnClickListener {


    String driver_bid_response_object;


    //Variables used to reference Views
    TextView txt_user_name;
    /*  TextView txt_user_loc_title;
      TextView txt_user_loc_address;*/
    TextView txt_arriving_time;
    /* TextView txt_route_fee;*/
    TextView txt_paid_unpaid;
    ImageButton btn_next;

    SessionManager session;

    String TAG = "===";
    String language = "";

    String reservation_id;
    Handler handler;
    Runnable runable;
    boolean nextBoolean = false;
    boolean ignoreBoolean = false;
    private boolean visible = false;

    boolean fromGCM;
    private NotificationCompat.Builder mBuilder;
    private ArrayList<HashMap<String, String>> driver_bid_response_object_array;
    private String user_id;
    private String user_full_name;
    private String user_distance;
    private String user_json_for_route;
    private String user_pp;
    private CircularImageView user_profile_pic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_driver_activity_sucess);
        session = new SessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);
        session = new SessionManager(Driver_ActivityBidSuccess.this);
        reservation_id = getIntent().getStringExtra("id");
        user_full_name = getIntent().getStringExtra("user_fullname");
        user_distance = getIntent().getStringExtra("distance_value");
        user_pp = getIntent().getStringExtra("user_pp");

        user_json_for_route = getIntent().getStringExtra("user_data_for");
        initViews();


        // user_full_name=
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
    }


    @Override
    public void onResume() {

        //EApplication.getInstance().showNotification(this, Driver_ActivityBidSuccess.class, 001, "Goto Track your user", getResources().getString(R.string.bid_succes), getResources().getString(R.string.acceptorcancel));
        super.onResume();
        visible = true;
    }

    @Override
    public void onDestroy() {
        visible = false;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        visible = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;
        super.onStop();
    }

    //methods to initialize views
    private void initViews() {
        if ((user_distance != null) || (user_distance != "")) {
            txt_user_name = (TextView) findViewById(R.id.txt_user_name);

            txt_arriving_time = (TextView) findViewById(R.id.txt_arriving_time);
            user_profile_pic = (com.EddyCabLatest.application.views.CircularImageView) findViewById(R.id.user_profile);
            Picasso.with(this)
                    .load(Config.APP_BASE_URL + user_pp)
                    .placeholder(R.drawable.contact_avatar)   // optional
                    .error(R.drawable.contact_avatar)         // optional
                    .rotate(0)                             // optional
                    .into(user_profile_pic);

            btn_next = (ImageButton) findViewById(R.id.btn_next);
            btn_next.setOnClickListener(this);

            txt_user_name.setText(user_full_name);
            txt_arriving_time.setText(formatNumber(Double.parseDouble(user_distance)) + "..." + getResources().getString(R.string.away));
            EApplication.getInstance().showNotification(Driver_ActivityBidSuccess.this, Driver_ActivityBidSuccess.class, 011, getResources().getString(R.string.bid_succes), getResources().getString(R.string.congratulation), getResources().getString(R.string.success));
     /*   txt_paid_unpaid = (TextView) findViewById(R.id.txt_paid_unpaid);*/
        }

    }

    //handles event handling on View Of layout inflated
    @Override

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(011);
/*EApplication.ref.removeEventListener(EApplication.ref.e);*/
                reserveAssignToUser();
                break;

            default:
                break;
        }
    }

    //Method to assign Reservation To User
    private void reserveAssignToUser() {
        if (NetworkUtils.isNetworkAvailable(Driver_ActivityBidSuccess.this)) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_ActivityBidSuccess.this);
            if (visible) CustomProgressBarDialog.progressDialog.show();
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            final HashMap<String, String> params = new HashMap<>();
            params.put("reservation_id", reservation_id);
            params.put("driver_id", user_id);
            Log.e("Parameters", params.toString());
            String REQUESTURL = Config.APP_BASE_URL + Config.DRIVER_RESERVATION_ASSIGN_POST;
            StringRequest jsObjRequest = new StringRequest(Request.Method.POST, REQUESTURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String responseObject) {
                            Log.e(TAG, "ReservationResponse" + responseObject);
                            String newResponse = responseObject;
                            if (responseObject.contains("ï»¿")) {
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }
                            try {
                                JSONObject response = new JSONObject(newResponse);
                                boolean error = response.getBoolean("error");
                                String message = response.getString("message");
                                if (!error) {
                                    //   EApplication.ref.child(user_id + "/new_reservations/" + reservation_id + "/accepted/").removeValue();
                                    Intent onRouteIntent = new Intent(Driver_ActivityBidSuccess.this, Driver_OnRouteActivity.class);


                                    onRouteIntent.putExtra("PASSED_RESPONSE", user_json_for_route);
                                    Log.e(TAG, "On Next Button" + driver_bid_response_object);
                                    startActivity(onRouteIntent);
                                    finish();
                                } else {
                                    ShowErrorResponseDialoge(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            CustomProgressBarDialog.progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                /*    Log.e("Error: ", error.getMessage());*/

                    EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.check_yoor_connection));
                    CustomProgressBarDialog.progressDialog.dismiss();
                }
            }
            )

            {
                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            int socketTimeout = 20000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsObjRequest.setRetryPolicy(policy);

            EApplication.getInstance().addToRequestQueue(jsObjRequest, "high");

        } else {

            NetworkUtils.showNoConnectionDialog(Driver_ActivityBidSuccess.this);
        }
    }

    //Dialoge to show error Response when accepting User by driver
    private void ShowErrorResponseDialoge(String message) {
        final Dialog dialog = new Dialog(Driver_ActivityBidSuccess.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_success_failure_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        dialog_title.setText("");

        TextView dialog_message = (TextView) dialog.findViewById(R.id.dialog_message);
        dialog_message.setText(message);

        Button dialog_action = (Button) dialog.findViewById(R.id.dialog_action);
        dialog_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Driver_ActivityBidSuccess.this, Driver_MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        if (visible) dialog.show();

    }

    //Method to cancel User Booking
    private void reservationIgnoreToUser() {

        if (NetworkUtils.isNetworkAvailable(Driver_ActivityBidSuccess.this)) {

            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_ActivityBidSuccess.this);
            CustomProgressBarDialog.progressDialog.setCancelable(false);
            CustomProgressBarDialog.progressDialog.show();
            HashMap<String, String> reservationWithDriver = session.getReservationIdWithDriver();
            String RESERVATION_ID = reservationWithDriver.get(SessionManager.RESERVATION_ID);
            String DRIVER_ID = reservationWithDriver.get(SessionManager.DRIVER_ID);

            final HashMap<String, String> params = new HashMap<>();
            params.put("reservation_id", reservation_id);
            params.put("driver_id", user_id);
            Log.e("===", "Parameters is:" + params.toString());

            String REQUESTURL = Config.APP_BASE_URL + Config.DRIVER_RESERVATION_IGNORE_POST;

            StringRequest jsObjRequest = new StringRequest(Request.Method.POST, REQUESTURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String responseObject) {


                            String newResponse = responseObject;
                            if (responseObject.contains("ï»¿")) {
                                newResponse = newResponse.substring(newResponse.indexOf("{"));
                            }


                            try {
                                JSONObject response = new JSONObject(newResponse);
                                boolean error = response.getBoolean("error");
                                String message = response.getString("message");
                                Log.e("===", "ResponseAccept" + response.toString());
                                if (!error) {
                                    EApplication.noOfJobsBidded -= 1;
                                    EApplication.ref.child(user_id).removeValue();
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                    Log.e(TAG, "Reservation Ignored Success");
                                    Intent bidIntent = new Intent(Driver_ActivityBidSuccess.this, Driver_MainActivity.class);
                                    startActivity(bidIntent);
                                    finish();
                                } else {
                                    CustomProgressBarDialog.progressDialog.dismiss();
                                    ShowErrorResponseDialoge(message);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                CustomProgressBarDialog.progressDialog.dismiss();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("Error: ", error);
                    CustomProgressBarDialog.progressDialog.dismiss();
                    Log.e(TAG, "Error occurred on posting Reservation Ignored  to User");


                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.e("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };

            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EApplication.getInstance().addToRequestQueue(jsObjRequest, "high");

        } else {
            NetworkUtils.showNoConnectionDialog(Driver_ActivityBidSuccess.this);
        }

    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        //generateWarningDialog();
        EApplication.getInstance().showToastMessageFunction("Please ignore or accept user proceed");
    }

    private void generateWarningDialog() {
        final Dialog dialog = new Dialog(Driver_ActivityBidSuccess.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reservation_bidding_dailog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams paramLayout = new WindowManager.LayoutParams();
        paramLayout.copyFrom(dialog.getWindow().getAttributes());
        paramLayout.width = WindowManager.LayoutParams.MATCH_PARENT;
        paramLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
        paramLayout.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(paramLayout);

        TextView WarningMsg = (TextView) dialog.findViewById(R.id.msgTitleBidding);
        WarningMsg.setText(R.string.warningTitle);
        WarningMsg.setTextColor(getResources().getColor(R.color.orange));

        TextView msgContentBidding = (TextView) dialog.findViewById(R.id.msgContentBidding);
        msgContentBidding.setText(R.string.backPressWarningMsg);
        WarningMsg.setTextColor(getResources().getColor(R.color.secondaryTextColor));

        Button btnActionSubmit = (Button) dialog.findViewById(R.id.btnActionSubmit);
        btnActionSubmit.setText(R.string.confirm_request);
        btnActionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                reserveAssignToUser();

            }
        });
        Button btnActionCancel = (Button) dialog.findViewById(R.id.btnActionCancel);
        btnActionCancel.setText(R.string.ignore_request);
        btnActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                reservationIgnoreToUser();
            }

        });

        if (visible) dialog.show();

    }


    private String formatNumber(double distance) {
        String unit = getResources().getString(R.string.km);
        if (distance < 1) {
            distance *= 1000;
            unit = getResources().getString(R.string.meter);
        }

        return String.format("%4.3f%s", distance, unit);
    }

    private void passResponseToInstance(String s) {

        driver_bid_response_object = s;

    }

}
