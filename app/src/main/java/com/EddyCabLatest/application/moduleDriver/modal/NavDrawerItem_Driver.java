package com.EddyCabLatest.application.moduleDriver.modal;

/**
 * Created by User on 6/30/2015.
 */
public class NavDrawerItem_Driver {
    String title;

    public NavDrawerItem_Driver(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
