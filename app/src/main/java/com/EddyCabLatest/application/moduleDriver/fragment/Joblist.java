package com.EddyCabLatest.application.moduleDriver.fragment;


/**
 * Created by User on 3/14/2016.
 */


public class Joblist {
    public static String bidding;

    public static String dropoff;
    public static String id;
    public static String location_pickup_title;

    public static String  location_dropoff_title;

    public static String  location_pickup_address;
    public static String  location_dropoff_address;


    public static String  location_pickup_longitude;
    public static String location_pickup_latitude;

    public static String location_dropoff_latitude;
    public static String location_dropoff_longitude;
    public static String notes;
    public static String pickup;
    public static String status;
    public static String user_id;

public Joblist(String user_id, String status, String notes, String location_dropoff_longitude, String location_dropoff_latitude, String location_pickup_latitude, String location_pickup_longitude, String location_pickup_title, String location_dropoff_title, String location_pickup_address, String location_dropoff_address, String id, String dropoff, String pickup, String bidding)
{


    this.user_id=user_id;
    this.status= status;
    this.notes= notes;
    this.location_dropoff_longitude=location_dropoff_longitude;
    this.location_dropoff_latitude= location_dropoff_latitude;
    this.location_pickup_latitude=location_pickup_latitude;
    this.location_pickup_longitude= location_pickup_longitude;
    this.location_pickup_title=location_pickup_title;
    this.location_dropoff_title=location_dropoff_title;
    this.location_pickup_address=location_pickup_address;
    this.location_dropoff_address= location_dropoff_address;
    this.id= id;
    this.pickup=pickup;
    this.dropoff=dropoff;
    this.bidding=bidding;



}

    public Joblist(String location_pickup_title, String location_dropoff_title, String bidding) {

        this.location_pickup_title=location_pickup_title;
        this.location_dropoff_title=location_dropoff_title;
        this.bidding=bidding;


    }


    public static String getBidding() {
        return bidding;
    }

    public static void setBidding(String bidding) {
        Joblist.bidding = bidding;
    }
    public static String getDropoff() {
        return dropoff;
    }

    public static void setDropoff(String dropoff) {
        Joblist.dropoff = dropoff;
    }
    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        Joblist.id = id;
    }
    public static String getLocation_pickup_title() {
        return Joblist.location_pickup_title;
    }

    public static void setLocation_pickup_title(String location_pickup_title) {
        Joblist.location_pickup_title = location_pickup_title;
    }

    public static String getLocation_dropoff_title() {
        return Joblist.location_dropoff_title;
    }

    public static void setLocation_dropoff_title(String location_dropoff_title) {
        Joblist.location_dropoff_title = location_dropoff_title;
    }
    public static String getLocation_pickup_address() {
        return location_pickup_address;
    }

    public static void setLocation_pickup_address(String location_pickup_address) {
        Joblist.location_pickup_address = location_pickup_address;
    }
    public static String getLocation_dropoff_address() {
        return location_dropoff_address;
    }

    public static void setLocation_dropoff_address(String location_dropoff_address) {
        Joblist.location_dropoff_address = location_dropoff_address;
    }
    public static String getLocation_pickup_longitude() {
        return location_pickup_longitude;
    }

    public static void setLocation_pickup_longitude(String location_pickup_longitude) {
        Joblist.location_pickup_longitude = location_pickup_longitude;
    }
    public static String getLocation_pickup_latitude() {
        return location_pickup_latitude;
    }

    public static void setLocation_pickup_latitude(String location_pickup_latitude) {
        Joblist.location_pickup_latitude = location_pickup_latitude;
    }
    public static String getLocation_dropoff_latitude() {
        return location_dropoff_latitude;
    }

    public static void setLocation_dropoff_latitude(String location_dropoff_latitude) {
        Joblist.location_dropoff_latitude = location_dropoff_latitude;
    }

    public static String getLocation_dropoff_longitude() {
        return location_dropoff_longitude;
    }

    public static void setLocation_dropoff_longitude(String location_dropoff_longitude) {
        Joblist.location_dropoff_longitude = location_dropoff_longitude;
    }

    public static String getNotes() {
        return Joblist.notes;
    }

    public static void setNotes(String notes) {
        Joblist.notes = notes;
    }

    public static String getPickup() {
        return pickup;
    }

    public static void setPickup(String pickup) {
        Joblist.pickup = pickup;
    }

    public static String getStatus() {
        return status;
    }

    public static void setStatus(String status) {
        Joblist.status = status;
    }

    public static String getUser_id() {
        return user_id;
    }

    public static void setUser_id(String user_id) {
        Joblist.user_id = user_id;
    }

    /*@Override
    public String toString() { return "status"+status +"user_id"+id +"drop_off"+location_dropoff_title;}
*/


}
