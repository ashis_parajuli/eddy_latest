package com.EddyCabLatest.application.moduleDriver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.SelectUserType;
import com.EddyCabLatest.application.utilities.SessionManager;

/**
 * Created by dell on 9/14/2015.
 */
public class DriverLoginScreenActivity extends AppCompatActivity {


    Toolbar app_toolBar;
    TextView login;
    Button tour, help, createAccount;
    private boolean visible = false;
    private TextView apptitle_toolbar_driver_help;
    SessionManager session;
    EApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_drawer_login);

        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        apptitle_toolbar_driver_help = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
        apptitle_toolbar_driver_help.setText(getResources().getString(R.string.title_activity_driver_login));
        session = new SessionManager(getApplicationContext());
        init();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentLogin = new Intent(DriverLoginScreenActivity.this, Driver_Activity_Login.class);
                startActivity(intentLogin);
                finish();
            }
        });

        tour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentTourLogout = new Intent(DriverLoginScreenActivity.this, Driver_ActivityTourSlider.class);
                startActivity(intentTourLogout);
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentHelpLogout = new Intent(DriverLoginScreenActivity.this, DriverHelpActivity.class);
                startActivity(intentHelpLogout);
            }
        });

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverLoginScreenActivity.this, DriverRegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private void init() {
        login = (TextView) findViewById(R.id.login_user);
        tour = (Button) findViewById(R.id.btn_tour_logout);
        help = (Button) findViewById(R.id.btn_help_logout);
        createAccount = (Button) findViewById(R.id.create_account);
        if ((session.getAppLanguage()) != "en") {
            login.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            tour.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            help.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            createAccount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent_user = new Intent(DriverLoginScreenActivity.this, SelectUserType.class);
            startActivity(intent_user);
            this.finish();
            overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent_user = new Intent(DriverLoginScreenActivity.this, SelectUserType.class);
        startActivity(intent_user);
        this.finish();
        overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
    }
}
