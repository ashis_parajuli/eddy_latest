package com.EddyCabLatest.application.moduleDriver.fragment;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_ActivityBidSuccess;
import com.EddyCabLatest.application.moduleDriver.activity.Driver_MainActivity;
import com.EddyCabLatest.application.utilities.CustomRequest;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.utilities.swipe.Attributes;
import com.EddyCabLatest.application.utilities.swipe.ListViewAdapter;
import com.EddyCabLatest.application.utilities.swipe.SwipeLayout;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Driver_FragmentMessages extends Fragment {


    private SessionManager session;

    private EApplication app;
    String user_id;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    TextView empty_msg;
    ListView messagesListView;
    private ListViewAdapter mAdapter;
    private String language;

    private ChildEventListener eventlistener;
    private Context mcontext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_fragment_messages, container, false);

        // Initializing Application Instances
        app = EApplication.getInstance();
        session = new SessionManager(getActivity());
        mcontext=container.getContext();
        //gets user id for getting messages from API
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);


        //referencing ListView
        messagesListView = (ListView) rootView.findViewById(R.id.messagesListView);
        empty_msg = (TextView) rootView.findViewById(R.id.empty_message);
        messagesListView.setEmptyView(empty_msg);
        mAdapter = new ListViewAdapter(getActivity(), dataList);
        mAdapter.setInflater((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        messagesListView.setAdapter(mAdapter);
        mAdapter.setMode(Attributes.Mode.Single);

        messagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((SwipeLayout) (messagesListView.getChildAt(position - messagesListView.getFirstVisiblePosition()))).open(true);
            }
        });

        messagesListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                mAdapter.mItemManger.closeAllItems();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        getjoblistfirebase();
    }

    @Override
    public void onDestroy() {

        if(eventlistener!=null)
        {

            EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(eventlistener);
        }
        super.onDestroy();
    }

    private void executeDeleteFunction(String notification_id) {

        CustomRequest req = new CustomRequest(Request.Method.GET, Config.APP_BASE_URL + Config.DELETE_NOTIFICATION_MESSAGE + notification_id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean error = response.getBoolean("error");
                            if (!error) {
                                Log.e("response", response.toString());
                            } else {
                                EApplication.getInstance().showToastMessageFunction("Error true: " + error);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                EApplication.getInstance().showToastMessageFunction("Error Listiner active");
            }
        });
        EApplication.getInstance().addToRequestQueue(req, "high");
    }


    private String timeConversion(int seconds) {
        String h = " hrs ";
        String d = " days ";
        String m = " mins ";
        String s = " secs ";
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);

        if (day <= 1) {
            d = " day ";
        }
        if (hours <= 1) {
            h = " hr ";
        }
        if (minute <= 1) {
            m = " min ";
        }
        if (second <= 1) {
            s = " sec ";
        }

        if (day == 0) {

            if (hours == 0) {
                if (minute == 0) {
                    if (second == 0) {
                        return getResources().getString(R.string.not_null);
                    } else {
                        return second + s;
                    }

                } else {
                    return minute + m + second + s;
                }

            } else {
                return hours + h + minute + m + second + s;
            }

        } else {
            return day + d + hours + h + minute + m + second + s;
        }

    }

    public void getjoblistfirebase() {
        eventlistener = EApplication.ref.child(user_id + "/new_reservations/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    if ((dataSnapshot.child("accepted").getValue() .equals(false)) && (dataSnapshot.child("status").getValue().toString().equalsIgnoreCase("new"))) {
                        Intent intent = new Intent(mcontext, Driver_MainActivity.class);


if(isAdded())
                        startActivity(intent);

                        if (isAdded()) {
                            EApplication.getInstance().showNotification(mcontext, Driver_MainActivity.class, 011, getResources().getString(R.string.new_job), getResources().getString(R.string.new_job), getResources().getString(R.string.new_job_avaiable));

                        }
                    }
                    else
                    {

                        getMessageFromAPI();
                    }


                    Log.e("data_joblist", dataSnapshot + "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (isAdded()) {
                    if (dataSnapshot.getValue() != null) {

                        if (dataSnapshot.child("accepted").exists()) {
                            if (dataSnapshot.child("accepted").getValue() .equals(true)) {
                                //EApplication.ref.child(userID + "/new_reservations/" + dataSnapshot.getKey() + "/accepted/").removeValue();
                                EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                if (eventlistener != null) {
                                    EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                    if (eventlistener != null) {

                                        EApplication.ref.child(user_id + "/new_reservations/").removeEventListener(this);
                                    }
                                }
                                try {
                                    if (dataSnapshot.getValue() != null) {

                                        Intent intent_bid_succes = new Intent(mcontext, Driver_ActivityBidSuccess.class);

                                        Map<String, String> usersMap_data = (Map<String, String>) dataSnapshot.getValue();
                                        JSONObject usersJSON = null;
                                        usersJSON = new JSONObject(usersMap_data);

                                        String selected_id = dataSnapshot.getKey();
                                        String user_fullname = dataSnapshot.child("user_fullname").getValue().toString();
                                        String distance_value = dataSnapshot.child("distance").getValue().toString();
                                        String user_pp = dataSnapshot.child("user_pp").getValue().toString();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("id", selected_id);

                                        bundle.putString("user_fullname", user_fullname);
                                        bundle.putString("user_pp", user_pp);
                                        bundle.putString("distance_value", distance_value);
                                        bundle.putString("user_data_for", usersJSON.toString());

                                        intent_bid_succes.putExtras(bundle);


                                        mcontext.startActivity(intent_bid_succes);
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            } else {
                               // getMessageFromAPI();

                            }

                        }

                    }
                }


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // jobListRequest1();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }

    public void getMessageFromAPI() {
        if (NetworkUtils.isNetworkAvailable(getActivity())) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(getActivity());
            CustomProgressBarDialog.progressDialog.show();

            CustomRequest requestForMessages = new CustomRequest(Request.Method.GET, Config.APP_BASE_URL + Config.GET_MESSAGES_FROM_GCM_URL + user_id, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            CustomProgressBarDialog.progressDialog.dismiss();
                            Log.e("response>>", response.toString());
                            try {
                                boolean error = response.getBoolean("error");
                                if (!error) {

                                    JSONArray notificationsArray = response.getJSONArray("notifications");
                                    for (int i = 0; i < notificationsArray.length(); i++) {

                                        HashMap<String, String> mapData = new HashMap<>();

                                        JSONObject notificationObject = notificationsArray.getJSONObject(i);
                                        String notification_id = notificationObject.getString("id");
                                        String user_id = notificationObject.getString("user_id");
                                        String message = notificationObject.getString("message");
                                        String type = notificationObject.getString("type");
                                        String timeSinceUpdate = notificationObject.getString("timeSinceUpdate");
                                        int sinceUpdateTime = Integer.parseInt(timeSinceUpdate);
                                        String formattedTime = timeConversion(sinceUpdateTime);

                                        mapData.put("notification_id", notification_id);
                                        mapData.put("user_id", user_id);
                                        mapData.put("message", message);
                                        mapData.put("type", type);
                                        mapData.put("time", formattedTime);

                                        dataList.add(mapData);

                                    }

                                    mAdapter.notifyDataSetChanged();

                                    /*final MessageAdapter adapter = new MessageAdapter(dataList, getActivity());
                                    messagesListView.setAdapter(adapter);

                                    final SwipeToDismissTouchListener<ListViewAdapter> touchListener =
                                            new SwipeToDismissTouchListener<>(
                                                    new ListViewAdapter(messagesListView),
                                                    new SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter>() {
                                                        @Override
                                                        public boolean canDismiss(int position) {
                                                            return true;
                                                        }

                                                        @Override
                                                        public void onDismiss(ListViewAdapter view, int position) {
                                                            //adapter.remove(position);

                                                            executeDeleteFunction(dataList.get(position).get("notification_id"));
                                                        }
                                                    });
                                    messagesListView.setOnTouchListener(touchListener);
                                    // Setting this scroll listener is required to ensure that during ListView scrolling,
                                    // we don't look for swipes.
                                    messagesListView.setOnScrollListener((AbsListView.OnScrollListener) touchListener.makeScrollListener());
                                    messagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            if (touchListener.existPendingDismisses()) {
                                                touchListener.undoPendingDismiss();
                                            } else {
                                                Toast.makeText(getActivity(), "Position " + position, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });*/


                                } else {

                                }

                            } catch (JSONException e) {
                                CustomProgressBarDialog.progressDialog.dismiss();
                                e.printStackTrace();
                            }

                            CustomProgressBarDialog.progressDialog.dismiss();

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    CustomProgressBarDialog.progressDialog.dismiss();
                    Log.e("error", volleyError.toString());
                }
            }) {


                @Override
                public Request.Priority getPriority() {
                    return Request.Priority.HIGH;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                    String current_language = session.getAppLanguage();
                    Log.i("current_language", current_language);
                    if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                        language = "en";
                    }
                    if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                        language = "np";
                    }
                    headers.put("lang", language);
                    return headers;
                }
            };
            int socketTimeout = 30000; //30 seconds
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            requestForMessages.setRetryPolicy(policy);
            app.addToRequestQueue(requestForMessages, "low");

        } else {
            Toast.makeText(getActivity(), "No Network Connected", Toast.LENGTH_LONG);
            NetworkUtils.showNoConnectionDialog(getActivity());
        }
    }

}
