package com.EddyCabLatest.application.moduleDriver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.EddyCabLatest.application.gcmNotification.GCMClientManager;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.EddyCabLatest.application.EApplication;
import com.EddyCabLatest.application.R;
import com.EddyCabLatest.application.constant.Config;
import com.EddyCabLatest.application.moduleUser.activity.Activity_UserForgetPassword;
import com.EddyCabLatest.application.utilities.NetworkUtils;
import com.EddyCabLatest.application.utilities.NewGPSTracker;
import com.EddyCabLatest.application.utilities.SessionManager;
import com.EddyCabLatest.application.views.CustomProgressBarDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Driver_Activity_Login extends ActionBarActivity implements View.OnClickListener {
    EditText driver_email_or_phone;
    EditText driver_enter_pass;
    Button btnSubmit;
    TextView txt_forgetPassword;
    Button btn_action_joinFree;
    Toolbar app_toolBar;

    SessionManager session;
    NewGPSTracker gps_tracker;

    String PROJECT_NUMBER = Config.GCM_SENDER_KEY;
    String GCM_REG_DEVICE_ID;
    String language = "";
    private boolean visible = false;

    String TAG = "===";
    private TextView apptitle_toolbar_driver_help;
    private GCMClientManager pushClientManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_driver_login);


              /*ToolBar SetUp*/
        app_toolBar = (Toolbar) findViewById(R.id.toolbar_header);
        setSupportActionBar(app_toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        app_toolBar.setNavigationIcon(R.drawable.ic_action_nav_back);
        apptitle_toolbar_driver_help = (TextView) app_toolBar.findViewById(R.id.applicationTitle);
        apptitle_toolbar_driver_help.setText(getResources().getString(R.string.login));
            /* Activity Loading Animation */
        overridePendingTransition(R.anim.right_slide_in, R.anim.no_anim);
        /* Session Init */
        session = new SessionManager(Driver_Activity_Login.this);
        gps_tracker = new NewGPSTracker(Driver_Activity_Login.this);
        /*referencing Views inside Activity Driver Layout*/
        driver_email_or_phone = (EditText) findViewById(R.id.driver_email_or_phone);
        driver_enter_pass = (EditText) findViewById(R.id.driver_enter_pass);
        btnSubmit = (Button) findViewById(R.id.btn_Submit);
        txt_forgetPassword = (TextView) findViewById(R.id.forget_password);
        txt_forgetPassword.setText(Html.fromHtml("<u>" + getResources().getString(R.string.forget_pass) + "</u>"));
        //txt_forgetPassword.setTextColor(Color.BLUE);
        txt_forgetPassword.setOnClickListener(this);
        btn_action_joinFree = (Button) findViewById(R.id.btn_action_joinFree);
        /*Firing onClick*/
        txt_forgetPassword.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        btn_action_joinFree.setOnClickListener(this);
        if ((session.getAppLanguage()) != "en") {
            btn_action_joinFree.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
            btnSubmit.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
        }
    }
    @Override
    public void onResume() {
        visible = true;
        super.onResume();
    }

    @Override
    public void onDestroy() {
        visible = false;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        visible = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        visible = false;
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                Intent intent = new Intent(Driver_Activity_Login.this, DriverLoginScreenActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.no_anim, R.anim.right_slide_out);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forget_password:
                forgetPassword();
                break;
            case R.id.btn_Submit:
                methodLoginCall();
                break;
            case R.id.btn_action_joinFree:
                Intent intent = new Intent(Driver_Activity_Login.this, DriverRegisterActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

    }

    private void methodLoginCall() {
        if (!driver_email_or_phone.getText().toString().trim().equals("") && !driver_enter_pass.getText().toString().trim().equals("")) {
            CustomProgressBarDialog.progressDialog = CustomProgressBarDialog.showProgressDialog(Driver_Activity_Login.this);
          /*  CustomProgressBarDialog.progressDialog.setCancelable(true);*/
            if (visible) CustomProgressBarDialog.progressDialog.show();

            if (NetworkUtils.isNetworkAvailable(Driver_Activity_Login.this)) {
                pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
                pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                    @Override
                    public void onSuccess(String registrationId, boolean isNewRegistration) {
                        GCM_REG_DEVICE_ID = registrationId;
                        GCM_REG_DEVICE_ID = registrationId;

                        if (NetworkUtils.isNetworkAvailable(Driver_Activity_Login.this)) {
                            if (!TextUtils.isEmpty(GCM_REG_DEVICE_ID)) {
                                final HashMap<String, String> params = new HashMap<>();
                                params.put("mobile_number", driver_email_or_phone.getText().toString().trim());
                                params.put("password", driver_enter_pass.getText().toString().trim());
                                params.put("type", Config.USER_TYPE_DRIVER);
                                params.put("device_type", Config.DEVICE_TYPE);
                                params.put("device_id", GCM_REG_DEVICE_ID);
                                Log.e("device_driver", params.toString() + "");

                                String REQUESTURL = Config.APP_BASE_URL + Config.USER_LOGIN_URL;

                                StringRequest req = new StringRequest(Request.Method.POST, REQUESTURL,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String responseObject) {
                                                String newResponse = responseObject;

                                                if (responseObject.contains("ï»¿")) {
                                                    newResponse = newResponse.substring(newResponse.indexOf("{"));
                                                }
                                                try {
                                                    JSONObject response = new JSONObject(newResponse);
                                                    Log.e("Response:", response.toString());
                                                    boolean error = response.getBoolean("error");
                                                    String message = response.getString("message");
                                                    if (!error) {
                                                        JSONObject user_details = response.getJSONObject("User");
                                                        JSONObject driver_details = response.getJSONObject("driver");
                                                        Log.e("User Details", user_details.toString());
                                                        String user_id = user_details.getString("id");
                                                        Log.e("User_id", String.valueOf(user_id));
                                                        String user_name = user_details.getString("fullname");
                                                        String acces_token = user_details.getString("access_token");
                                                        String driver_type = driver_details.getString("driver_type");
                                                        String user_email = user_details.getString("email");
                                                       /* String total_credit = user_details.getString("total_credit");*/
                                                        String user_rating = user_details.getString("rating");
                                                        String user_picture = response.getJSONObject("driver").getString("profile_picture");
                                                        String mobile_no = user_details.getString("mobile_number");

                                                        String liscense_no = response.getJSONObject("driver").getString("license_no");
                                                        String referral_code = user_details.getString("referral_code");
                                                        Log.e("User Picture ::", user_picture);
                                                        String corrected_rating = "";
                                                        if (!TextUtils.equals(user_rating, "null") && !TextUtils.isEmpty(user_rating))
                                                            corrected_rating = user_rating;
                                                        else corrected_rating = "0.0";

                                                        Log.e("User Rating ::", corrected_rating);

                                                        String user_referral_code = "";
                                                        if (!TextUtils.equals(referral_code, "null") && !TextUtils.isEmpty(referral_code))
                                                            user_referral_code = referral_code;
                                                        else user_referral_code = "xxxx";

                                                        Log.e("User Rating ::", corrected_rating);

                                            /* Save User Details to the Shared Preferences*/
                                                        session.createLoginSession(user_id, user_name, user_email, user_picture, corrected_rating, user_details.toString(), user_referral_code, mobile_no, liscense_no, driver_type, acces_token);
                                            /* Save user banana Credit */

                                            /* Dismiss the activity and resume Main Activity */
                                                        Intent in = new Intent(Driver_Activity_Login.this, Driver_MainActivity.class);
                                                        startActivity(in);
                                                        finish();
                                                    } else {
                                                        EApplication.getInstance().showToastMessageFunction(message);
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                CustomProgressBarDialog.progressDialog.dismiss();
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("Error: ", error + "");
                                        CustomProgressBarDialog.progressDialog.dismiss();
                                    }
                                }) {
                                    @Override
                                    public Request.Priority getPriority() {
                                        return Request.Priority.HIGH;
                                    }

                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        return params;
                                    }

                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        HashMap<String, String> headers = new HashMap<String, String>();
                            /*headers.put("Content-Type", "application/json");*/
                                        String current_language = session.getAppLanguage();
                                        Log.e("current_language", current_language);
                                        if (current_language.equalsIgnoreCase(Config.LANG_ENG)) {
                                            language = "en";
                                        }
                                        if (current_language.equalsIgnoreCase(Config.LANG_THAI)) {
                                            language = "np";
                                        }
                                        headers.put("lang", language);


                                        return headers;
                                    }
                                };
                                int socketTimeout = 20000; //30 seconds
                                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                req.setRetryPolicy(policy);
                                EApplication.getInstance().addToRequestQueue(req, "high");


                            } else
                                Log.e("===", "Please wait a while device registering to GCM !!!");

                        } else {
                            Toast.makeText(Driver_Activity_Login.this, "No Network Connected", Toast.LENGTH_LONG);
                            NetworkUtils.showNoConnectionDialog(Driver_Activity_Login.this);
                        }

                    }

                    @Override
                    public void onFailure(String ex) {
                        CustomProgressBarDialog.progressDialog.dismiss();
                        runOnUiThread(new Runnable() {
                            public void run() {
                                EApplication.getInstance().showToastMessageFunction(getResources().getString(R.string.please_tryagain));
                            }
                        });

                        super.onFailure(ex);
                    }
                });
            } else NetworkUtils.showNoConnectionDialog(Driver_Activity_Login.this);


        } else {
            if (driver_email_or_phone.getText().toString().trim().equals("")) {
                driver_email_or_phone.setError(getResources().getString(R.string.correct_mobile));

            }
            if (driver_enter_pass.getText().toString().trim().equals("")) {
                driver_enter_pass.setError(getResources().getString(R.string.please_enter_password));
            }

        }


    }

    private void forgetPassword() {
        Intent intent = new Intent(Driver_Activity_Login.this, Activity_UserForgetPassword.class);
        intent.putExtra("type", "driver");
        startActivity(intent);
        finish();
    }
}
